package fileCRUD

import (
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

func TestCreateSchema(t *testing.T) {
	tmpDir := filepath.ToSlash(os.TempDir())
	dbDir := filepath.FromSlash(filepath.Join(tmpDir, "fileDB_testSchema"))
	err := os.MkdirAll(dbDir, 0600)
	if err != nil {
		t.Fatal(err)
	}
	setupLastUpdate(dbDir, t)
	err = Init(dbDir)
	if err != nil {
		t.Fatal("couldn't initialize the test directory. Error: " + err.Error())
	}
	err = Create(nil)
	if err == nil {
		t.Error("should fail if schema is nil")
	}
	schema := common.Schema{
		FirestoreID:  "TestCreateSchema",
		NextSchemaID: common.String("TestCreateSchema"),
		IndustryID:   "TestCreateSchema",
		DomainID:     "TestCreateSchema",
		SchemaID:     "TestCreateSchema",
		DataTypes:    make(map[string]string),
		DataHelpText: make(map[string]string),
		Deprecated:   common.Bool(false),
	}
	schema.DataTypes["a"] = "S"
	schema.DataKeys = []string{"a"}
	schema.DataHelpText["a"] = "generic"

	err = Create(&schema)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	b, err := ioutil.ReadFile(filepath.Join(dbDir, common.CollectionSchema, "TestCreateSchema.json"))
	if err != nil {
		t.Fatal("couldn't check if the test set some data because the document did not exist: " + err.Error())
	}
	if !bytes.Contains(b, []byte(`"a":"S"`)) {
		t.Error("expected to have written some JSON out to the file")
	}
}

func TestReadSchema(t *testing.T) {
	tmpDir := filepath.ToSlash(os.TempDir())
	dbDir := filepath.FromSlash(filepath.Join(tmpDir, "fileDB_testSchema"))
	err := os.MkdirAll(dbDir, 0600)
	if err != nil {
		t.Fatal(err)
	}
	err = Init(dbDir)
	if err != nil {
		t.Fatal("couldn't initialize the test directory. Error: " + err.Error())
	}
	fileBytes := []byte(`{
	"DataTypes": {"a": "S"}
}
`)
	testFile := filepath.Join(dbDir, common.CollectionSchema, "TestReadSchema.json")
	err = ioutil.WriteFile(testFile, fileBytes, 0600)
	if err != nil {
		t.Fatal("couldn't create some test data in the file system for ReadSchema: " + err.Error())
	}

	d := common.Schema{
		FirestoreID: "TestReadSchema",
	}
	err = Read(&d)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	if d.DataTypes["a"] != "S" {
		t.Error("didn't get the 'a' string in DataTypes")
	}
	if d.FirestoreID != "TestReadSchema" {
		t.Error("should set FirestoreID")
	}
}

func TestUpdateSchema(t *testing.T) {
	tmpDir := filepath.ToSlash(os.TempDir())
	dbDir := filepath.FromSlash(filepath.Join(tmpDir, "fileDB_testSchema"))
	err := os.MkdirAll(dbDir, 0600)
	if err != nil {
		t.Fatal(err)
	}
	err = Init(dbDir)
	if err != nil {
		t.Fatal("couldn't initialize the test directory. Error: " + err.Error())
	}

	fileBytes := []byte(`{
	"polyappSchemaID":"TestUpdateSchema",
	"polyappDomainID":"TestUpdateSchema",
	"polyappIndustryID":"TestUpdateSchema",
	"SchemaName":"Testing",
	"DataTypes":{"a":"S"},
	"DataHelpText":{"a":"generic"},
	"DataKeys":["a"]
}
`)
	testFile := filepath.Join(dbDir, common.CollectionSchema, "TestUpdateSchema.json")
	err = ioutil.WriteFile(testFile, fileBytes, 0600)
	if err != nil {
		t.Fatal("couldn't create some test data in the file system for UpdateSchema: " + err.Error())
	}

	schema := common.Schema{
		FirestoreID:  "TestUpdateSchema",
		NextSchemaID: common.String("TestUpdateSchema"),
		IndustryID:   "TestUpdateSchema",
		DomainID:     "TestUpdateSchema",
		SchemaID:     "TestUpdateSchema",
		DataTypes:    make(map[string]string),
		DataHelpText: make(map[string]string),
		DataKeys:     []string{},
		Deprecated:   common.Bool(false),
	}
	err = Update(&schema)
	if err != nil {
		t.Fatal("unexpected error while updating: " + err.Error())
	}

	// verify
	outBytes, err := ioutil.ReadFile(testFile)
	if err != nil {
		t.Fatal("couldn't read file: " + err.Error())
	}
	if !bytes.Contains(outBytes, []byte(`"SchemaName":"new"`)) {
		t.Error("SchemaID update didn't take")
	}
}

func TestDeleteSchema(t *testing.T) {
	tmpDir := filepath.ToSlash(os.TempDir())
	dbDir := filepath.FromSlash(filepath.Join(tmpDir, "fileDB_testSchema"))
	err := os.MkdirAll(dbDir, 0600)
	if err != nil {
		t.Fatal(err)
	}
	err = Init(dbDir)
	if err != nil {
		t.Fatal("couldn't initialize the test directory. Error: " + err.Error())
	}
	fileBytes := []byte(`{
	"a": "B"
}
`)
	testFile := filepath.Join(dbDir, common.CollectionSchema, "TestDeleteSchema.json")
	err = ioutil.WriteFile(testFile, fileBytes, 0600)
	if err != nil {
		t.Fatal("couldn't create some test data in the file system for DeleteSchema: " + err.Error())
	}

	toDelete := common.Schema{
		FirestoreID: "TestDeleteSchema",
	}
	err = Delete(&toDelete)
	if err != nil {
		t.Fatal("unexpected error: " + err.Error())
	}

	_, err = os.Open(testFile)
	if !os.IsNotExist(err) {
		t.Error("the file should not exist since it should have been deleted")
	}
}
