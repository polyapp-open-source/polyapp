package fileCRUD

import (
	"fmt"
	"os"
	"path/filepath"
	"sync"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

var (
	fileCRUDMux sync.Mutex
)

var (
	baseDirectory   = ""
	dataDirectory   = ""
	schemaDirectory = ""
	userDirectory   = ""
	uxDirectory     = ""
	roleDirectory   = ""
	taskDirectory   = ""
)

// Init sets the baseDirectory, which specifies where in the file system to put all of the documents.
// This function fails if you have not already created the base directory in the file system.
func Init(initBaseDirectory string) error {
	if common.GetCloudProvider() != "" {
		initBaseDirectory = ""
	}
	if initBaseDirectory == "" {
		initBaseDirectory = filepath.Join(common.GetPublicPath(), "..", "cmd", "defaultservice", "baseDocuments")
	}
	fileCRUDMux.Lock()
	defer fileCRUDMux.Unlock()
	_, err := os.Open(initBaseDirectory)
	if err != nil {
		return fmt.Errorf("could not init base directory: %w", err)
	}
	baseDirectory = filepath.ToSlash(initBaseDirectory)
	dataDirectory = filepath.Join(baseDirectory, common.CollectionData)
	err = os.MkdirAll(dataDirectory, 0600)
	if err != nil {
		return fmt.Errorf("could not make Data directory: %w", err)
	}
	schemaDirectory = filepath.Join(baseDirectory, common.CollectionSchema)
	err = os.MkdirAll(schemaDirectory, 0600)
	if err != nil {
		return fmt.Errorf("could not make Schema directory: %w", err)
	}
	userDirectory = filepath.Join(baseDirectory, common.CollectionUser)
	err = os.MkdirAll(userDirectory, 0600)
	if err != nil {
		return fmt.Errorf("could not make User directory: %w", err)
	}
	uxDirectory = filepath.Join(baseDirectory, common.CollectionUX)
	err = os.MkdirAll(uxDirectory, 0600)
	if err != nil {
		return fmt.Errorf("could not make UX directory: %w", err)
	}
	roleDirectory = filepath.Join(baseDirectory, common.CollectionRole)
	err = os.MkdirAll(roleDirectory, 0600)
	if err != nil {
		return fmt.Errorf("could not make Role directory: %w", err)
	}
	taskDirectory = filepath.Join(baseDirectory, common.CollectionTask)
	err = os.MkdirAll(taskDirectory, 0600)
	if err != nil {
		return fmt.Errorf("could not make Task directory: %w", err)
	}
	return nil
}
