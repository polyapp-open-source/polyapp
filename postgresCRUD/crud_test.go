package postgresCRUD

import (
	"gitlab.com/polyapp-open-source/polyapp/common"
	"reflect"
	"strings"
	"testing"
)

func TestCRUD(t *testing.T) {
	var err error
	q := &common.Task{
		FirestoreID:               "TestPostgresCRUD",
		IndustryID:                "TestPostgresCRUD",
		DomainID:                  "TestPostgresCRUD",
		Name:                      "TestPostgresCRUDName",
		HelpText:                  "TestPostgresCRUDHelpText",
		TaskGoals:                 nil,
		FieldSecurity:             nil,
		BotsTriggeredAtLoad:       []string{"1", "2"},
		BotsTriggeredContinuously: []string{"1", "2", "3"},
		BotsTriggeredAtDone:       []string{"1", "2", "3", "4"},
		BotStaticData:             nil,
		IsPublic:                  false,
		Deprecated:                nil,
	}
	err = Create(q)
	if err != nil {
		t.Fatal("Create: " + err.Error())
	}
	qRead1 := &common.Task{
		FirestoreID: q.FirestoreID,
	}
	err = Read(qRead1)
	if err != nil {
		t.Fatal("Read: " + err.Error())
	}
	simplifiedQ, err := q.Simplify()
	if err != nil {
		t.Fatalf("q.Simplifiy: %v", err.Error())
	}
	simplifiedQRead1, err := qRead1.Simplify()
	if err != nil {
		t.Fatalf("qRead1.Simplify: %v", err.Error())
	}
	for k, v := range simplifiedQ {
		readValue, ok := simplifiedQRead1[k]
		if !ok {
			t.Errorf("qRead1 missing key %v", k)
		} else if !reflect.DeepEqual(readValue, v) {
			t.Errorf("qRead1 value %v did not match simplifiedQRead1 value %v", v, readValue)
		}
		delete(simplifiedQRead1, k)
	}
	if len(simplifiedQRead1) > 3 {
		t.Error("some values were in simplifiedQ which were not in simplifiedQRead1. Expected: TaskGoals, BotStaticData, and FieldSecurity because they are maps and maps aren't supported")
	}
	q.BotsTriggeredAtDone = []string{""}
	q.HelpText = "TestPostgresCRUDHelpText2"

	err = Update(q)
	if err != nil {
		t.Fatal("Update: " + err.Error())
	}
	qRead2 := &common.Task{
		FirestoreID: q.FirestoreID,
	}
	err = Read(qRead2)
	if err != nil {
		t.Fatal("Read: " + err.Error())
	}
	simplifiedQ, err = q.Simplify()
	if err != nil {
		t.Fatalf("q.Simplifiy: %v", err.Error())
	}
	simplifiedQRead2, err := qRead2.Simplify()
	if err != nil {
		t.Fatalf("qRead1.Simplify: %v", err.Error())
	}
	for k, v := range simplifiedQRead2 {
		readValue, ok := simplifiedQRead2[k]
		if !ok {
			t.Errorf("qRead1 missing key %v", k)
		} else if !reflect.DeepEqual(readValue, v) {
			t.Errorf("qRead1 value %v did not match simplifiedQRead1 value %v", v, readValue)
		}
		delete(simplifiedQRead2, k)
	}
	if len(simplifiedQRead2) > 0 {
		t.Error("some values were in simplifiedQ which were not in simplifiedQRead1")
	}

	err = Delete(qRead2.FirestoreID, qRead2.CollectionName())
	if err != nil {
		t.Fatal("Delete: " + err.Error())
	}
	qRead3 := &common.Task{
		FirestoreID: q.FirestoreID,
	}
	err = Read(qRead3)
	if err == nil {
		t.Fatal("Read did not throw an error")
	}
	if !strings.Contains(err.Error(), "code = NotFound") {
		t.Error("Read after delete did not throw an error containing \"code = NotFound\" instead it threw: " + err.Error())
	}
}
