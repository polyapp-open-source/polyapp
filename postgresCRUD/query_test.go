package postgresCRUD

import (
	"gitlab.com/polyapp-open-source/polyapp/common"
	"testing"
)

func TestPostgresQuery(t *testing.T) {
	var err error
	task := &common.Task{
		FirestoreID:               "TestPostgresQuery",
		IndustryID:                "TestPostgresQuery",
		DomainID:                  "TestPostgresQuery",
		Name:                      "TestPostgresQueryName",
		HelpText:                  "TestPostgresQuery Help Textasdf lasdk sadk sadkafdk al sadfkasdlasdlkasdklasdjkldjflkdasjlfkjasdlkas",
		TaskGoals:                 nil,
		FieldSecurity:             nil,
		BotsTriggeredAtLoad:       nil,
		BotsTriggeredContinuously: nil,
		BotsTriggeredAtDone:       nil,
		BotStaticData:             nil,
		IsPublic:                  false,
		Deprecated:                nil,
	}
	task2 := &common.Task{
		FirestoreID: "TestPostgresQuery2",
		IndustryID:  "TestPostgresQuery2",
		DomainID:    "TestPostgresQuery2",
		Name:        "TestPostgresQuery2Name",
		HelpText:    "TestPostgresQuery2 Help Text sadfkasdlasdlkasdklasdjkldjflkdasjlfkjasdlkas",
	}
	err = Delete(task.FirestoreID, common.CollectionTask)
	if err != nil {
		t.Fatal(err.Error())
	}
	err = Create(task)
	if err != nil {
		t.Fatal(err.Error())
	}
	err = Delete(task2.FirestoreID, common.CollectionTask)
	if err != nil {
		t.Fatal(err.Error())
	}
	err = Create(task2)
	if err != nil {
		t.Fatal(err.Error())
	}

	q := Query{}
	q.Init("", "", "", common.CollectionTask)
	err = q.AddFullTextSearch("TestPostgresQuery", "Name", common.PolyappFirestoreID)
	if err != nil {
		t.Fatal(err.Error())
	}
	iter, err := q.QueryRead()
	if err != nil {
		t.Fatal(err.Error())
	}
	queryable, err := iter.Next()
	if err == common.IterDone {
		t.Error("no results found but there should have been one matching result")
	} else {
		// happy path
		if queryable.CollectionName() != common.CollectionTask {
			t.Error("should have returned Task queryable")
		} else {
			// happy path
			taskReturned := queryable.(*common.Task)
			if taskReturned.Name != "TestPostgresQueryName" {
				t.Error("taskReturned.Name != TestPostgresQueryName - equaled: " + taskReturned.Name)
			}
			if taskReturned.HelpText != "TestPostgresQuery Help Textasdf lasdk sadk sadkafdk al sadfkasdlasdlkasdklasdjkldjflkdasjlfkjasdlkas" {
				t.Error("taskReturnd.HelpText != TestPostgresQuery Help Text: " + taskReturned.HelpText)
			}
		}
	}
	_, err = iter.Next()
	if err != common.IterDone {
		t.Error("more than one result returned")
	}

	q2 := Query{}
	q2.Init("", "", "", common.CollectionTask)
	err = q2.AddFullTextSearch("sadfkasdlasdlkasdklasdjkldjflkdasjlfkjasdlkas", "HelpText")
	if err != nil {
		t.Fatal(err.Error())
	}
	iter, err = q2.QueryRead()
	if err != nil {
		t.Fatal(err.Error())
	}
	queryable, err = iter.Next()
	if err != nil {
		t.Fatal(err.Error())
	}
	queryable2, err := iter.Next()
	if err != nil {
		t.Fatal(err.Error())
	}
	_, err = iter.Next()
	if err != common.IterDone {
		t.Error("expected common.IterDone after 2 entries")
	}

	if queryable.CollectionName() != common.CollectionTask || queryable2.CollectionName() != common.CollectionTask {
		t.Error("returned queryables were not from the task collection")
	} else {
		taskRead1 := queryable.(*common.Task)
		taskRead2 := queryable2.(*common.Task)
		if taskRead1.Name == taskRead2.Name {
			t.Error("did not return 2 distinct results")
		}
		if taskRead1.Name == "TestPostgresQueryName" {
			if taskRead2.Name != "TestPostgresQuery2Name" {
				t.Error("taskRead2.Name should have been TestPostgresQuery2")
			}
		} else {
			if taskRead1.Name != "TestPostgresQuery2Name" {
				t.Error("taskRead2.Name should have been TestPostgresQuery2")
			}
			if taskRead2.Name != "TestPostgresQueryName" {
				t.Error("taskRead1.Name should have been TestPostgresQuery")
			}
		}
	}
}
