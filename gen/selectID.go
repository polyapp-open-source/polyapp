package gen

import (
	"bytes"
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"html/template"
	"io"
	"net/url"
	"strconv"
)

type SelectID struct {
	Label             string
	HelpText          string
	ID                string
	InitialHumanValue string
	InitialIDValue    string
	Collection        string
	Field             string // Field includes prefixes. Examples: "polyappIndustryID" or "ind_dom_sch_field"
	Readonly          bool
}

func (s SelectID) Validate() error {
	if s.Label == "" {
		return errors.New("Label was empty")
	}
	if s.HelpText == "" {
		return errors.New("HelpText was empty")
	}
	if s.ID == "" {
		return errors.New("s.ID was empty")
	}
	switch s.Collection {
	case common.CollectionAction, common.CollectionData, common.CollectionBot, common.CollectionTask, common.CollectionSchema,
		common.CollectionUX, common.CollectionUser, common.CollectionRole:
	default:
		return fmt.Errorf("s.Collection (%v) was not one of the acceptable collection strings", s.Collection)
	}
	if s.Field == "" {
		return errors.New("s.Field was empty")
	}
	if s.InitialHumanValue != "" && s.InitialIDValue == "" {
		return errors.New("if setting InitialHumanValue must also set InitialIDValue")
	}
	if s.InitialIDValue != "" && s.InitialHumanValue == "" {
		return errors.New("if setting InitialIDValue must also set InitialHumanValue")
	}
	return nil
}

// GenSelectID an ID Selection control which allows you to choose a Data, Task, Schema, or other collection's ID
// as those this were a <select>.
//
// The person typing searches a particular field in a particular collection. The <select> is populated on the server
// with up to 100 values when a GET request is made.
func GenSelectID(s SelectID, w io.Writer) error {
	err := s.Validate()
	if err != nil {
		return fmt.Errorf("StaticContent failed validation: %w", err)
	}
	s.ID = url.PathEscape(s.ID)
	staticContentTemplate := `
<div class="mb-4">
	<label for="{{.ID}}">{{.Label}}</label>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>
	<div class="input-group">
{{if .Readonly}}<input type="text" class="form-control" id="{{.ID}}" value="{{.InitialHumanValue}}" readonly />
{{else}}<input type="text" class="form-control" id="{{.ID}}" data-polyappJS-select-value="{{.InitialIDValue}}" value="{{.InitialHumanValue}}"
			data-polyappJS-select-collection="{{.Collection}}" data-polyappJS-select-field="{{.Field}}" aria-autocomplete="list"
			data-polyappJS-select-field-initial-human-value="{{.InitialHumanValue}}" />{{end}}
		<div class="input-group-append">
			<span class="input-group-text">
				<img src="/assets/icons/search-24px.svg" aria-hidden="true" height="24px" width="24px" />
			</span>
		</div>
{{if eq .Collection "data" -}}{{/* The structure of the below code is coupled to main.js and handle.go code */}}
<div class="input-group-append">
	<span class="input-group-text">
		<a href="" class="btn btn-link d-flex justify-content-center p-0"><i class="fa fa-link"></i></a>
	</span>
</div>
{{- end}}
	</div>
	<div id="polyappSearchSelectResults{{.ID}}" class="position-relative p-2">
	</div>
	<div id="polyappError{{.ID}}" class="formErrorUnderInput">
	</div>
</div>
`
	t, err := template.New("s").Parse(staticContentTemplate)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	err = t.Execute(w, s)
	if err != nil {
		return fmt.Errorf("template.Execute: %w", err)
	}
	return nil
}

type ListSelectIDInitialValue struct {
	HumanValue string
	IDValue    string
	Readonly   bool   // not required; overridden by parent
	ID         string // not required; overridden by parent
	Field      string // not required; overridden by parent
	Collection string // not required; overridden by parent
}

type ListSelectID struct {
	Label           string
	HelpText        string
	ID              string
	Collection      string
	Field           string // Field includes prefixes. Examples: "polyappIndustryID" or "ind_dom_sch_field"
	Readonly        bool
	InitialValues   []ListSelectIDInitialValue
	DisplayNoneItem ListSelectIDInitialValue // not required; overridden by parent
}

func (s ListSelectID) Validate() error {
	if s.Label == "" {
		return errors.New("Label was empty")
	}
	if s.HelpText == "" {
		return errors.New("HelpText was empty")
	}
	if s.ID == "" {
		return errors.New("s.ID was empty")
	}
	switch s.Collection {
	case common.CollectionAction, common.CollectionData, common.CollectionBot, common.CollectionTask, common.CollectionSchema,
		common.CollectionUX, common.CollectionUser, common.CollectionRole:
	default:
		return fmt.Errorf("s.Collection (%v) was not one of the acceptable collection strings", s.Collection)
	}
	if s.Field == "" {
		return errors.New("s.Field was empty")
	}
	return nil
}

// GenListSelectID is an ID Selection control which allows you to choose a Data, Task, Schema, or other collection's ID
// as those this were a list of <select> controls.
//
// The person typing searches a particular field in a particular collection. The <select> is populated on the server
// with up to 100 values when a GET request is made.
func GenListSelectID(s ListSelectID, w io.Writer) error {
	err := s.Validate()
	if err != nil {
		return fmt.Errorf("StaticContent failed validation: %w", err)
	}
	s.ID = url.PathEscape(s.ID)

	var addImgButton bytes.Buffer
	err = GenIconButton(IconButton{
		ID:        "polyappJShtml5sortable-add" + s.ID,
		ImagePath: "/assets/icons/add-24px.svg",
		Text:      s.Label,
		AltText:   "Add",
		Readonly:  s.Readonly,
	}, &addImgButton)
	if err != nil {
		return fmt.Errorf("err in GenIconButton for adding purposes")
	}

	// make sure the items under the parent conform to the parent's ID, Field, Collection, and Readonly
	for i := range s.InitialValues {
		s.InitialValues[i] = ListSelectIDInitialValue{
			HumanValue: s.InitialValues[i].HumanValue,
			IDValue:    s.InitialValues[i].IDValue,
			Readonly:   s.Readonly,
			ID:         s.ID + "_" + strconv.FormatInt(int64(i+1), 10),
			Field:      s.Field,
			Collection: s.Collection,
		}
	}
	s.DisplayNoneItem = ListSelectIDInitialValue{
		HumanValue: "",
		IDValue:    "",
		Readonly:   s.Readonly,
		ID:         "polyappJSDisplayNone" + s.ID + "_willbereplaced",
		Field:      s.Field,
		Collection: s.Collection,
	}

	var removeItemIconButton bytes.Buffer
	err = GenIconButton(IconButton{
		ID:        "polyappJShtml5sortable-remove" + s.ID,
		ImagePath: "/assets/icons/red_minus.png",
		Text:      "",
		AltText:   "Remove List Item",
		Readonly:  s.Readonly,
		NoBorder:  true,
	}, &removeItemIconButton)
	if err != nil {
		return fmt.Errorf("GenIconButton err: %w", err)
	}

	staticContentTemplate := `{{define "RemoveItemIconButton"}}
` + removeItemIconButton.String() + `
{{end}}
{{define "ListSelectIDItem"}}
<div class="list-group-item list-group-item-dark list-group-item-action grabbable">
	<div class="input-group">
		{{if .Readonly}}<input type="text" class="form-control" id="{{.ID}}" value="{{.IDValue}}" readonly />
		{{else}}<input type="text" class="form-control" id="{{.ID}}" data-polyappJS-select-value="{{.IDValue}}" value="{{.HumanValue}}"
					data-polyappJS-select-collection="{{.Collection}}" data-polyappJS-select-field="{{.Field}}" aria-autocomplete="list"
					data-polyappJS-select-field-initial-human-value="{{.HumanValue}}" />{{end}}
				<div class="input-group-append">
					<span class="input-group-text">
						<img src="/assets/icons/search-24px.svg" aria-hidden="true" height="24px" width="24px" />
					</span>
				</div>
		{{if eq .Collection "data" -}}{{/* The structure of the below code is coupled to main.js and handle.go code */}}
		<div class="input-group-append">
			<span class="input-group-text">
				<a href="" class="btn btn-link d-flex justify-content-center p-0"><i class="fa fa-link"></i></a>
			</span>
		</div>
		<div class="input-group-append">
			{{template "RemoveItemIconButton"}}
		</div>
		{{- end}}
	</div>
	<div id="polyappSearchSelectResults{{.ID}}" class="position-relative p-0"></div>
</div>
{{end}}

{{define "ListSelectID" -}}
<div class="form-group" id="{{.ID}}">{{/* must use form-group to trigger list behavior in insertDataIntoDOM */}}
	<label for="{{.ID}}">{{.Label}}</label>
	<div class="imgButtonLabel pointer">
		<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
	</div>

	<div class="list-group html5sortable">
{{range .InitialValues}}
	{{template "ListSelectIDItem" .}}
{{end}}
	</div>
	<div class="displayNone">{{template "ListSelectIDItem" .DisplayNoneItem}}</div>
	` + addImgButton.String() + `
	<div id="polyappError{{.ID}}" class="formErrorUnderInput"></div>
</div>
{{- end}}
`
	t, err := template.New("ListSelectID").Parse(staticContentTemplate)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	err = t.Execute(w, s)
	if err != nil {
		return fmt.Errorf("template.Execute: %w", err)
	}
	return nil
}

type SelectIDResult struct {
	IDValue    string
	HumanValue string
	Context    string
	Link       string
}

// GenSelectIDResultHTML generates some HTML which can be appended to the end of a GenSelectID. When one of the results
// is clicked, it updates the GenSelectID appropriately.
func GenSelectIDResultHTML(results []SelectIDResult, w io.Writer) error {
	t := `<div class="modal-content position-absolute" style="z-index:100;max-width:500px">
<div class="modal-header">
	<h5>Search Results</h5>
	<button type="button" class="close" aria-label="Close" onclick="this.parentNode.parentNode.remove()">
	  <span aria-hidden="true">×</span>
	</button>
</div>
<div class="modal-body">
	<ul class="list-group">{{range .}}
	  <li class="list-group-item pointer" data-polyappJS-select-option-value="{{.IDValue}}" data-polyappjs-select-option-link="{{.Link}}">{{.HumanValue}}
		{{if .Context}}<br><span class="small">↳ in {{.Context}}</span>{{end}}
	  </li>{{end}}
	</ul>
</div>
</div>`
	parsed, err := template.New("t").Parse(t)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	err = parsed.Execute(w, results)
	if err != nil {
		return fmt.Errorf("template.Execute: %w", err)
	}
	return nil
}

// GenSelectIDNoResultsHTML generates some HTML which can be appended to the end of a GenSelectID.
func GenSelectIDNoResultsHTML() string {
	return `<div class="modal-content position-absolute" style="z-index:100;max-width:500px">
<div class="modal-header">
	<h5>Search Results</h5>
	<button type="button" class="close" aria-label="Close" onclick="this.parentNode.parentNode.remove()">
	  <span aria-hidden="true">×</span>
	</button>
</div>
<div class="modal-body">
	<p>No results found. Searches are case-sensitive and must be exact matches.</p>
</div>
</div>`
}
