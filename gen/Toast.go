package gen

import (
	"fmt"
	"html/template"
	"io"
)

type Toast struct {
	Heading string
	Text    string
}

// Validate that Toast is fully populated.
func (t Toast) Validate() error {
	return nil
}

// GenToast generates a Toast, which is typically used for notifications or showing errors.
func GenToast(t Toast, w io.Writer) error {
	err := t.Validate()
	if err != nil {
		return fmt.Errorf("Toast.Validate: %w", err)
	}
	toastTemplate := `{{define "toastTemplate"}}
<div class="toast ml-auto" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false">
  <div class="toast-header">
    <strong class="mr-auto">{{.Heading}}</strong>
    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div class="toast-body">
    {{.Text}}
  </div>
</div>{{end}}`
	temp, err := template.New("toastTemplate").Parse(toastTemplate)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	err = temp.Execute(w, t)
	if err != nil {
		return fmt.Errorf("template.Execute: %w", err)
	}
	return nil
}
