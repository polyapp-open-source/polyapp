package gen

import (
	"bytes"
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

func TestGenNestedAccordion(t *testing.T) {
	type args struct {
		nestedAccordions []NestedAccordion
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "nil",
			args: args{
				nestedAccordions: nil,
			},
			wantErr: true,
		},
		{
			name: "zero length",
			args: args{
				nestedAccordions: make([]NestedAccordion, 0),
			},
			wantErr: true,
		},
		{
			name: "bad validation in beginning",
			args: args{
				nestedAccordions: []NestedAccordion{
					{
						TitleField:   "",
						InnerContent: "<p>some content</p>",
						Ref: common.CreateRef("TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion",
							"TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion"),
					},
					{
						TitleField:   "some title",
						InnerContent: "<p>some content 2</p>",
						Ref: common.CreateRef("TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion",
							"TestGenNestedAccordion"),
					},
				},
			},
			wantErr: true,
		},
		{
			name: "bad validation in middle",
			args: args{
				nestedAccordions: []NestedAccordion{
					{
						TitleField:   "some title 1",
						InnerContent: "<p>some content 1</p>",
						Ref: common.CreateRef("TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion",
							"TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion"),
					},
					{
						TitleField:   "some title 2",
						InnerContent: "",
						Ref: common.CreateRef("TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion",
							"TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion"),
					},
					{
						TitleField:   "some title 3",
						InnerContent: "<p>some content 3</p>",
						Ref: common.CreateRef("TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion",
							"TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion"),
					},
				},
			},
			wantErr: true,
		},
		{
			name: "bad Ref validation in the middle",
			args: args{
				nestedAccordions: []NestedAccordion{
					{
						TitleField:   "some title 1",
						InnerContent: "<p>some content 1</p>",
						Ref: common.CreateRef("TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion",
							"TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion"),
					},
					{
						TitleField:   "some title 2",
						InnerContent: "<p>some content 1</p>",
						Ref: common.CreateRef("", "TestGenNestedAccordion", "TestGenNestedAccordion",
							"TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion"),
					},
					{
						TitleField:   "some title 3",
						InnerContent: "<p>some content 3</p>",
						Ref: common.CreateRef("TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion",
							"TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion"),
					},
				},
			},
			wantErr: true,
		},
		{
			name: "reasonable accordion with 3 entries",
			args: args{
				nestedAccordions: []NestedAccordion{
					{
						TitleField:   "some title 1",
						InnerContent: "<p>some content 1</p>",
						Ref: common.CreateRef("TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion",
							"TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion"),
					},
					{
						TitleField:   "some title 2",
						InnerContent: "<p>some content 2</p>",
						Ref: common.CreateRef("TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion",
							"TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion"),
					},
					{
						TitleField:   "some title 3",
						InnerContent: "<p>some content 3</p>",
						Ref: common.CreateRef("TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion",
							"TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion"),
					},
				},
			},
			wantErr: false,
		},
		{
			name: "reasonable accordion with 1 entry",
			args: args{
				nestedAccordions: []NestedAccordion{
					{
						TitleField:   "some title 1 ?@(D",
						InnerContent: "<p>some content 1</p>",
						Ref: common.CreateRef("TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion",
							"TestGenNestedAccordion", "TestGenNestedAccordion", "TestGenNestedAccordion"),
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			writer := &bytes.Buffer{}
			err := GenNestedAccordion(tt.args.nestedAccordions, writer)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenNestedAccordion() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
