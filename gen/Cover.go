package gen

import (
	"errors"
	"fmt"
	"html/template"
	"io"
	"strings"
)

type Cover struct {
	// Scaling must be "Resize". If "Resize" the image is made smaller on smaller screen sizes.
	Scaling string
	// MediaURL is a URL of where to find the media used in the Cover.
	MediaURL string
	// AltText (optional) is the alt text placed on the media from MediaURL.
	AltText string
	// BackgroundColor (optional) for the Cover.
	BackgroundColor string
	// TextColor (optional) for the Cover. Default black.
	TextColor string
	// Heading (optional) for the Cover.
	Heading string
	// HeadingFont (optional) for the Cover. From the list of Google Fonts families.
	HeadingFont string
	// Subheading (optional) for the Cover.
	Subheading string
	// SubheadingFont (optional) for the Cover. From the list of Google Fonts families.
	SubheadingFont string
}

// Validate the Cover object can be used to create the Cover in HTML.
func (c *Cover) Validate() error {
	if c.Scaling != "Resize" {
		return errors.New("Invalid Scaling value: " + c.Scaling)
	}
	if c.MediaURL == "" {
		return errors.New("MediaURL not provided")
	}
	return nil
}

// GenCover generates a Cover, which is comprised of an image or video over a colored background and may include a heading
// or subheading. The returned headings are optimized to improve loading speed.
//
// GenCover returns a string, fontLinks, which must be added to the header. It also can return an error.
func GenCover(c Cover, writer io.Writer) (fontLinks string, err error) {
	err = c.Validate()
	if err != nil {
		return "", fmt.Errorf("Cover.Validate: %w", err)
	}
	if c.HeadingFont != "" {
		fontLinks += `<link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="https://fonts.googleapis.com/css2?family=` + strings.ReplaceAll(c.HeadingFont, " ", "+") + `">`
	}
	if c.SubheadingFont != "" {
		fontLinks += `<link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="https://fonts.googleapis.com/css2?family=` + strings.ReplaceAll(c.SubheadingFont, " ", "+") + `">`
	}
	CoverTemplate := `{{/* . is Cover */}}
{{define "CoverTemplate"}}
<div class="container-fluid" style="{{if .BackgroundColor}}background-color:{{.BackgroundColor}};{{end}}">
<div class="row justify-content-between align-items-center">
<div class="col-12 col-sm-6 p-0 text-center">
<img src="{{.MediaURL}}" {{if .AltText}}alt="{{.AltText}}"{{end}} class="img-fluid" />
</div>

<div class="col-12 col-sm-6 text-center" style="{{if .TextColor}}color:{{.TextColor}};{{end}}">
	{{if .Heading}}<h1{{if .HeadingFont}} style="font-family: '{{.HeadingFont}}';"{{end}}>{{.Heading}}</h1>{{end}}
	{{if .Subheading}}<h3{{if .SubheadingFont}} style="font-family: '{{.SubheadingFont}}';"{{end}}>{{.Subheading}}</h3>{{end}}
</div>
</div>
</div>
{{- end}}`
	t, err := template.New("CoverTemplate").Parse(CoverTemplate)
	if err != nil {
		return "", fmt.Errorf("template.New: %w", err)
	}
	err = t.Execute(writer, c)
	if err != nil {
		return "", fmt.Errorf("t.Execute: %w", err)
	}
	return fontLinks, nil
}
