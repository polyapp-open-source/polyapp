package gen

import (
	"errors"
	"fmt"
	"html/template"
	"io"
)

// FullTextSearch can be used to upload an audio file.
type FullTextSearch struct {
	ID         string
	Label      string
	HelpText   string
	Readonly   bool
	Collection string
}

// Validate returns an error if not all required inputs are populated.
func (a FullTextSearch) Validate() error {
	if a.ID == "" {
		return errors.New("ID not populated")
	}
	return nil
}

// GenFullTextSearch generates a control which is used to upload audio.
func GenFullTextSearch(f FullTextSearch, w io.Writer) error {
	err := f.Validate()
	if err != nil {
		return fmt.Errorf("Validate: %w", err)
	}
	FullTextSearchTemplate := `<div class="mb-4">
	{{if .Label}}<div>
		<p class="d-inline">{{.Label}}</p>
		<div class="imgButtonLabel pointer">
			<img src="/assets/icons/help-24px.svg" tabindex="0" role="button" alt="Help" data-toggle="popover" title="Help" data-content="{{.HelpText}}" />
		</div>
	</div>{{end}}
	<div class="input-group">
	{{if .Readonly}}<input type="text" class="form-control" id="{{.ID}}" readonly />
	{{else}}<input type="text" class="form-control" id="{{.ID}}" data-polyappjs-full-text-search-collection="{{.Collection}}" aria-autocomplete="list" />{{end}}
		<div class="input-group-append">
			<span class="input-group-text">
				<img src="/assets/icons/search-24px.svg" aria-hidden="true" height="24px" width="24px" />
			</span>
		</div>
	</div>
	<div id="polyappFullTextSearchResults{{.ID}}" class="position-relative p-2">
	</div>
	<div id="polyappError{{.ID}}" class="formErrorUnderInput">
	</div>
</div>
`
	t, err := template.New("").Parse(FullTextSearchTemplate)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	err = t.Execute(w, f)
	if err != nil {
		return fmt.Errorf("template.Execute: %w", err)
	}
	return nil
}
