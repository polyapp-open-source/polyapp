package integrity

import "testing"

func TestValidateIndexedDB(t *testing.T) {
	type args struct {
		simplified map[string]interface{}
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name:    "nil",
			args:    args{simplified: nil},
			wantErr: true,
		},
		{
			name:    "empty",
			args:    args{simplified: make(map[string]interface{})},
			wantErr: false,
		},
		{
			name: "empty name",
			args: args{simplified: map[string]interface{}{
				"": "hi",
			}},
			wantErr: true,
		},
		{
			name: "short",
			args: args{simplified: map[string]interface{}{
				"a":   "B",
				"qed": true,
			}},
			wantErr: false,
		},
		{
			name: "medium",
			args: args{simplified: map[string]interface{}{
				"kasdlkj409023)(*45098230982456890)($%098234509813908)(*()&$%!$)&%^*@& dsaf d[}{}[S}| f[] FD>fds.f\"<:/<fl;<L:|\\}]{{=++-_~`,?": "hi",
			}},
			wantErr: false,
		},
		{
			name: "medium valid",
			args: args{simplified: map[string]interface{}{
				"shorter key": "45098230982456890)($%098234509813908)(*()&$%!$)&%^*@& dsaf d[}{}[S}| f[] FD>fds.f\"<:/<fl;<L:|\\}]{{=++-_~`,?",
			}},
			wantErr: false,
		},
		{
			name: "polyappPrefixTest",
			args: args{simplified: map[string]interface{}{
				"polyappHello": "polyapp value",
			}},
			wantErr: false,
		},
		{
			name: "polyappTest",
			args: args{simplified: map[string]interface{}{
				"polyapp": "polyapp value",
			}},
			wantErr: false,
		},
		{
			name: "polyapp value test",
			args: args{simplified: map[string]interface{}{
				"anything": "polyapp",
			}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidateIndexedDB(tt.args.simplified); (err != nil) != tt.wantErr {
				t.Errorf("ValidateIndexedDB() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
