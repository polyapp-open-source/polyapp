package integrity

import (
	"errors"
	"fmt"
	"strings"
)

// ValidateElasticsearch ensures the information being stored in Elasticsearch will not break the storage medium.
//
// Source: https://www.elastic.co/guide/en/elasticsearch/reference/current/general-recommendations.html
// Documents must not be > 100MB or 25_000_000 unicode characters, however they should be much smaller than that usually.
//
// elasticsearch limit is 512 bytes for document IDs https://discuss.elastic.co/t/maximum-length-of-a-specified-document-id/4262/3
// 4 bytes per string character; 512 / 4 = 128.
// We will reserve len("polyapp")*4 = 28 bytes in every key.
// Therefore our maximum ID size is ~99.
// But right now IDs are randomly generated so that should not be a problem.
func ValidateElasticsearch(simplified map[string]interface{}) error {
	if simplified == nil {
		return errors.New("simplified must be initialized")
	}
	docSize := 0
	for k, v := range simplified {
		err := ValidateElasticsearchKey(k)
		if err != nil {
			return fmt.Errorf("ValidateElasticsearchKey ("+k+"): %w", err)
		}

		switch value := v.(type) {
		case string:
			docSize += len(value) + len(k)
		case []byte:
			// []byte will not be stored in elasticsearch so we can ignore it.
		case map[string]string:
			// for maps, the size = {"key":"value"} or something like that.
			totalSize := 2 + 5*len(value) + len(k)
			for innerKey, innerValue := range value {
				totalSize += len(innerKey) + len(innerValue)
			}
			docSize += totalSize
		case map[string]int64:
			// (5 + 4)*len where int64 takes 4 bytes to store
			totalSize := 2 + 9*len(value) + len(k)
			for innerKey := range value {
				totalSize += len(innerKey)
			}
			docSize += totalSize
		case map[string]bool:
			// (5 + 1)*len where bool takes 1 bytes to store
			totalSize := 2 + 6*len(value) + len(k)
			for innerKey := range value {
				totalSize += len(innerKey)
			}
			docSize += totalSize
		case map[string]float64:
			// (5 + 4)*len where float64 takes 4 bytes to store
			totalSize := 2 + 9*len(value) + len(k)
			for innerKey := range value {
				totalSize += len(innerKey)
			}
			docSize += totalSize
		default:
			// a guesstimate
			docSize += 1 + len(k)
		}
	}
	if docSize > 25_000_000 {
		return errors.New("document size is too large")
	}

	return nil
}

// ValidateElasticsearchKey returns an error if the key provided is invalid or unsafe.
func ValidateElasticsearchKey(k string) error {
	if k == "" {
		return errors.New("Key can't be empty")
	}
	if len(k) > 300 {
		// limit in Firestore is 1,500 bytes
		// limit in Elasticsearch is essentially unlimited.
		// https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping.html under "index.mapping.field_name_length.limit"
		// The problem is that fields with long names cause problems and our field names are all prefixed with
		// Industry Names and Domains and Schema IDs, so they are already long. So it is important that we impose some
		// kind of a limit.
		return errors.New("key too long - must be under 300 characters")
	}
	// https://discuss.elastic.co/t/index-name-type-name-and-field-name-rules/133039/2
	if strings.Contains(k, ".") {
		return errors.New("Key can't contain a period since a period is short-hand for objects")
	}
	if strings.TrimSpace(k) == "" {
		return errors.New("Key can't be only whitespace")
	}
	return nil
}
