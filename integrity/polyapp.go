package integrity

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

// ValidatePolyapp ensures Polyapp's storage format is not being compromised by the data being stored.
func ValidatePolyapp(m map[string]interface{}) error {
	var err error
	combinedErr := ""
	for k := range m {
		err = ValidatePolyappKey(k)
		if err != nil {
			combinedErr += fmt.Sprintf("key (%v) was invalid because (%v); ", k, err.Error())
		}
	}
	if combinedErr != "" {
		return errors.New(combinedErr)
	}
	return nil
}

// ValidatePolyappKey ensures a key k with a prefix "_ind_dom_sch_key" or "ind_dom_sch_key" or without a prefix like "key"
// does not contain restricted characters which would break Polyapp's client-side or server-side code.
//
// This should be 2 different functions - one which is for prefixed keys and one which is not. But it isn't.
func ValidatePolyappKey(k string) error {
	if len(k) < 1 {
		return errors.New("key too short")
	}
	numSplit := strings.Count(k, "_")
	if numSplit == 0 {
		// This function is called under a variety of circumstances. Some of those include cases where there is no prefix
		// to the key. In those circumstances we expect numSplit == 0.
		return nil
	} else if k[0] == '_' {
		// _ind_dom_sch_key name
		if numSplit != 4 {
			return errors.New("incorrect number of _ in key (expected 4). Number of _: " + strconv.Itoa(numSplit))
		}
	} else {
		// ind_dom_sch_key name
		if numSplit != 3 {
			return errors.New("incorrect number of _ in key (expected 3). Number of _: " + strconv.Itoa(numSplit))
		}
	}
	return nil
}
