package integrity

import (
	"errors"
	"fmt"

	"gitlab.com/polyapp-open-source/polyapp/common"
	"gitlab.com/polyapp-open-source/polyapp/firestoreCRUD"
)

// Convert from common.Data in the format of "SchemaID" to the format of the schema's master Schema.
func Convert(data *common.Data) error {
	// TODO implement this when implementing 'NextSchemaID' functionality.
	return nil
}

// BackPopulate pulls the Schema of this Data and if there are any blank fields in Data it populates them with zero values.
// This is very useful to ensure the Data in the database is fully populated, which is required by some functions like TaskGoalEvaluate.
func BackPopulate(data *common.Data) error {
	if data == nil {
		return errors.New("data was nil")
	}
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return fmt.Errorf("firestoreCRUD.GetClient: %w", err)
	}
	if data.SchemaCache.FirestoreID == "" {
		data.SchemaCache, err = firestoreCRUD.ReadSchema(ctx, client, data.SchemaID)
		if err != nil {
			return fmt.Errorf("firestoreCRUD.ReadSchema: %w", err)
		}
	}
	for fieldName, fieldType := range data.SchemaCache.DataTypes {
		switch fieldType {
		case "S":
			_, ok := data.S[fieldName]
			if !ok {
				data.S[fieldName] = common.String("")
			}
		case "B":
			_, ok := data.B[fieldName]
			if !ok {
				data.B[fieldName] = common.Bool(false)
			}
		case "F":
			_, ok := data.F[fieldName]
			if !ok {
				data.F[fieldName] = common.Float64(0)
			}
		case "I":
			_, ok := data.I[fieldName]
			if !ok {
				data.I[fieldName] = common.Int64(0)
			}
		case "Ref":
			_, ok := data.Ref[fieldName[1:]] // Schema keys contain _ but ARefs do not.
			if !ok {
				data.Ref[fieldName[1:]] = common.String("")
			}
		case "AS":
			_, ok := data.AS[fieldName]
			if !ok {
				data.AS[fieldName] = make([]string, 0)
			}
		case "AB":
			_, ok := data.AB[fieldName]
			if !ok {
				data.AB[fieldName] = make([]bool, 0)
			}
		case "AF":
			_, ok := data.AF[fieldName]
			if !ok {
				data.AF[fieldName] = make([]float64, 0)
			}
		case "AI":
			_, ok := data.AI[fieldName]
			if !ok {
				data.AI[fieldName] = make([]int64, 0)
			}
		case "ARef":
			_, ok := data.ARef[fieldName[1:]] // Schema keys contain _ but ARefs do not.
			if !ok {
				data.ARef[fieldName[1:]] = make([]string, 0)
			}
		}
	}
	return nil
}
