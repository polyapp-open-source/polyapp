# Development Environment Setup

## Environment Variable Setup
* BUCKET (set to anything; you can get the current bucket from storage in the console) [https://console.cloud.google.com/storage](https://console.cloud.google.com/storage)
* LAST_BUCKET (not set unless you are operating multiple environments)
* GOOGLE_APPLICATION_CREDENTIALS (path to your app credentials, like "C:\Users\something\project-name.json")
* GOOGLE_CLOUD_PROJECT ("project-name")
* TLSPATH (path to server.key and server.crt files used for https://localhost:8080 like "C:\Users")

### Old environment variables (previous versions only)
* ELASTICSEARCH_KEY (your key)
* HUB_URL (https://polyapp-hub.appspot.com)
* REDISHOST (127.0.0.1)
* REDISPORT (6379)

## Postgres
Go to the "SQL" in the Google Cloud Console: [https://console.cloud.google.com/sql/](https://console.cloud.google.com/sql/)
* In the instances tab, if you followed the installation instructions there should be a postgres database with a name like: polyapp-postgres-00000
* In the Connections tab, make sure "Public IP" is selected. Add your IP address to "Authorized Networks". Ensure all apps are authorized. Create a client certificate which is NOT prefixed with "polyapp-postgres" and download it. In your root directory, add a sslcert (client-cert.pem), sslkey (client RSA Private Key), and sslrootcert (server certificate) so you can connect with SSL. Add environment variables: POSTGRES_DB_NAME ("polyapp"), POSTGRES_IP_ADDRESS (from connection information)
* In the Users tab, add a new user for yourself which is NOT prefixed with "polyapp". Add environment variables: POSTGRES_PASSWORD (from user password), POSTGRES_USERNAME (from username)
* In the Databases tab, there should be a "polyapp" database.

# FAQ and QA
Problem: "A connection attempt failed because the connected party did not properly respond after a period of time, or established connection failed because connected host has failed to respond".

Solution: Whitelist your IP address on the Connections tab for your project here: [https://console.cloud.google.com/sql/](https://console.cloud.google.com/sql/)

Problem: "Context Deadline Exceeded"

Solution: ? Try again ?
