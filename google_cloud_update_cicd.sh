# Assume we have already set up Postgres server and stored the certificate in gcloud secrets
PROJECT_GROUP=$1
PROJECT_NAME=$2
gcloud config set project $GOOGLE_CLOUD_PROJECT_ID

POSTGRES_PASSWORD=$RANDOM$RANDOM$RANDOM$RANDOM
POSTGRES_USERNAME=$(gcloud secrets versions access latest --secret=postgres-username)
if [ "$POSTGRES_USERNAME" = "" ]
then
  POSTGRES_USERNAME=polyapp$RANDOM$RANDOM
  gcloud secrets create postgres-username
  echo "$POSTGRES_USERNAME" | gcloud secrets versions add postgres-username --data-file=-
fi
SQL_INSTANCE_NAME=$(gcloud sql instances list --limit 1 --format="value(name)")
SQL_IP_ADDRESS=$(gcloud sql instances list --limit 1 --format="value(ipAddresses[0].ipAddress)")
CLIENT_CERTIFICATE_NAME=$(gcloud sql ssl client-certs list --format="value(commonName)" --instance=$SQL_INSTANCE_NAME --sort-by=commonName --limit=1)
gcloud sql users set-password $POSTGRES_USERNAME --instance=$SQL_INSTANCE_NAME --password=$POSTGRES_PASSWORD
POSTGRES_SSL_KEY=$(gcloud secrets versions access latest --secret=postgres)

POSTGRES_SSL_PUBLIC=$(gcloud sql ssl client-certs describe $CLIENT_CERTIFICATE_NAME --instance=$SQL_INSTANCE_NAME --format="value(cert)")

REGION=$(gcloud app describe --format="value(locationId.scope())")
# SQL_REGION example: us-central1 (vs. REGION: us-central)
SQL_REGION=$(gcloud compute zones list --filter="region ~ $REGION" --limit=1 --format="value(region)")

# Write the environment variables into app.yaml
# An alternative is to create another program like cmd/defaultservice/initGCP
# Another alternative is to add something to install()
# But this seemed like the simplest option.
# Syntax example: https://github.com/koalaman/shellcheck/wiki/SC2129
{
  echo $"  POSTGRES_USERNAME: \"$POSTGRES_USERNAME\"
  POSTGRES_PASSWORD: \"$POSTGRES_PASSWORD\"
  POSTGRES_IP_ADDRESS: \"$SQL_IP_ADDRESS\"
  POSTGRES_DB_NAME: \"polyapp\"
  INSTANCE_CONNECTION_NAME: \"$GOOGLE_CLOUD_PROJECT_ID:$SQL_REGION:$SQL_INSTANCE_NAME\""
} >> /go/src/gitlab.com/$PROJECT_GROUP/$PROJECT_NAME/cmd/defaultservice/app.yaml

# Write variables to files. They will be deployed when the system is deployed to app engine, but these file names
# are excluded from git so they will not be committed.
rm sslrootcert
rm sslcert
rm sslkey
echo "$SQL_SERVER_CERT" >> /go/src/gitlab.com/$PROJECT_GROUP/$PROJECT_NAME/sslrootcert
echo "$POSTGRES_SSL_PUBLIC" >> /go/src/gitlab.com/$PROJECT_GROUP/$PROJECT_NAME/sslcert
echo "$POSTGRES_SSL_KEY" >> /go/src/gitlab.com/$PROJECT_GROUP/$PROJECT_NAME/sslkey
