package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
)

// TestAPIWorkflow ensures the following process works:
// Request a new API ID and Key and get the Schema ID assigned to that ID
// Need to PUT so we are issued a new Data ID.
// Given a API ID and Key which were issued, access the specific Schema ID you are supposed to have access to and try to perform a Read.
// Given a API ID and Key which were issued, access the specific Schema ID you are supposed to have access to and try to perform a Write.
// Given a API ID and Key try to access GETDataTable in polyapp/Hub
// Given an API ID and Key try to delete an added Data.
// Delete an API ID / Key combination
// Try to access a deactivated API ID / Key combination.
func TestAPIWorkflow(t *testing.T) {
	var err error

	// Request a new API ID and Key and get the Schema ID assigned to that ID
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/APICredentials", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	err = GETAPICredentials(c)
	if err != nil {
		t.Fatal("GETAPICredentials: " + err.Error())
	}
	if rec.Result().StatusCode != http.StatusOK {
		t.Fatal(fmt.Errorf("GETAPICredentials Status Code != 200; was %v", rec.Result().StatusCode))
	}
	var GETAPICredentialsResponse struct {
		APIID       string
		APIKey      string
		HubSchemaID string
		HubTaskID   string
		HubUXID     string
	}
	err = json.Unmarshal(rec.Body.Bytes(), &GETAPICredentialsResponse)
	if err != nil {
		t.Fatal(fmt.Errorf("json.Unmarshal: %w", err))
	}
	if GETAPICredentialsResponse.APIID == "" {
		t.Fatal("Did not receive an APIID in the GETAPICredentials response")
	}
	if GETAPICredentialsResponse.HubSchemaID == "" {
		t.Fatal("Did not receive a HubSchemaID in the GETAPICredentials response")
	}
	if GETAPICredentialsResponse.HubTaskID == "" {
		t.Fatal("Did not receive a HubTaskID in the GETAPICredentials response")
	}
	if GETAPICredentialsResponse.HubUXID == "" {
		t.Fatal("Did not receive a HubUXID in the GETAPICredentials response")
	}
	if GETAPICredentialsResponse.APIKey == "" {
		t.Fatal("Did not receive an APIKey in the GETAPICredentials response")
	}

	POSTRequest := common.POSTRequest{
		MessageID:           common.GetRandString(25),
		IndustryID:          "polyapp",
		OverrideIndustryID:  "polyappNone",
		DomainID:            "Hub",
		OverrideDomainID:    "polyappNone",
		TaskID:              GETAPICredentialsResponse.HubTaskID,
		TaskCache:           nil,
		OverrideTaskID:      "polyappNone",
		Data:                map[string]map[string]interface{}{
			// TODO add something here to replace hubData
		},
		UXID:                GETAPICredentialsResponse.HubUXID,
		UXCache:             nil,
		SchemaID:            GETAPICredentialsResponse.HubSchemaID,
		SchemaCache:         nil,
		DataID:              "",
		UserID:              "polyappNone",
		UserCache:           nil,
		PublicMapOwningUser: common.User{},
		RoleID:              "polyappNone",
		ModifyID:            "",
		PublicPath:          "",
		RecaptchaResponse:   "",
		FinishURL:           "",
		IsDone:              false,
		WantDocuments:       common.WantDocuments{},
	}
	body, err := json.Marshal(POSTRequest)
	if err != nil {
		t.Fatal("json.Marshal: " + err.Error())
	}
	// Need to PUT so we are issued a new Data ID.
	// We _could_ add some information to the document during the PUT request but instead I'm testing a new and empty Data.
	req = httptest.NewRequest(http.MethodPut, "/api/t/polyapp/Hub/"+GETAPICredentialsResponse.HubTaskID+"?ux="+GETAPICredentialsResponse.HubUXID+"&schema="+GETAPICredentialsResponse.HubSchemaID+"&ID="+GETAPICredentialsResponse.APIID+"&key="+GETAPICredentialsResponse.APIKey, bytes.NewReader(body))
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	err = APIPUTHandler(c)
	if err != nil {
		t.Fatal("APIPUTHandler: " + err.Error())
	}
	if rec.Result().StatusCode != 200 {
		// Expect redirect to the new Data
		t.Fatal(fmt.Errorf("APIGETHandler Status Code != 200; was %v", rec.Result().StatusCode))
	}
	response := new(common.POSTResponse)
	err = json.Unmarshal(rec.Body.Bytes(), response)
	if err != nil {
		t.Fatal("json.Unmarshal: " + err.Error())
	}
	u, err := url.Parse(response.NewURL)
	if err != nil {
		t.Fatal(fmt.Errorf("url.Parse: %w", err))
	}
	parsedPUTResponse, err := common.CreateGETRequest(*u)
	if err != nil {
		t.Fatal(fmt.Errorf("common.CreateGETRequest: %w", err))
	}
	readCreatedData, err := allDB.ReadData(parsedPUTResponse.DataID)
	if err != nil {
		t.Fatal("could not find data which should have been saved with ID " + parsedPUTResponse.DataID)
	}
	if readCreatedData.S["polyapp_Hub_"+GETAPICredentialsResponse.HubSchemaID+"_User ID"] == nil || *readCreatedData.S["polyapp_Hub_"+GETAPICredentialsResponse.HubSchemaID+"_User ID"] != "polyappAdminUser" {
		t.Error("User ID was not saved during PUTHandler")
	}
	if len(readCreatedData.ARef) != 2 {
		t.Error("expected 2 ARef")
	}
	otherToDelete := make([]string, 0)
	for _, refs := range readCreatedData.ARef {
		for i := range refs {
			parsed, err := common.ParseRef(refs[i])
			if err != nil {
				t.Error("common.ParseRef: " + err.Error())
				continue
			}
			otherToDelete = append(otherToDelete, parsed.DataID)
		}
	}
	for _, ref := range readCreatedData.Ref {
		if ref != nil {
			parsed, err := common.ParseRef(*ref)
			if err != nil {
				t.Error("common.ParseRef: " + err.Error())
				continue
			}
			otherToDelete = append(otherToDelete, parsed.DataID)
		}
	}
	// Clean up the document we created when creating this test
	defer func() {
		_ = allDB.DeleteData(parsedPUTResponse.DataID)
		for _, id := range otherToDelete {
			_ = allDB.DeleteData(id)
		}
	}()

	// Given a API ID and Key which were issued, access the specific Schema ID you are supposed to have access to and try to perform a Read.
	e = echo.New()
	// Task and UX are from https://localhost:8080/t/polyapp/Hub/timwXghrohGAmFRHdhusUojXj?ux=EuykMbYuOVbRjtloPqXvpMCDM&schema=tKPCahjxcZHviTXprUnFmHJSj&data=LTrLchSAnuIAEwbyCFJCkxmhh&user=polyappAdminUser&role=polyappAdminUser
	// I assume the Schema from there == the schema in the destination.
	// If that schema is ever updated a conversion will have to be performed and some other work will be necessary too
	// The Data isn't accurate for this Schema. It is replaced with the targetData created during this function.
	req = httptest.NewRequest(http.MethodGet, "/api/t/polyapp/Hub/"+GETAPICredentialsResponse.HubTaskID+"?ux="+GETAPICredentialsResponse.HubUXID+"&schema="+GETAPICredentialsResponse.HubSchemaID+"&data="+parsedPUTResponse.DataID+"&ID="+GETAPICredentialsResponse.APIID+"&key="+GETAPICredentialsResponse.APIKey, nil)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	err = APIGETHandler(c)
	if err != nil {
		t.Fatal("APIGETHandler: " + err.Error())
	}
	if rec.Result().StatusCode != http.StatusOK {
		t.Fatal(fmt.Errorf("APIGETHandler Status Code != 200; was %v", rec.Result().StatusCode))
	}
	if !bytes.Contains(rec.Body.Bytes(), []byte(`polyapp_Hub_`+GETAPICredentialsResponse.HubSchemaID+`_User%20ID`)) {
		t.Error("Did not receive expected body")
	}

	// Do the same as (2) but perform a write.
	e = echo.New()
	POSTReq := &common.POSTRequest{
		MessageID:  "sadfkjdasklasdljasdkla",
		IndustryID: "polyapp",
		DomainID:   "Hub",
		TaskID:     GETAPICredentialsResponse.HubTaskID,
		UXID:       GETAPICredentialsResponse.HubUXID,
		SchemaID:   GETAPICredentialsResponse.HubSchemaID,
		DataID:     parsedPUTResponse.DataID,
		Data: map[string]map[string]interface{}{
			"%2Ft%2Fpolyapp%2FHub%2F" + GETAPICredentialsResponse.HubTaskID + "%3Fux=" + GETAPICredentialsResponse.HubUXID + "&schema=tKPCahjxcZHviTXprUnFmHJSj&data=" + parsedPUTResponse.DataID + "": {
				"polyapp_Hub_tKPCahjxcZHviTXprUnFmHJSj_User%20ID": "",
			},
		},
	}
	POSTBody, err := json.Marshal(POSTReq)
	if err != nil {
		t.Fatal(fmt.Errorf("json.Marshal: %v", POSTReq))
	}
	req = httptest.NewRequest(http.MethodPost, "/api/t/polyapp/Hub/"+GETAPICredentialsResponse.HubTaskID+"?ux="+GETAPICredentialsResponse.HubUXID+"&schema="+GETAPICredentialsResponse.HubSchemaID+"&data="+parsedPUTResponse.DataID+"&ID="+GETAPICredentialsResponse.APIID+"&key="+GETAPICredentialsResponse.APIKey, bytes.NewReader(POSTBody))
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	err = APIPOSTHandler(c)
	if err != nil {
		t.Fatal("APIPOSTHandler: " + err.Error())
	}
	if rec.Result().StatusCode != http.StatusOK {
		t.Fatal(fmt.Errorf("DELETEAPICredentials Status Code != 200; was %v", rec.Result().StatusCode))
	}

	// Given a API ID and Key try to access GETDataTable in polyapp/Hub
	e = echo.New()
	req = httptest.NewRequest(http.MethodGet, "/api/polyappDataTable/?industry=polyapp&domain=Hub&schema="+GETAPICredentialsResponse.HubSchemaID+
		"&requiredColumns=&draw=2&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=polyapp_Hub_"+GETAPICredentialsResponse.HubSchemaID+"_User%20ID"+
		"&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D="+
		"&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=polyapp_Hub_"+GETAPICredentialsResponse.HubSchemaID+"_Done"+
		"&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D="+
		"&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=polyappFirestoreID"+
		"&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D="+
		"&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start=0&length=50"+
		"&search%5Bvalue%5D=&search%5Bregex%5D=false&LastDocumentID=&_=1614893368774"+
		"&ID="+GETAPICredentialsResponse.APIID+"&key="+GETAPICredentialsResponse.APIKey, nil)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	err = APIGETDataTable(c)
	if err != nil {
		t.Fatal("GETDataTable: " + err.Error())
	}
	if rec.Result().StatusCode != http.StatusOK {
		t.Fatal(fmt.Errorf("GETDataTable Status Code != 200; was %v", rec.Result().StatusCode))
	}
	if !bytes.Contains(rec.Body.Bytes(), []byte(`"error":""`)) {
		t.Error("GETDataTable response contained an error: " + rec.Body.String())
	}

	// Delete the PUT Data
	e = echo.New()
	req = httptest.NewRequest(http.MethodDelete, "/api/t/polyapp/Hub/"+GETAPICredentialsResponse.HubTaskID+"?ux="+GETAPICredentialsResponse.HubUXID+"&schema="+GETAPICredentialsResponse.HubSchemaID+"&data="+parsedPUTResponse.DataID+"&ID="+GETAPICredentialsResponse.APIID+"&key="+GETAPICredentialsResponse.APIKey, nil)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	err = APIDELETEHandler(c)
	if err != nil {
		t.Fatal("APIDELETEHandler: " + err.Error())
	}
	if rec.Result().StatusCode != http.StatusOK {
		t.Fatal(fmt.Errorf("APIDELETEHandler Status Code != 200; was %v", rec.Result().StatusCode))
	}
	_, err = allDB.ReadData(parsedPUTResponse.DataID)
	if err != nil && strings.Contains(err.Error(), "NotFound") {
		// success
	} else if err != nil {
		t.Error("allDB.ReadData of the data which was supposed to have been deleteed by the DELETE request: " + err.Error())
	}

	// Delete an API ID / Key combination
	e = echo.New()
	req = httptest.NewRequest(http.MethodDelete, "/APICredentials?ID="+GETAPICredentialsResponse.APIID+"&key="+GETAPICredentialsResponse.APIKey, nil)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	err = DELETEAPICredentials(c)
	if err != nil {
		t.Fatal("DELETEAPICredentials: " + err.Error())
	}
	if rec.Result().StatusCode != http.StatusNoContent {
		t.Fatal(fmt.Errorf("DELETEAPICredentials Status Code != 204; was %v", rec.Result().StatusCode))
	}

	// Try to access a deactivated API ID / Key combination
	e = echo.New()
	req = httptest.NewRequest(http.MethodDelete, "/APICredentials?ID="+GETAPICredentialsResponse.APIID+"&key="+GETAPICredentialsResponse.APIKey, nil)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	err = DELETEAPICredentials(c)
	if err != nil {
		t.Fatal("DELETEAPICredentials: " + err.Error())
	}
	if rec.Result().StatusCode != http.StatusNoContent {
		// I expect this to work because if you skip the Authentication middleware & directly call DELETEAPICredentials it won't list
		// the versions which do not exist, so it will list 0 versions, so it will delete nothing and respond with a 204.
		t.Fatal(fmt.Errorf("DELETEAPICredentials Status Code != 204; was %v", rec.Result().StatusCode))
	}
}

func TestAPIAuthenticateAndAuthorize(t *testing.T) {
	var err error
	// Request a new API ID and Key and get the Schema ID assigned to that ID
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/APICredentials", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	err = GETAPICredentials(c)
	if err != nil {
		t.Fatal("GETAPICredentials: " + err.Error())
	}
	if rec.Result().StatusCode != http.StatusOK {
		t.Fatal(fmt.Errorf("GETAPICredentials Status Code != 200; was %v", rec.Result().StatusCode))
	}
	var GETAPICredentialsResponse struct {
		APIID       string
		APIKey      string
		HubSchemaID string
		HubTaskID   string
		HubUXID     string
	}
	err = json.Unmarshal(rec.Body.Bytes(), &GETAPICredentialsResponse)
	if err != nil {
		t.Fatal(fmt.Errorf("json.Unmarshal: %w", err))
	}
	if GETAPICredentialsResponse.APIID == "" {
		t.Fatal("Did not receive an APIID in the GETAPICredentials response")
	}
	if GETAPICredentialsResponse.HubSchemaID == "" {
		t.Fatal("Did not receive a HubSchemaID in the GETAPICredentials response")
	}
	if GETAPICredentialsResponse.HubTaskID == "" {
		t.Fatal("Did not receive a HubTaskID in the GETAPICredentials response")
	}
	if GETAPICredentialsResponse.HubUXID == "" {
		t.Fatal("Did not receive a HubUXID in the GETAPICredentials response")
	}
	if GETAPICredentialsResponse.APIKey == "" {
		t.Fatal("Did not receive an APIKey in the GETAPICredentials response")
	}

	// Test API middleware
	// Note: This test fails if the running person does not have access to view secrets (roles/secretmanager.admin works)
	POSTRequest := common.POSTRequest{
		MessageID:  common.GetRandString(25),
		IndustryID: "polyapp",
		DomainID:   "Hub",
		TaskID:     GETAPICredentialsResponse.HubTaskID,
		Data:       map[string]map[string]interface{}{},
		UXID:       GETAPICredentialsResponse.HubUXID,
		SchemaID:   GETAPICredentialsResponse.HubSchemaID,
		DataID:     "",
	}
	body, err := json.Marshal(POSTRequest)
	if err != nil {
		t.Fatal(fmt.Errorf("json.Marshal: %w", err))
	}
	e = echo.New()
	req = httptest.NewRequest(http.MethodPut, "/api/t/polyapp/Hub/"+GETAPICredentialsResponse.HubTaskID+"?ux="+GETAPICredentialsResponse.HubUXID+"&schema="+GETAPICredentialsResponse.HubSchemaID+"&ID="+GETAPICredentialsResponse.APIID+"&key="+GETAPICredentialsResponse.APIKey, bytes.NewReader(body))
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	c.SetHandler(
		APIAuthenticate(
			APIAuthorize(
				echo.WrapHandler(http.NotFoundHandler()),
			),
		),
	)
	err = c.Handler()(c)
	if err != nil {
		t.Error(err)
	}

	e = echo.New()
	req = httptest.NewRequest(http.MethodGet, "/api/t/polyapp/Hub/"+GETAPICredentialsResponse.HubTaskID+"?ux="+GETAPICredentialsResponse.HubUXID+"&schema="+GETAPICredentialsResponse.HubSchemaID+"&data=asdsadsadas"+"&ID="+GETAPICredentialsResponse.APIID+"&key="+GETAPICredentialsResponse.APIKey, bytes.NewReader(body))
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	c.SetHandler(
		APIAuthenticate(
			APIAuthorize(
				echo.WrapHandler(http.NotFoundHandler()),
			),
		),
	)
	err = c.Handler()(c)
	if err != nil {
		t.Error(err)
	}
	e = echo.New()
	req = httptest.NewRequest(http.MethodGet, "/api/t/polyapp/NotHub/"+GETAPICredentialsResponse.HubTaskID+"?ux="+GETAPICredentialsResponse.HubUXID+"&schema="+GETAPICredentialsResponse.HubSchemaID+"&data=asdsadsadas"+"&ID="+GETAPICredentialsResponse.APIID+"&key="+GETAPICredentialsResponse.APIKey, bytes.NewReader(body))
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	c.SetHandler(
		APIAuthenticate(
			APIAuthorize(
				echo.WrapHandler(http.NotFoundHandler()),
			),
		),
	)
	err = c.Handler()(c)
	if err != nil && strings.Contains(err.Error(), "tried to access Industry polyapp and Domain NotHub") {
		// we want failure to occur
	} else if err != nil {
		t.Error(err)
	} else {
		t.Error("MethodGet succeeded when it should have not allowed access to industry polyapp and domain NotHub")
	}

	deleteFailData := common.Data{}
	deleteFailData.Init(nil)
	deleteFailData.FirestoreID = common.GetRandString(25)
	deleteFailData.IndustryID = "polyapp"
	deleteFailData.DomainID = "Hub"
	deleteFailData.SchemaID = "gjGLMlcNApJyvRjmgQbopsXPI" // Edit Task's Schema will not match the HubSchemaID, but does exist so ought to be valid.
	err = allDB.CreateData(&deleteFailData)
	if err != nil {
		t.Error("allDB.CreateData for deleteFailData: " + err.Error())
	}

	deleteSuccessData := common.Data{}
	deleteSuccessData.Init(nil)
	deleteSuccessData.FirestoreID = common.GetRandString(25)
	deleteSuccessData.IndustryID = "polyapp"
	deleteSuccessData.DomainID = "Hub"
	deleteSuccessData.SchemaID = GETAPICredentialsResponse.HubSchemaID
	err = allDB.CreateData(&deleteSuccessData)
	if err != nil {
		t.Error("allDB.CreateData for deleteSuccessData: " + err.Error())
	}
	e = echo.New()
	req = httptest.NewRequest(http.MethodDelete, "/api/t/polyapp/Hub/"+GETAPICredentialsResponse.HubTaskID+"?ux="+GETAPICredentialsResponse.HubUXID+"&schema="+GETAPICredentialsResponse.HubSchemaID+"&data="+deleteFailData.FirestoreID+"&ID="+GETAPICredentialsResponse.APIID+"&key="+GETAPICredentialsResponse.APIKey, bytes.NewReader(body))
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	c.SetHandler(
		APIAuthenticate(
			APIAuthorize(
				echo.WrapHandler(http.NotFoundHandler()),
			),
		),
	)
	err = c.Handler()(c)
	if err != nil && strings.Contains(err.Error(), "tried to access Industry polyapp and Domain Hub and Schema gjGLMlcNApJyvRjmgQbopsXPI") {
		// this is what we want: throw an error and do not proceed with the request
	} else if err != nil {
		t.Error(err)
	} else {
		t.Error("allowed deleteFailData to pass through")
	}
	e = echo.New()
	req = httptest.NewRequest(http.MethodDelete, "/api/t/polyapp/Hub/"+GETAPICredentialsResponse.HubTaskID+"?ux="+GETAPICredentialsResponse.HubUXID+"&schema="+GETAPICredentialsResponse.HubSchemaID+"&data="+deleteSuccessData.FirestoreID+"&ID="+GETAPICredentialsResponse.APIID+"&key="+GETAPICredentialsResponse.APIKey, bytes.NewReader(body))
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	c.SetHandler(
		APIAuthenticate(
			APIAuthorize(
				echo.WrapHandler(http.NotFoundHandler()),
			),
		),
	)
	err = c.Handler()(c)
	if err != nil {
		t.Error(err)
	}
	err = allDB.DeleteData(deleteFailData.FirestoreID)
	if err != nil {
		t.Error("allDB.DeleteData " + deleteFailData.FirestoreID + ": " + err.Error())
	}
	err = allDB.DeleteData(deleteSuccessData.FirestoreID)
	if err != nil {
		t.Error("allDB.DeleteData " + deleteSuccessData.FirestoreID + ": " + err.Error())
	}

	// verify that the body was put back
	byteArray := make([]byte, 2)
	n, err := req.Body.Read(byteArray)
	if err != nil {
		t.Error(err)
	}
	if n != 2 {
		t.Error("did not read 2 bytes from Body")
	}

	// Delete an API ID / Key combination
	e = echo.New()
	req = httptest.NewRequest(http.MethodDelete, "/APICredentials?ID="+GETAPICredentialsResponse.APIID+"&key="+GETAPICredentialsResponse.APIKey, nil)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)
	err = DELETEAPICredentials(c)
	if err != nil {
		t.Fatal("DELETEAPICredentials: " + err.Error())
	}
	if rec.Result().StatusCode != http.StatusNoContent {
		t.Fatal(fmt.Errorf("DELETEAPICredentials Status Code != 204; was %v", rec.Result().StatusCode))
	}
}
