// Command polyapp is the main service for Polyapp.
package main

import (
	"bytes"
	secretmanager "cloud.google.com/go/secretmanager/apiv1"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/actions"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"gitlab.com/polyapp-open-source/polyapp/firebaseCRUD"
	"gitlab.com/polyapp-open-source/polyapp/gen"
	"gitlab.com/polyapp-open-source/polyapp/postgresCRUD"
	secretmanagerpb "google.golang.org/genproto/googleapis/cloud/secretmanager/v1"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"
	"sync"

	"cloud.google.com/go/firestore"

	"google.golang.org/api/iterator"

	"gitlab.com/polyapp-open-source/polyapp/firestoreCRUD"

	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/fileCRUD"

	"firebase.google.com/go/auth"

	"gitlab.com/polyapp-open-source/polyapp/handlers"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var (
	publicPath = ""
)

func main() {
	// same as some code in templates.go
	publicPath = common.GetPublicPath()
	err := os.Chdir(publicPath)
	if err != nil {
		common.LogError(fmt.Errorf("could not set current working directory to public path: %w", err))
		wd, err := os.Getwd()
		if err != nil {
			common.LogError(fmt.Errorf("err getting wd: %w", err))
		} else {
			common.LogError(errors.New("wd: " + wd))
		}
	}

	// https://echo.labstack.com/cookbook/subdomains
	polyappMux := CreateMux()
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	TLSPath, TLSPathExists := os.LookupEnv("TLSPATH")
	if !TLSPathExists {
		// Used by App Engine
		polyappMux.Logger.Fatal(polyappMux.Start("localhost:" + port))
	} else {
		cert := TLSPath + string(os.PathSeparator) + "server.crt"
		key := TLSPath + string(os.PathSeparator) + "server.key"
		polyappMux.Logger.Fatal(polyappMux.StartTLS("localhost:"+port, cert, key))
	}
}

// CreateMux gives you the echo Mux you can use to serve the application
func CreateMux() *echo.Echo {
	e := echo.New()
	// TODO check if running in Azure or AWS and take appropriate actions
	cloudProvider := common.GetCloudProvider()
	if cloudProvider == "GOOGLE" {
		logWriter, err := common.GetLogger(os.Getenv("GAE_SERVICE"), os.Getenv("GAE_VERSION"))
		if err != nil {
			panic(err)
		}
		defer common.CloseLogging()
		e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
			Skipper: middleware.DefaultSkipper,
			Output:  logWriter,
		}))
	} else {
		e.Use(middleware.Logger())
		// Google cloud can zip outgoing files for you so this middleware is cloud-specific.
		e.Use(middleware.Gzip())
	}
	e.Use(middleware.Recover())
	e.Use(middleware.SecureWithConfig(middleware.SecureConfig{
		Skipper:               middleware.DefaultSkipper,
		XSSProtection:         "1; mode=block",
		ContentTypeNosniff:    "nosniff",
		XFrameOptions:         "",
		HSTSMaxAge:            86400,
		HSTSExcludeSubdomains: true,
		// Summernote and some other UI libraries make this really difficult to use. Since only nonces are helpful,
		// and we can't create the nonces while we use Summernote, I'm not worrying about this right now.
		//
		// I believe only nonces are helpful based on this:
		// https://csp.withgoogle.com/docs/why-csp.html#:~:text=Why%20use%20the%20Content%20Security,malicious%20scripts%20on%20the%20page.
		//ContentSecurityPolicy: "default-src 'self' *.googleapis.com *.gstatic.com *.accountchooser.com www.youtube.com; img-src * data:",
	}))
	e.Use(middleware.AddTrailingSlash())
	// Redirecting from www or http to non-www is done by app.yaml
	// https://tools.ietf.org/html/rfc6265
	e.Use(middleware.CSRFWithConfig(middleware.CSRFConfig{
		TokenLength:    32,
		TokenLookup:    "header:" + echo.HeaderXCSRFToken,
		ContextKey:     "csrf",
		CookieName:     "_csrf",
		CookieMaxAge:   86400,
		CookiePath:     "/",   // make sure all requests use this one cookie.
		CookieSecure:   true,  // require TLS
		CookieHTTPOnly: false, // we make AJAX requests in which the cookie must be read
		Skipper: func(echoContext echo.Context) bool {
			// If the path is a static file then there is no need to run CSRF code.
			// This code was added after a series of resource request failures.
			escapedPath := echoContext.Request().URL.EscapedPath()
			if strings.HasSuffix(escapedPath, ".js") || strings.HasSuffix(escapedPath, ".css") ||
				strings.HasSuffix(escapedPath, ".js/") || strings.HasSuffix(escapedPath, ".css/") || strings.HasPrefix(escapedPath, "/assets") {
				return true
			}
			// If the path connects to an API then there is no need to run CSRF code.
			if strings.HasPrefix(escapedPath, "/api/") {
				return true
			}
			// If we're using altshell then ignore many security concerns.
			// This is obviously a bad idea. I should let people turn this off.
			if echoContext.Request().URL.Query().Get("altshell") != "" {
				return true
			}
			return false
		},
	}))
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowMethods:     []string{"GET", "POST", "PUT", http.MethodDelete},
		AllowCredentials: true,
	}))

	e.HTTPErrorHandler = errorHandler

	// a lot of content can be served directly out of the 'public' folder
	// this would include things like icons and manifest.json
	// you might also consider caching these paths in CDNs, although that will make updates harder
	e.Static("/assets", filepath.Join(filepath.ToSlash(publicPath), "assets"))
	e.Static("/bootstrap", filepath.Join(publicPath, "bootstrap"))
	e.Static("/bootstrap-select", filepath.Join(publicPath, "bootstrap-select"))
	e.Static("/jquery", filepath.Join(publicPath, "jquery"))
	e.Static("/datepicker", filepath.Join(publicPath, "datepicker"))
	e.Static("/firebase", filepath.Join(publicPath, "firebase"))
	e.Static("/firebaseui", filepath.Join(publicPath, "firebaseui"))
	e.Static("/idb", filepath.Join(publicPath, "idb"))
	e.Static("/html5sortable", filepath.Join(publicPath, "html5sortable"))
	e.Static("/DataTables", filepath.Join(publicPath, "DataTables"))
	e.Static("/summernote", filepath.Join(publicPath, "summernote", "dist"))
	e.File("/main.js", filepath.Join(publicPath, "main.js"))
	e.File("/sw.js", filepath.Join(publicPath, "sw.js"))
	e.File("/client-zip.js", filepath.Join(publicPath, "client-zip.js"))
	e.File("/manifest.json", filepath.Join(publicPath, "manifest.json"))
	e.File("/robots.txt", filepath.Join(publicPath, "robots.txt"))
	e.File("/400.html", filepath.Join(publicPath, "400.html"))
	e.File("/401.html", filepath.Join(publicPath, "401.html"))
	e.File("/403.html", filepath.Join(publicPath, "403.html"))
	e.File("/404.html", filepath.Join(publicPath, "404.html"))
	e.File("/405.html", filepath.Join(publicPath, "405.html"))
	e.File("/409.html", filepath.Join(publicPath, "409.html"))
	e.File("/500.html", filepath.Join(publicPath, "500.html"))
	e.File("/503.html", filepath.Join(publicPath, "503.html"))
	e.File("/favicon.ico", filepath.Join(publicPath, "assets", "giraffe192.jpg"))
	e.File("/index.css", filepath.Join(publicPath, "index.css"))

	e.GET("/blob/assets/*", BlobDownloadHandler, Authenticate, UserAuthorize)
	e.POST("/blob/assets/*", DummyHandler, Authenticate)

	// this handler is called by Google's App Engine to 'warm up' your app before traffic starts to arrive.
	e.GET("/_ah/warmup", func(c echo.Context) error {
		return nil
	})

	// Call this to put the .json files in baseDocuments into a new system. Can't be called twice but can be called
	// without authentication to allow the Continuous Integration (CI) pipeline to call it.
	e.GET("/polyapp/internal/JT2ZhFXXaKCyO184Qs3F/", Install)
	e.GET("/polyapp/internal/JT2ZhFXXaKCyO184Qs3F/force", ForceInstall, Authenticate, UserAuthorize)

	// We need to allow people to "GET" information which is used to get more information about the system
	// However we don't want to accidentally give them access to paths they should not have access to.
	// Therefore I've listed all of the potential pages you might GET without authorization explicitly.
	// This includes Home, Change Domain, Change Task, and Change Data.
	e.GET("/", GETHandler, Authenticate, UserAuthorize)
	e.POST("/", DummyHandler, Authenticate, UserAuthorize)
	e.GET("/polyappChangeContext", GETHandler, Authenticate, UserAuthorize)
	e.POST("/polyappChangeContext", POSTHandler, Authenticate, UserAuthorize)
	e.GET("/polyappChangeTask", GETHandler, Authenticate, UserAuthorize)
	e.POST("/polyappChangeTask", POSTHandler, Authenticate, UserAuthorize)
	e.GET("/polyappChangeData", GETHandler, Authenticate, UserAuthorize)
	e.POST("/polyappChangeData", POSTHandler, Authenticate, UserAuthorize)

	e.GET("/signIn", signInGET)
	e.POST("/signIn", signInPOST)

	// TODO rate limit requests to the apiGroup and to /APICredentials
	apiGroup := e.Group("/api", APIAuthenticate, APIAuthorize)
	apiGroup.GET("/t/*", APIGETHandler)
	apiGroup.POST("/t/*", APIPOSTHandler)
	apiGroup.PUT("/t/*", APIPUTHandler)
	apiGroup.DELETE("/t/*", APIDELETEHandler)
	apiGroup.GET("/polyappDataTable", APIGETDataTable)
	e.GET("/APICredentials", GETAPICredentials)
	e.DELETE("/APICredentials", DELETEAPICredentials, APIAuthenticate)

	// Private pages. Routing is a big pain if you don't explicitly place these under a specific URL prefix.
	e.File("/serviceworker.wasm", filepath.Join(publicPath, "serviceworker.wasm"), Authenticate, UserAuthorize)
	e.GET("/polyappDataTable", GETDataTable, Authenticate, UserAuthorize)
	e.GET("/polyappSearch", GETSelectSearch, Authenticate, UserAuthorize)
	e.GET("/polyappFullTextSearch", GETFullTextSearch, Authenticate, UserAuthorize)
	dynamicGroup := e.Group("/t/", Authenticate, UserAuthorize)
	dynamicGroup.GET("*", GETHandler)
	dynamicGroup.POST("*", POSTHandler)

	uploadGroup := e.Group("/u/", Authenticate, UserAuthorize)
	uploadGroup.POST("*", BlobUploadHandler)

	// Test pages. Used for testing out new User Interface widgets before their data transport & storage is tested.
	e.GET("/test", InternalGETHandler, Authenticate, UserAuthorize, PolyappAuthorize)
	e.POST("/test", InternalPOSTHandler, Authenticate, UserAuthorize, PolyappAuthorize)
	testGroup := e.Group("/test/", Authenticate, UserAuthorize, PolyappAuthorize)
	testGroup.GET("*", InternalGETHandler)
	testGroup.POST("*", InternalPOSTHandler)
	return e
}

// POSTHandler is the server-side equivalent of the client's clientPOSTHandler. It handles message events
// from the DOM and replies to the client with a JSON-encoded POSTResponse.
// This should be kept in sync with cmd/serviceworker/main.go clientPOSTHandler.
func POSTHandler(c echo.Context) error {
	POSTRequest := new(common.POSTRequest)
	// I used to use c.Bind() but c.Bind's implementation is erroring when examining query parameters.
	// I have no idea why it looks at query parameters when the docs say its purpose is to bind the body.
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return fmt.Errorf("ioutil.ReadAll body: %w", err)
	}
	err = json.Unmarshal(body, POSTRequest)
	if err != nil {
		return fmt.Errorf("json.Unmarshal body: %w", err)
	}
	err = common.FixPOSTRequest(POSTRequest)
	if err != nil {
		return fmt.Errorf("FixPOSTRequest: %w", err)
	}
	err = POSTRequest.Validate()
	if err != nil {
		return fmt.Errorf("could not validate POSTRequest: %w", err)
	}

	// set MessageID so we can use it if an error is thrown during the request
	c.Set("MessageID", POSTRequest.MessageID)

	userInterface := c.Get("User")
	if userInterface != nil {
		// this 'if' logic is included for /signIn and similar public pages which do not require authentication.
		user := userInterface.(*common.User)
		err = handlers.RoleAuthorize(user, common.GETRequest{
			IndustryID: POSTRequest.IndustryID,
			DomainID:   POSTRequest.DomainID,
			DataID:     POSTRequest.DataID,
			SchemaID:   POSTRequest.SchemaID,
		}, POSTRequest.RoleID, 'u')
		if err != nil {
			return echo.NewHTTPError(http.StatusUnauthorized, "could not authorize user for this request: "+err.Error())
		}
		err = handlers.RoleAuthorize(user, common.GETRequest{
			IndustryID: POSTRequest.GetIndustry(),
			DomainID:   POSTRequest.GetDomain(),
			DataID:     POSTRequest.DataID,
			SchemaID:   POSTRequest.SchemaID,
		}, POSTRequest.RoleID, 'u')
		if err != nil {
			return echo.NewHTTPError(http.StatusUnauthorized, "could not authorize user for the overrides in this request: "+err.Error())
		}
		POSTRequest.UserID = user.FirestoreID
		POSTRequest.UserCache = user
	}

	POSTResponse, err := handlers.HandlePOST(*POSTRequest)
	if err != nil {
		return fmt.Errorf("HandlePOST failed: %w", err)
	}
	err = POSTResponse.Validate()
	if err != nil {
		return fmt.Errorf("POSTResponse is invalid: %w", err)
	}

	return c.JSON(http.StatusOK, POSTResponse)
}

// PUTHandler
// TODO hook this up like APIPUTHandler with the same use-case of creating a new Data while working with a client not using Polyapp.
func PUTHandler(c echo.Context) error {
	return nil
}

// APIDELETEHandler has no body. Its signature matches that of a GET Request and it contains no body.
func APIDELETEHandler(c echo.Context) error {
	GETReq, err := common.CreateGETRequest(*c.Request().URL)
	if err != nil {
		return fmt.Errorf("common.CreateGETRequest: %w", err)
	}
	GETReq.UserID = "polyappHubAPIUser"
	err = GETReq.Validate()
	if err != nil {
		return fmt.Errorf("common.CreateGETRequest: %w", err)
	}
	err = allDB.DeleteDataAndChildren(GETReq.DataID)
	if err != nil {
		return fmt.Errorf("allDB.DeleteDataAndChildren: %w", err)
	}
	return nil
}

// APIPUTHandler is very similar to APIPOSTHandler but when the request is made it will create the document
// which will be edited in the rest of the request.
//
// The main difference is that the PUT handler ignores the passed in data ID parameter and instead creates its own
// Data ID. The Data ID is part of the returned "NewURL" field in the JSON response.
//
// Note: PUTHandler is helpful when creating links in to Polyapp which open a new Data. If you are creating an Action
// which opens a new Data, it is much faster to call allDB.CreateData yourself and set NewURL in the response to direct
// the browser to the new Data.
func APIPUTHandler(c echo.Context) error {
	POSTRequest := new(common.POSTRequest)
	// I used to use c.Bind() but c.Bind's implementation is erroring when examining query parameters.
	// I have no idea why it looks at query parameters when the docs say its purpose is to bind the body.
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return fmt.Errorf("ioutil.ReadAll body: %w", err)
	}
	err = json.Unmarshal(body, POSTRequest)
	if err != nil {
		return fmt.Errorf("json.Unmarshal body: %w", err)
	}
	err = common.FixPOSTRequest(POSTRequest)
	if err != nil {
		return fmt.Errorf("FixPOSTRequest: %w", err)
	}
	POSTRequest.DataID = common.GetRandString(25)
	// Don't trust the client to provide a correct UserID - we don't Authenticate by UserID when using API Keys
	POSTRequest.UserID = "polyappHubAPIUser"
	err = POSTRequest.Validate()
	if err != nil {
		return fmt.Errorf("could not validate POSTRequest: %w", err)
	}

	newData := common.Data{}
	_ = newData.Init(nil)
	newData.FirestoreID = POSTRequest.DataID
	newData.IndustryID = "polyapp"
	newData.DomainID = "Hub"
	newData.SchemaID = POSTRequest.SchemaID
	// This Data is invalid - the required fields aren't set - but we will ignore that problem & let backPopulate do its thing.
	err = allDB.CreateData(&newData)
	if err != nil {
		return fmt.Errorf("allDB.CreateData: %w", err)
	}
	// Fix the problem in POSTRequest.Data where the key does not contain the suffix "&data="+newData.FirestoreID because
	// the client does not know what FirestoreID it should be using in the key. TODO force new Data IDs because we don't trust the remote client? But if we force it then every time you sync with Hub you are adding a new entry in the remote.
	for k, v := range POSTRequest.Data {
		if !strings.Contains(k, "&data=") {
			POSTRequest.Data[k+"&data="+newData.FirestoreID] = v
			delete(POSTRequest.Data, k)
		}
	}

	// set MessageID so we can use it if an error is thrown during the request
	c.Set("MessageID", POSTRequest.MessageID)

	POSTResponse, err := handlers.HandlePOST(*POSTRequest)
	if err != nil {
		return fmt.Errorf("HandlePOST failed: %w", err)
	}
	err = POSTResponse.Validate()
	if err != nil {
		return fmt.Errorf("POSTResponse is invalid: %w", err)
	}

	POSTResponse.NewURL = common.CreateURL(POSTRequest.IndustryID, POSTRequest.DomainID, POSTRequest.TaskID, POSTRequest.UXID, POSTRequest.SchemaID, newData.FirestoreID, POSTRequest.UserID, POSTRequest.RoleID, "", "")

	return c.JSON(http.StatusOK, POSTResponse)
}

// APIPOSTHandler is very similar to POSTHandler but it does not perform an Authorization step.
//
// TODO we are currently trusting the remotely provided server to give us a packet of information which will not conflict
// with our own Firestore IDs. It is possible that they maliciously supply their own Firestore ID which == one in our
// database. I do not have a test which investigates what exactly happens in that situation. Presumably the sets fail
// and then the entire request fails, somewhat corrupting the entry. Presumably this corruption is limited to the current
// Schema. But I do not really know.
func APIPOSTHandler(c echo.Context) error {
	POSTRequest := new(common.POSTRequest)
	// I used to use c.Bind() but c.Bind's implementation is erroring when examining query parameters.
	// I have no idea why it looks at query parameters when the docs say its purpose is to bind the body.
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return fmt.Errorf("ioutil.ReadAll body: %w", err)
	}
	err = json.Unmarshal(body, POSTRequest)
	if err != nil {
		return fmt.Errorf("json.Unmarshal body: %w", err)
	}
	err = common.FixPOSTRequest(POSTRequest)
	if err != nil {
		return fmt.Errorf("FixPOSTRequest: %w", err)
	}
	POSTRequest.UserID = "polyappHubAPIUser"
	err = POSTRequest.Validate()
	if err != nil {
		return fmt.Errorf("could not validate POSTRequest: %w", err)
	}

	// set MessageID so we can use it if an error is thrown during the request
	c.Set("MessageID", POSTRequest.MessageID)

	POSTResponse, err := handlers.HandlePOST(*POSTRequest)
	if err != nil {
		return fmt.Errorf("HandlePOST failed: %w", err)
	}
	err = POSTResponse.Validate()
	if err != nil {
		return fmt.Errorf("POSTResponse is invalid: %w", err)
	}

	return c.JSON(http.StatusOK, POSTResponse)
}

// GETHandler is the server-side equivalent of the client's clientGETHandler. It handles navigation
// events by loading the proper task for the page.
// This should be kept in sync with cmd/serviceworker/main.go clientGETHandler.
//
// Requires that Authenticate and UserAuthorize have been called in sequence as middleware. Requires that the "User" key
// was populated in echo.Context.
func GETHandler(c echo.Context) error {
	req := c.Request()
	request, err := common.CreateGETRequest(*req.URL)
	if err != nil {
		return fmt.Errorf("CreateGETRequest failure: %w", err)
	}
	err = request.Validate()
	if err != nil {
		return fmt.Errorf("could not validate request: %w", err)
	}
	userInterface := c.Get("User")
	user := userInterface.(*common.User)
	err = handlers.RoleAuthorize(user, request, request.RoleID, 'r')
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "could not authorize user for this request: "+err.Error())
	}
	// Make sure the authorized user is the one we are using to access the system.
	// I had the idea a while ago that I should allow admin users to set the User parameter and then they could mimic
	// Users with lower security. But alas I haven't set that up yet so for now it's safer to ignore the UserID parameter.
	request.UserCache = user
	request.UserID = user.FirestoreID

	response, err := handlers.HandleGET(request)
	if err != nil {
		return fmt.Errorf("HandleGET failed: %w", err)
	}
	err = response.Validate()
	if err != nil {
		return fmt.Errorf("response is invalid: %w", err)
	}
	if response.RedirectURL != "" {
		return c.Redirect(http.StatusTemporaryRedirect, response.RedirectURL)
	}
	return c.HTMLBlob(http.StatusOK, []byte(response.HTML))
}

func DummyHandler(c echo.Context) error {
	return nil
}

// APIGETHandler is very similar to GETHandler but it does not perform an authorization step.
func APIGETHandler(c echo.Context) error {
	req := c.Request()
	rewrittenURL, err := url.Parse(strings.TrimPrefix(req.URL.String(), "/api"))
	if err != nil {
		return fmt.Errorf("url.Parse %v err: %w", req.URL.String(), err)
	}
	request, err := common.CreateGETRequest(*rewrittenURL)
	if err != nil {
		return fmt.Errorf("CreateGETRequest failure: %w", err)
	}
	request.UserID = "polyappHubAPIUser"
	err = request.Validate()
	if err != nil {
		return fmt.Errorf("could not validate request: %w", err)
	}
	response, err := handlers.HandleGET(request)
	if err != nil {
		return fmt.Errorf("HandleGET failed: %w", err)
	}
	err = response.Validate()
	if err != nil {
		return fmt.Errorf("response is invalid: %w", err)
	}
	if response.RedirectURL != "" {
		return c.Redirect(http.StatusTemporaryRedirect, response.RedirectURL)
	}
	return c.HTMLBlob(http.StatusOK, []byte(response.HTML))
}

// signInGET this page is special because it can't store any information while someone is typing it (we don't want
// to store the password as someone is typing it).
func signInGET(c echo.Context) error {
	req := c.Request()
	request, err := common.CreateGETRequest(*req.URL)
	if err != nil {
		return fmt.Errorf("CreateGETRequest failure: %w", err)
	}
	err = request.Validate()
	if err != nil {
		return fmt.Errorf("could not validate request: %w", err)
	}
	authorizationHeader := c.Request().Header.Get("Authorization")
	if len(authorizationHeader) > 0 {
		// we don't have to set the user to anything in particular; we just need to know they are already signed in
		authorizationHeaderSplit := strings.Split(authorizationHeader, " ")
		if len(authorizationHeaderSplit) != 2 {
			return echo.NewHTTPError(401, "auth header was not splittable")
		}
		authToken := authorizationHeaderSplit[1]
		if authToken == "" {
			return echo.NewHTTPError(401, "auth header not included in request")
		}
		ctx, client, err := firebaseCRUD.GetFirebaseClient()
		if err != nil {
			return echo.NewHTTPError(500, err.Error())
		}
		token, err := client.VerifyIDToken(ctx, authToken)
		if err != nil {
			return echo.NewHTTPError(500, err.Error())
		}
		if token.Firebase.SignInProvider == "anonymous" {
			request.UserID = ""
		} else {
			firebaseCtx, firebaseClient, err := firebaseCRUD.GetFirebaseClient()
			if err != nil {
				return echo.NewHTTPError(500, err.Error())
			}
			userRecord, err := firebaseClient.GetUser(firebaseCtx, token.UID)
			if err != nil {
				return echo.NewHTTPError(401, fmt.Errorf("error getting information about a user at UID ("+token.UID+"): %w", err))
			}
			if userRecord.CustomClaims["UserID"] == nil {
				// Happens if on /signIn or another page without authentication where we haven't run Authentication after a new user was created
				tokenUser := &common.User{
					UID:           common.String(userRecord.UID),
					FullName:      common.String(userRecord.DisplayName),
					PhotoURL:      common.String(userRecord.PhotoURL),
					EmailVerified: common.Bool(userRecord.EmailVerified),
					PhoneNumber:   common.String(userRecord.PhoneNumber),
					Email:         common.String(userRecord.Email),
				}
				err = createNewUser(firebaseCtx, firebaseClient, userRecord, tokenUser)
				if err != nil {
					return echo.NewHTTPError(500, fmt.Errorf("createNewUser: %w", err))
				}
				request.UserID = tokenUser.FirestoreID
			} else {
				request.UserID = userRecord.CustomClaims["UserID"].(string)
			}
		}
	}

	response, err := handlers.HandleGET(request)
	if err != nil {
		return fmt.Errorf("handlers.HandleGET: %w", err)
	}
	if response.RedirectURL != "" {
		return c.Redirect(http.StatusTemporaryRedirect, response.RedirectURL)
	}
	return c.HTMLBlob(http.StatusOK, []byte(response.HTML))
}

// signInPOST this page is special because it can't store any information while someone is typing it (we don't want
// to store the password as someone is typing it).
func signInPOST(c echo.Context) error {
	POSTRequest := new(common.POSTRequest)
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return fmt.Errorf("ioutil.ReadAll body: %w", err)
	}
	err = json.Unmarshal(body, POSTRequest)
	if err != nil {
		return fmt.Errorf("json.Unmarshal body: %w", err)
	}
	err = common.FixPOSTRequest(POSTRequest)
	if err != nil {
		return fmt.Errorf("FixPOSTRequest: %w", err)
	}
	err = POSTRequest.Validate()
	if err != nil {
		return fmt.Errorf("could not validate POSTRequest: %w", err)
	}
	authorizationHeader := c.Request().Header.Get("Authorization")
	if len(authorizationHeader) > 0 {
		// we don't have to set the user to anything in particular; we just need to know they are already signed in
		authorizationHeaderSplit := strings.Split(authorizationHeader, " ")
		if len(authorizationHeaderSplit) != 2 {
			return echo.NewHTTPError(401, "auth header was not splittable")
		}
		authToken := authorizationHeaderSplit[1]
		if authToken == "" {
			return echo.NewHTTPError(401, "auth header not included in request")
		}
		ctx, client, err := firebaseCRUD.GetFirebaseClient()
		if err != nil {
			return echo.NewHTTPError(500, err.Error())
		}
		token, err := client.VerifyIDToken(ctx, authToken)
		if err != nil {
			return echo.NewHTTPError(500, err.Error())
		}
		if token.Firebase.SignInProvider == "anonymous" {
			POSTRequest.UserID = ""
		} else {
			firebaseCtx, firebaseClient, err := firebaseCRUD.GetFirebaseClient()
			if err != nil {
				return echo.NewHTTPError(500, err.Error())
			}
			userRecord, err := firebaseClient.GetUser(firebaseCtx, token.UID)
			if err != nil {
				return echo.NewHTTPError(401, fmt.Errorf("error getting information about a user at UID ("+token.UID+"): %w", err))
			}
			if userRecord.CustomClaims["UserID"] == nil {
				// Happens if on /signIn or another page without authentication where we haven't run Authentication after a new user was created
				tokenUser := &common.User{
					UID:           common.String(userRecord.UID),
					FullName:      common.String(userRecord.DisplayName),
					PhotoURL:      common.String(userRecord.PhotoURL),
					EmailVerified: common.Bool(userRecord.EmailVerified),
					PhoneNumber:   common.String(userRecord.PhoneNumber),
					Email:         common.String(userRecord.Email),
				}
				err = createNewUser(firebaseCtx, firebaseClient, userRecord, tokenUser)
				if err != nil {
					return echo.NewHTTPError(500, fmt.Errorf("createNewUser: %w", err))
				}
				POSTRequest.UserID = tokenUser.FirestoreID
			} else {
				POSTRequest.UserID = userRecord.CustomClaims["UserID"].(string)
			}
		}
	}

	POSTResponse, err := handlers.HandlePOST(*POSTRequest)
	if err != nil {
		return fmt.Errorf("HandlePOST failed: %w", err)
	}
	err = POSTResponse.Validate()
	if err != nil {
		return fmt.Errorf("POSTResponse is invalid: %w", err)
	}

	return c.JSON(http.StatusOK, POSTResponse)
}

// BlobUploadHandler is designed to handle large file uploads (Blobs) and is a lot like a PUT request.
func BlobUploadHandler(c echo.Context) error {
	req := c.Request()
	getRequest, err := common.CreateGETRequest(*req.URL)
	if err != nil {
		return fmt.Errorf("CreateGETRequest failure: %w", err)
	}
	metadata := make(map[string]string)
	Title, _ := url.QueryUnescape(req.URL.Query().Get("Title"))
	if Title != "" {
		metadata["Title"] = Title
	}
	Type, _ := url.QueryUnescape(req.URL.Query().Get("Type"))
	if Type != "" {
		metadata["Type"] = Type
	}
	expectedLength := -1
	Length, _ := url.QueryUnescape(req.URL.Query().Get("Length"))
	if Length != "" {
		expectedLength, _ = strconv.Atoi(Length)
	}
	field, err := url.PathUnescape(req.URL.Query().Get("field"))
	if err != nil {
		return fmt.Errorf("url.PathUnescape %v: %w", req.URL.Query().Get("field"), err)
	}

	userInterface := c.Get("User")
	user := userInterface.(*common.User)
	err = handlers.RoleAuthorize(user, getRequest, getRequest.RoleID, 'u')
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "could not authorize user for this request: "+err.Error())
	}
	err = handlers.RoleAuthorize(user, getRequest, getRequest.RoleID, 'u')
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "could not authorize user for the overrides in this request: "+err.Error())
	}
	getRequest.UserID = user.FirestoreID
	getRequest.UserCache = user

	err = handlers.HandleUploads(req.Body, getRequest, field, url.PathEscape(req.URL.Query().Get("filesLength")), metadata, expectedLength)
	if err != nil {
		return fmt.Errorf("HandlePOST failed: %w", err)
	}

	return c.NoContent(http.StatusOK)
}

// BlobDownloadHandler handles serving any Blob file, like an image or audio file.
//
// Note: this implementation is coupled to common.BlobURL().
func BlobDownloadHandler(c echo.Context) error {
	URL := c.Request().URL
	userInterface := c.Get("User")
	user := userInterface.(*common.User)
	statusCode, outBytes, err := blobDownloadHandler(URL, user)
	if err != nil {
		return fmt.Errorf("blobDownloadHandler: %w", err)
	}
	c.Response().Header().Set("Cache-Control", "public, max-age=3600")
	if statusCode == http.StatusNotFound {
		return c.HTML(http.StatusNotFound, "<p>File was not found. It may have been deleted. Please navigate back to the last page to try again.</p>")
	}
	if bytes.HasPrefix(outBytes, []byte("<svg xmlns")) {
		// DetectContentType does not detect SVG.
		return c.Blob(statusCode, "image/svg+xml", outBytes)
	}
	if bytes.HasPrefix(outBytes, []byte("/*! * Bootstrap v4.4.1 (https://getbootstrap.com/)")) {
		// DetectContentType does not detect CSS
		return c.Blob(statusCode, "text/css", outBytes)
	}
	return c.Blob(statusCode, http.DetectContentType(outBytes), outBytes)
}

func blobDownloadHandler(URL *url.URL, user *common.User) (int, []byte, error) {
	fullPath := URL.EscapedPath()
	shortPath := strings.TrimPrefix(fullPath, "/blob/assets/")
	pathSplit := strings.Split(shortPath, "/")
	key, err := url.PathUnescape(pathSplit[1])
	if err != nil {
		return 0, nil, fmt.Errorf("key ("+key+") could not be escaped: %w", err)
	}
	if len(key) < 5 {
		return http.StatusNotFound, nil, errors.New("Key length was less than 5 so we can't process this key. It's probably length 0: " + key)
	}
	keySplit := strings.Split(key, "_")
	if len(keySplit) > 4 {
		// remove any section of the URL which is used to ID which Subtask (which Data document) this Field is referencing.
		key = strings.Join([]string{keySplit[0], keySplit[1], keySplit[2], keySplit[3]}, "_")
	}
	if key[len(key)-4:] == ".ico" {
		key = key[:len(key)-4]
	}

	getRequest := common.GETRequest{
		IndustryID: keySplit[0],
		DomainID:   keySplit[1],
		SchemaID:   keySplit[2],
		DataID:     pathSplit[0],
	}
	err = handlers.RoleAuthorize(user, getRequest, getRequest.RoleID, 'u')
	if err != nil {
		return 0, nil, echo.NewHTTPError(http.StatusUnauthorized, "could not authorize user for this request: "+err.Error())
	}
	err = handlers.RoleAuthorize(user, getRequest, getRequest.RoleID, 'u')
	if err != nil {
		return 0, nil, echo.NewHTTPError(http.StatusUnauthorized, "could not authorize user for the overrides in this request: "+err.Error())
	}
	getRequest.UserID = user.FirestoreID
	getRequest.UserCache = user

	d := common.Data{
		FirestoreID: pathSplit[0],
	}
	err = allDB.Read(&d)
	if err != nil {
		return 0, nil, fmt.Errorf("allDB.ReadData: %w", err)
	}
	if len(pathSplit) >= 4 {
		// ABytes (probably) which lets you view one image at a time
		keySplit := strings.Split(key, "/")
		aBytesKey := keySplit[0]
		aBytesIndex, err := strconv.Atoi(pathSplit[2])
		if err != nil {
			return http.StatusNotFound, nil, nil
		}
		d.ABytes[aBytesKey], err = allDB.ReadDataABytes(d.FirestoreID, aBytesKey)
		if err != nil {
			return 0, nil, fmt.Errorf("allDB.ReadDataABytes: %w", err)
		}
		if d.ABytes[aBytesKey][aBytesIndex] == nil || len(d.ABytes[aBytesKey]) == 0 {
			return http.StatusNotFound, nil, nil
		}
		return http.StatusOK, d.ABytes[aBytesKey][aBytesIndex], nil
	} else {
		d.ABytes[key], err = allDB.ReadDataABytes(d.FirestoreID, key)
		if err != nil {
			return 0, nil, fmt.Errorf("allDB.ReadDataABytes: %w", err)
		}
		d.Bytes[key], err = allDB.ReadDataBytes(d.FirestoreID, key)
		if err != nil {
			return 0, nil, fmt.Errorf("allDB.ReadDataBytes: %w", err)
		}
		if d.ABytes[key] != nil {
			// ABytes which wants to view all images at once
			html, err := handlers.HandleMultipleBlobs(d, key, URL)
			if err != nil {
				return 0, nil, fmt.Errorf("handlers.HandleMultipleBlobs: %w", err)
			}
			return http.StatusOK, html, nil
		} else {
			if d.Bytes[key] == nil || len(d.Bytes[key]) == 0 {
				return http.StatusNotFound, nil, nil
			}
			return http.StatusOK, d.Bytes[key], nil
		}
	}
}

// InternalPOSTHandler is for internal pages which are only accessible to Polyapp.
func InternalPOSTHandler(c echo.Context) error {
	POSTRequest := new(common.POSTRequest)
	// I used to use c.Bind() but c.Bind's implementation is erroring when examining query parameters.
	// I have no idea why it looks at query parameters when the docs say its purpose is to bind the body.
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return fmt.Errorf("ioutil.ReadAll body: %w", err)
	}
	err = json.Unmarshal(body, POSTRequest)
	if err != nil {
		return fmt.Errorf("json.Unmarshal body: %w", err)
	}
	err = common.FixPOSTRequest(POSTRequest)
	if err != nil {
		return fmt.Errorf("FixPOSTRequest: %w", err)
	}
	err = POSTRequest.Validate()
	if err != nil {
		return fmt.Errorf("could not validate POSTRequest: %w", err)
	}

	// set MessageID so we can use it if an error is thrown during the request
	c.Set("MessageID", POSTRequest.MessageID)

	POSTResponse, err := handlers.InternalPOST(*POSTRequest)
	if err != nil {
		return fmt.Errorf("HandlePOST failed: %w", err)
	}
	err = POSTResponse.Validate()
	if err != nil {
		return fmt.Errorf("POSTResponse is invalid: %w", err)
	}

	return c.JSON(http.StatusOK, POSTResponse)
}

// InternalGETHandler is for internal pages which are only accessible to Polyapp.
func InternalGETHandler(c echo.Context) error {
	req := c.Request()
	request, err := common.CreateGETRequest(*req.URL)
	if err != nil {
		return fmt.Errorf("CreateGETRequest failure: %w", err)
	}
	err = request.Validate()
	if err != nil {
		return fmt.Errorf("could not validate request: %w", err)
	}
	response, err := handlers.InternalGET(request)
	if err != nil {
		return fmt.Errorf("InternalGET failed: %w", err)
	}
	err = response.Validate()
	if err != nil {
		return fmt.Errorf("response is invalid: %w", err)
	}
	return c.HTMLBlob(http.StatusOK, []byte(response.HTML))
}

// GETDataTable is used by requests from DataTable. Because the DataTable has its own special request and response
// structure and we are not writing our own table JS requests / response stuff, it needs its own dedicated handler.
// That being said, make sure you are still taking the standard security precautions!
func GETDataTable(c echo.Context) error {
	req := c.Request()
	request, err := common.CreateDataTableRequest(*req.URL)
	if err != nil {
		return fmt.Errorf("CreateDataTableRequest failure: %w", err)
	}
	// normally the request would be validated after you GET it, but DataTable requests lack a Task so will never be valid.
	userInterface := c.Get("User")
	if userInterface == nil {
		return errors.New("GETDataTable must be authenticated")
	}
	user := userInterface.(*common.User)
	err = handlers.RoleAuthorize(user, common.GETRequest{
		IndustryID: request.IndustryID,
		DomainID:   request.DomainID,
	}, "", 'r')
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "could not authorize user for this request: "+err.Error())
	}
	if request.SchemaID == "ADdkaAyYPQwXOLkMfGUGqoPsl" {
		// We can't show users this Schema ID directly. Instead, show them only those records which they actually own.
		request.SchemaID = user.FirestoreID + "schema"
	}
	dataTableResponse, err := handlers.GETDataTable(request)
	if err != nil {
		return fmt.Errorf("handlers.GETDataTable: %w", err)
	}
	return c.JSON(http.StatusOK, dataTableResponse)
}

func APIGETDataTable(c echo.Context) error {
	req := c.Request()
	request, err := common.CreateDataTableRequest(*req.URL)
	if err != nil {
		return fmt.Errorf("CreateDataTableRequest failure: %w", err)
	}
	dataTableResponse, err := handlers.GETDataTable(request)
	if err != nil {
		return fmt.Errorf("handlers.GETDataTable: %w", err)
	}
	return c.JSON(http.StatusOK, dataTableResponse)
}

// GETSelectSearch performs a search among the specified collection and looks for a field which matches the input.
func GETSelectSearch(c echo.Context) error {
	request, err := common.CreateSelectSearchRequest(c.Request().URL)
	if err != nil {
		return fmt.Errorf("common.CreateSelectSearchRequest: %w", err)
	}
	// normally the request would be validated after you GET it, but we lack a Task so will never be valid.
	userInterface := c.Get("User")
	if userInterface == nil {
		return errors.New("GETSelectSearch must be authenticated")
	}
	user := userInterface.(*common.User)
	getReq := common.GETRequest{}
	switch request.Collection {
	case common.CollectionData:
		industry, domain, schema, _ := common.SplitField(request.Field)
		getReq.IndustryID = industry
		getReq.DomainID = domain
		getReq.SchemaID = schema
	case common.CollectionSchema, common.CollectionTask, common.CollectionUX:
		getReq.IndustryID = "polyapp"
		getReq.DomainID = "TaskSchemaUX"
	case common.CollectionBot:
		getReq.IndustryID = "polyapp"
		getReq.DomainID = "Bot"
	case common.CollectionAction:
		getReq.IndustryID = "polyapp"
		getReq.DomainID = "Action"
	case common.CollectionRole:
		getReq.IndustryID = "polyapp"
		getReq.DomainID = "Role"
	case common.CollectionUser:
		getReq.IndustryID = "polyapp"
		getReq.DomainID = "User"
	default:
		getReq.IndustryID = "polyapp"
		getReq.DomainID = "polyapp"
	}
	err = handlers.RoleAuthorize(user, getReq, "", 'r')
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "could not authorize user for GETSelectSearch request: "+err.Error())
	}
	selectSearchResponse, err := handlers.GETSelectSearch(request)
	if err != nil {
		return fmt.Errorf("handlers.GETSelectSearch: %w", err)
	}
	return c.JSON(http.StatusOK, selectSearchResponse)
}

// GETFullTextSearch performs a search among the specified collection's Name and (if available) Help Text fields.
// Support is limited to.
func GETFullTextSearch(c echo.Context) error {
	var err error
	request, err := common.CreateFullTextSearchRequest(c.Request().URL)
	if err != nil {
		return fmt.Errorf("common.CreateFullTextSearchRequest: %w", err)
	}

	userInterface := c.Get("User")
	if userInterface == nil {
		return errors.New("GETFullTextSearch must be authenticated")
	}
	user := userInterface.(*common.User)
	getReq := common.GETRequest{}
	switch request.Collection {
	case common.CollectionSchema, common.CollectionTask, common.CollectionUX:
		getReq.IndustryID = "polyapp"
		getReq.DomainID = "TaskSchemaUX"
		getReq.SchemaID = ""
	}
	err = handlers.RoleAuthorize(user, getReq, "", 'r')
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "could not authorize user for GETFullTextSearch request: "+err.Error())
	}

	fullTextSearchResponse, err := handlers.GETFullTextSearch(request)
	if err != nil {
		return fmt.Errorf("handlers.GETFullTextSearch: %w", err)
	}
	return c.JSON(http.StatusOK, fullTextSearchResponse)
}

// GETAPICredentials creates a unique ID / API Key and gives that combination to the requester.
// The ID / API Key are stored in secrets manager.
func GETAPICredentials(c echo.Context) error {
	var APIIDMapping struct {
		APIID       string `suffix:"API ID"`
		HubSchemaID string `suffix:"Hub Schema ID"`
		HubTaskID   string `suffix:"Hub Task ID"`
		HubUXID     string `suffix:"Hub UX ID"`
	}
	var err error
	APIIDMapping.APIID, err = common.GetCryptographicallyRandString(30)
	if err != nil {
		return fmt.Errorf("common.GetCryptographicallyRandString: %w", err)
	}
	APIIDMapping.HubSchemaID = common.GetRandString(30)
	APIIDMapping.HubTaskID = common.GetRandString(30)
	APIIDMapping.HubUXID = common.GetRandString(30)

	hubSchema, err := allDB.ReadSchema("tKPCahjxcZHviTXprUnFmHJSj")
	if err != nil {
		return fmt.Errorf("allDB.ReadSchema: %w", err)
	}
	hubSchema.FirestoreID = APIIDMapping.HubSchemaID
	hubSchema, err = common.SchemaConvertSchema(&hubSchema, hubSchema.FirestoreID)
	if err != nil {
		return fmt.Errorf("common.SchemaConvertSchema: %w", err)
	}
	err = allDB.CreateSchema(&hubSchema)
	if err != nil {
		return fmt.Errorf("allDB.CreateSchema: %w", err)
	}

	hubTask, err := allDB.ReadTask("timwXghrohGAmFRHdhusUojXj")
	if err != nil {
		return fmt.Errorf("hubTask Read: %w", err)
	}
	hubTask.FirestoreID = APIIDMapping.HubTaskID
	hubTask, err = common.SchemaConvertTask(&hubTask, hubSchema.FirestoreID)
	if err != nil {
		return fmt.Errorf("common.SchemaConvertTask: %w", err)
	}
	err = allDB.CreateTask(&hubTask)
	if err != nil {
		return fmt.Errorf("allDB.CreateTask: %w", err)
	}

	hubUX, err := allDB.ReadUX("EuykMbYuOVbRjtloPqXvpMCDM")
	if err != nil {
		return fmt.Errorf("allDB.ReadUX: %w", err)
	}
	hubUX.FirestoreID = APIIDMapping.HubUXID
	hubUX, err = common.SchemaConvertUX(&hubUX, hubSchema.FirestoreID)
	if err != nil {
		return fmt.Errorf("common.SchemaConvertTask: %w", err)
	}
	err = allDB.CreateUX(&hubUX)
	if err != nil {
		return fmt.Errorf("allDB.CreateUX: %w", err)
	}

	d := common.Data{}
	_ = d.Init(nil)
	err = common.StructureIntoData("polyapp", "API ID Mapping", "boDZGOgWZLgjnnMGqcVNZKdcH", &APIIDMapping, &d)
	if err != nil {
		return fmt.Errorf("actions.StructureIntoData: %w", err)
	}
	d.FirestoreID = common.GetRandString(25)
	err = allDB.CreateData(&d)
	if err != nil {
		return fmt.Errorf("allDB.CreateData: %w", err)
	}

	APIKey, err := common.GetCryptographicallyRandString(60)
	if err != nil {
		return fmt.Errorf("common.GetCryptographicallyRandString: %w", err)
	}
	ctx := context.Background()
	client, err := secretmanager.NewClient(ctx)
	if err != nil {
		return echo.NewHTTPError(500, fmt.Errorf("secretmanager.NewClient: %w", err))
	}
	createSecretRequest := &secretmanagerpb.CreateSecretRequest{
		Parent:   "projects/" + common.GetGoogleProjectID(),
		SecretId: "POLYAPP_HUB_API_KEYS_" + APIIDMapping.APIID,
		Secret: &secretmanagerpb.Secret{
			Replication: &secretmanagerpb.Replication{
				Replication: &secretmanagerpb.Replication_Automatic_{
					Automatic: &secretmanagerpb.Replication_Automatic{},
				},
			},
		},
	}
	_, err = client.CreateSecret(ctx, createSecretRequest)
	if err != nil {
		return echo.NewHTTPError(500, fmt.Errorf("client.CreateSecret: %w", err))
	}
	addSecretRequest := &secretmanagerpb.AddSecretVersionRequest{
		Parent: "projects/" + common.GetGoogleProjectID() + "/secrets/POLYAPP_HUB_API_KEYS_" + APIIDMapping.APIID,
		Payload: &secretmanagerpb.SecretPayload{
			Data: []byte(APIKey),
		},
	}
	_, err = client.AddSecretVersion(ctx, addSecretRequest)
	if err != nil {
		return echo.NewHTTPError(500, fmt.Errorf("client.AddSecret: %w", err))
	}

	var resp struct {
		APIID       string
		APIKey      string
		HubSchemaID string
		HubTaskID   string
		HubUXID     string
	}
	resp.APIID = APIIDMapping.APIID
	resp.APIKey = APIKey
	resp.HubSchemaID = APIIDMapping.HubSchemaID
	resp.HubTaskID = APIIDMapping.HubTaskID
	resp.HubUXID = APIIDMapping.HubUXID
	return c.JSON(200, resp)
}

// DELETEAPICredentials deactivates an ID / API Key combination.
//
// The secrets are not deleted immediately. Instead we will have to implement functionality later which goes through and
// "cleans up" deactivated secrets after a waiting period has passed (probably 30 days).
func DELETEAPICredentials(c echo.Context) error {
	ID := c.Request().URL.Query().Get("ID")

	ctx := context.Background()
	client, err := secretmanager.NewClient(ctx)
	if err != nil {
		return fmt.Errorf("secretmanager.NewClient: %v", err)
	}
	// Note: I initially tried to delete version /latest but unfortunately it seems to need a version # to delete.
	// I received this error when using /latest: client.DisableSecretVersion: rpc error: code = InvalidArgument desc = Resource ID [projects/.../secrets/.../versions/latest] is not in a valid format.
	listSecretVersionsRequest := &secretmanagerpb.ListSecretVersionsRequest{
		Parent: "projects/" + common.GetGoogleProjectID() + "/secrets/POLYAPP_HUB_API_KEYS_" + ID,
	}
	it := client.ListSecretVersions(ctx, listSecretVersionsRequest)
	for {
		resp, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return fmt.Errorf("failed to list secret versions: %v", err)
		}

		if resp.State == secretmanagerpb.SecretVersion_ENABLED {
			req := &secretmanagerpb.DisableSecretVersionRequest{
				Name: resp.Name,
			}
			if _, err := client.DisableSecretVersion(ctx, req); err != nil {
				return fmt.Errorf("client.DisableSecretVersion: %v", err)
			}
		}
	}
	return c.NoContent(http.StatusNoContent)
}

// errorHandler is shown if there's a 404 or 500 or similar error.
func errorHandler(errIn error, c echo.Context) {
	code := http.StatusInternalServerError
	if he, ok := errIn.(*echo.HTTPError); ok {
		code = he.Code
	}
	if c.Request().Method == "POST" {
		// request is being made not to load a page, but to update a page or insert
		// some HTML into that page. Therefore to formulate an effective response
		// we must create a JSON response which follows the user's expected format.
		messageIDInterface := c.Get("MessageID")
		messageID, ok := messageIDInterface.(string)
		if !ok {
			messageID = "none"
		}
		errResponse, err := errorPOSTResponse(messageID, errIn)
		if err != nil {
			c.Logger().Errorf("errorPOSTResponse: %w", err)
			c.Logger().Errorf("errIn: %v", errIn.Error())
			return
		}
		err = c.JSON(code, errResponse)
		if err != nil {
			c.Logger().Error("error replying to request: " + err.Error())
			c.Logger().Errorf("errIn: %v", errIn.Error())
		}
		return
	}

	html, err := handlers.AppShellHTML(&common.UX{Title: common.String("Polyapp " + strconv.Itoa(code) + " Error")})
	if err != nil {
		c.Logger().Error("error app shell could not be created: " + err.Error())
		c.Logger().Errorf("errIn: %v", errIn.Error())
		return
	}
	errorPage := filepath.Join(filepath.ToSlash(publicPath), fmt.Sprintf("%d.html", code))
	innerHTML, err := ioutil.ReadFile(errorPage)
	if err != nil {
		c.Logger().Error("error getting error text: " + err.Error())
		c.Logger().Errorf("errIn: %v", errIn.Error())
		return
	}
	innerHTML = append(innerHTML, []byte(`<p id="polyappJSServerErrorMessage">Error message: `+errIn.Error()+`</p>`)...)
	combinedHTML, err := handlers.InnerIntoAppShell(innerHTML, html)
	if err != nil {
		c.Logger().Error("error insertion of error into app shell could not be done: " + err.Error())
		c.Logger().Errorf("errIn: %v", errIn.Error())
		return
	}

	if err := c.HTMLBlob(code, []byte(combinedHTML)); err != nil {
		c.Logger().Error("error page could not be displayed: " + err.Error())
		c.Logger().Errorf("errIn: %v", errIn.Error())
		return
	}
}

// errorPOSTResponse composes a response which will show a banner to users notifying them of a failure.
//
// TODO This function is exposing information about the error to the user. This is the wrong thing to do from a
// security perspective, even if it is sometimes helpful from a usability perspective. This behavior should probably
// be toggleable. Perhaps an ID could be assigned to each error string & stored in the database; the ID could be
// shown to the person who sees the message, and a separate Task could be used to look up the actual error text from the ID.
func errorPOSTResponse(messageID string, errIn error) (common.POSTResponse, error) {
	var htmlBuf bytes.Buffer
	err := gen.GenToast(gen.Toast{
		Heading: "Error Saving",
		Text:    "An error occurred while saving. Error details: " + errIn.Error(),
	}, &htmlBuf)
	if err != nil {
		return common.POSTResponse{}, fmt.Errorf("GenToast: %w", err)
	}
	return common.POSTResponse{
		MessageID: messageID,
		ModDOMs: []common.ModDOM{
			{
				DeleteSelector: "#polyappJSToastContainer > *",
				InsertSelector: "#polyappJSToastContainer",
				Action:         "beforeend",
				HTML:           htmlBuf.String(),
			},
		},
	}, nil
}

// Authenticate is a middleware function to authenticate a request. It tries to fail fast for unauthorized requests.
// Side effect: This function sets UID in echo's Context.
func Authenticate(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		authorizationHeader := c.Request().Header.Get("Authorization")
		if len(authorizationHeader) < 1 {
			c.Response().Header().Set("WWW-Authenticate", "Bearer realm=Polyapp")
			return echo.NewHTTPError(401, "Authorization header not included")
		}
		authorizationHeaderSplit := strings.Split(authorizationHeader, " ")
		if len(authorizationHeaderSplit) != 2 {
			return echo.NewHTTPError(401, "auth header was not splittable")
		}
		authToken := authorizationHeaderSplit[1]
		if authToken == "" {
			return echo.NewHTTPError(401, "auth header not included in request")
		}
		ctx, client, err := firebaseCRUD.GetFirebaseClient()
		if err != nil {
			return echo.NewHTTPError(500, err.Error())
		}
		token, err := client.VerifyIDToken(ctx, authToken)
		if err != nil {
			return echo.NewHTTPError(403, "couldn't verify ID token: "+err.Error())
		}
		c.Set("UserToken", token) // unfortunate but necessary to cross boundaries

		return next(c)
	}
}

// UserAuthorize is a middleware function to ensure the Authorized User has some sort of access
// to Polyapp's systems. If the User does not seem to have a profile yet in Firestore and the User is not anonymous,
// we also create a profile for this user.
//
// It requires that "UserID" is available in echo's context storage.
//
// Side effect: "User" in echo's context is set to be a common.User object.
func UserAuthorize(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		token, ok := c.Get("UserToken").(*auth.Token)
		if !ok {
			return errors.New("UserToken not included by Authenticate middleware")
		}

		firebaseCtx, firebaseClient, err := firebaseCRUD.GetFirebaseClient()
		if err != nil {
			return echo.NewHTTPError(500, err.Error())
		}
		userRecord, err := firebaseClient.GetUser(firebaseCtx, token.UID)
		if err != nil {
			return echo.NewHTTPError(401, fmt.Errorf("error getting information about a user at UID ("+token.UID+"): %w", err))
		}
		tokenUser := &common.User{
			UID:           common.String(userRecord.UID),
			FullName:      common.String(userRecord.DisplayName),
			PhotoURL:      common.String(userRecord.PhotoURL),
			EmailVerified: common.Bool(userRecord.EmailVerified),
			PhoneNumber:   common.String(userRecord.PhoneNumber),
			Email:         common.String(userRecord.Email),
		}

		// Get the user corresponding to this token user.
		var userInDB *common.User
		userID, ok := userRecord.CustomClaims["UserID"]
		if !ok || userID == nil || userID == "" {
			// must be a new user since they lack a User ID, or this is an anonymous user.
			if userRecord.Email == "" {
				// anonymous user. No one else would lack an email.
				// Notice we are not setting the custom user claims. This is intentional so that this code path is taken on
				// subsequent requests.
				tokenUser.FirestoreID = "anonymous"
				uTemp, err := allDB.ReadUser(tokenUser.FirestoreID)
				if err != nil {
					return fmt.Errorf("allDB.ReadUser ("+tokenUser.FirestoreID+"): %w", err)
				}
				userInDB = &uTemp
				tokenUser = &uTemp
			} else {
				// A new user!
				err = createNewUser(firebaseCtx, firebaseClient, userRecord, tokenUser)
				if err != nil {
					return echo.NewHTTPError(500, fmt.Errorf("createNewUser: %w", err))
				}
				userInDB = tokenUser
			}
		} else {
			tokenUser.FirestoreID = userID.(string)
			uTemp, err := allDB.ReadUser(tokenUser.FirestoreID)
			if err != nil {
				return echo.NewHTTPError(500, fmt.Errorf("allDB.ReadUser (\"+userInDB.FirestoreID+\"): %w", err))
			}
			userInDB = &uTemp
		}

		// Now let's make sure the userInDB is valid and has access to the system.
		if userInDB.Deprecated != nil && *userInDB.Deprecated {
			return errors.New("user is deprecated")
		}

		// Synchronize the tokenUser and the userInDB by populating any missing userInDB fields with fields from tokenUser.
		// This should only happen if we are taking the code path which reads the user into userInDB instead of setting it for a new user.
		if !reflect.DeepEqual(userInDB.EmailVerified, tokenUser.EmailVerified) || !reflect.DeepEqual(userInDB.Email, tokenUser.Email) ||
			!reflect.DeepEqual(userInDB.PhotoURL, tokenUser.PhotoURL) || !reflect.DeepEqual(userInDB.FullName, tokenUser.FullName) ||
			!reflect.DeepEqual(userInDB.PhoneNumber, tokenUser.PhoneNumber) {
			// Do we want to update the information in the DB User with information from tokenUser?
			// I would say Yes, if the Token User has the source of truth for some of that information.
			// As I'm writing this, the Token User has the source of truth for all of the items below. So let's update.
			userInDB.Email = tokenUser.Email
			userInDB.EmailVerified = tokenUser.EmailVerified
			userInDB.PhoneNumber = tokenUser.PhoneNumber
			userInDB.PhotoURL = tokenUser.PhotoURL
			userInDB.FullName = tokenUser.FullName

			err := allDB.UpdateUser(userInDB)
			if err != nil {
				return echo.NewHTTPError(500, fmt.Errorf("could not update user ("+userInDB.FirestoreID+") with shortenedTokenUser: %w", err))
			}
			// userInDB should be equivalent to the DB right now.
		}

		// For now, I'll assume that if a user exists in Firestore and they aren't deprecated they are authorized
		// to access the system.
		c.Set("User", userInDB)

		return next(c)
	}
}

// PolyappAuthorize ensures the user's email matches one of a whitelist of emails.
// Put your own email in here and you'll be able to access functionality like test pages.
func PolyappAuthorize(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		var token *auth.Token
		UserTokenUntyped := c.Get("UserToken")
		switch tokenTyped := UserTokenUntyped.(type) {
		case *auth.Token:
			token = tokenTyped
		default:
			return errors.New("UserToken not included by Authenticate middleware")
		}

		firebaseCtx, firebaseClient, err := firebaseCRUD.GetFirebaseClient()
		if err != nil {
			return echo.NewHTTPError(500, err.Error())
		}
		userRecord, err := firebaseClient.GetUser(firebaseCtx, token.UID)
		if err != nil {
			return echo.NewHTTPError(500, fmt.Errorf("firebaseClient.GetUser "+token.UID+": %w", err))
		}

		// hacky but there are only a couple users for right now so it's OK.
		if userRecord.Email != "support@polyapp.tech" && userRecord.Email != "greg@polyapp.tech" {
			return echo.NewHTTPError(401, errors.New("not in PolyappAuthorize whitelist"))
		}
		return next(c)
	}
}

// APIAuthenticate verifies that the provided key and ID in the request match those stored in the secrets manager.
func APIAuthenticate(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		q := c.Request().URL.Query()
		APIKey := q.Get("key")
		ID := q.Get("ID")
		// Add a prefix to prevent problems where we read a secret which is
		// not related to Hub.
		ID = "POLYAPP_HUB_API_KEYS_" + ID
		ctx := context.Background()
		client, err := secretmanager.NewClient(ctx)
		if err != nil {
			return echo.NewHTTPError(500, fmt.Errorf("secretmanager.NewClient: %w", err))
		}
		req := &secretmanagerpb.AccessSecretVersionRequest{
			Name: "projects/" + common.GetGoogleProjectID() + "/secrets/" + ID + "/versions/latest",
		}
		result, err := client.AccessSecretVersion(ctx, req)
		if err != nil && strings.Contains(err.Error(), "PermissionDenied") {
			return echo.NewHTTPError(401, errors.New("client.AccessSecretVersion got PermissionDenied for secret ID "+ID))
		} else if err != nil && strings.Contains(err.Error(), "NotFound") {
			return echo.NewHTTPError(401, fmt.Errorf("client.AccessSecretVersion got NotFound for secret ID "+ID))
		} else if err != nil {
			return echo.NewHTTPError(500, fmt.Errorf("client.AccessSecretVersion %v: %w", ID, err))
		}
		if string(result.Payload.Data) != APIKey {
			return echo.NewHTTPError(403, "could not verify API Key")
		}
		return next(c)
	}
}

// APIAuthorize ensures the request is trying to access something it is authorized to access.
func APIAuthorize(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		q := c.Request().URL.Query()
		ID := q.Get("ID")

		targetSchemaID := ""
		targetTaskID := ""
		targetUXID := ""
		if c.Request().Method == http.MethodGet {
			// The simplest case is if we are trying to access a Hub Collection for Read.
			URLModified := *c.Request().URL
			URLModified.Path = strings.TrimPrefix(URLModified.Path, "/api")
			URLModified.RawPath = strings.TrimPrefix(URLModified.RawPath, "/api")
			GETReq, err := common.CreateGETRequest(URLModified)
			if err != nil {
				return fmt.Errorf("common.CreateGETRequest: %w", err)
			}
			if GETReq.GetIndustry() == "polyapp" && GETReq.GetDomain() == "Hub" {
				// anyone can Read from polyapp/Hub
				return next(c)
			} else {
				return echo.NewHTTPError(http.StatusUnauthorized, fmt.Errorf("API ID %v tried to access Industry %v and Domain %v", ID, GETReq.GetIndustry(), GETReq.GetDomain()))
			}
		} else if c.Request().Method == http.MethodDelete {
			URLModified := *c.Request().URL
			URLModified.Path = strings.TrimPrefix(URLModified.Path, "/api")
			URLModified.RawPath = strings.TrimPrefix(URLModified.RawPath, "/api")
			GETReq, err := common.CreateGETRequest(URLModified)
			if err != nil {
				return fmt.Errorf("common.CreateGETRequest: %w", err)
			}
			GETReq.UserID = "polyappHubAPIUser"
			err = GETReq.Validate()
			if err != nil {
				return fmt.Errorf("common.CreateGETRequest: %w", err)
			}
			dataToDelete := common.Data{
				FirestoreID: GETReq.DataID,
			}
			err = allDB.Read(&dataToDelete)
			if err != nil {
				return fmt.Errorf("allDB.ReadData: %w", err)
			}
			if dataToDelete.IndustryID != GETReq.IndustryID || dataToDelete.DomainID != GETReq.DomainID || dataToDelete.SchemaID != GETReq.SchemaID {
				return echo.NewHTTPError(http.StatusUnauthorized, fmt.Errorf("API ID %v tried to access Industry %v and Domain %v and Schema %v",
					dataToDelete.FirestoreID, dataToDelete.IndustryID, dataToDelete.DomainID, dataToDelete.SchemaID))
			}
			targetSchemaID = GETReq.SchemaID
			targetTaskID = GETReq.TaskID
			targetUXID = GETReq.UXID
		} else {
			POSTRequest := new(common.POSTRequest)
			body, err := ioutil.ReadAll(c.Request().Body)
			if err != nil {
				return fmt.Errorf("ioutil.ReadAll body: %w", err)
			}

			// put the body back
			_ = c.Request().Body.Close()
			c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(body))

			err = json.Unmarshal(body, POSTRequest)
			if err != nil {
				return fmt.Errorf("json.Unmarshal body: %w", err)
			}
			err = common.FixPOSTRequest(POSTRequest)
			if err != nil {
				return fmt.Errorf("FixPOSTRequest: %w", err)
			}
			if POSTRequest.DataID == "" {
				POSTRequest.DataID = "put"
			}
			POSTRequest.UserID = "polyappHubAPIUser"
			err = POSTRequest.Validate()
			if err != nil {
				return fmt.Errorf("could not validate POSTRequest: %w", err)
			}
			if POSTRequest.DataID == "put" {
				POSTRequest.DataID = ""
			}
			targetSchemaID = POSTRequest.SchemaID
			targetTaskID = POSTRequest.TaskID
			targetUXID = POSTRequest.UXID
		}
		if targetSchemaID == "" || targetUXID == "" || targetTaskID == "" {
			return errors.New("target was not set")
		}

		query := allDB.Query{}
		query.Init("polyapp", "API ID Mapping", "boDZGOgWZLgjnnMGqcVNZKdcH", common.CollectionData)
		query.AddEquals(common.AddFieldPrefix("polyapp", "API ID Mapping", "boDZGOgWZLgjnnMGqcVNZKdcH", "API ID"), ID)
		query.SetLength(2)
		iter, err := query.QueryRead()
		if err != nil {
			return fmt.Errorf("query.QueryRead: %w", err)
		}
		foundMappingData := &common.Data{}
		for {
			queryable, err := iter.Next()
			if err != nil && err == common.IterDone {
				break
			}
			if err != nil {
				return fmt.Errorf("iter.Next: %w", err)
			}
			d := queryable.(*common.Data)
			if foundMappingData.FirestoreID != "" {
				return fmt.Errorf("found more than one entry for this API ID (%v) which should not happen since we should not have issued the ID when there was a duplicate in this collection", ID)
			}
			foundMappingData = d
		}
		if foundMappingData.FirestoreID == "" {
			return echo.NewHTTPError(401, fmt.Errorf("did not find an entry in boDZGOgWZLgjnnMGqcVNZKdcH for the API ID (%v) which indicates the ID was not properly issued", ID))
		}
		hubSchemaID := foundMappingData.S[common.AddFieldPrefix("polyapp", "API ID Mapping", "boDZGOgWZLgjnnMGqcVNZKdcH", "Hub Schema ID")]
		hubTaskID := foundMappingData.S[common.AddFieldPrefix("polyapp", "API ID Mapping", "boDZGOgWZLgjnnMGqcVNZKdcH", "Hub Task ID")]
		hubUXID := foundMappingData.S[common.AddFieldPrefix("polyapp", "API ID Mapping", "boDZGOgWZLgjnnMGqcVNZKdcH", "Hub UX ID")]
		if hubSchemaID == nil {
			return fmt.Errorf("hubSchemaID was nil in Data ID %v for API ID %v", foundMappingData.FirestoreID, ID)
		}
		if hubTaskID == nil {
			return fmt.Errorf("hubTaskID was nil in Data ID %v for API ID %v", foundMappingData.FirestoreID, ID)
		}
		if hubUXID == nil {
			return fmt.Errorf("hubUXID was nil in Data ID %v for API ID %v", foundMappingData.FirestoreID, ID)
		}
		if *hubSchemaID != targetSchemaID {
			return echo.NewHTTPError(http.StatusUnauthorized, fmt.Errorf("hubSchemaID != targetSchemaID for Data ID %v and API ID %v", foundMappingData.FirestoreID, ID))
		}
		if *hubTaskID != targetTaskID {
			return echo.NewHTTPError(http.StatusUnauthorized, fmt.Errorf("hubTaskID != targetTaskID for Data ID %v and API ID %v", foundMappingData.FirestoreID, ID))
		}
		if *hubUXID != targetUXID {
			return echo.NewHTTPError(http.StatusUnauthorized, fmt.Errorf("hubUXID != targetUXID for Data ID %v and API ID %v", foundMappingData.FirestoreID, ID))
		}
		// Mere presence in this collection indicates the caller has full CRUD access to the data.
		return next(c)
	}
}

// ExportInstallation is not a real function. The database should be a slave to the configuration seen in the code, not
// the other way around. I never intend to implement this function, and it is here only for documentation of this intent.
func ExportInstallation() (err error) {
	return errors.New("The database should be a slave to the configuration seen in the git repo. There should be one source of truth")
}

// Install reads all files in the BaseDocuments folder (including subfolders), verifies everything constitutes a
// valid JSON object (throwing an error if not), transforms those objects into something which can be stored by firestore,
// checks if the docs exist in the DB (err if so), and stores them into firestore using the client provided.
// This would typically be used to set up a brand new environment, and is designed to be efficient and highly parallel.
// Side effects: this function redirects the fileCRUD directory. Therefore you must call 'init' AFTER calling this function.
func Install(c echo.Context) (err error) {
	var outErr error
	// Don't use a DoOnce because if an error is thrown there's no way for that particular server to re-run the installation.
	outErr = install(c, false)
	if c.Response().Size == 0 && outErr == nil {
		return c.HTML(200, `<Hub>Update has already started and may have completed</Hub>`)
	}
	return outErr
}

func ForceInstall(c echo.Context) error {
	userInterface := c.Get("User")
	user := userInterface.(*common.User)
	err := handlers.RoleAuthorize(user, common.GETRequest{
		IndustryID:         "polyapp",
		OverrideIndustryID: "",
		DomainID:           "polyapp",
		OverrideDomainID:   "",
		TaskID:             "polyapp",
		OverrideTaskID:     "",
		UXID:               "polyapp",
		SchemaID:           "polyapp",
		UserID:             user.FirestoreID,
		UserCache:          user,
	}, "", 'c')
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "could not authorize user for this request: "+err.Error())
	}

	err = install(c, true)
	if c.Response().Size == 0 && err == nil {
		return c.HTML(200, `<p>Update has already started and may have completed</p>`)
	}
	return err
}

func install(c echo.Context, force bool) (err error) {
	publicPath := common.GetPublicPath()
	folderPath := filepath.Clean(filepath.Join(publicPath, "..", "cmd", "defaultservice", "baseDocuments"))

	// validate with os.Stat to prevent some runtime panics with filepath.Walk
	info, err := os.Stat(folderPath)
	if err != nil {
		return fmt.Errorf("Couldn't stat the folderPath in Install: %w", err)
	}
	if !info.IsDir() {
		return errors.New("Can't walk the folder: " + folderPath + " : because it is not a directory")
	}

	err = fileCRUD.Init("")
	if err != nil {
		return fmt.Errorf("error initializing for fileCRUD: %w", err)
	}

	firestoreCtx, firestoreClient, err := firestoreCRUD.GetClient()
	if err != nil {
		return fmt.Errorf("firestoreCRUD.GetClient: %w", err)
	}

	// Immediately try to prevent other requests from being handled which perform this same operation since it's so long-running
	// and resource-intensive and the two different processes could also conflict with each other.
	if !force {
		doUpdate := false
		err = firestoreClient.RunTransaction(firestoreCtx, func(ctx context.Context, tx *firestore.Transaction) error {
			lastUpdateDoc := firestoreClient.Collection("lastUpdate").Doc("lastUpdate")
			doc, err := tx.Get(lastUpdateDoc)
			if err != nil && strings.Contains(err.Error(), "code = NotFound") {
				err = tx.Create(lastUpdateDoc, map[string]string{"lastUpdate": common.GetDeploymentID()})
				if err != nil {
					return fmt.Errorf("tx.Create: %w", err)
				}
				doUpdate = true
				return nil
			} else if err != nil {
				return fmt.Errorf("lastUpdate read: %w", err)
			}
			lastUpdateData, err := doc.DataAt("lastUpdate")
			if err != nil {
				return fmt.Errorf("lastUpdateDoc DataAt: %w", err)
			}
			deploymentID, ok := lastUpdateData.(string)
			if !ok {
				deploymentID = "LocalDeployment"
			}
			if common.GetDeploymentID() == deploymentID {
				// no updates need to be made since we are on the same deployment as the last time this was run.
				// Note: if you are always deploying inside a non-cloud environment you need to add an env. variable
				// and edit common.GetDeploymentID() to suit. TODO make this configurable with a config file.
				return nil
			}

			err = tx.Set(lastUpdateDoc, map[string]string{"lastUpdate": common.GetDeploymentID()})
			if err != nil {
				return fmt.Errorf("setting lastUpdate: %w", err)
			}
			doUpdate = true
			return nil
		}, firestore.MaxAttempts(1))
		if err != nil {
			return fmt.Errorf("RunTransaction for lastUpdate in Install: %w", err)
		}
		if !doUpdate {
			return c.HTML(200, "<p>Update already performed</p>")
		}
	}

	fmt.Println("Install: setting up this instance's bucket")

	// set up this instance's bucket
	_, _ = firestoreCRUD.GetStorageObjectMetadata(firestoreCRUD.GetBlobBucket(), "")

	// Only remove old data if we can get a replacement from that data from a different environment. Otherwise
	// we could be deleting the last copy of that data (in the case of blobs which are not backed up).
	doDeleteAndPullFromOldEnv := false
	copyFromBucket := common.GetLastBlobBucket()
	copyToBucket := firestoreCRUD.GetBlobBucket()
	switch copyFromBucket {
	case "":
		doDeleteAndPullFromOldEnv = false
	default:
		doDeleteAndPullFromOldEnv = true
		removeOldPolyappSites()
	}

	fmt.Println("Install: installPostgres")
	// Prepare Postgres
	err = installPostgres()
	if err != nil {
		return fmt.Errorf("installPostgres: %w", err)
	}

	fmt.Println("Install: reading all documents in the file system")
	data, role, schema, task, user, ux, bot, action, publicMap, CSS, err := fileCRUD.ReadAll(1)
	if err != nil {
		return fmt.Errorf("error reading all: " + err.Error())
	}
	combinedErrString := ""
	var combinedErrStringMux sync.Mutex
	var waitGroup sync.WaitGroup
	fmt.Println("Install: Roles")
	for _, r := range role {
		err := allDB.DeleteRole(r.FirestoreID)
		if err != nil && !strings.Contains(err.Error(), "code = NotFound") {
			combinedErrString += "; error deleting role " + r.FirestoreID + ": " + err.Error()
			continue
		}
		if r.Validate() == nil {
			err = allDB.CreateRole(r)
			if err != nil {
				combinedErrString += "; error installing role " + r.FirestoreID + ": " + err.Error()
				continue
			}
		}
		combinedErrString += ""
	}
	fmt.Println("Install: Schema")
	for _, s := range schema {
		err := allDB.DeleteSchema(s.FirestoreID)
		if err != nil && !strings.Contains(err.Error(), "code = NotFound") {
			combinedErrString += "; error updating Schema " + s.FirestoreID + ": " + err.Error()
			continue
		}
		if s.Validate() == nil {
			err = allDB.CreateSchema(s)
			if err != nil {
				combinedErrString += "; error installing Schema " + s.FirestoreID + ": " + err.Error()
				continue
			}
		}
		combinedErrString += ""
	}
	fmt.Println("Install: Tasks")
	for _, t := range task {
		err := allDB.DeleteTask(t.FirestoreID)
		if err != nil && !strings.Contains(err.Error(), "code = NotFound") {
			combinedErrString += "; error updating Task " + t.FirestoreID + ": " + err.Error()
			continue
		}
		if t.Validate() == nil {
			err = allDB.CreateTask(t)
			if err != nil {
				combinedErrString += "; error installing Task " + t.FirestoreID + ": " + err.Error()
				continue
			}
		}
		combinedErrString += ""
	}
	fmt.Println("Install: Users")
	for _, u := range user {
		// We don't want to delete users in the database.
		// Doing so would delete their passwords and leave them unable to log in.
		readUser, err := allDB.ReadUser(u.FirestoreID)
		if err != nil && strings.Contains(err.Error(), "code = NotFound") {
			err = allDB.CreateUser(u)
			if err != nil {
				combinedErrString += "; error installing user " + u.FirestoreID + ": " + err.Error()
				continue
			}
			if *u.Email == "support@polyapp.tech" {
				// when this user is created we must set its password else no one will be able to sign in
				err = allDB.UpdateUserPassword(u, "polyapp")
				if err != nil {
					combinedErrString += "; error updating support@polyapp.tech password for user " + u.FirestoreID + ": " + err.Error()
				}
			}
		} else {
			u.Roles = readUser.Roles
			err = allDB.UpdateUser(u)
			if err != nil && !strings.Contains(err.Error(), "code = NotFound") {
				combinedErrString += "; error updating user " + u.FirestoreID + ": " + err.Error()
				continue
			}
		}
	}
	fmt.Println("Install: UXs")
	for _, u := range ux {
		err := allDB.DeleteUX(u.FirestoreID)
		if err != nil && !strings.Contains(err.Error(), "code = NotFound") {
			combinedErrString += "; error updating UX " + u.FirestoreID + ": " + err.Error()
			continue
		}
		if u.Validate() == nil {
			err = allDB.CreateUX(u)
			if err != nil {
				combinedErrString += "; error installing UX " + u.FirestoreID + ": " + err.Error()
				continue
			}
		}
		combinedErrString += ""
	}
	fmt.Println("Install: Bots")
	for _, b := range bot {
		err := allDB.DeleteBot(b.FirestoreID)
		if err != nil && !strings.Contains(err.Error(), "code = NotFound") {
			combinedErrString += "; error updating Bot " + b.FirestoreID + ": " + err.Error()
			continue
		}
		if b.Validate() == nil {
			err = allDB.CreateBot(b)
			if err != nil {
				combinedErrString += "; error installing Bot " + b.FirestoreID + ": " + err.Error()
				continue
			}
		}
		combinedErrString += ""
	}
	fmt.Println("Install: Actions")
	for _, a := range action {
		err := allDB.DeleteAction(a.FirestoreID)
		if err != nil && !strings.Contains(err.Error(), "code = NotFound") {
			combinedErrString += "; error updating Action " + a.FirestoreID + ": " + err.Error()
			continue
		}
		if a.Validate() == nil {
			err = allDB.CreateAction(a)
			if err != nil {
				combinedErrString += "; error installing Action " + a.FirestoreID + ": " + err.Error()
				continue
			}
		}
		combinedErrString += ""
	}
	fmt.Println("Install: Public Maps")
	for _, pm := range publicMap {
		err := allDB.DeletePublicMap(pm.FirestoreID)
		if err != nil && !strings.Contains(err.Error(), "code = NotFound") {
			combinedErrString += "; error updating PublicMap " + pm.FirestoreID + ": " + err.Error()
			continue
		}
		if pm.Validate() == nil {
			err = allDB.CreatePublicMap(pm)
			if err != nil {
				combinedErrString += "; error installing PublicMap " + pm.FirestoreID + ": " + err.Error()
				continue
			}
		}
		combinedErrString += ""
	}
	fmt.Println("Install: CSS")
	for _, c := range CSS {
		err := allDB.DeleteCSS(c.FirestoreID)
		if err != nil && !strings.Contains(err.Error(), "code = NotFound") {
			combinedErrString += "; error updating CSS " + c.FirestoreID + ": " + err.Error()
			continue
		}
		if c.Validate() == nil {
			err = allDB.CreateCSS(c)
			if err != nil {
				combinedErrString += "; error installing CSS " + c.FirestoreID + ": " + err.Error()
				continue
			}
		}
		combinedErrString += ""
	}
	fmt.Println("Install: Datas")
	// Datas are stored in parallel because although allDB.Create may have dependencies upon Schema or Task or something else,
	// it should never have dependencies on other Data.
	if doDeleteAndPullFromOldEnv {
		for _, d := range data {
			waitGroup.Add(1)
			go func(d *common.Data) {
				defer waitGroup.Done()
				deleteDataErr := ""
				err := allDB.DeleteData(d.FirestoreID)
				if err != nil {
					deleteDataErr = err.Error()
				}
				if d.Validate() != nil {
					return
				}
				err = allDB.CreateData(d)
				if err != nil {
					combinedErrStringMux.Lock()
					combinedErrString += "; error creating data " + d.FirestoreID + ": " + err.Error() + "; deleteDataErr: " + deleteDataErr + ";"
					combinedErrStringMux.Unlock()
					return
				}
				objectNames, err := firestoreCRUD.ListStorageObjects(copyFromBucket, firestoreCRUD.GetStorageObjectName(common.CollectionData, d.FirestoreID, ""))
				if err != nil {
					combinedErrStringMux.Lock()
					combinedErrString += "; ListStorageObjects " + d.FirestoreID + ": " + err.Error()
					combinedErrStringMux.Unlock()
					return
				}
				for _, objectName := range objectNames {
					oBytes, err := firestoreCRUD.GetStorageObject(copyFromBucket, objectName)
					if err != nil {
						combinedErrStringMux.Lock()
						combinedErrString += "; GetStorageObject " + objectName + " for d.FirestoreID " + d.FirestoreID + ": " + err.Error()
						combinedErrStringMux.Unlock()
						continue
					}
					err = firestoreCRUD.SetStorageObject(copyToBucket, bytes.NewReader(oBytes), objectName, map[string]string{})
					if err != nil {
						combinedErrStringMux.Lock()
						combinedErrString += "; SetStorageObject " + objectName + " for d.FirestoreID " + d.FirestoreID + ": " + err.Error()
						combinedErrStringMux.Unlock()
						continue
					}
				}
			}(d)
		}
	} else {
		// Sometimes the Data needs to be updated, like when we need to roll back errors which broke an installation.
		for _, d := range data {
			waitGroup.Add(1)
			go func(d *common.Data) {
				defer waitGroup.Done()
				if d.Validate() != nil {
					return
				}
				err := allDB.UpdateData(d)
				if err != nil && strings.Contains(err.Error(), "code = NotFound") {
					err = allDB.CreateData(d)
					if err != nil {
						combinedErrStringMux.Lock()
						combinedErrString += "; ! doDeleteAndPullFromOldEnv error creating data " + d.FirestoreID + ": " + err.Error() + ";"
						combinedErrStringMux.Unlock()
					}
				} else if err != nil {
					combinedErrStringMux.Lock()
					combinedErrString += "; ! doDeleteAndPullFromOldEnv error updating data " + d.FirestoreID + ": " + err.Error() + ";"
					combinedErrStringMux.Unlock()
					return
				}
			}(d)
		}
	}
	waitGroup.Wait()

	err = installDashboards()
	if err != nil {
		combinedErrString += "installDashboards: " + err.Error()
	}

	err = installThemes()
	if err != nil {
		combinedErrString += "installThemes: " + err.Error()
	}

	fmt.Println("Install: Setting error")
	if combinedErrString != "" {
		// Since we are erroring, we need to be able to retry the update.
		lastUpdateDoc := firestoreClient.Collection("lastUpdate").Doc("lastUpdate")
		_, _ = lastUpdateDoc.Set(firestoreCtx, map[string]string{"lastUpdate": "NotADeploymentID"})
		return errors.New(combinedErrString)
	}

	return c.HTML(200, "<p>Update finished</p>")
}

func installThemes() error {
	var combinedErrString string
	var combinedErrStringMux sync.Mutex
	var err error
	var waitGroup sync.WaitGroup
	q := allDB.Query{}
	q.Init("polyapp", "TaskSchemaUX", "KxZRoxrRpyECTbCudVfXKnmHB", common.CollectionData)
	q.AddEquals(common.PolyappSchemaID, "KxZRoxrRpyECTbCudVfXKnmHB")
	iter, err := q.QueryRead()
	if err != nil {
		return fmt.Errorf("q.QueryRead: %w", err)
	}
	allDashboards := []*common.Data{}
	for {
		queryable, err := iter.Next()
		if err == common.IterDone {
			break
		}
		if err != nil {
			return fmt.Errorf("iter.Next: %w", err)
		}
		allDashboards = append(allDashboards, queryable.(*common.Data))
	}
	for i := range allDashboards {
		waitGroup.Add(1)
		go func(d *common.Data) {
			defer waitGroup.Done()
			d.B[common.AddFieldPrefix("polyapp", "TaskSchemaUX", "KxZRoxrRpyECTbCudVfXKnmHB", "Set As My Theme")] = common.Bool(false)
			err = actions.ActionCreateTheme(d, &common.POSTRequest{
				IndustryID: "polyapp",
				DomainID:   "TaskSchemaUX",
				TaskID:     "",
				UXID:       "",
				SchemaID:   "KxZRoxrRpyECTbCudVfXKnmHB",
				DataID:     d.FirestoreID,
				UserID:     "polyappAdminUser",
			}, &common.POSTResponse{
				NewURL:         "",
				BrowserActions: nil,
				ModDOMs:        []common.ModDOM{},
			})
			if err != nil {
				combinedErrStringMux.Lock()
				combinedErrString += fmt.Sprintf("actions.ActionCreateTheme: %v", err.Error())
				combinedErrStringMux.Unlock()
			}
		}(allDashboards[i])
	}
	waitGroup.Wait()
	if combinedErrString != "" {
		return errors.New(combinedErrString)
	}
	return nil
}

func installDashboards() error {
	var combinedErrString string
	var combinedErrStringMux sync.Mutex
	var err error
	var waitGroup sync.WaitGroup
	q := allDB.Query{}
	q.Init("polyapp", "Reporting", "ZhcLBvwSBdjcpFVApHUyAygqv", common.CollectionData)
	q.AddEquals(common.PolyappSchemaID, "ZhcLBvwSBdjcpFVApHUyAygqv")
	iter, err := q.QueryRead()
	if err != nil {
		return fmt.Errorf("q.QueryRead: %w", err)
	}
	allDashboards := []*common.Data{}
	for {
		queryable, err := iter.Next()
		if err == common.IterDone {
			break
		}
		if err != nil {
			return fmt.Errorf("iter.Next: %w", err)
		}
		allDashboards = append(allDashboards, queryable.(*common.Data))
	}
	for i := range allDashboards {
		waitGroup.Add(1)
		go func(d *common.Data) {
			defer waitGroup.Done()
			d.B[common.AddFieldPrefix("polyapp", "Reporting", "ZhcLBvwSBdjcpFVApHUyAygqv", "Set As Home Page")] = common.Bool(false)
			err = actions.ActionCreateDashboard(d, &common.POSTRequest{
				IndustryID: "polyapp",
				DomainID:   "Reporting",
				TaskID:     "",
				UXID:       "",
				SchemaID:   "ZhcLBvwSBdjcpFVApHUyAygqv",
				DataID:     d.FirestoreID,
				UserID:     "polyappAdminUser",
			}, &common.POSTResponse{
				NewURL:         "",
				BrowserActions: nil,
				ModDOMs:        []common.ModDOM{},
			})
			if err != nil {
				combinedErrStringMux.Lock()
				combinedErrString += fmt.Sprintf("actions.ActionCreateDashboard: %v", err.Error())
				combinedErrStringMux.Unlock()
			}
		}(allDashboards[i])
	}
	waitGroup.Wait()
	if combinedErrString != "" {
		return errors.New(combinedErrString)
	}
	return nil
}

func installPostgres() error {
	var err error
	err = postgresCRUD.CreateTableIfNotExists(&common.Task{
		FirestoreID:               "CreateTableIfNotExistsTask",
		IndustryID:                "CreateTableIfNotExists",
		DomainID:                  "CreateTableIfNotExists",
		Name:                      "CreateTableIfNotExists",
		HelpText:                  "CreateTableIfNotExists",
		TaskGoals:                 map[string]string{"CreateTableIfNotExists": "CreateTableIfNotExists"},
		FieldSecurity:             map[string]*common.FieldSecurityOptions{},
		BotsTriggeredAtLoad:       []string{"CreateTableIfNotExists"},
		BotsTriggeredContinuously: []string{"CreateTableIfNotExists"},
		BotsTriggeredAtDone:       []string{"CreateTableIfNotExists"},
		BotStaticData:             map[string]string{"CreateTableIfNotExists": "CreateTableIfNotExists"},
		IsPublic:                  false,
		Deprecated:                common.Bool(false),
	})
	if err != nil {
		return fmt.Errorf("postgresCRUD.CreateTableIfNotExists Task: %w", err)
	}

	err = postgresCRUD.CreateTableIfNotExists(&common.Schema{
		FirestoreID:  "CreateTableIfNotExistsSchema",
		NextSchemaID: common.String("CreateTableIfNotExists"),
		Name:         common.String("CreateTableIfNotExists"),
		IndustryID:   "CreateTableIfNotExists",
		DomainID:     "CreateTableIfNotExists",
		SchemaID:     "CreateTableIfNotExists",
		DataTypes:    map[string]string{"CreateTableIfNotExists": "CreateTableIfNotExists"},
		DataHelpText: map[string]string{"CreateTableIfNotExists": "CreateTableIfNotExists"},
		DataKeys:     []string{"CreateTableIfNotExists"},
		IsPublic:     false,
		Deprecated:   common.Bool(false),
	})
	if err != nil {
		return fmt.Errorf("postgresCRUD.CreateTableIfNotExists Schema: %w", err)
	}

	err = postgresCRUD.CreateTableIfNotExists(&common.UX{
		FirestoreID:     "CreateTableIfNotExistsUX",
		IndustryID:      "CreateTableIfNotExists",
		DomainID:        "CreateTableIfNotExists",
		SchemaID:        "CreateTableIfNotExists",
		HTML:            common.String("CreateTableIfNotExists"),
		Navbar:          common.String("CreateTableIfNotExists"),
		Title:           common.String("CreateTableIfNotExists"),
		MetaDescription: common.String("CreateTableIfNotExists"),
		MetaKeywords:    common.String("CreateTableIfNotExists"),
		MetaAuthor:      common.String("CreateTableIfNotExists"),
		HeadTag:         common.String("CreateTableIfNotExists"),
		IconPath:        common.String("CreateTableIfNotExists"),
		CSSPath:         common.String("CreateTableIfNotExists"),
		SelectFields:    map[string]string{"CreateTableIfNotExists": "CreateTableIfNotExists"},
		IsPublic:        false,
		FullAuthPage:    common.Bool(false),
		Deprecated:      common.Bool(false),
	})
	if err != nil {
		return fmt.Errorf("postgresCRUD.CreateTableIfNotExists UX: %w", err)
	}

	err = postgresCRUD.CreateTableIfNotExists(&common.Bot{
		FirestoreID: "CreateTableIfNotExistsBot",
		Name:        "CreateTableIfNotExists",
		HelpText:    "CreateTableIfNotExists",
		ActionIDs:   []string{"CreateTableIfNotExists"},
		Deprecated:  common.Bool(false),
	})
	if err != nil {
		return fmt.Errorf("postgresCRUD.CreateTableIfNotExists Bot: %w", err)
	}

	err = postgresCRUD.CreateTableIfNotExists(&common.Action{
		FirestoreID: "CreateTableIfNotExistsAction",
		Name:        "CreateTableIfNotExists",
		HelpText:    "CreateTableIfNotExists",
		Deprecated:  common.Bool(false),
	})
	if err != nil {
		return fmt.Errorf("postgresCRUD.CreateTableIfNotExists Action: %w", err)
	}

	err = postgresCRUD.CreateTableIfNotExists(&common.User{
		FirestoreID:         "CreateTableIfNotExistsUser",
		UID:                 common.String("CreateTableIfNotExists"),
		FullName:            common.String("CreateTableIfNotExists"),
		PhotoURL:            common.String("CreateTableIfNotExists"),
		EmailVerified:       common.Bool(false),
		PhoneNumber:         common.String("CreateTableIfNotExists"),
		Email:               common.String("CreateTableIfNotExists"),
		Roles:               []string{"CreateTableIfNotExists"},
		BotsTriggeredAtLoad: []string{"CreateTableIfNotExists"},
		BotsTriggeredAtDone: []string{"CreateTableIfNotExists"},
		HomeTask:            "CreateTableIfNotExists",
		HomeUX:              "CreateTableIfNotExists",
		HomeSchema:          "CreateTableIfNotExists",
		HomeData:            "CreateTableIfNotExists",
		Deprecated:          common.Bool(false),
	})
	if err != nil {
		return fmt.Errorf("postgresCRUD.CreateTableIfNotExists User: %w", err)
	}

	err = postgresCRUD.CreateTableIfNotExists(&common.Role{
		FirestoreID: "CreateTableIfNotExistsRole",
		Name:        "CreateTableIfNotExists",
		Access: []common.Access{
			{
				Industry: "polyapp",
				Domain:   "CreateTableIfNotExists",
				Schema:   "CreateTableIfNotExists",
				Create:   false,
				Read:     false,
				Update:   false,
				Delete:   false,
				KeyValue: map[string]string{"CreateTableIfNotExists": "CreateTableIfNotExists"},
			},
		},
		Deprecated: common.Bool(false),
	})
	if err != nil {
		return fmt.Errorf("postgresCRUD.CreateTableIfNotExists Role: %w", err)
	}

	return nil
}

func removeOldPolyappSites() {
	pmsToDelete := make([]string, 0)
	tasksToDelete := make([]string, 0)
	uxsToDelete := make([]string, 0)
	schemasToDelete := make([]string, 0)
	ctx, client, _ := firestoreCRUD.GetClient()
	allDocs := client.Collection(common.CollectionPublicMap).Documents(ctx)
	for {
		doc, err := allDocs.Next()
		if err != nil && err == iterator.Done {
			break
		}
		if err != nil {
			common.LogError(fmt.Errorf("allDocs.Next: %v\n", err.Error()))
			continue
		}
		pm := common.PublicMap{}
		pm.Init(doc.Data())
		if strings.HasPrefix(pm.HostAndPath, "suggestions.polyapp.tech") || strings.HasPrefix(pm.HostAndPath, "polyapp.tech") || strings.HasPrefix(pm.HostAndPath, "polyappbuilder.com") || strings.HasPrefix(pm.HostAndPath, "builda.page") {
			pmsToDelete = append(pmsToDelete, doc.Ref.ID)
			if pm.TaskID != nil && *pm.TaskID != "" {
				tasksToDelete = append(tasksToDelete, *pm.TaskID)
			}
			if pm.SchemaID != nil && *pm.SchemaID != "" {
				schemasToDelete = append(schemasToDelete, *pm.SchemaID)
			}
			if pm.UXID != nil && *pm.UXID != "" {
				uxsToDelete = append(uxsToDelete, *pm.UXID)
			}
		}
	}
	var err error
	for _, v := range pmsToDelete {
		err = allDB.DeletePublicMap(v)
		if err != nil {
			common.LogError(fmt.Errorf("allDB.DeletePublicMap %v: %v", v, err.Error()))
		}
	}
	for _, v := range tasksToDelete {
		err = allDB.DeleteTask(v)
		if err != nil {
			common.LogError(fmt.Errorf("allDB.DeleteTask %v: %v", v, err.Error()))
		}
	}
	for _, v := range schemasToDelete {
		err = allDB.DeleteSchema(v)
		if err != nil {
			common.LogError(fmt.Errorf("allDB.DeleteSchema %v: %v", v, err.Error()))
		}
	}
	for _, v := range uxsToDelete {
		err = allDB.DeleteUX(v)
		if err != nil {
			common.LogError(fmt.Errorf("allDB.DeleteUX %v: %v", v, err.Error()))
		}
	}
}

func createNewUser(firebaseCtx context.Context, firebaseClient *auth.Client, userRecord *auth.UserRecord, tokenUser *common.User) error {
	// We're going to throw out any ID passed up from the client because we can't trust that ID.
	tokenUser.FirestoreID = common.GetRandString(25)
	tokenUserData := common.Data{
		FirestoreID: common.TemplateUserDataID,
	}
	err := allDB.Read(&tokenUserData)
	if err != nil {
		return fmt.Errorf("allDB.Read(%v): %w", common.TemplateUserDataID, err)
	}
	tokenUserData.FirestoreID = common.GetRandString(25)
	tokenUserData.S[common.AddFieldPrefix("polyapp", "User", "TkfMDeIjfSaXCMSByYRsEbubp", "Email")] = common.String(userRecord.Email)
	tokenUserData.S[common.AddFieldPrefix("polyapp", "User", "TkfMDeIjfSaXCMSByYRsEbubp", "Full Name")] = common.String(userRecord.DisplayName)
	tokenUserData.S[common.AddFieldPrefix("polyapp", "User", "TkfMDeIjfSaXCMSByYRsEbubp", "UID")] = common.String(userRecord.UID)
	tokenUserData.S[common.AddFieldPrefix("polyapp", "User", "TkfMDeIjfSaXCMSByYRsEbubp", "Photo URL")] = common.String(userRecord.PhotoURL)
	tokenUserData.S[common.AddFieldPrefix("polyapp", "User", "TkfMDeIjfSaXCMSByYRsEbubp", "Phone Number")] = common.String(userRecord.PhoneNumber)
	tokenUserData.B[common.AddFieldPrefix("polyapp", "User", "TkfMDeIjfSaXCMSByYRsEbubp", "Email Verified")] = common.Bool(userRecord.EmailVerified)
	tokenUserData.S[common.AddFieldPrefix("polyapp", "User", "TkfMDeIjfSaXCMSByYRsEbubp", "User ID")] = common.String(tokenUser.FirestoreID)
	err = allDB.CreateData(&tokenUserData)
	if err != nil {
		return fmt.Errorf("allDB.CreateData (%v): %w", tokenUserData.FirestoreID, err)
	}
	// We must create a User in the database which is linked to the "Edit User" Data.
	tokenUserTemplate, err := allDB.ReadUser(common.TemplateUserUserID)
	if err != nil {
		return fmt.Errorf("allDB.ReadUser(%v): %w", common.TemplateUserUserID, err)
	}
	tokenUser.BotsTriggeredAtDone = tokenUserTemplate.BotsTriggeredAtDone
	tokenUser.BotsTriggeredAtLoad = tokenUserTemplate.BotsTriggeredAtLoad
	tokenUser.Roles = tokenUserTemplate.Roles
	tokenUser.HomeData = tokenUserTemplate.HomeData
	tokenUser.HomeSchema = tokenUserTemplate.HomeSchema
	tokenUser.HomeTask = tokenUserTemplate.HomeTask
	tokenUser.HomeUX = tokenUserTemplate.HomeUX
	tokenUser.UID = common.String(userRecord.UID)
	// not copying over "Deprecated"
	err = allDB.CreateUser(tokenUser)
	if err != nil {
		return fmt.Errorf("allDB.CreateUser ("+tokenUser.FirestoreID+"): %w", err)
	}
	return nil
}
