package elasticsearchCRUD

import (
	"testing"

	"cloud.google.com/go/firestore"
)

func TestArrayOfRefsAsJSON(t *testing.T) {
	refs := make([]*firestore.DocumentRef, 0)
	arrayString := ArrayOfRefsAsJSON(refs)
	if arrayString != "[]" {
		t.Error("Didn't construct the empty string properly")
	}

	testRef1 := &firestore.DocumentRef{ID: "testRef1"}
	refs = append(refs, testRef1)
	arrayString = ArrayOfRefsAsJSON(refs)
	if arrayString != `["testRef1"]` {
		t.Error("Did not properly construct an array of refs of length 1 - constructed: " + arrayString)
	}

	testRef2 := &firestore.DocumentRef{ID: "testRef2"}
	refs = append(refs, testRef2)
	arrayString = ArrayOfRefsAsJSON(refs)
	if arrayString != `["testRef1", "testRef2"]` {
		t.Error("Did not properly construct an array of refs of length 2 - constructed: " + arrayString)
	}
}

// TestIndex verifies that a good response is received by the server - it does NOT verify that elasticsearch actually did anything
func TestIndex(t *testing.T) {
	client, err := GetClient()
	if err != nil {
		t.Fatalf("Couldn't get an elastic client")
	}
	_, err = index(client, "jsdk", []byte(`{"testKey": "testData"}`), "test")
	if err != nil {
		t.Fatal("Failed to index document because: " + err.Error())
	}
	// Print the response status and indexed document version.
	//common.LogError(fmt.Errorf("%s; version=%d", r["result"], int(r["_version"].(float64)))

	_, err = index(client, "jsdk", []byte(`{"testKey": "testData", "testKey2": "testData2"}`), "test")
	if err != nil {
		t.Fatal("Failed to index document because: " + err.Error())
	}
	// Print the response status and indexed document version.
	//common.LogError(fmt.Errorf("%s; version=%d", r["result"], int(r["_version"].(float64)))

	_, err = index(client, "jsdk", []byte(`{"testKey": "testData", "testKey2": "testData2"}`), "test")
	if err != nil {
		t.Fatal("Failed to index document because: " + err.Error())
	}
}
