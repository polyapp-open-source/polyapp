package elasticsearchCRUD

import (
	"testing"

	"github.com/elastic/go-elasticsearch/v8"
)

// Test_update makes sure the request is being made successfully but does not verify anything else.
func Test_update(t *testing.T) {
	type args struct {
		client *elasticsearch.Client
		esID   string
		update map[string]interface{}
		index  string
	}
	client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}
	tests := []struct {
		name    string
		args    args
		wantR   map[string]interface{}
		wantErr bool
	}{
		{
			name: "nil client",
			args: args{
				client: nil,
				esID:   "Test_update",
				update: map[string]interface{}{"something": "hi"},
				index:  "user",
			},
			wantR:   nil,
			wantErr: true,
		},
		{
			name: "nil esID",
			args: args{
				client: client,
				esID:   "",
				update: map[string]interface{}{"something": "hi"},
				index:  "user",
			},
			wantR:   nil,
			wantErr: true,
		},
		{
			name: "nil update",
			args: args{
				client: client,
				esID:   "Test_update",
				update: nil,
				index:  "user",
			},
			wantR:   nil,
			wantErr: true,
		},
		{
			name: "nil index",
			args: args{
				client: client,
				esID:   "Test_update",
				update: map[string]interface{}{"something": "hi"},
				index:  "",
			},
			wantR:   nil,
			wantErr: true,
		},
		{
			name: "simple update",
			args: args{
				client: client,
				esID:   "Test_update",
				update: map[string]interface{}{"testKey": "Test_update"},
				index:  "test",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := update(tt.args.client, tt.args.esID, tt.args.update, tt.args.index)
			if (err != nil) != tt.wantErr {
				t.Errorf("update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
