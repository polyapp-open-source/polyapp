// Package elasticsearchCRUD provides APIs for storing and retrieving objects from Elasticsearch
// and searching among those objects.
package elasticsearchCRUD
