package common

import (
	"errors"
	"fmt"
	"net/url"
	"strconv"
	"strings"
)

// POSTRequest contains all information sent from the client.
// If you're making changes here you may also need to make changes in type 'GETRequest'.
//
// NOTE: If you modify this structure, you MUST consider the security implications for public access. See PublicMapSitePOST().
type POSTRequest struct {
	// MessageID identifies this message. This ensures 1 message gets 1 response.
	MessageID string `json:"MessageID"`
	// IndustryID is part of the context of this request.
	IndustryID string `json:"IndustryID"`
	// OverrideIndustryID is used if the URL includes 'industry' parameter.
	OverrideIndustryID string `json:"OverrideIndustryID"`
	// DomainID is part of the context of this request.
	DomainID string `json:"DomainID"`
	// OverrideDomainID is used if the URL includes 'domain' parameter.
	OverrideDomainID string `json:"OverrideDomainID"`
	// TaskID of the Task in the database is used to tie the User, Data, Schema, and UX together into a single package
	TaskID string `json:"TaskID"`
	// TaskCache is a cache of the Task when it was last read. This is not necessarily updated if the Task document is updated
	// during the request.
	TaskCache *Task
	// OverrideTaskID is used if the URL includes 'task' parameter.
	OverrideTaskID string `json:"OverrideTaskID"`
	// Data is the marshalled information from the Task. Data is in a map of Document ID -> Data in that Document.
	// Within Data in that Document, there is a map from ElementID -> data like booleans, strings, numbers, etc.
	//
	// You can create this data structure with func DataToPOSTData
	Data map[string]map[string]interface{} `json:"Data"`
	// UXID is the ID of the UI data in the database.
	UXID string `json:"UXID"`
	// UXCache is a cache of the UX when it was last read. This is not necessarily updated if the UX document is updated
	// during the request.
	UXCache *UX
	// SchemaID is the ID of the Data's schema in the database.
	SchemaID string `json:"SchemaID"`
	// SchemaCache is a cache of the Schema when it was last read. This is not necessarily updated if the Schema document is updated
	// during the request.
	SchemaCache *Schema
	// DataID of the data instance in the database.
	DataID string `json:"DataID"`
	// UserID of the user making the change. All access checks and the like are performed with this user.
	UserID string `json:"UserID"`
	// UserCache is a cache of the User when it was last read. This not necessarily updated if the User document is updated
	// during the request.
	UserCache *User
	// PublicMapOwningUserID is the ID of the User which owns (created) this URL.
	PublicMapOwningUser User
	// RoleID for a Role. This is NOT required - usually the User document's Role is considered instead.
	RoleID string `json:"RoleID"`
	// ModifyID for an object which is not
	ModifyID string `json:"ModifyID"`
	// PublicPath is used for public / semipublic pages to identify the path. This is used to avoid overloading TaskID.
	PublicPath string `json:"PublicPath"`
	// RecaptchaResponse is the response from the Recaptcha API or an empty string.
	RecaptchaResponse string `json:"RecaptchaResponse"`
	// FinishURL is where this page navigates to after this Task is done. Since the FinishURL could contain a FinishURL,
	// there could be a chain of pages you'll be going back to after visiting this one.
	FinishURL string
	// IsDone is set programatically and if true indicates that Data contained the Done boolean.
	IsDone bool
	// WantDocuments are arrays of FirestoreIDs of documents the client wants.
	WantDocuments WantDocuments `json:"WantDocuments"`
}

// GetIndustry considers any overrides to the industry.
func (p POSTRequest) GetIndustry() string {
	if p.OverrideIndustryID != "" && p.OverrideIndustryID != "polyappNone" {
		return p.OverrideIndustryID
	}
	return p.IndustryID
}

// GetDomain considers any overrides to the domain.
func (p POSTRequest) GetDomain() string {
	if p.OverrideDomainID != "" && p.OverrideDomainID != "polyappNone" {
		return p.OverrideDomainID
	}
	return p.DomainID
}

// GetTask considers any overrides to the task.
func (p POSTRequest) GetTask() string {
	if p.OverrideTaskID != "" && p.OverrideTaskID != "polyappNone" {
		return p.OverrideTaskID
	}
	return p.TaskID
}

// Validate ensures the structure of POSTRequest is valid
func (p *POSTRequest) Validate() error {
	if p.MessageID == "" {
		return errors.New("MessageID is empty")
	}
	if p.IndustryID == "" {
		return errors.New("IndustryID is empty")
	}
	if p.DomainID == "" {
		return errors.New("DomainID is empty")
	}
	if p.TaskID == "" {
		return errors.New("TaskID is empty")
	}
	if p.UXID == "" {
		return errors.New("UXID is empty")
	}
	if p.SchemaID == "" {
		return errors.New("SchemaID is empty")
	}
	if p.DataID == "" {
		return errors.New("DataID is empty")
	}
	if p.Data == nil {
		return errors.New("data is empty")
	}
	if p.UserID == "" {
		return errors.New("UserID is empty")
	}
	if p.SchemaCache != nil || p.TaskCache != nil || p.UserCache != nil || p.UXCache != nil {
		return errors.New("schema, task, ux, or user cache were not nil; this means the client set them (security problem) or they were set prior to calling Validate() (which is not allowed)")
	}
	return nil
}

// WantDocuments are arrays of FirestoreIDs of documents the client wants.
type WantDocuments struct {
	Data   []string
	Role   []string
	Schema []string
	Task   []string
	User   []string
	UX     []string
}

// ModDOM contains everything the client needs to modify the DOM with 'insertAdjacentHTML' function call.
type ModDOM struct {
	// DeleteSelector is all of the elements you're trying to remove
	DeleteSelector string
	// InsertSelector is the ID of the element we're inserting near
	InsertSelector string
	// Action is the position parameter in 'insertAdjacentHTML'. Options: beforebegin afterbegin beforeend afterend
	// Action also accepts "wrap" which calls jQuery's wrap() function.
	Action string
	// HTML is what we are putting in to the DOM. It's the text parameter in 'insertAdjacentHTML'
	HTML string
	// AddClassSelector is optional and can select more than one element. If populated, AddClasses should be populated.
	AddClassSelector string
	// AddClasses is optional. It's a list of CSS classes to add to the elements found by AddClassSelector.
	AddClasses []string
	// RemoveClassSelector is optional and can select more than one element. If populated, RemoveClasses should be populated.
	RemoveClassSelector string
	// RemoveClasses is optional. It's a list of CSS classes to remove from the elements found by RemoveClassSelector.
	RemoveClasses []string
}

// Validate ensures the structure of ModDOM is valid
func (mod ModDOM) Validate() error {
	if mod.Action != "" && mod.Action != "beforebegin" && mod.Action != "afterbegin" && mod.Action != "beforeend" && mod.Action != "afterend" && mod.Action != "wrap" {
		return errors.New("action name: " + mod.Action + " is invalid")
	}
	return nil
}

// BrowserAction initiates some Javascript which performs things which aren't doable with ModDOMs,
// like opening a new browser tab.
type BrowserAction struct {
	Name string // Name of the action to initiate. One of: BrowserActionOpenNewTab, ...
	Data string // Data which modifies how the browser action works. At some point this should be a map[string]interface{} instead.
}

// Validate ensures the values in BrowserAction are valid
func (b BrowserAction) Validate() error {
	switch b.Name {
	case BrowserActionOpenNewTab:
		if b.Data == "" {
			return errors.New("BrowserActionOpenNewTab requires Data to be set to a URL to open")
		}
	default:
		return errors.New("Unknown Browser Action Name")
	}
	return nil
}

var (
	BrowserActionOpenNewTab = "Open New Tab"
)

// POSTResponse applies some changes to the DOM (ModDOMs) in order.
type POSTResponse struct {
	// MessageID is a randomly generated string. It's identical to MessageID on the incoming message.
	MessageID string
	// NewURL is optional. Set this to set the address bar.
	NewURL string
	// BrowserActions is optional. Setting this initiates some Javascript which performs things which aren't doable with ModDOMs,
	// like opening a new browser tab.
	BrowserActions []BrowserAction
	// DataUpdates is a subset of the marshalled information from the Task. Data is in a map of Document ID -> Data in that Document.
	// Within Data in that Document, there is a map from ElementID -> data like booleans, strings, numbers, etc.
	//
	// You can create this data structure with func DataToPOSTData
	DataUpdates map[string]map[string]interface{}
	// DataDocuments can be cached on the client in their local database.
	DataDocuments []*Data
	// RoleDocuments can be cached on the client in their local database.
	RoleDocuments []*Role
	// SchemaDocuments can be cached on the client in their local database.
	SchemaDocuments []*Schema
	// TaskDocuments can be cached on the client in their local database.
	TaskDocuments []*Task
	// UserDocuments can be cached on the client in their local database.
	UserDocuments []*User
	// UXDocuments can be cached on the client in their local database.
	UXDocuments []*UX
	// ModDOMs are things we're putting in to the DOM.
	ModDOMs []ModDOM
}

// Validate ensures the structure of POSTResponse is valid
func (r POSTResponse) Validate() error {
	if r.MessageID == "" {
		return errors.New("POSTResponse didn't have an ID")
	}
	if r.ModDOMs == nil {
		return errors.New("POSTResponse didn't have initialized ModDOMs")
	}
	for i, a := range r.ModDOMs {
		err := a.Validate()
		if err != nil {
			return fmt.Errorf("invalid ModDOM at index %v: %w", i, err)
		}
	}
	return nil
}

// Init initializes the common.POSTResponse structure and removes any of its current contents.
func (r *POSTResponse) Init(request POSTRequest) {
	r.MessageID = request.MessageID
	r.ModDOMs = []ModDOM{
		{
			DeleteSelector: "#polyappJSPOSTError",
		},
	}
	r.DataUpdates = make(map[string]map[string]interface{})
	r.DataDocuments = []*Data{}
	r.RoleDocuments = []*Role{}
	r.SchemaDocuments = []*Schema{}
	r.TaskDocuments = []*Task{}
	r.UserDocuments = []*User{}
	r.UXDocuments = []*UX{}
	r.BrowserActions = []BrowserAction{}
}

// GETRequest is an incoming GET request.
// If you make changes here you may also need to make changes in type 'POSTRequest'.
type GETRequest struct {
	// IndustryID is part of the context of this request.
	IndustryID string `json:"IndustryID"`
	// OverrideIndustryID is used if the URL includes 'industry' parameter.
	OverrideIndustryID string `json:"OverrideIndustryID"`
	// DomainID is part of the context of this request.
	DomainID string `json:"DomainID"`
	// OverrideDomainID is used if the URL includes 'domain' parameter.
	OverrideDomainID string `json:"OverrideDomainID"`
	// TaskID of the Task in the database is used to tie the User, Data, Schema, and UX together into a single package
	TaskID string `json:"TaskID"`
	// OverrideTaskID is used if the URL includes 'task' parameter.
	OverrideTaskID string `json:"OverrideTaskID"`
	// UXID is the ID of the UI data in the database.
	UXID string `json:"UXID"`
	// SchemaID is the ID of the Data's schema in the database.
	SchemaID string `json:"SchemaID"`
	// DataID of the data instance in the database.
	DataID string `json:"DataID"`
	// UserID of the user making the change. All access checks and the like are performed with this user.
	// Do not confuse the UserID with the UID. The UID is from Firebase; the UserID from Polyapp's Firestore DB.
	// You could have a UID of xyz and a UserID of hyk if the User were to be a Bot authorized under a human's uid.
	UserID string `json:"UserID"`
	// UserCache is a cache of the User when the request began and it was authenticated.
	// This may not change if the User is updated during the request.
	UserCache *User
	// RoleID is the role for a particular user.
	RoleID string `json:"RoleID"`
	// ModifyID is the ID of an object in the DB you wish to modify. This is used exclusively on the server
	// and is used in tasks which are modifying objects like type UX or type Schema or if you want to refresh a report.
	ModifyID string `json:"ModifyID"`
	// AltShell if true uses lightshell.index instead of index.html to wrap the response from ux.HTML.
	AltShell string `json:"AltShell"`
	// FinishURL is where this page navigates to after this Task is done. Since the FinishURL could contain a FinishURL,
	// there could be a chain of pages you'll be going back to after visiting this one.
	//
	// Should be encoded with url.PathEscape prior to being used in a URL. Should be decoded with url.PathUnescape prior
	// to being stored into the FinishURL variable.
	FinishURL string
	// PublicPath is used for public / semipublic pages to identify the path. This is used to avoid overloading TaskID.
	PublicPath string `json:"PublicPath"`
}

// Validate ensures the structure of GETRequest is valid
func (request GETRequest) Validate() error {
	if request.IndustryID == "" {
		return errors.New("IndustryID is empty")
	}
	if request.DomainID == "" {
		return errors.New("DomainID is empty")
	}
	if request.TaskID == "" {
		return errors.New("TaskID is empty")
	}
	if request.UXID == "" {
		return errors.New("UXID is empty")
	}
	if request.SchemaID == "" {
		return errors.New("SchemaID is empty")
	}
	if request.DataID == "" {
		return errors.New("DataID is empty")
	}
	if request.UserID == "" {
		return errors.New("UserID is empty")
	}
	return nil
}

// GetIndustry considers any overrides to the industry.
func (request GETRequest) GetIndustry() string {
	if request.OverrideIndustryID != "" && request.OverrideIndustryID != "polyappNone" {
		return request.OverrideIndustryID
	}
	return request.IndustryID
}

// GetDomain considers any overrides to the domain.
func (request GETRequest) GetDomain() string {
	if request.OverrideDomainID != "" && request.OverrideDomainID != "polyappNone" {
		return request.OverrideDomainID
	}
	return request.DomainID
}

// GetTask considers any overrides to the task.
func (request GETRequest) GetTask() string {
	if request.OverrideTaskID != "" && request.OverrideTaskID != "polyappNone" {
		return request.OverrideTaskID
	}
	return request.TaskID
}

// GETResponse can be used to create a reply to an incoming GET request.
type GETResponse struct {
	// All of the HTML in the page you're responding with.
	HTML string
	// RedirectURL, if set, returns an HTTP redirect to the client.
	RedirectURL string
}

// Validate ensures the structure of GETResponse is valid
func (response GETResponse) Validate() error {
	if response.HTML == "" {
		return errors.New("no HTML in response")
	}
	return nil
}

// DataTableRequest is based on https://datatables.net/manual/server-side.
//
// Features turned on/off can be found when DataTables is initialized: $(headElement).find('.polyappDataTable').DataTable
//
// Features which are on and we need handling for on the server: lengthChange, ordering, paging, serverSide.
type DataTableRequest struct {
	// IndustryID of the request.
	IndustryID string `json:"industry"`
	// DomainID of the request.
	DomainID string `json:"domain"`
	// SchemaID of the request.
	SchemaID string `json:"schema"`
	// Draw counter. This is used by DataTables to ensure that the Ajax returns from server-side processing requests are
	// drawn in sequence by DataTables (Ajax requests are asynchronous and thus can return out of sequence).
	// This is used as part of the draw return parameter (see below).
	Draw int `json:"draw"`
	// Paging first record indicator. This is the start point in the current data set (0 index based - i.e. 0 is the first record).
	Start int `json:"start"`
	// Number of records that the table can display in the current draw. It is expected that the number of records
	// returned will be equal to this number, unless the server has fewer records to return. Note that this can be -1 to
	// indicate that all records should be returned (although that negates any benefits of server-side processing!).
	Length int `json:"length"`
	// format: order[i][column] where i is the 0-start index of the column.
	// Column to which ordering should be applied for the i-th column. This is an index reference to the columns array of information that is also submitted to the server.
	OrderColumn map[int]int
	// format: order[i][dir] where i is the 0-start index of the column.
	// Ordering direction for the i-th column. It will be asc or desc to indicate ascending ordering or descending ordering, respectively.
	OrderDir map[int]string
	// format: columns[i][name] where i is the 0-start index of the column.
	// Column's name, as defined by columns.name.
	ColumnsName []string
	// format: columns[i][orderable] where i is the 0-start index of the column.
	// Flag to indicate if this column is orderable (true) or not (false). This is controlled by columns.orderable.
	ColumnsOrderable []bool
	// LastDocumentID is a set of custom query parameters created to hold the last row of data currently visible on screen.
	// This is supposed to be used by Firestore to enable pagination.
	LastDocumentID string
	// RequiredColumns is an array of IDs of columns whose values must be present for a row to be visible.
	RequiredColumns []string
	// SearchInput is the input to the search box. Searching is handled as part of the AllDBQuery when you do a GET
	// request for DataTables. See that function for more information.
	//
	// TODO support regex, smart, and caseInsensitive options not being false (right now all must be false).
	SearchInput string
	// InitialSearch (optional) if set when the page loads this value becomes the initial value sent up in the GET
	// request for SearchInput.
	InitialSearch string
	// InitialOrderColumn (optional) if set when the page loads this value becomes the initial value sent up in the GET
	// request for OrderColumn (sort of - this sets the column # being sorted).
	InitialOrderColumn string
	// InitialLastDocumentID (optional) if set when the page loads this value becomes the initial value sent up in the GET
	// request for LastDocumentID.
	InitialLastDocumentID string
}

// DataTableResponse is based on https://datatables.net/manual/server-side
type DataTableResponse struct {
	// The draw counter that this object is a response to - from the draw parameter sent as part of the data request.
	// Note that it is strongly recommended for security reasons that you cast this parameter to an integer, rather than
	// simply echoing back to the client what it sent in the draw parameter, in order to prevent Cross Site Scripting
	// (XSS) attacks.
	Draw int `json:"draw"`
	// Total records, before filtering (i.e. the total number of records in the database)
	RecordsTotal int `json:"recordsTotal"`
	// Total records, after filtering (i.e. the total number of records after filtering has been applied - not just the
	// number of records being returned for this page of data).
	RecordsFiltered int `json:"recordsFiltered"`
	// The data to be displayed in the table. This is an array of data source objects, one for each row, which will be
	// used by DataTables. Note that this parameter's name can be changed using the ajax option's dataSrc property.
	Data [][]string `json:"data"`
	// Optional: If an error occurs during the running of the server-side processing script, you can inform the user of
	// this error by passing back the error message to be displayed using this parameter. Do not include if there is
	// no error.
	Error string `json:"error"`
	// LastDocumentID is the last returned DocumentID
	LastDocumentID string `json:"lastDocumentID"`
}

// CreateGETRequest returns a GETRequest and an error if it fails. It does NOT handle retrieving the auth header.
// FYI this is substantially similar to public/main.js function createPOSTBody.
func CreateGETRequest(URL url.URL) (GETRequest, error) {
	r := GETRequest{
		IndustryID:         "polyappNone",
		OverrideIndustryID: "polyappNone",
		DomainID:           "polyappNone",
		OverrideDomainID:   "polyappNone",
		TaskID:             "polyappNone",
		OverrideTaskID:     "polyappNone",
		UXID:               "polyappNone",
		SchemaID:           "polyappNone",
		DataID:             "polyappNone",
		UserID:             "polyappNone",
		RoleID:             "polyappNone",
	}
	fullURI := URL.RequestURI()
	if strings.HasPrefix(fullURI, "/index.html") {
		// this redirect-like behavior drops query parameters
		// so it should not be relied upon to work
		fullURI = "/"
	}

	vals := URL.Query()
	if vals.Get("ux") != "" {
		r.UXID = vals.Get("ux")
	}
	if vals.Get("schema") != "" {
		r.SchemaID = vals.Get("schema")
	}
	if vals.Get("data") != "" {
		r.DataID = vals.Get("data")
	}
	if vals.Get("user") != "" {
		r.UserID = vals.Get("user")
	}
	if vals.Get("role") != "" {
		r.RoleID = vals.Get("role")
	}
	if vals.Get("modify") != "" {
		r.ModifyID = vals.Get("modify")
	}
	if vals.Get("altshell") != "" {
		r.AltShell = vals.Get("altshell")
	}
	if vals.Get("finishURL") != "" {
		var err error
		r.FinishURL, err = url.PathUnescape(vals.Get("finishURL"))
		if err != nil {
			return GETRequest{}, fmt.Errorf("FinishURL PathUnescape: %w", err)
		}
	}

	path, err := url.PathUnescape(URL.EscapedPath())
	if err != nil {
		return GETRequest{}, fmt.Errorf("PathUnescape: %w", err)
	}
	splitPath := strings.Split(path, "/")
	if len(splitPath) > 2 && (splitPath[1] == "t" || splitPath[1] == "test" || splitPath[1] == "u") {
		// if url is localhost:8080/t/test/test/test/idb
		// splitPath = ["", t, test, test, test, idb, ""]
		if len(splitPath) > 2 && splitPath[2] != "" && !strings.Contains(splitPath[2], ".") {
			r.IndustryID = splitPath[2]
		}
		if len(splitPath) > 3 && splitPath[3] != "" && !strings.Contains(splitPath[3], ".") {
			r.DomainID = splitPath[3]
		}
		if len(splitPath) > 4 && splitPath[4] != "" && !strings.Contains(splitPath[4], ".") {
			r.TaskID = splitPath[4]
		}
	} else {
		// These are public pages under this domain. Example: polyappChangeContext
		publicPath := URL.EscapedPath()
		if len(publicPath) > 0 {
			if publicPath[len(publicPath)-1:] != "/" {
				publicPath = publicPath + "/"
			}
			if publicPath[:1] != "/" {
				publicPath = "/" + publicPath
			}
			r.PublicPath = publicPath
		}
	}

	// Used when defining industry and domain and task, such as in /polyappChangeContext
	// Also used for generic Tasks which can apply across multiple industries, domains, etc. such as polyapp/Task's Create Task
	// This only works because it's overriding the industry, domain and task seen in the URL's route/path.
	if vals.Get("industry") != "" {
		r.OverrideIndustryID = vals.Get("industry")
	}
	if vals.Get("domain") != "" {
		r.OverrideDomainID = vals.Get("domain")
	}
	if vals.Get("task") != "" {
		r.OverrideTaskID = vals.Get("task")
	}

	return r, nil
}

// FixPOSTRequest modifies a passed in POSTRequest. It does NOT handle retrieving the auth header.
//
// Call this to deal with special characters in the URL and trailing slashes and such.
func FixPOSTRequest(r *POSTRequest) error {
	var err error
	r.IndustryID, err = url.PathUnescape(r.IndustryID)
	if err != nil {
		return fmt.Errorf("PathUnescape: %w", err)
	}
	r.DomainID, err = url.PathUnescape(r.DomainID)
	if err != nil {
		return fmt.Errorf("PathUnescape: %w", err)
	}
	r.TaskID, err = url.PathUnescape(r.TaskID)
	if err != nil {
		return fmt.Errorf("PathUnescape: %w", err)
	}
	r.OverrideIndustryID, err = url.PathUnescape(r.OverrideIndustryID)
	if err != nil {
		return fmt.Errorf("PathUnescape: %w", err)
	}
	r.OverrideDomainID, err = url.PathUnescape(r.OverrideDomainID)
	if err != nil {
		return fmt.Errorf("PathUnescape: %w", err)
	}
	r.OverrideTaskID, err = url.PathUnescape(r.OverrideTaskID)
	if err != nil {
		return fmt.Errorf("PathUnescape: %w", err)
	}
	if r.PublicPath != "" {
		if r.PublicPath[:1] != "/" {
			r.PublicPath = "/" + r.PublicPath
		}
		if r.PublicPath[len(r.PublicPath)-1:] != "/" {
			r.PublicPath = r.PublicPath + "/"
		}
	}
	// We'd like to use common.Data's Init function, but when we do the Keys
	// are containing escaped characters like %20 in the specific case that the data is coming from a web client.
	// In other cases where Init is used, such as from the DB, this is not a problem and unescaping should not occur.
	// Therefore in this specific case we must handle PathUnescape for the keys.
	for k, v := range r.Data {
		newInnerMap := make(map[string]interface{})
		for IDKey, innerValue := range v {
			newKey, err := url.PathUnescape(IDKey)
			if err != nil {
				return fmt.Errorf("PathUnescape for key "+IDKey+": %w", err)
			}
			newInnerMap[newKey] = innerValue
		}
		r.Data[k] = newInnerMap
	}
	r.UserCache = nil
	r.UXCache = nil
	r.TaskCache = nil
	r.SchemaCache = nil
	return nil
}

// CreateRef is an alias for CreateURL. It forms a URL which is valid as a Ref in common.Data documents.
//
// An example output is: /t/asdlkjad/adslkj/BCiXCONsZuDxjTYEwNOonlLZc?ux=BCiXCONsZuDxjTYEwNOonlLZc&schema=BCiXCONsZuDxjTYEwNOonlLZc&data=BCiXCONsZuDxjTYEwNOonlLZc&user=&role=
func CreateRef(IndustryID string, DomainID string, TaskID string, UXID string, SchemaID string, DataID string) string {
	return CreateURL(IndustryID, DomainID, TaskID, UXID, SchemaID, DataID, "", "", "", "")
}

// ParseRef is the opposite of CreateRef. It parses a string Ref into a GETRequest data structure.
func ParseRef(ref string) (GETRequest, error) {
	u, err := url.Parse(ref)
	if err != nil {
		return GETRequest{}, fmt.Errorf("url.Parse %v: %w", ref, err)
	}
	g, err := CreateGETRequest(*u)
	if err != nil {
		return GETRequest{}, fmt.Errorf("CreateGETRequest %v: %w", ref, err)
	}
	return g, nil
}

// CreateURL returns a formed URL based on the information provided. polyappNone is converted to "" before use.
//
// An example output is: /t/asdlkjad/adslkj/BCiXCONsZuDxjTYEwNOonlLZc?ux=BCiXCONsZuDxjTYEwNOonlLZc&schema=BCiXCONsZuDxjTYEwNOonlLZc&data=BCiXCONsZuDxjTYEwNOonlLZc&user=&role=
//
// additionalQueryString lets you add arbitrary strings to the end of the URL.
func CreateURL(IndustryID string, DomainID string, TaskID string, UXID string, SchemaID string, DataID string, UserID string, RoleID string,
	OverrideIndustryID string, OverrideDomainID string, additionalQueryString ...string) string {
	if IndustryID == "polyappNone" {
		IndustryID = ""
	}
	if DomainID == "polyappNone" {
		DomainID = ""
	}
	if OverrideIndustryID == "polyappNone" {
		OverrideIndustryID = ""
	}
	if OverrideDomainID == "polyappNone" {
		OverrideDomainID = ""
	}
	if TaskID == "polyappNone" {
		TaskID = ""
	}
	if UXID == "polyappNone" {
		UXID = ""
	}
	if SchemaID == "polyappNone" {
		SchemaID = ""
	}
	if DataID == "polyappNone" {
		DataID = ""
	}
	if UserID == "polyappNone" {
		UserID = ""
	}
	if RoleID == "polyappNone" {
		RoleID = ""
	}

	URL := "/t/" + IndustryID + "/" + DomainID + "/" + TaskID

	if OverrideIndustryID != "" && OverrideDomainID != "" {
		URL += "?industry=" + OverrideIndustryID + "&domain=" + OverrideDomainID + "&ux=" + UXID + "&schema=" + SchemaID + "&data=" + DataID
	} else if OverrideIndustryID != "" {
		URL += "?industry=" + OverrideIndustryID + "&ux=" + UXID + "&schema=" + SchemaID + "&data=" + DataID
	} else if OverrideDomainID != "" {
		URL += "?domain=" + OverrideDomainID + "&ux=" + UXID + "&schema=" + SchemaID + "&data=" + DataID
	} else {
		URL += "?ux=" + UXID + "&schema=" + SchemaID + "&data=" + DataID
	}

	if UserID != "" {
		URL += "&user=" + UserID
	}
	if RoleID != "" {
		URL += "&role=" + RoleID
	}
	for _, s := range additionalQueryString {
		URL += s
	}
	return URL
}

// CreateURLFromRequest translates a POSTRequest into a string URL.
func CreateURLFromRequest(request POSTRequest) string {
	return CreateURL(request.IndustryID, request.DomainID, request.TaskID, request.UXID, request.SchemaID, request.DataID, request.UserID, request.RoleID, request.OverrideIndustryID, request.OverrideDomainID)
}

type SelectSearchRequest struct {
	Collection string
	Field      string
	Value      string
}

type SelectSearchResponse struct {
	ResultHTML string
}

// CreateSelectSearchRequest from a /polyappSearch request.
func CreateSelectSearchRequest(URL *url.URL) (SelectSearchRequest, error) {
	if URL == nil {
		return SelectSearchRequest{}, errors.New("URL was nil")
	}
	vals := URL.Query()
	r := SelectSearchRequest{
		Collection: vals.Get("collection"),
		Field:      vals.Get("field"),
		Value:      vals.Get("value"),
	}
	if r.Collection == "" {
		return SelectSearchRequest{}, errors.New("Collection was empty")
	}
	if r.Field == "" {
		return SelectSearchRequest{}, errors.New("Field was empty")
	}
	if r.Value == "" {
		return SelectSearchRequest{}, errors.New("Value was empty")
	}
	return r, nil
}

type FullTextSearchRequest struct {
	Collection string
	Fields     []string
	Value      string
}

type FullTextSearchResponse struct {
	Results    []FullTextSearchResponseResults
	ResultHTML string // ResultHTML is HTML which can be used to display search results in lieu of parsing the results yourself.
}

type FullTextSearchResponseResults struct {
	Name          string // Name or Title of the result
	HelpText      string // HelpText of the result is only available when searching collection "task".
	ChangeDataURL string // URL of the result's Change Data page
	NewDataURL    string // URL of the result's New Data page
	EditTaskURL   string // URL of the result's Edit Task page
}

// CreateFullTextSearchRequest from a /polyappFullTextSearch request.
func CreateFullTextSearchRequest(URL *url.URL) (FullTextSearchRequest, error) {
	if URL == nil {
		return FullTextSearchRequest{}, errors.New("URL was nil")
	}
	vals := URL.Query()
	f := FullTextSearchRequest{
		Collection: vals.Get("collection"),
		Value:      vals.Get("value"),
	}
	if f.Collection == "" {
		return FullTextSearchRequest{}, errors.New("Collection was empty")
	}
	switch f.Collection {
	case CollectionTask:
		f.Fields = []string{"Name", "HelpText"}
	case CollectionSchema:
		f.Fields = []string{"Name"}
	case CollectionUX:
		f.Fields = []string{"Title"}
	default:
		return FullTextSearchRequest{}, errors.New("unsupported collection: " + f.Collection)
	}
	if f.Value == "" {
		return FullTextSearchRequest{}, errors.New("Value was empty")
	}
	return f, nil
}

// CreateDataTableRequest from a /polyappDataTable request URL.
func CreateDataTableRequest(URL url.URL) (DataTableRequest, error) {
	r := DataTableRequest{
		IndustryID:       "polyappNone",
		DomainID:         "polyappNone",
		SchemaID:         "polyappNone",
		Draw:             0,
		Start:            0,
		Length:           0,
		OrderColumn:      map[int]int{},
		OrderDir:         map[int]string{},
		ColumnsName:      []string{},
		ColumnsOrderable: []bool{},
		LastDocumentID:   "",
	}
	var err error
	vals := URL.Query()
	if vals.Get("draw") == "NaN" {
		r.Draw = 0
	} else if vals.Get("draw") != "" {
		r.Draw, err = strconv.Atoi(vals.Get("draw"))
		if err != nil {
			return r, fmt.Errorf("could not cast draw: %w", err)
		}
	}
	if vals.Get("start") != "" {
		r.Start, err = strconv.Atoi(vals.Get("start"))
		if err != nil {
			return r, fmt.Errorf("could not cast start: %w", err)
		}
	}
	if vals.Get("length") != "" {
		r.Length, err = strconv.Atoi(vals.Get("length"))
		if err != nil {
			return r, fmt.Errorf("could not cast length: %w", err)
		}
	}
	if vals.Get("industry") != "" {
		r.IndustryID = vals.Get("industry")
	}
	if vals.Get("domain") != "" {
		r.DomainID = vals.Get("domain")
	}
	if vals.Get("schema") != "" {
		r.SchemaID = vals.Get("schema")
	}
	if vals.Get("LastDocumentID") != "" {
		r.LastDocumentID = vals.Get("LastDocumentID")
	}
	if vals.Get("search[value]") != "" {
		r.SearchInput = vals.Get("search[value]")
	}
	if vals.Get("table-initial-search") != "" {
		r.InitialSearch = vals.Get("table-initial-search")
	}
	if vals.Get("table-initial-order-column") != "" {
		r.InitialOrderColumn = vals.Get("table-initial-order-column")
	}
	if vals.Get("table-initial-last-document-ID") != "" {
		r.InitialLastDocumentID = vals.Get("table-initial-last-document-ID")
	}
	i := 0
	for {
		stringI := strconv.Itoa(i)
		oc := vals.Get("order[" + stringI + "][column]")
		if oc != "" {
			orderColumn, err := strconv.Atoi(oc)
			if err != nil {
				return r, errors.New("malformed order[i][column]: " + oc)
			}
			r.OrderColumn[i] = orderColumn
			r.OrderDir[i] = vals.Get("order[" + stringI + "][dir]")
		}

		co := vals.Get("columns[" + stringI + "][orderable]")
		if co == "" {
			// There should be as many columns as there are order elements in the array
			break
		}
		columnsOrderable, err := strconv.ParseBool(co)
		if err != nil {
			return r, errors.New("malformed columns[i][orderable]: " + co)
		}
		r.ColumnsOrderable = append(r.ColumnsOrderable, columnsOrderable)
		r.ColumnsName = append(r.ColumnsName, vals.Get("columns["+stringI+"][name]"))
		i++
	}
	return r, nil
}

// DataToPOSTData converts a *common.Data into the map[string]map[string]interface{} Data which is used in POSTRequest
// as field "Data" and in POSTResponse as field "DataUpdates".
//
// TODO this function only works on Head Data. Make it work on child Data (subtask Data) too.
func DataToPOSTData(data *Data, request POSTRequest) (map[string]map[string]interface{}, error) {
	simplified, err := data.Simplify()
	if err != nil {
		return nil, fmt.Errorf("data.Simplify: %w", err)
	}
	return map[string]map[string]interface{}{
		GetDataRef(request): simplified,
	}, nil
}
