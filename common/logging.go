package common

import (
	"fmt"
	"io"
	"os"
)

// Logging goals:
// 1. Log somewhere where I can see the log entries.

// GetLogger populates the logging clients and contexts. It also gives you a Writer you can use for error logging.
//
// After calling this, you MUST call 'defer CloseLogging()'
func GetLogger(serviceName string, serviceVersion string) (io.Writer, error) {
	switch GetCloudProvider() {
	default:
		return os.Stderr, nil
	}
}

// CloseLogging closes the logging connections and flushes all of the logs out.
func CloseLogging() {
	switch GetCloudProvider() {
	default:
		return
	}
}

// LogError logs the error to the main logger.
func LogError(errComingIn error) {
	switch GetCloudProvider() {
	default:
		fmt.Printf("%v\n", errComingIn.Error())
	}
}
