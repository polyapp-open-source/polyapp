# Design
Understanding Polyapp's design can help you know which packages are relevant to a change you want to make. It can also be helpful for getting a feel for how complicated (or uncomplicated) a change might be.

To learn more about why Polyapp exists and what its goals are, read the [README](README.md)

To read a commentary on this design, read the [DESIGN COMMENTARY](DESIGN_COMMENTARY.md)

[[_TOC_]]

## History
This is the third version of Polyapp. The first version was written in .NET with plain HTML and CSS. This was abandoned because .NET assumes you are building a traditional web application whereas Polyapp uses a variable model and a variable view.

The second version was written with Angular for the frontend and Go for the web server. Unfortunately Angular also proved too inflexible; it was designed for each view to have some attached javascript. While dynamically loading views is possible, you can't dynamically generate views which are then dynamically loaded without a terrible performance penalty.

The third version is written in Go, plain HTML and plain Javascript. Bootstrap is used for CSS. This is the current version.

The database structure has also undergone revisions. The first version was written with SQL (MariaDB) and structured all data and metadata in graphs which had pointers to the next version of the data or metadata. This approach seemed nice on a whiteboard but in reality it created large graphs which had to be sequentially read, causing many trips to and from the database.

The current database structure was created for Firestore, a nonrelational, distributed database owned by Google.

## Database Design
The database's goal is to hold state in the system and to not mix the data up. For every database we can define some functions which ensure the data does not get "mixed up". These are located in package integrity.

The database holds state in collections (Firestore) or Indexes (Elasticsearch). All database collections have a struct type in the common package, which also has more documentation about them.

To support querying by a particular field in a particular form, every field must have a unique name or path in Firestore. Due to Firestore's oddities these paths should be no more than one level down in the path (so data/field but not data/industry/field). Therefore fields' keys were made very long: "Industry_Domain_Schema_Field Name"

To reduce coupling between collections and to speed up information retrieval it is expected that the client should always know lots of information about a form: Task ID, Schema ID, Data ID. The client's suggested Task, Schema, User ID, Role, etc. for the Data it supplies are always verified before any data is modified. If a user modifies one of these fields so they are no longer valid the server request should fail fast.

**action** Actions have code associated with them, so adding an action means making code changes. Actions are triggered by Bots. Actions are configured with BotStaticData, which is set in the Task.

**bot** is composed of Actions which are run sequentially.

**css** lets you use a compiled Bootstrap CSS file different than the default one. It is referenced from UX.

**data** holds all data. Documents contain special fields denoting their Industry, Domain, and Schema IDs. Data also contains information put into a form and its keys are made by combining the Industry, Domain, Schema, and form field's label. For instance if you had a form field "Name" in Industry "Industry", Domain "Domain" and Collection "Schema" there would be a field in its Data document with the key "Industry_Domain_Schema_Name" and the value of that field.

**lastUpdate** is used by Action code to coordinate between different web servers.

**publicmap** maps HostAndPath (like "example.com/some/path") to UX IDs, Task IDs, Schema IDs, Industry, Domain, etc. Basically it lets you request the "/path/here" page and get a Task in response. Task URLs can get very long so this can be helpful.

**role** defines a Role which has access to all Domains in a list and all Industries in a list. For access to be granted both the Industry AND Domain need to be in these lists. This isn't the best system but it works for now.

**schema** has a copy of all keys in a Task (like "Industry_Domain_Schema_Name"), knows what Type they are (like "S" for string), and has Help Text for each field.

**task** holds information about validation (like if a field is required), Help Text, and configuration information for Bots which are available to Actions.

**user** records hold information for authentication, user emails, names, etc. They also have lists of Roles associated with a user and payment data.

**ux** holds the HTML for a webpage, information for the browser like where to find its Icon, the page description, the page title, etc. You can create a UX without a Task, Schema, or Data and point to it from publicmap and the page will load fine.


## Server Design
The server's primary goal is to deliver the client static files and CRUD form data to and from the database via POST requests.

We know which files to serve, which form data to CRUD, and which forms to CRUD based on parameters in the URL.

The server's secondary goal is to do something interesting with that data. This takes the form of Bots (collection of Actions), where Actions are configurable code.

To create or modify a form you open up the Task -> Data for that form. Then you modify that Data. When you press "Done" the Bots Triggered At Done run. One of these Bots runs an Action which runs some code. The code reads the Data sent to the server and updates that form's referenced Task, Schema, and UX documents. In other words the Actions in Bots and their associated code are used to create new "forms" (called Tasks in the system). Other than modifying database state in Action code, there is one other mechanism to create documents, update them, and (some day) delete them. This is via the POSTHandler code. In this code there is hard-coded support for Creating a new Data. When a Form is modified on the client & the updated version is sent to the server then an Update is applied to that Data. At this time there is no way to delete a Data unless you write some code to do it yourself, link an Action to that code, and call that Action from a Bot, and call that Bot from a Task. In order to modify any of the non-Data collections in the database you are expected to write code which is called by Actions.

Another way of explaining how you create or modify forms (Tasks) is that you use the form (Task) which is hooked up to the Bot which has the Action which creates or modified forms (Tasks).

Occasionally you want to do more than Create, Read, Update, or Delete operations. Actions can be good for this functionality if you don't need any new UI. If you need more than standard form-based UI check out the "User Interface" field in the Task Designer. This includes options like Tables with custom queries (queries configured with Bot Static Data), buttons, the Summernote text editor, and can easily be extended by adding more widgets.

Occasionally you want to view graphs of data in the system or perform other analyses on the data. Polyapp can't do that with the native Firestore database easily. Instead our expectation is that you will export this data to a file and then open it in Excel or a similar program to do the analysis you want to do. You could also code a new Action to do the analysis for you automatically.

### Services and Routing
The primary service is defaultservice. In Go if you have more than one main function you put your main functions into cmd/*/main.go

**defaultservice** is a web server. It takes HTTP requests. It uses [Echo](https://echo.labstack.com/) to set up the middleware and request routing.

* Some HTTP requests are for static assets in public/* and those are served directly from their files.
* GET requests are often to static assets.
* GET requests to /test/ bring you to the Test pages. These test UI elements (javascript code, CSS, HTML).
* GET requests to / bring you to the home page.
* GET requests to /signIn bring you to the sign in page, which does not require Authorization.
* GET requests to /polyappChangeContext let you select an Industry and Domain from the search bar. For example: "Custom Computer Programming Services" / "Website Builder". You can make a new Domain but not a new Industry. Press Done and you go to /polyappChangeTask
* GET requests to /polyappChangeTask let you choose a Task to perform. Tasks are basically complicated forms. You can create a new Task (which takes you to a new Data for the "Task Creation" Task) or select an existing Task, which takes you to /polyappChangeData
* GET requests to /polyappChangeData let you choose a Data to edit or open a new one. To help you choose which ID to pick the fields in the Task are columns in the table. Selecting one brings you to /t/...
* GET requests to /t/Industry/Domain/TaskID?ux=UXID&schema=SchemaID&data=DataID&user=USERID&role=ROLEID open up a Task. This loads the Data specified by the data= parameter, places that Data into the fields you see on screen, and can handle updating the Data as changes are made. It also runs Bots via three triggers: At Load (when the page first loads), Continuously (whenever a request is made to "auto-save" the form), and At Done (when the user presses the "Done" button).
* POST requests to /test/ are hard coded in internalHandle. This is used for testing User Interface widgets and making sure they look pretty before you hook them up to the backend.
* POST requests to /polyappChangeContext and /polyappChangeTask and /polyappChangeData are handled with hard-coded code in handlers/hardCodedTasks.go
* POST requests to /u/ trigger an upload (a big []byte like images or videos or audio). These files don't fit in the regular JSON sent to the server and are loaded from the server via separate HTML elements.
* POST requests to /t/ URLs run through handlers/handle.go and update the database document referenced by data= and its children documents (children are used by nested forms).

**serviceworker** is part of the unfinished WASM features: JS <-> WASM interface <-> Go compiled to WASM

### Packages
**actions** holds Action code. If you are looking to quickly and easily add a new Action and don't know where to put it, put it here.

**allDB** package holds APIs for Create, Read, Update, Delete (CRUD) and Query operations which are database-agnostic. This is needed so we don't compile, say, Firestore code into WASM, which has no use for it.
allDB has some _js.go files which only compile in WASM and some _server.go files which only compile for the server.
You can learn more in the allDB/doc.go file.

**cmd** hosts the service's main.go files and deployment and configuration files. defaultservice/baseDocuments also contains files needed to "bootstrap" the system of Task creation, like the Task, UX, Schema, etc. documents which are needed to create Tasks.

**common** package holds data structures. It must be the last link in all dependency trees which include it since it must run on all supported GOOS/GOARCH.

**elasticsearchCRUD** contains CRUD for elasticsearch and query APIs. It is not available in javascript. Eventually it may be deprecated in favor of a Postgres full text search index.

**fileCRUD** contains CRUD for files, like the cmd/defaultservice/baseDocuments folders. It is not available in javascript.

**firestoreCRUD** contains CRUD and Querying for objects with Firestore. It is not available in javascript.

**gen** contains UIs controls. It is a lot of HTML and rendering code. If you modify this package you typically test your changes
both with unit tests and by setting up a test in /test/.

**handlers** package is a bunch of code which handles incoming requests. It isn't in the main.go files or in cmd/*/*.go because it is supposed to be able to run on both the client (in WASM) and on the server (Linux or Windows or whatever)

**idbCRUD** is a javascript-only file which is supposed to interface with the IDB database in browsers. Its lack of implementation is the primary reason why there is no WASM support yet.

**integrity** package is called from allDB package. It is supposed to protect the system from entering a corrupt or invalid state. Learn more in integrity/doc.go

**public** is a folder which holds files which can be served directly to browser clients. Some of the .html files are actually text/template files. You'd have to open and read the first line of a file to know if it is text/template vs. an HTML file.

## Client Design
In general the client code's goal is to communicate form data to and from the server securely and simply while supporting many types of controls which can be part of that form.

The client is written mostly in plain javascript (some jQuery is used). External dependencies are avoided unless they are for a particular UI element like [Summernote](https://summernote.org/).

### General Principles
* Write as little Javascript as possible and don't use libraries either. This means avoiding client-side rendering and only using Javascript for animations and UI elements.
* Adopt elements seen in a Progressive Web Apps. Use an App Shell, start and manage a service worker, use JSON POST requests, and after the first rendering insert HTML directly into the DOM instead of refreshing the whole page.
* Authenticate requests using Firebase Auth
* Have generic error pages (public/*.html files)

### Data-related Code
* When data in the form fields changes send a POST request (auto-save functionality). Key functions which register event listeners: listenToLoad() addAllEventListeners() addListenersForHandleEvent() 
Key functions which are triggered by event listeners: handleEvent() setAllData()
Key functions which actually make the POST request: transmitUpdate() transmitUpdateNow()
* Handle POST request responses and allow: A new URL to be navigated to; other browser actions like opening a new tab; arbitrary DOM modifications. handleResponse()
* Handle big object uploads which are too large to be put into JSON like images, videos, etc.

### UI-related Code
* Handle resizing a sidebar when the window size changes. (Note: The sidebar has been removed so this code is deprecated)
* Extensions for summernote (a Word-like text editor): InsertSocialButton(), 
* Allow the enlarging of images using a modal: polyappEnlargeImage()
* Support various UI elements and structures, from <input> elements to lists of input elements to forms within forms.
* Support tables (use DataTables.net)
* Use Bootstrap for CSS whenever possible.
* Keep a few assets in an "assets" folder like icons or assets referenced explicitly in code. All other assets should be dynamically loaded from the DB based on the UX document or Data document.

### Data Structures
var AllData = {};

This variable is all of the data in the data model. It is a key:value where each key is the ID of a Data document and each
value is an object representing that Data document. Inside each Data document is a key: value set of data put into an Object.
Each key is formatted like it would be on the database: "industry_domain_schema_field name".
2 special cases exists. Keys in arrays have a number indicating their index in the array: "industry_domain_schema_field name_0"
Keys which are references are prepended with an underscore. Ex: "_industry_domain_schema_reference field name"
Each value is the current value at that variable name. Values are updated in setAllData().

### Source Files
public/*
