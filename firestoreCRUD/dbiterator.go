package firestoreCRUD

import (
	"errors"
	"fmt"

	"cloud.google.com/go/firestore"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"google.golang.org/api/iterator"
)

type DBIterator struct {
	iter *firestore.DocumentIterator
	q    common.Queryable
	// length of results. -1 means we don't know the length.
	length int
}

// CreateDBIterator creates an iterator which can operate over any Queryable type.
// This one is coupled to Firestore.
func CreateDBIterator(iter *firestore.DocumentIterator, lengthOfResults int) *DBIterator {
	return &DBIterator{
		iter:   iter,
		length: lengthOfResults,
	}
}

// Next returns the next result. Its second return value is iterator.Done if there
// are no more results. Once Next returns Done, all subsequent calls will return Done.
func (d *DBIterator) Next() (common.Queryable, error) {
	doc, err := d.iter.Next()
	if err == iterator.Done {
		return nil, common.IterDone
	}
	if err != nil {
		return nil, fmt.Errorf("couldn't iterate over Data documents: %w", err)
	}
	// Init is not guaranteed to 'zero out' a data structure. Since we are re-using the common.Queryable structure
	// over and over and over again but with different values we must create a new reference to 'clear it out' ourselves.
	// This is actually better than having Init clear it out since if Init clears it, other things with a ref to the
	// struct could lose data since it's all pointing back and the same underlying memory.
	switch doc.Ref.Parent.ID {
	case common.CollectionSchema:
		d.q = &common.Schema{
			FirestoreID: doc.Ref.ID,
		}
	case common.CollectionTask:
		d.q = &common.Task{
			FirestoreID: doc.Ref.ID,
		}
	case common.CollectionUser:
		d.q = &common.User{
			FirestoreID: doc.Ref.ID,
		}
	case common.CollectionRole:
		d.q = &common.Role{
			FirestoreID: doc.Ref.ID,
		}
	case common.CollectionUX:
		d.q = &common.UX{
			FirestoreID: doc.Ref.ID,
		}
	case common.CollectionData:
		d.q = &common.Data{
			FirestoreID: doc.Ref.ID,
		}
	case common.CollectionAction:
		d.q = &common.Action{
			FirestoreID: doc.Ref.ID,
		}
	case common.CollectionBot:
		d.q = &common.Bot{
			FirestoreID: doc.Ref.ID,
		}
	default:
		return nil, errors.New("unhandled Queryable type")
	}
	err = d.q.Init(doc.Data())
	return d.q, err
}

// Stop stops the iterator, freeing its resources.
// Always call Stop when you are done with a DocumentIterator.
// It is not safe to call Stop concurrently with Next.
func (d *DBIterator) Stop() {
	d.iter.Stop()
}

// Length of the results. Returns -1 if we don't know the length.
func (d *DBIterator) Length() int {
	return d.length
}

type CombinedDBIterator struct {
	iters []*firestore.DocumentIterator
	// q is the current queryable we are returning as a query result.
	q common.Queryable
	// length of results
	lengths []int
	// index in iters & queries.
	index int
}

// CreateCombinedDBIterator creates an iterator which can operate over any Queryable type.
// This one is coupled to Firestore.
func CreateCombinedDBIterator(iters []*firestore.DocumentIterator, lengthOfResults []int) *CombinedDBIterator {
	return &CombinedDBIterator{
		iters:   iters,
		lengths: lengthOfResults,
	}
}

// Next returns the next result. Its second return value is iterator.Done if there
// are no more results. Once Next returns Done, all subsequent calls will return Done.
func (d *CombinedDBIterator) Next() (common.Queryable, error) {
	var doc *firestore.DocumentSnapshot
	var err error
	for i := d.index; i < len(d.iters); i++ {
		doc, err = d.iters[d.index].Next()
		if err != iterator.Done {
			break
		} else if err == iterator.Done {
			d.iters[d.index].Stop()
			d.index++
			err = nil
		}
	}
	if d.index >= len(d.iters) {
		return nil, common.IterDone
	}
	if err != nil {
		return nil, fmt.Errorf("couldn't iterate over Data documents: %w", err)
	}
	// Init is not guaranteed to 'zero out' a data structure. Since we are re-using the common.Queryable structure
	// over and over and over again but with different values we must create a new reference to 'clear it out' ourselves.
	// This is actually better than having Init clear it out since if Init clears it, other things with a ref to the
	// struct could lose data since it's all pointing back and the same underlying memory.
	switch doc.Ref.Parent.ID {
	case common.CollectionSchema:
		d.q = &common.Schema{
			FirestoreID: doc.Ref.ID,
		}
	case common.CollectionTask:
		d.q = &common.Task{
			FirestoreID: doc.Ref.ID,
		}
	case common.CollectionUser:
		d.q = &common.User{
			FirestoreID: doc.Ref.ID,
		}
	case common.CollectionRole:
		d.q = &common.Role{
			FirestoreID: doc.Ref.ID,
		}
	case common.CollectionUX:
		d.q = &common.UX{
			FirestoreID: doc.Ref.ID,
		}
	case common.CollectionData:
		d.q = &common.Data{
			FirestoreID: doc.Ref.ID,
		}
	case common.CollectionAction:
		d.q = &common.Action{
			FirestoreID: doc.Ref.ID,
		}
	case common.CollectionBot:
		d.q = &common.Bot{
			FirestoreID: doc.Ref.ID,
		}
	default:
		return nil, errors.New("unhandled Queryable type")
	}
	err = d.q.Init(doc.Data())
	return d.q, err
}

// Stop stops the iterator, freeing its resources.
// Always call Stop when you are done with a DocumentIterator.
// It is not safe to call Stop concurrently with Next.
func (d *CombinedDBIterator) Stop() {
	for i := range d.iters {
		d.iters[i].Stop()
	}
}

// Length of the results.
func (d *CombinedDBIterator) Length() int {
	l := 0
	for i := range d.lengths {
		if d.lengths[i] < 0 {
			return -1
		}
		l += d.lengths[i]
	}
	return l
}
