package firestoreCRUD

import (
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

func TestCreateUser(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionUser).Doc("TestCreateUser").Delete(ctx)
	err = Create(ctx, client, nil)
	if err == nil {
		t.Error("should error if data == nil")
	}
	d := common.User{
		FirestoreID:   "TestCreateUser",
		UID:           common.String("TestCreateUser"),
		FullName:      common.String("TestCreateUser"),
		PhotoURL:      common.String("TestCreateUser"),
		EmailVerified: common.Bool(true),
		PhoneNumber:   common.String("TestCreateUser"),
		Email:         common.String("TestCreateUser"),
	}
	err = Create(ctx, nil, &d)
	if err == nil {
		t.Error("should error if client == nil")
	}
	err = Create(ctx, client, &d)
	if err != nil {
		t.Error("should not have errored when creating that document. Error: " + err.Error())
	}
	docOut, err := client.Collection(common.CollectionUser).Doc(d.FirestoreID).Get(ctx)
	if err != nil {
		t.Fatalf("document should have been created: " + err.Error())
	}
	if googleID, err := docOut.DataAt("UID"); err != nil || googleID != "TestCreateUser" {
		t.Error("did not set UID")
	}
}

func TestReadUser(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	s := common.User{
		FirestoreID:   "TestReadUser",
		UID:           common.String("TestReadUser"),
		FullName:      common.String("TestReadUser"),
		PhotoURL:      common.String("TestReadUser"),
		EmailVerified: common.Bool(true),
		PhoneNumber:   common.String("TestReadUser"),
		Email:         common.String("TestReadUser"),
	}
	_, err = client.Collection(common.CollectionUser).Doc("TestReadUser").Set(ctx, &s)
	if err != nil {
		t.Fatal(err)
	}

	q := common.User{
		FirestoreID: "TestReadUser",
	}
	err = Read(ctx, nil, &q)
	if err == nil {
		t.Error("passing in nil client should not be allowed")
	}

	o := common.User{
		FirestoreID: "TestReadUser",
	}
	err = Read(ctx, client, &o)
	if err != nil {
		t.Fatal("unexpected error: " + err.Error())
	}
	if o.FirestoreID != "TestReadUser" {
		t.Error("should have set FirestoreID")
	}
}

func TestUpdateUser(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionUser).Doc("TestUpdateUser").Delete(ctx)
	s := common.User{
		FirestoreID:   "TestUpdateUser",
		UID:           common.String("TestUpdateUser"),
		FullName:      common.String("TestUpdateUser"),
		PhotoURL:      common.String("TestUpdateUser"),
		EmailVerified: common.Bool(true),
		PhoneNumber:   common.String("TestUpdateUser"),
		Email:         common.String("TestUpdateUser"),
	}
	err = Update(ctx, client, &s)
	if err == nil {
		t.Error("should have thrown an error because the document does not exist")
	}
	_, _ = client.Collection(common.CollectionUser).Doc("TestUpdateUser").Set(ctx, make(map[string]string))
	err = Update(ctx, client, &s)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
}

func TestDeleteUser(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionUser).Doc("TestDeleteUser").Set(ctx, make(map[string]interface{}))
	q := common.User{
		FirestoreID: "TestDeleteUser",
	}
	err = Delete(ctx, nil, &q)
	if err == nil {
		t.Error("expected error if client is nil")
	}
	err = Delete(ctx, client, &q)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
}

func TestDeprecateUser(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionUser).Doc("TestDeprecateUser").Set(ctx, make(map[string]interface{}))
	q := common.User{
		FirestoreID: "TestDeprecateUser",
	}
	err = Deprecate(ctx, nil, &q)
	if err == nil {
		t.Error("expected error if client is nil")
	}
	err = Deprecate(ctx, client, &q)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	d, err := client.Collection(common.CollectionUser).Doc("TestDeprecateUser").Get(ctx)
	if err != nil {
		t.Fatal(err)
	}
	v, err := d.DataAt("polyappDeprecated")
	if err != nil {
		t.Fatal("unexpected error: " + err.Error())
	}
	if !v.(bool) {
		t.Error("should have deprecated the document")
	}
}
