package firestoreCRUD

import (
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

func TestCreateRole(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionRole).Doc("TestCreateRole").Delete(ctx)
	err = Create(ctx, client, nil)
	if err == nil {
		t.Error("should error if data == nil")
	}
	d := common.Role{
		FirestoreID: "TestCreateRole",
		Access: []common.Access{
			{Industry: "polyapp", Domain: "pol", Schema: "col", Create: false, Read: true, Update: false, Delete: false},
		},
		Deprecated: common.Bool(false),
	}
	err = Create(ctx, nil, &d)
	if err == nil {
		t.Error("should error if client == nil")
	}
	err = Create(ctx, client, &d)
	if err != nil {
		t.Error("should not have errored when creating that document. Error: " + err.Error())
	}
	_, err = client.Collection(common.CollectionRole).Doc(d.FirestoreID).Get(ctx)
	if err != nil {
		t.Errorf("document should have been created: " + err.Error())
	}
}

func TestReadRole(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	s := common.Role{
		FirestoreID: "TestReadRole",
		Access: []common.Access{
			{Industry: "polyapp", Domain: "pol", Schema: "col", Create: false, Read: true, Update: false, Delete: false},
		},
		Deprecated: nil,
	}
	_, err = client.Collection(common.CollectionRole).Doc("TestReadRole").Set(ctx, &s)
	if err != nil {
		t.Fatal(err)
	}

	o := common.Role{
		FirestoreID: "TestReadRole",
	}
	err = Read(ctx, nil, &o)
	if err == nil {
		t.Error("passing in nil client should not be allowed")
	}

	err = Read(ctx, client, &o)
	if err != nil {
		t.Fatal("unexpected error: " + err.Error())
	}
	if o.FirestoreID != "TestReadRole" {
		t.Fatal("should have set FirestoreID")
	}
	if o.Access == nil {
		t.Log(o.Access)
		t.Fatal("o.Access was nil")
	}
	if o.Access[0].Industry != "polyapp" || o.Access[0].Domain != "pol" || o.Access[0].Schema != "col" ||
		o.Access[0].Create != false || o.Access[0].Read != true || o.Access[0].Update != false || o.Access[0].Delete != false {
		t.Errorf("Incorrect Access values in Role")
	}
}

func TestUpdateRole(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionRole).Doc("TestUpdateRole").Delete(ctx)
	role := common.Role{
		FirestoreID: "TestUpdateRole",
		Access: []common.Access{common.Access{
			Industry: "polyapp",
			Domain:   "pol",
			Schema:   "pol",
			Create:   true,
			Read:     false,
			Update:   false,
			Delete:   true,
		}},
		Deprecated: nil,
	}
	err = Update(ctx, client, &role)
	if err == nil {
		t.Error("should have thrown an error because the document does not exist")
	}
	_, err = client.Collection(common.CollectionRole).Doc("TestUpdateRole").Set(ctx, make(map[string]string))
	if err != nil {
		t.Fatal(err.Error())
	}
	err = Update(ctx, client, &role)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
}

func TestDeleteRole(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionRole).Doc("TestDeleteRole").Set(ctx, make(map[string]interface{}))
	role := common.Role{
		FirestoreID: "TestDeprecateRole",
	}
	err = Delete(ctx, nil, &role)
	if err == nil {
		t.Error("expected error if client is nil")
	}
	err = Delete(ctx, client, &role)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
}

func TestDeprecateRole(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionRole).Doc("TestDeprecateRole").Set(ctx, make(map[string]interface{}))
	role := common.Role{
		FirestoreID: "TestDeprecateRole",
	}
	err = Deprecate(ctx, nil, &role)
	if err == nil {
		t.Error("expected error if client is nil")
	}
	err = Deprecate(ctx, client, &role)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	d, err := client.Collection(common.CollectionRole).Doc("TestDeprecateRole").Get(ctx)
	if err != nil {
		t.Fatal(err)
	}
	v, err := d.DataAt("polyappDeprecated")
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	if !v.(bool) {
		t.Error("should have deprecated the document")
	}
}
