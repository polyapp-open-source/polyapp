package firestoreCRUD

import (
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

func TestCreateTask(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionTask).Doc("TestCreateTask").Delete(ctx)
	err = Create(ctx, client, nil)
	if err == nil {
		t.Error("should error if data == nil")
	}
	d := common.Task{
		FirestoreID: "TestCreateTask",
		IndustryID:  "TestCreateTask",
		DomainID:    "TestCreateTask",
		Name:        "Name",
		HelpText:    "HelpText",
	}
	err = Create(ctx, nil, &d)
	if err == nil {
		t.Error("should error if client == nil")
	}
	err = Create(ctx, client, &d)
	if err != nil {
		t.Error("should not have errored when creating that document. Error: " + err.Error())
	}
	_, err = client.Collection(common.CollectionTask).Doc(d.FirestoreID).Get(ctx)
	if err != nil {
		t.Errorf("document should have been created: " + err.Error())
	}
}

func TestReadTask(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	s := common.Task{
		FirestoreID: "TestReadTask",
		IndustryID:  "TestReadTask",
		DomainID:    "TestReadTask",
		Name:        "Name",
		HelpText:    "HelpText",
	}
	_, err = client.Collection(common.CollectionTask).Doc("TestReadTask").Set(ctx, &s)
	if err != nil {
		t.Fatal(err)
	}

	task := common.Task{
		FirestoreID: "TestReadTask",
	}
	err = Read(ctx, nil, &task)
	if err == nil {
		t.Error("passing in nil client should not be allowed")
	}

	err = Read(ctx, client, &task)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	if task.FirestoreID != "TestReadTask" {
		t.Error("should have set FirestoreID")
	}
}

func TestUpdateTask(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	_, _ = client.Collection(common.CollectionTask).Doc("TestUpdateTask").Delete(ctx)
	s := common.Task{
		FirestoreID: "TestUpdateTask",
		IndustryID:  "TestUpdateTask",
		DomainID:    "TestUpdateTask",
		Name:        "Name",
		HelpText:    "HelpText",
	}
	err = Update(ctx, client, &s)
	if err == nil {
		t.Error("should have thrown an error because the document does not exist")
	}
	_, _ = client.Collection(common.CollectionTask).Doc("TestUpdateTask").Set(ctx, make(map[string]string))
	err = Update(ctx, client, &s)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
}

func TestDeleteTask(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	o := common.Task{
		FirestoreID: "TestReadTask",
	}
	_, _ = client.Collection(common.CollectionTask).Doc("TestDeleteTask").Set(ctx, make(map[string]interface{}))
	err = Delete(ctx, nil, &o)
	if err == nil {
		t.Error("expected error if client is nil")
	}
	err = Delete(ctx, client, &o)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
}

func TestDeprecateTask(t *testing.T) {
	t.Parallel()
	ctx, client, err := GetClient()
	if err != nil {
		t.Fatal(err)
	}

	o := common.Task{
		FirestoreID: "TestDeprecateTask",
	}
	_, _ = client.Collection(common.CollectionTask).Doc("TestDeprecateTask").Set(ctx, make(map[string]interface{}))
	err = Deprecate(ctx, nil, &o)
	if err == nil {
		t.Error("expected error if client is nil")
	}
	err = Deprecate(ctx, client, &o)
	if err != nil {
		t.Error("unexpected error: " + err.Error())
	}
	d, err := client.Collection(common.CollectionTask).Doc("TestDeprecateTask").Get(ctx)
	if err != nil {
		t.Fatal(err)
	}
	v, err := d.DataAt(common.PolyappDeprecated)
	if err != nil {
		t.Fatal("unexpected error: " + err.Error())
	}
	if !v.(bool) {
		t.Error("should have deprecated the document")
	}
}
