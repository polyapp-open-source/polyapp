package firestoreCRUD

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"cloud.google.com/go/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"cloud.google.com/go/firestore"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"google.golang.org/api/iterator"
	"google.golang.org/genproto/googleapis/type/latlng"
)

// CreateData creates a new DataTypes document and sets it. It DOES require that you've already generated an ID.
//
// If a document exists in the DB when this is called, it will be overwritten.
//
// Deprecated: Use Create API instead.
func CreateData(ctx context.Context, client *firestore.Client, data *common.Data) error {
	if client == nil {
		return errors.New("client was nil in CreateData")
	}
	if data == nil {
		return errors.New("data was nil in CreateData")
	}
	err := data.Validate()
	if err != nil {
		return fmt.Errorf("data invalid: %w", err)
	}

	firestoreData, err := data.Simplify()
	if err != nil {
		return fmt.Errorf("data could not be simplified: %w", err)
	}
	storageErrChan := make(chan error)
	lenStorageErrChan := 0
	for k, v := range firestoreData {
		switch bv := v.(type) {
		case []byte:
			go func(byteArray []byte, firestoreID string, key string, errChan chan error) {
				objectName := GetStorageObjectName(common.CollectionData, firestoreID, key)
				err = SetStorageObject(GetBlobBucket(), bytes.NewReader(byteArray), objectName, nil)
				if err != nil {
					errChan <- fmt.Errorf("SetStorageObject for name (%q): %w", objectName, err)
					return
				}
				errChan <- nil
			}(bv, data.FirestoreID, k, storageErrChan)
			delete(firestoreData, k)
			lenStorageErrChan++
		case [][]byte:
			go func(byteArrays [][]byte, firestoreID string, key string, errChan chan error) {
				for i := range byteArrays {
					objectName := GetStorageObjectArrayName(common.CollectionData, firestoreID, key, i)
					err = SetStorageObject(GetBlobBucket(), bytes.NewReader(byteArrays[i]), objectName, nil)
					if err != nil {
						errChan <- fmt.Errorf("SetStorageObject for name (%q): %w", objectName, err)
						return
					}
				}
				errChan <- nil
			}(bv, data.FirestoreID, k, storageErrChan)
			delete(firestoreData, k)
			lenStorageErrChan++
		}
	}
	err = client.RunTransaction(ctx, func(ctx context.Context, transaction *firestore.Transaction) error {
		// Create new Firestore Document
		dataDoc := client.Collection(common.CollectionData).Doc(data.FirestoreID)
		err = transaction.Create(dataDoc, firestoreData)
		if err != nil {
			// we could try to handle the error by, say, checking if it's too large of a document and falling back somehow
			// but I would rather pass the error up so we could potentially utilize other resources like GCP Buckets
			return fmt.Errorf("transaction.Create: %w", err)
		}

		return nil
	}, firestore.MaxAttempts(3))
	if err != nil {
		return fmt.Errorf("transaction: %w; data: %v", err, firestoreData)
	}

	// This is messy because if the transaction fails the storage updates are still be applied.
	// I can't currently think of a better way to do this other than implementing my own transaction code which 'rolls back'
	// the changes if one or more of the sets fails.
	combinedErr := ""
	for i := 0; i < lenStorageErrChan; i++ {
		err = <-storageErrChan
		if err != nil {
			combinedErr += err.Error() + "; "
		}
	}
	if combinedErr != "" {
		return errors.New("storage sets: " + combinedErr)
	}

	return nil
}

// ReadData reads from Firestore into a Data object.
func ReadData(ctx context.Context, client *firestore.Client, firestoreID string) (common.Data, error) {
	if client == nil || firestoreID == "" {
		return common.Data{}, errors.New("not all inputs were populated for ReadData")
	}
	snap, err := client.Collection(common.CollectionData).Doc(firestoreID).Get(ctx)
	if err != nil {
		return common.Data{}, fmt.Errorf("couldn't read document with id: "+firestoreID+" error: %w", err)
	}
	data, err := firestoreDocToData(snap)
	if err != nil {
		return common.Data{}, fmt.Errorf("firestoreDocToData: %w", err)
	}
	if data.SchemaID == "" {
		return common.Data{}, fmt.Errorf("data.SchemaID was empty for ID: %v", firestoreID)
	}
	if data.SchemaCache.FirestoreID == "" {
		data.SchemaCache, err = ReadSchema(ctx, client, data.SchemaID)
		if err != nil {
			return common.Data{}, fmt.Errorf("ReadSchema %v: %w", data.SchemaID, err)
		}
	}
	err = common.ConvertDataToMatchSchema(&data, &data.SchemaCache)
	if err != nil {
		return common.Data{}, fmt.Errorf("common.ConvertDataToMatchSchema: %w", err)
	}
	err = common.RemoveExtraFields(&data, &data.SchemaCache)
	if err != nil {
		return common.Data{}, fmt.Errorf("common.RemoveExtraField: %w", err)
	}

	// Get the []byte objects.
	objectNamePrefix := GetStorageObjectName(common.CollectionData, firestoreID, "")
	objectNames, err := ListStorageObjects(GetBlobBucket(), objectNamePrefix)
	if err != nil {
		return common.Data{}, fmt.Errorf("listStorageObjects: %w", err)
	}
	type bytePkg struct {
		ByteArray []byte
		Key       string
	}
	storageErrChan := make(chan error)
	byteArrayChan := make(chan bytePkg)
	lenStorageErrChan := 0
	for _, objectName := range objectNames {
		if strings.Contains(objectName, "/") {
			// must be ABytes
			// collectionName + "_" + firestoreID + "_" + key + "/" + index
			splitNewBytesPkgKey := strings.Split(objectName, "/")
			// [collectionName + "_" + firestoreID + "_" + key, index]
			slicedObjectName := strings.SplitN(splitNewBytesPkgKey[0], "_", 3)
			aBytesKey, err := url.PathUnescape(slicedObjectName[2])
			if data.ABytes[aBytesKey] != nil || err != nil {
				continue
			}
			aLength, _ := strconv.Atoi(splitNewBytesPkgKey[1])
			// if index was 0, length must be 1 or more
			aLength++
			// the problem is that I need to know how long to make this array (aLength). I can't know that without knowing
			// the largest index in the array. To find the largest index in the array I have to examine the names of
			// other objects in the Bucket. To do that I have to iterate over those objects.
			for _, o := range objectNames {
				if strings.Contains(objectName, "/") {
					// must be ABytes
					splitOName := strings.Split(o, "/")
					oNameBytesKey := splitOName[0]
					if oNameBytesKey == splitNewBytesPkgKey[0] {
						// these 2 objects are part of the same path which means they are part of the same array
						index, _ := strconv.Atoi(splitOName[1])
						// if index was 0, length must be 1 or more
						index++
						if index > aLength {
							aLength = index
						}
					}
				}
			}
			data.ABytes[aBytesKey] = make([][]byte, aLength)
		}
	}
	for _, objectName := range objectNames {
		go func(objectName string, byteArrayChan chan bytePkg, errChan chan error) {
			b, err := GetStorageObject(GetBlobBucket(), objectName)
			if err != nil {
				errChan <- fmt.Errorf("getStorageObject for name (%q): %w", objectName, err)
				byteArrayChan <- bytePkg{}
				return
			}
			byteArrayChan <- bytePkg{
				ByteArray: b,
				Key:       GetKeyFromStorageObjectName(objectName),
			}
			errChan <- nil
		}(objectName, byteArrayChan, storageErrChan)
		lenStorageErrChan++
	}
	// A []byte, B []byte, C [][]byte{D []byte, E []byte}
	// objectNames will only list A, B, D, E
	combinedErr := ""
	for i := 0; i < lenStorageErrChan; i++ {
		newBytesPkg := <-byteArrayChan
		err = <-storageErrChan
		if err != nil {
			combinedErr += err.Error() + "; "
			continue
		}
		if strings.Contains(newBytesPkg.Key, "/") {
			// must be ABytes
			// collectionName + "_" + firestoreID + "_" + key + "/" + index
			splitNewBytesPkgKey := strings.Split(newBytesPkg.Key, "/")
			// [collectionName + "_" + firestoreID + "_" + key, index]
			aBytesKey := splitNewBytesPkgKey[0]
			aBytesIndex, err := strconv.Atoi(splitNewBytesPkgKey[1])
			if err != nil {
				combinedErr += err.Error() + "; "
				continue
			}
			data.ABytes[aBytesKey][aBytesIndex] = newBytesPkg.ByteArray
		} else {
			// must be Bytes
			data.Bytes[newBytesPkg.Key] = newBytesPkg.ByteArray
		}
	}
	if combinedErr != "" {
		return common.Data{}, errors.New("storage gets: " + combinedErr)
	}
	return data, nil
}

// UpdateData in Firestore. You must already know the document ID. Does not allow updating documents which do not exist.
// The input Data struct must be valid.
func UpdateData(ctx context.Context, client *firestore.Client, newData *common.Data) error {
	if client == nil || newData == nil {
		return errors.New("not all inputs were populated for UpdateData")
	}
	err := newData.Validate()
	if err != nil {
		return fmt.Errorf("data invalid: %w", err)
	}

	storageErrChan := make(chan error)
	lenStorageErrChan := 0
	for k, v := range newData.Bytes {
		go func(byteArray []byte, firestoreID string, key string, errChan chan error) {
			objectName := GetStorageObjectName(common.CollectionData, firestoreID, key)
			err = SetStorageObject(GetBlobBucket(), bytes.NewReader(byteArray), objectName, nil)
			if err != nil {
				errChan <- fmt.Errorf("SetStorageObject for name (%q): %w", objectName, err)
				return
			}
			errChan <- nil
		}(v, newData.FirestoreID, k, storageErrChan)
		delete(newData.Bytes, k)
		lenStorageErrChan++
	}
	for k, bv := range newData.ABytes {
		go func(byteArrays [][]byte, firestoreID string, key string, errChan chan error) {
			for i := range byteArrays {
				objectName := GetStorageObjectArrayName(common.CollectionData, firestoreID, key, i)
				err = SetStorageObject(GetBlobBucket(), bytes.NewReader(byteArrays[i]), objectName, nil)
				if err != nil {
					errChan <- fmt.Errorf("SetStorageObject for name (%q): %w", objectName, err)
					return
				}
			}
			errChan <- nil
		}(bv, newData.FirestoreID, k, storageErrChan)
		delete(newData.ABytes, k)
		lenStorageErrChan++
	}

	// exists because Update should not succeed if the document does not exist
	err = client.RunTransaction(ctx, func(ctx context.Context, tx *firestore.Transaction) error {
		oldDoc, err := tx.Get(&firestore.DocumentRef{
			Path: client.Collection(common.CollectionData).Doc(newData.FirestoreID).Path,
			ID:   newData.FirestoreID,
		})
		if codes.NotFound == status.Code(err) {
			return err
		} else if err != nil {
			return fmt.Errorf("error getting document to update: %w", err)
		}
		oldDocSchema, err := tx.Get(&firestore.DocumentRef{
			Path: client.Collection(common.CollectionSchema).Doc(newData.SchemaID).Path,
			ID:   newData.SchemaID,
		})
		if codes.NotFound == status.Code(err) {
			return err
		} else if err != nil {
			return fmt.Errorf("tx.Get oldDocSchema: %w", err)
		}
		schema := &common.Schema{}
		_ = schema.Init(oldDocSchema.Data())
		schema.FirestoreID = oldDocSchema.Ref.ID

		oldData := &common.Data{
			FirestoreID: oldDoc.Ref.ID,
		}
		err = oldData.Init(oldDoc.Data())
		if err != nil {
			return fmt.Errorf("oldData init: %w", err)
		}

		err = common.ConvertDataToMatchSchema(oldData, schema)
		if err != nil {
			return fmt.Errorf("ConvertDataToMatchSchema for oldData schema: %w", err)
		}
		err = common.RemoveExtraFields(oldData, schema)
		if err != nil {
			return fmt.Errorf("common.RemoveExtraFields for oldData schema: %w", err)
		}

		mergedData := &common.Data{
			FirestoreID: oldDoc.Ref.ID,
		}
		err = mergedData.Init(oldDoc.Data())
		if err != nil {
			return fmt.Errorf("mergedData init: %w", err)
		}
		err = common.ConvertDataToMatchSchema(mergedData, schema)
		if err != nil {
			return fmt.Errorf("ConvertDataToMatchSchema for mergedData schema: %w", err)
		}
		err = common.RemoveExtraFields(mergedData, schema)
		if err != nil {
			return fmt.Errorf("common.RemoveExtraFields for oldData schema: %w", err)
		}
		err = mergedData.Merge(newData)
		if err != nil {
			return fmt.Errorf("merge: %w", err)
		}
		mergedDataMap, err := mergedData.Simplify()
		if err != nil {
			return fmt.Errorf("data could not be simplified: %w", err)
		}

		// set all fields since we have fully merged the new document into the old
		err = tx.Set(client.Collection(common.CollectionData).Doc(newData.FirestoreID), mergedDataMap)
		if err != nil {
			// we could try to handle the error by, say, checking if it's too large of a document and falling back somehow
			// but I would rather pass the error up so we could potentially utilize other resources like GCP Buckets
			return fmt.Errorf("data Set: %w", err)
		}

		return nil
	})
	if err != nil {
		return fmt.Errorf("transaction failed: %w", err)
	}

	// This is messy because if the transaction fails the storage updates are still be applied.
	// I can't currently think of a better way to do this other than implementing my own transaction code which 'rolls back'
	// the changes if one or more of the sets fails.
	combinedErr := ""
	for i := 0; i < lenStorageErrChan; i++ {
		err = <-storageErrChan
		if err != nil {
			combinedErr += err.Error() + "; "
		}
	}
	if combinedErr != "" {
		return errors.New("storage sets: " + combinedErr)
	}

	return nil
}

// DeleteData removes a document. Allows you to delete documents which do not exist.
func DeleteData(ctx context.Context, client *firestore.Client, firestoreID string) error {
	if client == nil {
		return errors.New("not all inputs were populated for DeleteData")
	}

	// Get the []byte objects.
	objectNamePrefix := GetStorageObjectName(common.CollectionData, firestoreID, "")
	objectNames, err := ListStorageObjects(GetBlobBucket(), objectNamePrefix)
	if err != nil {
		return fmt.Errorf("listStorageObjects: %w", err)
	}
	storageErrChan := make(chan error)
	lenStorageErrChan := 0
	for _, objectName := range objectNames {
		go func(objectName string, errChan chan error) {
			err := DeleteStorageObject(GetBlobBucket(), objectName)
			if err != nil {
				errChan <- fmt.Errorf("DeleteStorageObject for name (%q): %w", objectName, err)
				return
			}
			errChan <- nil
		}(objectName, storageErrChan)
		lenStorageErrChan++
	}
	combinedErr := ""
	for i := 0; i < lenStorageErrChan; i++ {
		err = <-storageErrChan
		if err != nil {
			combinedErr += err.Error() + "; "
		}
	}
	if combinedErr != "" {
		return errors.New("storage deletes: " + combinedErr)
	}

	toDelete := client.Collection(common.CollectionData).Doc(firestoreID)
	_, err = toDelete.Delete(ctx)
	if err != nil && status.Code(err) == codes.NotFound {
		// OK
	} else if err != nil {
		return fmt.Errorf("toDelete.Delete: %w", err)
	}
	return nil
}

// DeprecateData sets the 'polyappDeprecated' property.
func DeprecateData(ctx context.Context, client *firestore.Client, firestoreID string) error {
	if client == nil || firestoreID == "" {
		return errors.New("not all inputs were populated for DeprecateData")
	}
	deprecateMap := make(map[string]bool)
	deprecateMap["polyappDeprecated"] = true
	_, err := client.Collection(common.CollectionData).Doc(firestoreID).Set(ctx, deprecateMap, firestore.MergeAll)
	if err != nil {
		return fmt.Errorf("failed to deprecate document: %w", err)
	}
	return nil
}

// CreateDataContext creates a single new document based on some schema information.
func CreateDataContext(ctx context.Context, client *firestore.Client, polyappIndustryID string, polyappDomainID string, polyappSchemaID string, data *common.Data) error {
	if client == nil || data == nil {
		return errors.New("client or data ref was nil")
	}
	if polyappSchemaID == "" || polyappDomainID == "" || polyappIndustryID == "" {
		return errors.New("must provide all parameters with non-empty strings")
	}
	data.FirestoreID = "notadocument"
	data.SchemaID = polyappSchemaID
	data.DomainID = polyappDomainID
	data.IndustryID = polyappIndustryID
	err := data.Validate()
	if err != nil {
		return fmt.Errorf("data validation failure: %w", err)
	}
	data.SchemaID = polyappSchemaID
	data.DomainID = polyappDomainID
	data.IndustryID = polyappIndustryID
	newDoc := client.Collection(common.CollectionData).NewDoc()
	data.FirestoreID = newDoc.ID
	return CreateData(ctx, client, data)
}

// ReadDataContext reads from Firestore into data based on some schema information.
// Because there will be many matching documents for this query, an iterator is returned giving access to those documents.
func ReadDataContext(ctx context.Context, client *firestore.Client, polyappIndustryID string, polyappDomainID string, polyappSchemaID string) (common.Iter, error) {
	if client == nil || polyappIndustryID == "" || polyappDomainID == "" || polyappSchemaID == "" {
		return nil, errors.New("not all inputs were populated for ReadDataContext")
	}
	query := client.Collection(common.CollectionData).Where("polyappSchemaID", "==", polyappSchemaID).Where(
		"polyappDomainID", "==", polyappDomainID).Where("polyappIndustryID", "==", polyappIndustryID)
	iter := query.Documents(ctx)
	return CreateDBIterator(iter, -1), nil
}

// UpdateDataContext iterates over all matching Firestore documents and updates all of them to match data *common.Data
// with a merge operation. I suspect this will be helpful for schema conversions.
func UpdateDataContext(ctx context.Context, client *firestore.Client, polyappIndustryID string, polyappDomainID string, polyappSchemaID string, data *common.Data) error {
	if client == nil || polyappIndustryID == "" || polyappDomainID == "" || polyappSchemaID == "" {
		return errors.New("not all inputs were populated for UpdateDataContext")
	}
	query := client.Collection(common.CollectionData).Where("polyappSchemaID", "==", polyappSchemaID).Where(
		"polyappDomainID", "==", polyappDomainID).Where("polyappIndustryID", "==", polyappIndustryID)
	iter := query.Documents(ctx)

	sawADocument := false
	failures := ""
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return fmt.Errorf("couldn't iterate over Data documents: %w", err)
		}
		sawADocument = true

		err = UpdateData(ctx, client, data)
		if err != nil {
			failures += doc.Ref.Path + " failed because: " + err.Error() + "; "
		}
	}
	if !sawADocument {
		return status.Error(codes.NotFound, "no document to update with the provided contexts: "+polyappIndustryID+", "+polyappDomainID+", "+polyappSchemaID)
	}
	if failures != "" {
		return errors.New(failures)
	}
	return nil
}

// DeprecateDataContext deprecates a particular schema / schema in Polyapp. It does NOT delete the actual document.
func DeprecateDataContext(ctx context.Context, client *firestore.Client, polyappIndustryID string, polyappDomainID string, polyappSchemaID string) error {
	if client == nil || polyappIndustryID == "" || polyappDomainID == "" || polyappSchemaID == "" {
		return errors.New("not all inputs were populated for DeprecateDataContext")
	}
	query := client.Collection(common.CollectionData).Where("polyappSchemaID", "==", polyappSchemaID).Where(
		"polyappDomainID", "==", polyappDomainID).Where("polyappIndustryID", "==", polyappIndustryID)
	iter := query.Documents(ctx)

	sawADocument := false
	failures := ""
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return fmt.Errorf("iter.Next: %w", err)
		}
		sawADocument = true

		err = DeprecateData(ctx, client, doc.Ref.ID)
		if err != nil {
			failures += doc.Ref.Path + " failed because: " + err.Error() + "; "
		}
	}
	if !sawADocument {
		return errors.New("no document to deprecate with the provided contexts: " + polyappIndustryID + ", " + polyappDomainID + ", " + polyappSchemaID)
	}
	if failures != "" {
		return errors.New(failures)
	}
	return nil
}

// QueryRead runs a generic Query and gives you common.Iter to operate over the results.
// common.Iter figures out what type of Queryable to return in 'Next()' by looking at the Query Schema.
//
// Side note: There used to be QueryUpdate and QueryDelete functions too. Now if you wish to perform such operations,
// first use QueryRead and then iterate over each document & update / delete them individually.
func QueryRead(ctx context.Context, client *firestore.Client, industryID string, domainID string, schemaID string, query firestore.Query) (common.Iter, error) {
	iter := query.Documents(ctx)
	return CreateDBIterator(iter, -1), nil
}

// firestoreDocToData is helpful when reading data
func firestoreDocToData(snap *firestore.DocumentSnapshot) (common.Data, error) {
	if snap == nil {
		return common.Data{}, errors.New("you must fully initialize snapshot before passing it in to firestoreDocToData")
	}
	firestoreData := snap.Data()
	data := common.Data{}
	data.Init(firestoreData)
	// Init does not handle references at all
	for _, v := range firestoreData {
		switch v.(type) {
		case *firestore.DocumentRef:
			return common.Data{}, errors.New("firestore document contained a reference. Support for this was deprecated on 5/13/2020")
		case []byte:
			return common.Data{}, errors.New("firestore document contained an array of bytes. This is unhandled in firestoreDocToData")
		case time.Time:
			return common.Data{}, errors.New("firestore document contained a time.Time. This is unhandled in firestoreDocToData")
		case latlng.LatLng:
			return common.Data{}, errors.New("firestore document contained a latlng.LatLng. This is unhandled in firestoreDocToData")
		case map[string]interface{}:
			return common.Data{}, errors.New("firestore document contained a map. This is unhandled in firestoreDocToData")
		}
	}
	data.FirestoreID = snap.Ref.ID
	return data, nil
}

var (
	storageClient     *storage.Client
	storageClientLock sync.Mutex
)

// getStorageClient returns a storage client and everything else you need to perform storage operations.
//
// Proper usage involves calling this, checking the error, and then calling "defer cancel()"
func getStorageClient() (*storage.Client, context.Context, context.CancelFunc, error) {
	ctx := context.Background()
	storageClientLock.Lock()
	if storageClient == nil {
		var err error
		storageClient, err = storage.NewClient(ctx)
		if err != nil {
			storageClientLock.Unlock()
			return nil, ctx, nil, fmt.Errorf("storage.NewClient: %w", err)
		}
	}
	if os.Getenv("BUCKET") == "" {
		// Happens if the bucket was never set up as an environment variable in app.yaml
		it := storageClient.Buckets(ctx, common.GetGoogleProjectID())
		it.PageInfo()
		for {
			bucketAttrs, err := it.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return nil, ctx, nil, fmt.Errorf("it.Next searching for buckets: %w", err)
			}
			// It is not clear how to select the "correct" bucket to use.
			// I know that the installation script always creates a bucket with a # at the beginning so I'll use
			// that to determine the correct bucket. Assumption is that either (a) the project only contains Polyapp
			// so the only numbered bucket is Polyapp, or (b) the project contains other things and the people
			// working with the project are giving the other buckets human-readable names.
			// There is a reference to this strategy in google_cloud_install.sh
			if len(bucketAttrs.Name) > 0 && '0' <= bucketAttrs.Name[0] && bucketAttrs.Name[0] <= '9' {
				err = os.Setenv("BUCKET", bucketAttrs.Name)
				if err != nil {
					return nil, nil, nil, fmt.Errorf("os.Setenv: %w", err)
				}
				break
			}
		}
	}
	storageClientLock.Unlock()
	ctx, cancel := context.WithTimeout(ctx, time.Second*30)
	return storageClient, ctx, cancel, nil
}

// ListStorageObjects gets a list of "objectName" with a particular prefix and returns it as an array of strings.
func ListStorageObjects(bucketName string, objectNamePrefix string) ([]string, error) {
	storageClient, ctx, cancel, err := getStorageClient()
	if err != nil {
		return nil, fmt.Errorf("getStorageClient: %w", err)
	}
	defer cancel()

	it := storageClient.Bucket(bucketName).Objects(ctx, &storage.Query{
		Delimiter: "",
		Prefix:    objectNamePrefix,
		Versions:  false,
	})
	out := make([]string, 0)
	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, fmt.Errorf("Bucket(%q).Objects(): %w", GetBlobBucket(), err)
		}
		out = append(out, attrs.Name)
	}
	return out, nil
}

// SetStorageObject sets the "bucketName" bucket with objectName "objectName" with all of the readable data in 'data'.
func SetStorageObject(bucketName string, data io.Reader, objectName string, metadata map[string]string) error {
	storageClient, ctx, cancel, err := getStorageClient()
	if err != nil {
		return fmt.Errorf("getStorageClient: %w", err)
	}
	defer cancel()

	wc := storageClient.Bucket(bucketName).Object(objectName).NewWriter(ctx)
	if metadata != nil {
		if wc.Metadata == nil {
			wc.Metadata = make(map[string]string)
		}
		for k, v := range metadata {
			if k == "Type" {
				wc.ContentType = v
			} else {
				// add to metadata; don't delete any metadata which is already there.
				wc.Metadata[k] = v
			}
		}
	}
	_, err = io.Copy(wc, data)
	if err != nil {
		return fmt.Errorf("io.Copy: %w", err)
	}
	err = wc.Close()
	if err != nil {
		return fmt.Errorf("Writer.Close: %w", err)
	}
	return nil
}

// GetStorageObject retrieves the "bucketName" bucket at object "objectName" and returns it as an array of bytes, the metadata,
// and an error (or not).
func GetStorageObject(bucketName string, objectName string) ([]byte, error) {
	storageClient, ctx, cancel, err := getStorageClient()
	if err != nil {
		return nil, fmt.Errorf("getStorageClient: %w", err)
	}
	defer cancel()

	rc, err := storageClient.Bucket(bucketName).Object(objectName).NewReader(ctx)
	if err != nil {
		return nil, fmt.Errorf("Object(%q).NewReader: %w", objectName, err)
	}
	defer rc.Close()
	data, err := ioutil.ReadAll(rc)
	if err != nil {
		return nil, fmt.Errorf("ioutil.ReadAll: %w", err)
	}

	return data, nil
}

// GetStorageObjectMetadata retrieves only the metadata for a Storage Object.
func GetStorageObjectMetadata(bucketName string, objectName string) (map[string]string, error) {
	storageClient, ctx, cancel, err := getStorageClient()
	if err != nil {
		return nil, fmt.Errorf("getStorageClient: %w", err)
	}
	defer cancel()
	objAttrs, err := storageClient.Bucket(bucketName).Object(objectName).Attrs(ctx)
	if err != nil && strings.Contains(err.Error(), "doesn't exist") {
		return map[string]string{}, nil
	} else if err != nil {
		return nil, fmt.Errorf("Attrs: %w", err)
	}
	return objAttrs.Metadata, nil
}

// DeleteStorageObject deletes looks in the bucket at "bucketName" and deletes the object at "objectName".
func DeleteStorageObject(bucketName string, objectName string) error {
	storageClient, ctx, cancel, err := getStorageClient()
	if err != nil {
		return fmt.Errorf("getStorageClient: %w", err)
	}
	defer cancel()

	o := storageClient.Bucket(bucketName).Object(objectName)
	err = o.Delete(ctx)
	if err != nil {
		return fmt.Errorf("Object(%q).Delete: %w", objectName, err)
	}
	return nil
}

// GetStorageObjectName can turn a collection name like "data", a firestore ID for the Data document, and the object's
// key in .Bytes into the storage object name in GCP.
func GetStorageObjectName(collectionName string, firestoreID string, key string) string {
	return collectionName + "_" + firestoreID + "_" + key
}

// GetStorageObjectArrayName is similar to GetStorageObjectName, but it allows for arrays of [] bytes, and therefore
// supports the ABytes data type in Data.
//
// index is used by [][]byte to indicate which index in the array this object is present at.
func GetStorageObjectArrayName(collectionName string, firestoreID string, key string, index int) string {
	return collectionName + "_" + firestoreID + "_" + key + "/" + strconv.Itoa(index)
}

func GetKeyFromStorageObjectName(objectName string) string {
	split := strings.Split(objectName, "_")
	out := ""
	for i, s := range split {
		if i == len(split)-1 {
			n, _ := url.PathUnescape(s)
			out += n
		} else if i > 1 {
			n, _ := url.PathUnescape(s)
			out += n + "_"
		}
	}
	return out
}

// GetBlobBucket returns the name of the Google Cloud Bucket / S3 container / etc. into which to store blobs.
// This should be the same across all environments except for localhost, which won't have one.
func GetBlobBucket() string {
	var b string
	switch common.GetCloudProvider() {
	case "GOOGLE":
		b = os.Getenv("BUCKET")
	default:
		b = os.Getenv("BUCKET")
	}
	if b == "" {
		// happens if bucket hasn't been called yet was never set up with Google environments.
		_, _, _, _ = getStorageClient()
		b = os.Getenv("BUCKET")
	}
	return b
}
