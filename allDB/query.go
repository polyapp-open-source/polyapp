package allDB

import (
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Query implements common.Query via: &Query.
type Query struct {
	// IndustryID is the name of the industry the query will be run against. This is required for Data FirestoreCollection
	// since fields are prefixed with industry_domain_schema.
	IndustryID string
	// DomainID is the name of the domain the query will be run against. This is required for Data FirestoreCollection
	// since fields are prefixed with industry_domain_schema.
	DomainID string
	// Schema is the name of the schema the query will be performed across. This is required for Data FirestoreCollection
	// since fields are prefixed with industry_domain_schema.
	Schema string
	// FirestoreCollection is the name of the firestore collection to perform the query across, like "ux" or "schema"
	FirestoreCollection string
	// Equals requires that in a given document, the key k must equal the value v.
	// If this condition is not met the document is not returned.
	Equals map[string]interface{}
	// ArrayContains is a search for the value 'v' in the array 'k'
	ArrayContains map[string]string
	// SortBy is an ordered list of column keys to sort by.
	SortBy []string
	// SortDirection is an ordered list of directions to sort by - either "asc" or "desc"
	SortDirection []string
	// StartAfterDocumentID is the last document ID in the previous table.
	StartAfterDocumentID string
	// Offset is extremely inefficient.
	Offset int
	// Length is the number of documents to return.
	Length int
}

// AllDBQuery is deprecated because allDB.AllDBQuery stutters. Use Query as allDB.Query instead.
type AllDBQuery = Query

func (q *Query) validate() error {
	if q.Equals == nil {
		return errors.New("Equals was nil")
	}
	err := common.ValidateCollection(q.FirestoreCollection)
	if err != nil {
		return err
	}
	if q.FirestoreCollection == common.CollectionData && (q.IndustryID == "" || q.DomainID == "" || q.Schema == "") {
		return errors.New("must include industry, domain and schema else can't query fields in the data collection")
	}
	if len(q.SortBy) != len(q.SortDirection) {
		return errors.New("SortBy and SortDirection were not the same length")
	}
	return nil
}

// Init ensures this Query will pass validation. FirestoreCollection is always required, but Industry, Domain, and SchemaID
// are not necessarily required, depending on the collection being queried. They are definitely required for the Data collection.
func (q *Query) Init(industryID string, domainID string, schemaID string, firestoreCollection string) {
	q.Equals = make(map[string]interface{})
	q.ArrayContains = make(map[string]string)
	q.SortBy = make([]string, 0)
	q.SortDirection = make([]string, 0)
	q.IndustryID = industryID
	q.DomainID = domainID
	q.Schema = schemaID
	q.FirestoreCollection = firestoreCollection
}

// AddEquals adds a condition to the query results that the key k must equal the string v.
func (q *Query) AddEquals(k string, v string) {
	if q.Equals == nil {
		q.Equals = make(map[string]interface{})
	}
	q.Equals[k] = v
}

// AddArrayContains adds a search for the value 'v' in the array 'k'
func (q *Query) AddArrayContains(k string, v string) error {
	err := q.validate()
	if err != nil {
		return fmt.Errorf("invalid Query: %w", err)
	}
	q.ArrayContains[k] = v
	return nil
}

// AddSortBy declares how the output data should be ordered. k is key and AscDesc is either asc or desc.
// This only applies to QueryRead() calls.
func (q *Query) AddSortBy(k string, AscDesc string) error {
	if k == "" || AscDesc == "" {
		return errors.New("not all inputs were provided")
	}
	if AscDesc != "asc" && AscDesc != "desc" {
		return errors.New("invalid AscDesc value")
	}
	q.SortBy = append(q.SortBy, k)
	q.SortDirection = append(q.SortDirection, AscDesc)
	return nil
}

// SetOffset is extremely expensive. Sorry.
func (q *Query) SetOffset(n int) {
	q.Offset = n
}

// SetStartAfterDocumentID is the last row in the last query results. We need to start our new query after this document.
func (q *Query) SetStartAfterDocumentID(id string) {
	if id != "" {
		q.StartAfterDocumentID = id
	}
}

// SetLength sets the maximum number of documents we are returning.
func (q *Query) SetLength(length int) {
	if length > 10_000 {
		q.Length = 10_000
	} else if length > 0 {
		q.Length = length
	}
}

// QueryRead retrieves all Data matching the request conditions.
//
// To view the next document in the query results call q.Next()
//
// The request is over if err == common.IterDone
func (q *Query) QueryRead() (common.Iter, error) {
	err := q.validate()
	if err != nil {
		return nil, fmt.Errorf("invalid Query: %w", err)
	}
	return q.queryRead()
}

// QueryEquals returns at most one common.Queryable where key == value, or nil if no Queryable was found.
// If more than one Queryable is returned for this key == value an error is thrown.
func QueryEquals(firestoreCollection string, key string, value string) (common.Queryable, error) {
	q := Query{}
	if firestoreCollection == common.CollectionData {
		ind, dom, sch, _ := common.SplitField(key)
		q.Init(ind, dom, sch, firestoreCollection)
	} else {
		q.Init("", "", "", firestoreCollection)
	}
	q.AddEquals(key, value)
	q.SetLength(2)
	iter, err := q.QueryRead()
	if err != nil {
		return nil, fmt.Errorf("q.QueryRead: %w", err)
	}
	var toReturn common.Queryable
	count := 0
	for {
		queryable, err := iter.Next()
		if err != nil && err == common.IterDone {
			break
		}
		if err != nil {
			return nil, fmt.Errorf("iter.Next: %w", err)
		}
		count++
		if count > 1 {
			return nil, fmt.Errorf("more than one result found for EqualsQuery key %v value %v", key, value)
		}
		toReturn = queryable
	}
	if toReturn == nil || toReturn.GetFirestoreID() == "" {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("collection %v key %v value %v", firestoreCollection, key, value))
	}
	return toReturn, nil
}
