//+build js

package allDB

import (
	"errors"
	"fmt"

	"gitlab.com/polyapp-open-source/polyapp/common"
	"gitlab.com/polyapp-open-source/polyapp/idbCRUD"
)

func createData(industryID string, domainID string, data *common.Data) error {
	s, err := data.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Create(common.CollectionData, data.FirestoreID, s)
}

func readData(industryID string, domainID string, firestoreID string) (common.Data, error) {
	dMap, err := idbCRUD.Read(common.CollectionData, firestoreID)
	if err != nil {
		return common.Data{}, fmt.Errorf("could not Read: %w", err)
	}
	d := common.Data{
		FirestoreID: firestoreID,
	}
	d.Init(dMap)
	return d, errors.New("not finished")
}

func updateData(industryID string, domainID string, data *common.Data) error {
	s, err := data.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Update(common.CollectionData, data.FirestoreID, s)
}

func deleteData(industryID string, domainID string, firestoreID string) error {
	return idbCRUD.Delete(common.CollectionData, firestoreID)
}

func deprecateData(industryID string, domainID string, firestoreID string) error {
	return idbCRUD.Deprecate(common.CollectionData, firestoreID)
}
func createSchema(industryID string, domainID string, schema *common.Schema) error {
	s, err := schema.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Create(common.CollectionSchema, schema.FirestoreID, s)
}

func readSchema(industryID string, domainID string, firestoreID string) (common.Schema, error) {
	dMap, err := idbCRUD.Read(common.CollectionSchema, firestoreID)
	if err != nil {
		return common.Schema{}, fmt.Errorf("could not Read: %w", err)
	}
	d := common.Schema{
		FirestoreID: firestoreID,
	}
	err = d.Init(dMap)
	return d, errors.New("not finished")
}

func updateSchema(industryID string, domainID string, schema *common.Schema) error {
	s, err := schema.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Update(common.CollectionSchema, schema.FirestoreID, s)
}

func deleteSchema(industryID string, domainID string, firestoreID string) error {
	return idbCRUD.Delete(common.CollectionSchema, firestoreID)
}

func deprecateSchema(industryID string, domainID string, firestoreID string) error {
	return idbCRUD.Deprecate(common.CollectionSchema, firestoreID)
}
func createUser(industryID string, domainID string, user *common.User) error {
	s, err := user.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Create(common.CollectionUser, user.FirestoreID, s)
}

func readUser(industryID string, domainID string, firestoreID string) (common.User, error) {
	dMap, err := idbCRUD.Read(common.CollectionUser, firestoreID)
	if err != nil {
		return common.User{}, fmt.Errorf("could not Read: %w", err)
	}
	d := common.User{
		FirestoreID: firestoreID,
	}
	err = d.Init(dMap)
	return d, errors.New("not finished")
}

func updateUser(industryID string, domainID string, user *common.User) error {
	s, err := user.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Update(common.CollectionUser, user.FirestoreID, s)
}

func deleteUser(industryID string, domainID string, firestoreID string) error {
	return idbCRUD.Delete(common.CollectionUser, firestoreID)
}

func deprecateUser(industryID string, domainID string, firestoreID string) error {
	return idbCRUD.Deprecate(common.CollectionUser, firestoreID)
}
func createUX(industryID string, domainID string, ux *common.UX) error {
	s, err := ux.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Create(common.CollectionUX, ux.FirestoreID, s)
}

func readUX(industryID string, domainID string, firestoreID string) (common.UX, error) {
	dMap, err := idbCRUD.Read(common.CollectionUX, firestoreID)
	if err != nil {
		return common.UX{}, fmt.Errorf("could not Read: %w", err)
	}
	d := common.UX{
		FirestoreID: firestoreID,
	}
	err = d.Init(dMap)
	return d, errors.New("not finished")
}

func updateUX(industryID string, domainID string, ux *common.UX) error {
	s, err := ux.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Update(common.CollectionUX, ux.FirestoreID, s)
}

func deleteUX(industryID string, domainID string, firestoreID string) error {
	return idbCRUD.Delete(common.CollectionUX, firestoreID)
}

func deprecateUX(industryID string, domainID string, firestoreID string) error {
	return idbCRUD.Deprecate(common.CollectionUX, firestoreID)
}
func createTask(industryID string, domainID string, task *common.Task) error {
	s, err := task.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Create(common.CollectionTask, task.FirestoreID, s)
}

func readTask(industryID string, domainID string, firestoreID string) (common.Task, error) {
	dMap, err := idbCRUD.Read(common.CollectionTask, firestoreID)
	if err != nil {
		return common.Task{}, fmt.Errorf("could not Read: %w", err)
	}
	d := common.Task{
		FirestoreID: firestoreID,
	}
	err = d.Init(dMap)
	return d, errors.New("not finished")
}

func updateTask(industryID string, domainID string, task *common.Task) error {
	s, err := task.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Update(common.CollectionTask, task.FirestoreID, s)
}

func deleteTask(industryID string, domainID string, firestoreID string) error {
	return idbCRUD.Delete(common.CollectionTask, firestoreID)
}

func deprecateTask(industryID string, domainID string, firestoreID string) error {
	return idbCRUD.Deprecate(common.CollectionTask, firestoreID)
}
func createRole(industryID string, domainID string, role *common.Role) error {
	s, err := role.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Create(common.CollectionRole, role.FirestoreID, s)
}

func readRole(industryID string, domainID string, firestoreID string) (common.Role, error) {
	dMap, err := idbCRUD.Read(common.CollectionRole, firestoreID)
	if err != nil {
		return common.Role{}, fmt.Errorf("could not Read: %w", err)
	}
	d := common.Role{
		FirestoreID: firestoreID,
	}
	err = d.Init(dMap)
	return d, errors.New("not finished")
}

func updateRole(industryID string, domainID string, role *common.Role) error {
	s, err := role.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Update(common.CollectionRole, role.FirestoreID, s)
}

func deleteRole(industryID string, domainID string, firestoreID string) error {
	return idbCRUD.Delete(common.CollectionRole, firestoreID)
}

func deprecateRole(industryID string, domainID string, firestoreID string) error {
	return idbCRUD.Deprecate(common.CollectionRole, firestoreID)
}

func createBot(industryID string, domainID string, bot *common.Bot) error {
	s, err := bot.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Create(common.CollectionBot, bot.FirestoreID, s)
}

func readBot(industryID string, domainID string, firestoreID string) (common.Bot, error) {
	dMap, err := idbCRUD.Read(common.CollectionBot, firestoreID)
	if err != nil {
		return common.Bot{}, fmt.Errorf("could not Read: %w", err)
	}
	d := common.Bot{
		FirestoreID: firestoreID,
	}
	err = d.Init(dMap)
	return d, errors.New("not finished")
}

func updateBot(industryID string, domainID string, bot *common.Bot) error {
	s, err := bot.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Update(common.CollectionBot, bot.FirestoreID, s)
}

func deleteBot(industryID string, domainID string, firestoreID string) error {
	return idbCRUD.Delete(common.CollectionBot, firestoreID)
}

func createAction(industryID string, domainID string, action *common.Action) error {
	s, err := action.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Create(common.CollectionAction, action.FirestoreID, s)
}

func readAction(industryID string, domainID string, firestoreID string) (common.Action, error) {
	dMap, err := idbCRUD.Read(common.CollectionAction, firestoreID)
	if err != nil {
		return common.Action{}, fmt.Errorf("could not Read: %w", err)
	}
	d := common.Action{
		FirestoreID: firestoreID,
	}
	err = d.Init(dMap)
	return d, errors.New("not finished")
}

func updateAction(industryID string, domainID string, action *common.Action) error {
	s, err := action.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Update(common.CollectionAction, action.FirestoreID, s)
}

func deleteAction(industryID string, domainID string, firestoreID string) error {
	return idbCRUD.Delete(common.CollectionAction, firestoreID)
}

func createPublicMap(industryID string, domainID string, publicMap *common.PublicMap) error {
	s, err := publicMap.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Create(common.CollectionPublicMap, publicMap.FirestoreID, s)
}

func readPublicMap(industryID string, domainID string, firestoreID string) (common.PublicMap, error) {
	dMap, err := idbCRUD.Read(common.CollectionPublicMap, firestoreID)
	if err != nil {
		return common.PublicMap{}, fmt.Errorf("could not Read: %w", err)
	}
	d := common.PublicMap{
		FirestoreID: firestoreID,
	}
	err = d.Init(dMap)
	return d, errors.New("not finished")
}

func updatePublicMap(industryID string, domainID string, publicMap *common.PublicMap) error {
	s, err := publicMap.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Update(common.CollectionPublicMap, publicMap.FirestoreID, s)
}

func deletePublicMap(industryID string, domainID string, firestoreID string) error {
	return idbCRUD.Delete(common.CollectionPublicMap, firestoreID)
}

func createCSS(industryID string, domainID string, CSS *common.CSS) error {
	s, err := CSS.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Create(common.CollectionCSS, CSS.FirestoreID, s)
}

func readCSS(industryID string, domainID string, firestoreID string) (common.CSS, error) {
	dMap, err := idbCRUD.Read(common.CollectionCSS, firestoreID)
	if err != nil {
		return common.CSS{}, fmt.Errorf("could not Read: %w", err)
	}
	d := common.CSS{
		FirestoreID: firestoreID,
	}
	err = d.Init(dMap)
	return d, errors.New("not finished")
}

func updateCSS(industryID string, domainID string, CSS *common.CSS) error {
	s, err := CSS.Simplify()
	if err != nil {
		return fmt.Errorf("could not simplify: %w", err)
	}
	return idbCRUD.Update(common.CollectionCSS, CSS.FirestoreID, s)
}

func deleteCSS(industryID string, domainID string, firestoreID string) error {
	return idbCRUD.Delete(common.CollectionCSS, firestoreID)
}
