package handlers

import (
	"bytes"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/gen"
	"strconv"
	"time"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// InternalPOST is the entry point for responding ot all internal POST requests.
//
// Internal calls are used for testing, like test pages at /test/
func InternalPOST(request common.POSTRequest) (common.POSTResponse, error) {
	response := common.POSTResponse{}
	response.Init(request)

	var w bytes.Buffer
	err := gen.GenFormError(gen.FormError{
		Text: "alert warning text",
	}, &w)
	if err != nil {
		return common.POSTResponse{}, fmt.Errorf("failed alert generation: %w", err)
	}
	response.ModDOMs = append(response.ModDOMs, common.ModDOM{
		DeleteSelector: "#labeledInputTextAlert",
		InsertSelector: "#labeledInputText",
		Action:         "afterend",
		HTML:           w.String(),
	})

	return response, nil
}

// InternalGET is the entry point for responding ot all internal GET requests.
func InternalGET(request common.GETRequest) (common.GETResponse, error) {
	shell, err := AppShellHTML(&common.UX{Title: common.String("Polyapp Internal")})
	if err != nil {
		return common.GETResponse{}, fmt.Errorf("error in AppShellHTML: %w", err)
	}
	inner := []byte("no handling for this page in InternalGET")
	switch request.IndustryID {
	case "polyappNone":
		// links to all of our test pages
		inner = []byte(`<button id="click me">click me</button><br><a href="/">/</a><br>
<a href="/t/task">/t/task</a><br><a href="/t/polyappNone">/t/polyappNone</a><br>
<a href="/t/Test/Test/Test/Test?ux=Test&schema=Test&data=Test">/t/Test/Test/Test/Test</a><br>
<h1>Component Testing</h1>
<h3>Standard Components</h3>
<a href="/test/fieldComponents">Single Field Components</a><br>
<a href="/test/nestedComponents">Nested Components for Refs</a><br>
<a href="/test/fullscreenButtons">Fullscreen Buttons</a><br>
<a href="/test/giantLinks">Giant Links</a><br>
<a href="/test/dataTable?industry=polyapp&domain=polyapp&schema=contextualize">Data Table</a><br>
<a href="/test/iFrame">IFrame</a><br>
<a href="/test/images">Images</a><br>
<a href="/test/videos">Videos</a><br>
<a href="/test/audio">Audio</a><br>
<a href="/test/list">List</a><br>
<a href="/test/blobUpload">Blob Upload</a><br>
<a href="/test/time">Time</a><br>
<a href="/test/SelectID">Select ID</a><br>
<a href="/test/browserAPIs">Browser APIs</a><br>
<a href="/test/location">Location Controls</a><br>
<h3>Relationship Components</h3>
<a href="/test/coupledFields">Coupled Fields Test</a><br>
<a href="/test/oneToOne">One To One Components</a><br>
<a href="/test/oneToMany">One to Many Components</a><br>
<a href="/test/manyToMany">Many To Many Components</a><br>
<a href="/test/manyToOne">Many To One Components</a>
<h3>UI Testing</h3>
<a href="/test/cover">Cover</a>
<h3>Test Tasks</h3>
<a href="/t/Wet%20Corn%20Milling/Mill%20Management/AYDWWouNpVkdgT24ukvU?industry=polyapp&domain=Task&ux=KryBXXH5GakJfibTQVTo&schema=polyappTask&data=YxdgqCBmnjNcwyItImFteKCPa">Task Design for an existing Task</a><br>
<a href="/t/Custom%20Computer%20Programming%20Services/Website%20Builder/bSNKnlBA2bXhltkwogjH?ux=Vuv0ceGZVQh7rbQ8oORy&schema=qhpYpZc3L9LDnyf7jgEI">Website Editor</a><br>
<a href="https://polyappstaticdev.com">Public to Internal: polyappstaticdev.com should be a static test page.</a><br>
`)
	case "nestedComponents":
		inner, err = nestedComponents()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("nestedComponents failure: %w", err)
		}
	case "fullscreenButtons":
		inner, err = fullscreenButtons()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("fullscreenButtons failure: %w", err)
		}
	case "giantLinks":
		inner, err = giantLinks()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("giantLinks failure: %w", err)
		}
	case "dataTable":
		inner, err = dataTable()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("dataTable failure: %w", err)
		}
	case "fieldComponents":
		inner, err = fieldComponents()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("fieldComponents failure: %w", err)
		}
	case "iFrame":
		inner, err = iFrame()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("iFrame failure: %w", err)
		}
	case "images":
		inner, err = images()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("images: %w", err)
		}
	case "videos":
		inner, err = videos()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("videos: %w", err)
		}
	case "audio":
		inner, err = audio()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("audio: %w", err)
		}
	case "list":
		inner, err = list()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("list: %w", err)
		}
	case "coupledFields":
		inner, err = coupledFields()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("coupledFields: %w", err)
		}
	case "oneToOne":
		inner, err = oneToOne()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("oneToOne failure: %w", err)
		}
	case "oneToMany":
		inner, err = oneToMany()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("oneToMany failure: %w", err)
		}
	case "manyToMany":
		inner, err = manyToMany()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("manyToMany failure: %w", err)
		}
	case "manyToOne":
		inner, err = manyToOne()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("manyToOne failure: %w", err)
		}
	case "cover":
		inner, err = cover()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("cover failure: %w", err)
		}
	case "blobUpload":
		inner, err = blobUpload()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("blobUpload: %w", err)
		}
	case "time":
		inner, err = showTime()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("showTime: %w", err)
		}
	case "SelectID":
		inner, err = selectID()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("SelectID: %w", err)
		}
	case "browserAPIs":
		inner, err = browserAPIs()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("browserAPIs: %w", err)
		}
	case "location":
		inner, err = location()
		if err != nil {
			return common.GETResponse{}, fmt.Errorf("location: %w", err)
		}
	}
	html, err := InnerIntoAppShell(inner, shell)
	if err != nil {
		return common.GETResponse{}, fmt.Errorf("InnerIntoAppShell: %w", err)
	}
	return common.GETResponse{HTML: html}, nil
}

// nestedComponents are used when there is a Ref inside Data or an array of Refs inside Data.
func nestedComponents() ([]byte, error) {
	referencedData := &common.Data{
		FirestoreID: "ReferencedDoc",
		IndustryID:  "polyapp",
		DomainID:    "polyapp",
		SchemaID:    "polyapp",
		Deprecated:  common.Bool(false),
		S: map[string]*string{
			"nested 1": common.String("nested value 1"),
			"nested 2": common.String("nested value 2"),
		},
		F: map[string]*float64{
			"nested F1": common.Float64(11.1),
			"nested F2": common.Float64(22.2),
		},
		I: map[string]*int64{
			"nested I1": common.Int64(11),
			"nested I2": common.Int64(22),
		},
		B: map[string]*bool{
			"nested B1": common.Bool(true),
			"nested B2": common.Bool(false),
		},
		Ref: map[string]*string{
			"doubly nested Ref": common.String("DoublyNestedRefID"),
		},
	}
	referencedSchema := &common.Schema{
		FirestoreID: "ReferencedSchema",
		IndustryID:  "polyapp",
		DomainID:    "polyapp",
		SchemaID:    "polyapp",
		DataTypes: map[string]string{
			"nested 1":          "S",
			"nested 2":          "S",
			"nested F1":         "F",
			"nested F2":         "F",
			"nested I1":         "I",
			"nested I2":         "I",
			"nested B1":         "B",
			"nested B2":         "B",
			"doubly nested Ref": "/polyapp/polyapp/ReferencedTask?data=" + referencedData.FirestoreID + "&schema=ReferencedSchema&ux=Something",
		},
		DataHelpText: map[string]string{
			"nested 1":          "S",
			"nested 2":          "S",
			"nested F1":         "F",
			"nested F2":         "F",
			"nested I1":         "I",
			"nested I2":         "I",
			"nested B1":         "B",
			"nested B2":         "B",
			"doubly nested Ref": "/polyapp/polyapp/ReferencedTask?data=" + referencedData.FirestoreID + "&schema=ReferencedSchema&ux=Something",
		},
		DataKeys: []string{
			"nested 1",
			"nested 2",
			"nested F1",
			"nested F2",
			"nested I1",
			"nested I2",
			"nested B1",
			"nested B2",
			"doubly nested Ref",
		},
	}
	referencedTask := common.Task{
		FirestoreID: "ReferencedTask",
		IndustryID:  "polyapp",
		DomainID:    "polyapp",
		Name:        "A Sub-Task",
		HelpText:    "This Sub-Task is an example of how to create a nested Task manually",
		TaskGoals:   map[string]string{},
		Deprecated:  nil,
	}
	referencedUX, err := createReferencedUX(referencedData, referencedSchema)
	if err != nil {
		return nil, fmt.Errorf("nestedAccordionContent: %w", err)
	}

	mainDoc := common.Data{
		FirestoreID: "Main",
		IndustryID:  "polyapp",
		DomainID:    "polyapp",
		SchemaID:    "polyapp",
		Deprecated:  common.Bool(false),
		Ref: map[string]*string{
			"single Ref": common.String("/t/polyapp/polyapp/ReferencedTask?data=ReferencedDoc&schema=ReferencedSchema&ux=" + referencedUX.FirestoreID),
		},
		S: map[string]*string{
			"something": common.String("something value"),
		},
	}

	var linkInAccordion bytes.Buffer
	err = gen.GenLink(gen.NavigationRef{
		LinkPath: *mainDoc.Ref["single Ref"],
		Text:     "single Ref",
	}, &linkInAccordion)
	if err != nil {
		return nil, fmt.Errorf("ref GenLink: %w", err)
	}

	var innerSubtask bytes.Buffer
	err = gen.GenLabeledInput([]gen.LabeledInput{
		{
			WrapperID:    "innerSubtaskWrapper",
			HelpText:     "innerSubtask",
			Label:        "innerSubtask",
			ID:           "innerSubtask",
			Type:         "text",
			InitialValue: "",
		},
	}, &innerSubtask)
	if err != nil {
		return nil, fmt.Errorf("innerSubtask GenLabeledInput: %w", err)
	}
	err = gen.GenSelectInput(gen.SelectInput{
		HelpText:      "test for select having the right z-index",
		ID:            "selectzindex",
		Label:         "select z-index",
		SelectOptions: []gen.SelectOption{{Value: "uno"}, {Value: "dos"}, {Value: "tres"}, {Value: "uno"}, {Value: "dos"}, {Value: "tres"}, {Value: "uno"}, {Value: "dos"}, {Value: "tres"}},
		MultiSelect:   false,
	}, &innerSubtask)
	if err != nil {
		return nil, fmt.Errorf("innerSubtask GenSelectInput: %w", err)
	}

	var arrayOfSubTasks bytes.Buffer
	err = gen.GenListAccordion(gen.ListAccordion{
		ID:       "_" + common.AddFieldPrefix("polyapp", "polyapp", "Polyapp", "subTask2"),
		Label:    "subTask2",
		HelpText: "subTask2 needs help text I guess",
		RenderedSubTasks: []string{
			`<p>Here is some text for the first rendered subtask</p>`,
			innerSubtask.String(),
		},
		SubTaskNames: []string{
			"Paragraph static Subtask",
			"Inner subtask",
		},
		DisplayNoneRenderedSubTask: `<p>Hello World</p><input id="test id" />`,
		DisplayNoneTitleField:      "Test Added Task",
		SubTaskRefs: []string{
			*mainDoc.Ref["single Ref"],
			*mainDoc.Ref["single Ref"],
		},
		DisplayNoneSubTaskRef: *mainDoc.Ref["single Ref"],
	}, &arrayOfSubTasks)
	if err != nil {
		return nil, fmt.Errorf("GenListAccordion: %w", err)
	}

	var w bytes.Buffer
	err = gen.GenNestedAccordion([]gen.NestedAccordion{
		{
			TitleField:   referencedTask.Name,
			InnerContent: *referencedUX.HTML,
			Ref:          common.CreateRef(referencedData.IndustryID, referencedData.DomainID, referencedTask.FirestoreID, referencedUX.FirestoreID, referencedSchema.FirestoreID, referencedData.FirestoreID),
		},
		// this second nested accordion demonstrates how to create a Link using the Accordion
		{
			TitleField:   "Sub-Task with only a Link",
			InnerContent: linkInAccordion.String(),
			Ref:          *mainDoc.Ref["single Ref"],
		},
		{
			TitleField:   "Sub-Task with an array of Sub-Tasks",
			InnerContent: arrayOfSubTasks.String(),
			Ref:          *mainDoc.Ref["single Ref"],
		},
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenNestedAccordion: %w", err)
	}
	return w.Bytes(), nil
}

// createReferencedUX creates a new UX object for use in the test page.
func createReferencedUX(data *common.Data, schema *common.Schema) (*common.UX, error) {
	var w bytes.Buffer
	allLabeledInputs := make([]gen.LabeledInput, 0)
	for k, v := range data.I {
		allLabeledInputs = append(allLabeledInputs, gen.LabeledInput{
			WrapperID:    k + "Wrapper",
			Label:        k,
			HelpText:     schema.DataHelpText[k],
			ID:           k,
			InitialValue: strconv.FormatInt(*v, 10),
			Type:         "number",
		})
	}
	for k, v := range data.F {
		allLabeledInputs = append(allLabeledInputs, gen.LabeledInput{
			WrapperID:    k + "Wrapper",
			Label:        k,
			HelpText:     schema.DataHelpText[k],
			ID:           k,
			InitialValue: strconv.FormatFloat(*v, 'f', -1, 64), // f' format was picked arbitrarily
			Type:         "number",
		})
	}
	for k, v := range data.B {
		allLabeledInputs = append(allLabeledInputs, gen.LabeledInput{
			WrapperID:    k + "Wrapper",
			Label:        k,
			HelpText:     schema.DataHelpText[k],
			ID:           k,
			InitialValue: strconv.FormatBool(*v),
			Type:         "checkbox",
		})
	}
	for k, v := range data.Ref {
		err := gen.GenLink(gen.NavigationRef{
			LinkPath: *v,
			Text:     k,
		}, &w)
		if err != nil {
			return nil, fmt.Errorf("ref GenLink: %w", err)
		}
	}
	ct := 0
	for k, v := range data.S {
		allLabeledInputs = append(allLabeledInputs, gen.LabeledInput{
			WrapperID:    k + "Wrapper",
			Label:        k,
			HelpText:     schema.DataHelpText[k],
			ID:           k,
			InitialValue: *v,
			Type:         "textarea",
		})
		if ct < 1 {
			err := gen.GenSummernote(gen.Summernote{
				ID: k + "_kljsdjklsd",
			}, &w)
			if err != nil {
				return nil, err
			}
		}
		ct++
	}
	err := gen.GenLabeledInput(allLabeledInputs, &w)
	if err != nil {
		return nil, fmt.Errorf("GenLabeledInput: %w", err)
	}
	return &common.UX{
		FirestoreID: common.GetRandString(25),
		IndustryID:  data.IndustryID,
		DomainID:    data.DomainID,
		SchemaID:    data.SchemaID,
		HTML:        common.String(w.String()),
		Deprecated:  common.Bool(false),
	}, nil
}

// fullscreenButtons composes an innerHTML webpage which is several huge buttons which are tied to different selection options.
// The buttons all have a single label which is at the top of the screen.
// When a button is pressed ??????????
func fullscreenButtons() ([]byte, error) {
	var w bytes.Buffer
	err := gen.GenGiantOptionSelect(gen.GiantOptionSelect{
		WrapperID: "giantOptionSelectWrapper",
		Name:      "giantOptionSelect",
		Label:     "Giant Option Select",
		SelectOptions: []gen.SelectOption{
			{
				Selected: false,
				Value:    "Option 1",
			},
			{
				Selected: true,
				Value:    "Option 2",
			},
			{
				Selected: false,
				Value:    "Option 3",
			},
			{
				Selected: false,
				Value:    "Option 4",
			},
			{
				Selected: false,
				Value:    "Option 5",
			},
		},
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("error generating fullscreen buttons: %w", err)
	}

	return w.Bytes(), nil
}

func giantLinks() ([]byte, error) {
	var w bytes.Buffer
	err := gen.GenGiantLinks(gen.GiantLinks{
		Label: "Giant Links test",
		Links: []gen.NavigationRef{
			{
				LinkPath: "/test/",
				Text:     "Test Homepage",
			},
			{
				LinkPath: "/test/giantLinks",
				Text:     "Giant Links Test Page",
			},
			{
				LinkPath: "/",
				Text:     "Home Page",
			},
		},
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("error generating giant links: %w", err)
	}
	return w.Bytes(), nil
}

func dataTable() ([]byte, error) {
	var w bytes.Buffer
	w.WriteString("<h1>Data Table</h1>")
	err := gen.GenTable(gen.Table{
		ID:       "testDataTable",
		Industry: "polyapp",
		Domain:   "polyapp",
		Schema:   "contextualize",
		Columns: []gen.Column{
			{
				Header: "Industry",
				ID:     "industry",
			},
			{
				Header: "Manual Note",
				ID:     "manualNote",
			},
		},
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenTable: %w", err)
	}
	return w.Bytes(), nil
}

// fieldComponents composes an inner HTML webpage made up of many components which are attached to a single field.
// This means that each component ends up with a single value, although that value could be an array.
// Examples would be input boxes, selection controls, and buttons where there is no relationship between different fields.
func fieldComponents() ([]byte, error) {
	var w bytes.Buffer
	w.WriteString("<h1>Field Components</h1>")
	err := gen.GenLabeledInput([]gen.LabeledInput{
		{
			WrapperID:    "labeledInputTextWrapper",
			HelpText:     "generic",
			Label:        "Labeled Input of Type text. Modify a field to trigger an alert.",
			ID:           "labeledInputText",
			InitialValue: "an initial text",
			Type:         "text",
		},
		{
			WrapperID:    "labeledInputTextareaWrapper",
			HelpText:     "generic",
			Label:        "Labeled Input of Type textarea",
			ID:           "labeledInputTextarea",
			InitialValue: "an initial textarea text",
			Type:         "textarea",
		},
		{
			WrapperID:    "labeledInputCheckboxWrapper",
			HelpText:     "generic",
			Label:        "Labeled Input of Type Checkbox - starts checked",
			ID:           "labeledInputCheckbox",
			InitialValue: "true",
			Type:         "checkbox",
		},
		{
			WrapperID:    "labeledInputTextWrapper",
			HelpText:     "generic",
			Label:        "Radio option 1",
			ID:           "labeledInputRadio",
			InitialValue: "",
			Type:         "radio",
		},
		{
			WrapperID:    "labeledInputRadio2Wrapper",
			HelpText:     "generic",
			Label:        "Radio option 2 - starts checked",
			ID:           "labeledInputRadio",
			InitialValue: "true",
			Type:         "radio",
		},
		{
			WrapperID:    "labeledInputNumberWrapper",
			HelpText:     "generic",
			Label:        "Labeled Input of Type number",
			ID:           "labeledInputNumber",
			InitialValue: "222.0",
			Type:         "number",
		},
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("error generating labeled input: %w", err)
	}

	err = gen.GenSummernote(gen.Summernote{
		ID: "summernoteTest",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenSummernote: %w", err)
	}

	err = gen.GenDiscussionThread(gen.DiscussionThread{
		Label: "Discussion Thread Read Only",
		ID:    "discussionThreadTestReadOnly",
		InitialComments: []gen.DiscussionThreadComment{
			{
				Name:     "Name",
				DateTime: "4/28/2021 at 11:45AM",
				Comment:  "First Comment",
			},
			{
				Name:     "Name",
				DateTime: "4/28/2021 at 11:46AM",
				Comment:  "Second Comment Second Comment Second Comment Second Comment Second Comment Second Comment Second Comment Second Comment Second Comment Second Comment Second Comment Second Comment Second Comment Second Comment Second Comment Second Comment Second Comment Second Comment Second Comment Second Comment",
			},
		},
		Readonly: true,
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenDiscussionThread: %w", err)
	}
	err = gen.GenDiscussionThread(gen.DiscussionThread{
		Label: "Discussion Thread With Initial Values",
		ID:    "discussionThreadTestWithInitialValues",
		InitialComments: []gen.DiscussionThreadComment{
			{
				Name:     "Name",
				DateTime: "4/28/2021 at 11:45AM",
				Comment:  "First Comment",
			},
			{
				Name:     "Name",
				DateTime: "4/28/2021 at 11:46AM",
				Comment:  "Second Comment",
			}},
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenDiscussionThread: %w", err)
	}
	err = gen.GenDiscussionThread(gen.DiscussionThread{
		Label: "Discussion Thread No Initial Values",
		ID:    "discussionThreadTestNoInitialValues",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenDiscussionThread: %w", err)
	}

	err = gen.GenSelectInput(gen.SelectInput{
		WrapperID: "selectInput1Wrapper",
		HelpText:  "generic",
		ID:        "selectInput1",
		Label:     "Select Input without multi select",
		SelectOptions: []gen.SelectOption{
			{
				Selected: false,
				Value:    "option uno",
			},
			{
				Selected: false,
				Value:    "option dos option dos option dos option dos option dos option dos option dos option dos option dos option dos",
			},
		},
		MultiSelect: false,
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("non-multi-select error: %w", err)
	}

	err = gen.GenSelectInput(gen.SelectInput{
		WrapperID: "selectInput2Wrapper",
		HelpText:  "generic",
		ID:        "selectInput2",
		Label:     "Select Input WITH multi select",
		SelectOptions: []gen.SelectOption{
			{
				Selected: true,
				Value:    "option one",
			},
			{
				Selected: false,
				Value:    "option 2",
			},
		},
		MultiSelect: true,
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("multi-select error: %w", err)
	}

	err = gen.GenSearchLabeledInput(gen.SearchLabeledInput{
		Label:        "Search Labeled Input",
		HelpText:     "generic",
		ID:           "searchLabeledInput",
		InitialValue: "hello",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("search labeled input error: %w", err)
	}

	err = gen.GenListLabeledInput(gen.ListLabeledInput{
		HelpText:      "generic",
		Label:         "List Labeled Input Text",
		ID:            "Industry_Domain_Schema_listLabeledInputText",
		InitialValues: nil,
		Type:          "text",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("list labeled input text error: %w", err)
	}
	err = gen.GenListLabeledInput(gen.ListLabeledInput{
		HelpText:      "generic",
		Label:         "List Labeled Input Number",
		ID:            "Industry_Domain_Schema_listLabeledInputNumber",
		InitialValues: []string{"110.5", "2", "5", "6"},
		Type:          "number",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("list labeled input number error: %w", err)
	}
	err = gen.GenListLabeledInput(gen.ListLabeledInput{
		HelpText:      "generic",
		Label:         "List Labeled Input Checkbox",
		ID:            "Industry_Domain_Schema_listLabeledInputCheckbox",
		InitialValues: []string{"true", ""},
		DefaultValue:  "true",
		Type:          "checkbox",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("list labeled input checkbox error: %w", err)
	}

	err = gen.GenIconButton(gen.IconButton{
		ID:        "iconButton",
		ImagePath: "/assets/giraffe192.jpg",
		Text:      "Giraffe Image",
		AltText:   "Polyapp's Giraffe Logo",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("icon button error: %w", err)
	}
	err = gen.GenTextButton(gen.TextButton{
		ID:      "textButton",
		Text:    "Success",
		Styling: "success",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("text button error: %w", err)
	}

	return w.Bytes(), nil
}

// iFrame composes an inner HTML webpage which is simply a wrapper for the iFrame component
func iFrame() ([]byte, error) {
	var w bytes.Buffer
	w.WriteString("<h1>IFrame is below</h1>")
	err := gen.GenIFrame(gen.IFrame{
		ID:     "iFrame",
		Source: "/test/fieldComponents",
		Title:  "Field Components Test Page",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenIFrame: %w", err)
	}
	return w.Bytes(), nil
}

func images() ([]byte, error) {
	var w bytes.Buffer
	var err error
	w.WriteString("<h1>Image Control testing</h1>")
	err = gen.GenImage(gen.Image{
		ID:      "test1b",
		URL:     "/assets/giraffe192.jpg",
		AltText: "Polyapp's Giraffe logo",
		Width:   "",
		Height:  "",
		Caption: "",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenImage for responsive WITHOUT a width and height: %w", err)
	}
	w.WriteString("<br>")
	err = gen.GenImage(gen.Image{
		ID:      "test1",
		URL:     "/assets/giraffe192.jpg",
		AltText: "Polyapp's Giraffe logo",
		Width:   "100px",
		Height:  "100px",
		Caption: "",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenImage for responsive with a width and height: %w", err)
	}
	w.WriteString("<br>")
	err = gen.GenImage(gen.Image{
		ID:      "test2",
		URL:     "/assets/giraffe192.jpg",
		AltText: "Polyapp's Giraffe logo",
		Width:   "",
		Height:  "",
		Caption: "Giraffe sees you without any width or height limits",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenImage for captioned: %w", err)
	}
	w.WriteString("<br>")
	err = gen.GenImage(gen.Image{
		ID:      "test2c",
		URL:     "/assets/giraffe192.jpg",
		AltText: "Polyapp's Giraffe logo",
		Width:   "100px",
		Height:  "100px",
		Caption: "Giraffe sees you with a width and height of 100px",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenImage for captioned with a width and height: %w", err)
	}
	w.WriteString("<br>")
	err = gen.GenImageUpload(gen.ImageUpload{
		ID:       "testupload",
		Label:    "This is the upload label",
		HelpText: "some help text",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenImageUpload for labeled: %w", err)
	}
	err = gen.GenImageUpload(gen.ImageUpload{
		ID:       "testuploadNoLabel",
		Label:    "",
		HelpText: "should not be displayed",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenImageUpload without a label: %w", err)
	}
	return w.Bytes(), nil
}

func videos() ([]byte, error) {
	var w bytes.Buffer
	var err error
	err = gen.GenVideo(gen.Video{
		ID:       "testVideoID",
		VideoURL: "https://www.youtube.com/watch?v=y0BA7iX14Wo",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenVideo: %w", err)
	}
	return w.Bytes(), nil
}

func audio() ([]byte, error) {
	var w bytes.Buffer
	var err error
	err = gen.GenAudioUpload(gen.AudioUpload{
		ID:       "polyapp_polyapp_polyapp_TestAudioUpload",
		Label:    "",
		HelpText: "",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenAudioUpload: %w", err)
	}
	err = gen.GenAudioPlayer(gen.AudioPlayer{
		Title:    "Audio Title LooooooooooOsad jlj asdlk asdlkasdl ends here",
		Subtitle: "Gregory Towner Frederickson the Third and Morty \"The Horny\" Platypus",
		Style:    "light",
		Src:      "/blob/assets/1pM97iASr3RZ3yoTuf6A/Custom%20Computer%20Programming%20Services_Website%20Builder_IHwWTkidajDaPgTlLykzplJbb_BodyBlobFIbmC3kmwq31111Fq34FPLrD7.mp3",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenVideo: %w", err)
	}
	return w.Bytes(), nil
}

func list() ([]byte, error) {
	var w bytes.Buffer
	var err error
	err = gen.GenList(gen.List{
		ID: "polyapp_gen_list_field name",
		ListItems: []gen.ListItem{
			{
				Heading:       "a heading",
				SmallTopRight: "",
				Content:       "",
				SmallBottom:   "",
			},
			{
				Heading:       "",
				SmallTopRight: "small top right",
				Content:       "",
				SmallBottom:   "",
			},
			{
				Heading:       "",
				SmallTopRight: "",
				Content:       "some content",
				SmallBottom:   "",
			},
			{
				Heading:       "",
				SmallTopRight: "",
				Content:       "",
				SmallBottom:   "small bottom",
			},
			{
				Heading:       "second heading",
				SmallTopRight: "second small top right",
				Content:       "some content",
				SmallBottom:   "small bottom",
			},
		},
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenList: %w", err)
	}

	err = gen.GenTaskList([]gen.TaskListItem{
		{
			ID:             "id1",
			Name:           "Test Task Name",
			HelpText:       "Help Text",
			ChangeDataLink: "/",
			NewDataLink:    "/polyappChangeData",
			EditTaskLink:   "/",
		},
		{
			ID:             "id2",
			Name:           "Second",
			HelpText:       "2 Help Text",
			ChangeDataLink: "/polyappChangeData",
			NewDataLink:    "/",
			EditTaskLink:   "/polyappChangeData",
		},
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenTaskList: %w", err)
	}

	err = gen.GenReportList([]gen.ReportListItem{
		{
			ID:             "id1",
			Name:           "Test Report Name",
			HelpText:       "Report Help Text",
			EditReportLink: "/t/polyapp/Reporting/ltygzBCYpjlqeXRhSVPZtzJKb?ux=cYdflemxtmQCdlsgiGVcsgFpm&schema=nMyfcJQJSykcdLpvjqFRZXkeO&data=eEqksgyeJyqoNOIJSkKYuvKKD",
			ViewReportLink: "/blob/assets/eEqksgyeJyqoNOIJSkKYuvKKD/polyapp_Reporting_nMyfcJQJSykcdLpvjqFRZXkeO_Chart%20HTML?cacheBuster=TSpjttGkhH",
		},
		{
			ID:             "id2",
			Name:           "Second Report",
			HelpText:       "2 Report Help Text",
			EditReportLink: "/t/polyapp/Reporting/zIoQjcmACDOdQsrKckUmmzLTZ?ux=GjxnRTamLcVfyALAMjpWaywYm&schema=IYrpkFurrcTnXqbLuMawFknMd&data=MLjOxrwtBNgQpPhaDRyEoxvNB",
			ViewReportLink: "/blob/assets/MLjOxrwtBNgQpPhaDRyEoxvNB/polyapp_Reporting_IYrpkFurrcTnXqbLuMawFknMd_Table%20HTML?cacheBuster=lasdlas",
		},
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenReportList: %w", err)
	}

	err = gen.GenReportList([]gen.ReportListItem{
		{
			Name:           "Checking Next report name",
			HelpText:       "Report Help Text",
			EditReportLink: "/t/polyapp/Reporting/ltygzBCYpjlqeXRhSVPZtzJKb?ux=cYdflemxtmQCdlsgiGVcsgFpm&schema=nMyfcJQJSykcdLpvjqFRZXkeO&data=eEqksgyeJyqoNOIJSkKYuvKKD",
			ViewReportLink: "/blob/assets/eEqksgyeJyqoNOIJSkKYuvKKD/polyapp_Reporting_nMyfcJQJSykcdLpvjqFRZXkeO_Chart%20HTML?cacheBuster=TSpjttGkhH",
		},
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenReportList: %w", err)
	}

	return w.Bytes(), nil
}

func coupledFields() ([]byte, error) {
	var w bytes.Buffer
	var err error
	err = gen.GenLabeledInput([]gen.LabeledInput{
		{
			HelpText:     "this field should populate the other fields downstream",
			Label:        "Type Here to Update other fields on this page",
			ID:           "industry_domain_schema_field name_309832089230923",
			Type:         "text",
			InitialValue: "hello",
		},
		{
			HelpText: "downstream",
			Label:    "text field",
			ID:       "industry_domain_schema_field name_9804398023802",
			Type:     "text",
		},
		{
			HelpText: "downstream",
			Label:    "textarea",
			ID:       "industry_domain_schema_field name_8905380208",
			Type:     "textarea",
		},
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenListLabeledInput: %w", err)
	}
	err = gen.GenSelectInput(gen.SelectInput{
		HelpText:      "downstream",
		ID:            "industry_domain_schema_field name_0894358020",
		Label:         "select",
		SelectOptions: []gen.SelectOption{gen.SelectOption{Value: "option 1"}, gen.SelectOption{Value: "option 2"}},
		MultiSelect:   false,
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenSelectInput: %w", err)
	}
	err = gen.GenTextButton(gen.TextButton{
		ID:      "industry_domain_schema_field name_9084308920",
		Text:    "text from server",
		Styling: "primary",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenTextButton: %w", err)
	}
	err = gen.GenSummernote(gen.Summernote{
		ID: "industry_domain_schema_field name_290230923",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenSummernote: %w", err)
	}
	err = gen.GenStaticContent(gen.StaticContent{
		ID:      "industry_domain_schema_field name_902902394",
		Content: "Content which should be overwritten",
		Tag:     "h6",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenStaticContent: %w", err)
	}
	err = gen.GenStaticContent(gen.StaticContent{
		ID:      "industry_domain_schema_something else",
		Content: "some paragraph content",
		Tag:     "p",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenStaticContent: %w", err)
	}
	err = gen.GenImageUpload(gen.ImageUpload{
		ID:    "industry_domain_schema_field name_9028902083",
		Label: "Downstream",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenImageUpload: %w", err)
	}
	err = gen.GenImage(gen.Image{
		ID:      "industry_domain_schema_field name_9035083082308",
		URL:     "/assets/giraffe192.jpg",
		AltText: "some giraffe",
		Width:   "299",
		Height:  "400",
		Caption: "hello there",
		Layout:  "Title and Caption Below",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenImage: %w", err)
	}
	err = gen.GenSearchLabeledInput(gen.SearchLabeledInput{
		Label:              "Search labeled input",
		HelpText:           "This thing should be downstream?",
		ID:                 "industry_domain_schema_field name_92893498289120",
		InitialValue:       "nothing",
		QueryPrefixedField: "polyapp_Schema_polyappSchema_DataKeys",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenSearchLabeledInput: %w", err)
	}

	w.WriteString("<h2>Coupled Array Fields</h2>")
	err = gen.GenListLabeledInput(gen.ListLabeledInput{
		HelpText:      "this is a list. is that going to work as well as the single fields? probably not.",
		Label:         "list for coupling",
		ID:            "industry_domain_schema_array field_90503",
		InitialValues: nil,
		DefaultValue:  "",
		Type:          "text",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenListLabeledInput: %w", err)
	}
	err = gen.GenListLabeledInput(gen.ListLabeledInput{
		HelpText: "this is a list.",
		Label:    "second list for coupling",
		ID:       "industry_domain_schema_array field_9203",
		Type:     "text",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenListLabeledInput 2: %w", err)
	}

	w.WriteString("<h2>Subtasks</h2>")
	w.WriteString("<p>Check that:</p>")
	w.WriteString("<ul><li>The first field is not experiencing inter-Task coupling</li><li>The second field is coupled to the Accordion's name</li><li>The third field is just there</li></ul>")
	var subTasks bytes.Buffer
	err = gen.GenLabeledInput([]gen.LabeledInput{
		{
			Label:        "first input shoudl not be coupled",
			HelpText:     "no coupling!",
			ID:           "industry_domain_schema_subtaskNoCouple",
			InitialValue: "22",
			Type:         "text",
			Readonly:     false,
		},
		{
			Label:        "second input should populate the subtask's name in the accordion",
			HelpText:     "please be coupled!",
			ID:           "industry_domain_schema_subTaskCoupledToName",
			InitialValue: "coupled name",
			Type:         "text",
		},
	}, &subTasks)
	if err != nil {
		return nil, fmt.Errorf("GenLabeledInput: %w", err)
	}
	err = gen.GenSummernote(gen.Summernote{
		ID: "industry_domain_schema_someField",
	}, &subTasks)
	if err != nil {
		return nil, fmt.Errorf("GenSummernote: %w", err)
	}
	err = gen.GenListAccordion(gen.ListAccordion{
		ID:                         "_industry_domain_schema_subtasks and stuff",
		Label:                      "Coupled Accordion",
		HelpText:                   "This is testing the coupling in n accordion. Check that: <ul><li>The first field is not experiencing inter-Task coupling</li><li>The second field is coupled to the Accordion's name</li></ul>",
		DisplayNoneRenderedSubTask: subTasks.String(),
		DisplayNoneTitleField:      "industry_domain_schema_subTaskCoupledToName",
		DisplayNoneSubTaskRef:      common.CreateRef("industry", "domain", "task", "ux", "schema", "polyappShouldBeOverwritten"),
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenListAccordion: %w", err)
	}

	err = gen.GenFullTextSearch(gen.FullTextSearch{
		ID:         "testID",
		Label:      "test task search",
		HelpText:   "some help text",
		Readonly:   false,
		Collection: "task",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenFullTextSearch: %w", err)
	}
	return w.Bytes(), nil
}

// oneToOne composes an inner HTML webpage made up of many components which show a relationship between 2 fields.
func oneToOne() ([]byte, error) {
	var w bytes.Buffer
	w.WriteString("<h1>One to One Components</h1>")
	err := gen.GenOneToOneFromTo([]gen.OneToOneFromTo{
		{
			WrapperID:        "wrapFromTo",
			HelpText:         "generic",
			FromID:           "FromText",
			FromLabel:        "From _something_",
			FromInitialValue: "initial from",
			FromType:         "text",
			ToID:             "ToText",
			ToLabel:          "To _something_",
			ToInitialValue:   "blegh",
			ToType:           "textarea",
		},
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("GenOneToOneFromTo error: %w", err)
	}
	return w.Bytes(), nil
}

// oneToMany composes an inner HTML webpage made up of many components which show a relationship between 1
// field and many other fields.
func oneToMany() ([]byte, error) {
	var w bytes.Buffer
	return w.Bytes(), nil
}

// manyToMany composes an inner HTML webpage made up of many components which show a relationship between many
// fields and many other fields.
func manyToMany() ([]byte, error) {
	var w bytes.Buffer
	return w.Bytes(), nil
}

// manyToOne composes an inner HTML webpage made up of many components which show a relationship between many
// fields and a single other fields
func manyToOne() ([]byte, error) {
	var w bytes.Buffer
	return w.Bytes(), nil
}

// cover composes an inner HTML webpage made up only of a Cover, which is an HTML component used on websites.
func cover() ([]byte, error) {
	var w bytes.Buffer
	var coverBuf bytes.Buffer
	fontLinks, err := gen.GenCover(gen.Cover{
		Scaling:         "Resize",
		MediaURL:        "/assets/giraffe192.jpg",
		AltText:         "Giraffe",
		BackgroundColor: "#000000",
		TextColor:       "#058959",
		Heading:         "Heading which is pretty long",
		HeadingFont:     "Varela Round",
		Subheading:      "Subheading which is pretty long, and even longer than the heading",
		SubheadingFont:  "Roboto",
	}, &coverBuf)
	if err != nil {
		return nil, fmt.Errorf("gen.GenCover: %w", err)
	}
	w.WriteString(fontLinks)
	w.Write(coverBuf.Bytes())
	return w.Bytes(), nil
}

// blobUpload tests uploading an arbitrary blob of data.
func blobUpload() ([]byte, error) {
	var w bytes.Buffer
	var err error
	err = gen.GenUpload(gen.Upload{
		ID:       "uploadID",
		Label:    "Upload Label",
		HelpText: "Upload Help Text",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenUpload: %w", err)
	}
	return w.Bytes(), nil
}

// showTime tests the time control.
func showTime() ([]byte, error) {
	var w bytes.Buffer
	var err error
	err = gen.GenTime(gen.Time{
		ID:           "timeID",
		Label:        "Time Label",
		HelpText:     "Time Help Text",
		InitialValue: 10,
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenTime: %w", err)
	}
	err = gen.GenTime(gen.Time{
		ID:           "timeID2",
		Label:        "Time Label",
		HelpText:     "Time Help Text",
		InitialValue: time.Now().Unix(),
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenTime: %w", err)
	}

	err = gen.GenTime(gen.Time{
		ID:           "timeID3",
		Label:        "Date",
		HelpText:     "Time Help Text",
		InitialValue: time.Now().Unix(),
		Format:       "L",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenTime: %w", err)
	}

	err = gen.GenTime(gen.Time{
		ID:           "timeID4",
		Label:        "Time Only",
		HelpText:     "Time Help Text",
		InitialValue: time.Now().Unix(),
		Format:       "LT",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenTime: %w", err)
	}

	err = gen.GenTime(gen.Time{
		ID:           "timeID5",
		Label:        "Time Only Readonly",
		HelpText:     "Time Help Text",
		InitialValue: time.Now().Unix(),
		Format:       "LT",
		Readonly:     true,
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenTime: %w", err)
	}

	return w.Bytes(), nil
}

func selectID() ([]byte, error) {
	var w bytes.Buffer
	var err error

	err = gen.GenListSelectID(gen.ListSelectID{
		Label:      "List of Data",
		HelpText:   "Data Help Text",
		ID:         "industry_domain_schema_field name",
		Collection: common.CollectionData,
		Field:      "polyapp_Reporting_nMyfcJQJSykcdLpvjqFRZXkeO_Domain",
		InitialValues: []gen.ListSelectIDInitialValue{
			{
				HumanValue: "human value",
				IDValue:    "idlist1",
			},
			{
				HumanValue: "human value 2",
				IDValue:    "idlist2",
			},
		},
	}, &w)

	err = gen.GenSelectID(gen.SelectID{
		Label:             "Data",
		HelpText:          "Data Help Text",
		ID:                "datatest",
		InitialHumanValue: "",
		Collection:        common.CollectionData,
		Field:             "polyapp_Reporting_nMyfcJQJSykcdLpvjqFRZXkeO_Domain",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenSelectID: %w", err)
	}
	err = gen.GenSelectID(gen.SelectID{
		Label:             "Data With Initial Value",
		HelpText:          "Data Help Text",
		ID:                "datatest2",
		InitialHumanValue: "Action",
		InitialIDValue:    "mNDvgiPUNuifZQhlLBEkYcCiy",
		Collection:        common.CollectionData,
		Field:             "polyapp_Reporting_nMyfcJQJSykcdLpvjqFRZXkeO_Domain",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenSelectID: %w", err)
	}
	err = gen.GenSelectID(gen.SelectID{
		Label:             "Bot",
		HelpText:          "Data Help Text",
		ID:                "bottest",
		InitialHumanValue: "",
		Collection:        common.CollectionBot,
		Field:             "Name",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenSelectID: %w", err)
	}
	err = gen.GenSelectID(gen.SelectID{
		Label:             "Action",
		HelpText:          "Data Help Text",
		ID:                "actiontest",
		InitialHumanValue: "",
		Collection:        common.CollectionAction,
		Field:             "Name",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenSelectID: %w", err)
	}
	err = gen.GenSelectID(gen.SelectID{
		Label:             "Task",
		HelpText:          "Data Help Text",
		ID:                "tasktest",
		InitialHumanValue: "",
		Collection:        common.CollectionTask,
		Field:             "Name",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenSelectID: %w", err)
	}
	err = gen.GenSelectID(gen.SelectID{
		Label:             "Schema",
		HelpText:          "Data Help Text",
		ID:                "schematest",
		InitialHumanValue: "",
		Collection:        common.CollectionSchema,
		Field:             "Name",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenSelectID: %w", err)
	}
	err = gen.GenSelectID(gen.SelectID{
		Label:             "UX",
		HelpText:          "Data Help Text",
		ID:                "uxtest",
		InitialHumanValue: "",
		Collection:        common.CollectionUX,
		Field:             "Title",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenSelectID: %w", err)
	}
	err = gen.GenSelectID(gen.SelectID{
		Label:             "User",
		HelpText:          "Data Help Text",
		ID:                "usertest",
		InitialHumanValue: "",
		Collection:        common.CollectionUser,
		Field:             "FullName",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenSelectID: %w", err)
	}
	err = gen.GenSelectID(gen.SelectID{
		Label:             "Role",
		HelpText:          "Data Help Text",
		ID:                "roletest",
		InitialHumanValue: "",
		Collection:        common.CollectionRole,
		Field:             "Name",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenSelectID: %w", err)
	}

	return w.Bytes(), nil
}

func browserAPIs() ([]byte, error) {
	var w bytes.Buffer
	var err error
	err = gen.GenGeolocationAPI(gen.GeolocationAPI{
		ID:       "gengeolocationapi",
		Label:    "Geolocation Element",
		HelpText: "Geolocation Help Text",
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenGeolocationAPI: %w", err)
	}
	return w.Bytes(), nil
}

func location() ([]byte, error) {
	var w bytes.Buffer
	var err error
	err = gen.GenPlaceAutocomplete(gen.PlaceAutocomplete{
		Label:    "genplaceautocomplete label",
		HelpText: "genplaceautocomplete help text",
		ID:       "genplaceautocomplete",
		Readonly: false,
	}, &w)
	if err != nil {
		return nil, fmt.Errorf("gen.GenPlaceAutocomplete: %w", err)
	}
	return w.Bytes(), nil
}
