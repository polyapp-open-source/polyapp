package handlers

import (
	"bytes"
	"fmt"
	"net/url"
	"strings"

	"gitlab.com/polyapp-open-source/polyapp/gen"

	"gitlab.com/polyapp-open-source/polyapp/common"
)

// CheckGoalsSatisfied goes through every Goal in the Task and checks if it is satisfied.
// userErrors map from Data keys to Alerts which specify severity and the error message.
// userErrors are complete sentences you should show underneath a field if it has an error.
//
// appendIDToKey is typically true if the Task / Data are for a subtask or part of an array with itemwise validation (itemwise validation is not implemented right now).
func CheckGoalsSatisfied(t *common.Task, d *common.Data, appendIDToKey bool) (userErrors map[string]gen.FormError, err error) {
	userErrors = make(map[string]gen.FormError)
	for k := range t.TaskGoals {
		userError, err := common.TaskGoalEvaluate(k, t, d)
		if err != nil {
			return nil, fmt.Errorf("TaskGoalEvaluate for task ("+t.FirestoreID+") and data ("+d.FirestoreID+"): %w", err)
		}
		userErrorKey := k
		if appendIDToKey {
			userErrorKey += "_" + d.FirestoreID
		}
		if userError != "" {
			userErrors[userErrorKey] = gen.FormError{
				Text: userError,
			}
		}
	}
	return userErrors, nil
}

// UserErrorModDOMs returns an array of ModDOM objects you can use to show user errors.
//
// userErrors is the output of CheckGoalsSatisfied. Errors are placed just below the fields they occurred in if possible.
//
// formWrappingID is the div wrapping the form. Task level errors are placed just after this.
// If the form contains a "Done" button you could use that ID instead.
//
// The first 2 entries always wipes out all error entries if wipe is true. This is sort of a legacy code path; it may be removed after
// July if it's still in use.
func UserErrorModDOMs(formWrappingID string, userErrors map[string]gen.FormError, wipe bool) ([]common.ModDOM, error) {
	var out []common.ModDOM
	i := 0
	if wipe {
		out = make([]common.ModDOM, len(userErrors)+2)
		out[0] = common.ModDOM{
			DeleteSelector: "[id^=polyappError] > *",
		}
		out[1] = common.ModDOM{
			DeleteSelector: "#" + common.CSSEscape("polyappError"+formWrappingID),
			InsertSelector: "#" + common.CSSEscape(formWrappingID),
			Action:         "beforeend",
			HTML:           `<div id="polyappError` + formWrappingID + `"></div>`,
		}
		i = 2
	} else {
		out = make([]common.ModDOM, len(userErrors))
	}

	var w bytes.Buffer
	for k, v := range userErrors {
		err := gen.GenFormError(v, &w)
		if err != nil {
			return nil, fmt.Errorf("GenFormError: %w", err)
		}
		if strings.HasPrefix(k, "polyappError") {
			k = url.PathEscape(k)
			cssKey := common.CSSEscape(k)
			// field-specific errors
			out[i] = common.ModDOM{
				DeleteSelector:   "#" + cssKey + " > *",
				InsertSelector:   "#" + cssKey,
				Action:           "afterbegin",
				HTML:             w.String(),
				AddClassSelector: "#" + strings.TrimPrefix(cssKey, "polyappError"),
				AddClasses:       []string{"is-invalid"},
			}
		} else {
			// task-level errors
			out[i] = common.ModDOM{
				InsertSelector: "#" + common.CSSEscape("polyappError"+formWrappingID),
				Action:         "afterend",
				HTML:           w.String(),
			}
		}
		i++
		w.Reset()
	}
	return out, nil
}
