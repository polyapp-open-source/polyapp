# Polyapp: Free, Business Oriented Form Builder
Polyapp is a powerful, business-oriented form builder. You can try building a form [on the public instance](https://polyapp-public.appspot.com/t/polyapp/TaskSchemaUX/gqyLWclKrvEaWHalHqUdHghrt?ux=hRpIEGPgKvxSzvnCeDpAVcwgJ&schema=gjGLMlcNApJyvRjmgQbopsXPI&data=new&user=dBxKPjxhvUbznNCPaIMsgoSpI) - your browser will authenticate you and then refresh automatically.

**Quick Links**

[Documentation and Video Tutorials.](https://polyapp-open-source.gitlab.io/polyapp-docs/)

[Godoc.](https://pkg.go.dev/gitlab.com/polyapp-open-source/polyapp)

[MIT License.](./LICENSE)

[Security Advisories, License Analysis, and Dependencies.](https://deps.dev/go/gitlab.com%2Fpolyapp-open-source%2Fpolyapp)

We also use the vulnerability scanning provided by Gitlab (a set of static analysis tools) to identify and resolve potential vulnerabilities.

# Use Cases
* Polyapp is extremely flexible. You can use it to create a minimum viable product for any type of form based software.
* Polyapp lets you change workflows without talking to a developer, so business departments which need forms to collect data like purchase requests or requests for time off can create those forms themselves.
* Polyapp records users as they move through the system. This makes it work well when you need an audit trail of work or to track employee productivity, as with repetitive, form-based tasks which process some structured data.
* Polyapp's forms are easy to modify, and you can call external code too. This makes Polyapp a great way to display data retrieved from a 3rd party API and view that data. If the 3rd party API changes, removing a field or adding a new one, it is a simple matter of updating your own form to mirror the API changes.
* Polyapp scales well when many documents are being edited at the same time, but does not support collaboration - more than one person in one document. This means it works well for collecting data from company-wide surveys or being a form linked from your website. It does not work as a Word or Google Docs replacement.

# Limitations
Only up to date versions of Chrome, Edge, Safari, and Firefox are supported. Internet Explorer, Samsung Internet, and other browsers are not supported.

Functional limitations have Issues associated with them in Gitlab.

# Get Started
You can use the public instance to experiment with Polyapp: https://polyapp-public.appspot.com/

[Documentation and Video Tutorials.](https://polyapp-open-source.gitlab.io/polyapp-docs/)

To set up your own instance please follow the [Installation Guide](./INSTALL_GUIDE.md)


# Developing with Polyapp
Developing with Polyapp has 3 steps:
1. [Install Polyapp into your own Google Cloud Project.](INSTALL_GUIDE.md)
2. Create [Task(s)](https://polyapp-open-source.gitlab.io/polyapp-docs/fundamentals/tasks.html), [Chart(s)](https://polyapp-open-source.gitlab.io/polyapp-docs/fundamentals/reporting.html), and [Table Report(s)](https://polyapp-open-source.gitlab.io/polyapp-docs/fundamentals/reporting.html) which meet your needs.
3. All custom code / behavior should be done with [Microservice Bots. See a complete video tutorial explaining how to create these here.](https://polyapp-open-source.gitlab.io/polyapp-docs/advanced/MicroserviceBotDevelopment.html)

To understand Polyapp on a technical level you should read [DESIGN.md](DESIGN.md).

# Contribute
Contributions are welcome. The only rules for contributions are that they must follow ["Effective Go"](https://golang.org/doc/effective_go.html) and they must work. Please read [DESIGN.md](DESIGN.md) to get a general overview of the project and its packages.


