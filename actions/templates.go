package actions

import (
	"bytes"
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"io/ioutil"
	textTemplate "text/template"
	"time"
)

type TextTemplate struct {
	BotStaticData      []string `suffix:"Bot Static Data"`
	TextTemplate       string   `suffix:"Text Template"`
	ReportStorageField string   `suffix:"Report Storage Field"`
	BotID              string   `suffix:"Bot ID"`
	ReportStorageData  string   `suffix:"Report Storage Data"`
	Description        string   `suffix:"Description"`
	Name               string   `suffix:"Name"`
}

// ActionTextTemplate A Text Template is a document template created using a what you see is what you get document editor.
// After you have created the document, you can add template variables which are populated from a Data. For example,
// let's imagine that you regularly submit a textual report which outlines the work you conducted for a client.
// Let's further say that the work you conducted was performed with Polyapp. You could use this Task to create a
// text-based report, add in template variables which will be populated from the Data like "{{.Name}}",
// and then use the Bot and Bot Static Data configuration information listed here to add the report to a Task.
// When you click "Done" on the Task, the report is generated and stored in a configurable location for your convenience.
func ActionTextTemplate(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	TextTemplate := TextTemplate{}
	err = common.DataIntoStructure(data, &TextTemplate)
	if err != nil {
		return fmt.Errorf("common.DataIntoStructure: %w", err)
	}

	// Input validation
	switch TextTemplate.ReportStorageData {
	case "Task Data":
	default:
		return errors.New("unhandled TextTemplate.ReportStorageData value: " + TextTemplate.ReportStorageData)
	}

	TextTemplate.BotID = "xBIPBkTBlfWWrFkDnInORJMPU"
	TextTemplate.BotStaticData = []string{
		"TextTemplateReportCreatorConfigData " + data.FirestoreID,
	}

	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &TextTemplate, data)
	if err != nil {
		return fmt.Errorf("common.StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}

	response.NewURL = common.CreateURLFromRequest(*request)

	return nil
}

var funcMap = textTemplate.FuncMap{
	"time": func(i *int64) (time.Time, error) {
		return time.Unix(*i, 0), nil
	},
}

// ActionTextTemplateValidate ensures the "Text Template" field is valid.
// If it is invalid it inserts an error into the DOM.
//
// Bot ID is ZDKOBDoDcsQoHsSjbOlEzNjAC
func ActionTextTemplateValidate(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	textTemplateValue := data.S[common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, "Text Template")]
	if textTemplateValue == nil {
		return errors.New("no field found at Text Template")
	}
	temp, err := textTemplate.New("").Funcs(funcMap).Parse(*textTemplateValue)
	if err != nil {
		return fmt.Errorf("textTemplate.New.Parse for Text Template: %w", err)
	}
	m := make(map[string]interface{})
	err = temp.Execute(ioutil.Discard, m)
	if err != nil {
		return fmt.Errorf("temp.Execute: %w", err)
	}

	return nil
}

func ActionTextTemplateReportCreator(data *common.Data, botStaticData map[string]string) error {
	var err error
	configDataID := botStaticData["TextTemplateReportCreatorConfigData"]
	if configDataID == "" {
		return errors.New("Bot Static Data TextTemplateReportCreatorConfigData was not populated with a Data ID")
	}

	configData := common.Data{
		FirestoreID: configDataID,
	}
	err = allDB.Read(&configData)
	if err != nil {
		return fmt.Errorf("allDB.Read: %w", err)
	}
	TextTemplate := TextTemplate{}
	err = common.DataIntoStructure(&configData, &TextTemplate)
	if err != nil {
		return fmt.Errorf("common.DataIntoStructure: %w", err)
	}

	unprefixedData, err := dataToMapRecursive(data, false, "PascalCase", false)
	if err != nil {
		return fmt.Errorf("dataToMapRecursive: %w", err)
	}

	var out bytes.Buffer
	temp, err := textTemplate.New("").Funcs(funcMap).Parse(TextTemplate.TextTemplate)
	if err != nil {
		return fmt.Errorf("textTemplate.New.Parse for template (%v): %w", TextTemplate.TextTemplate, err)
	}
	err = temp.Execute(&out, unprefixedData)
	if err != nil {
		return fmt.Errorf("temp.Execute: %w", err)
	}

	simplifiedData, err := data.Simplify()
	if err != nil {
		return fmt.Errorf("data.Simplify: %w", err)
	}
	simplifiedData[common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, TextTemplate.ReportStorageField)] = out.Bytes()
	err = data.Init(simplifiedData)
	if err != nil {
		return fmt.Errorf("data.Init: %w", err)
	}
	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}

	return nil
}
