package actions

import (
	"bytes"
	secretmanager "cloud.google.com/go/secretmanager/apiv1"
	"context"
	"encoding/base64"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/fileCRUD"
	"gitlab.com/polyapp-open-source/polyapp/integrity"
	secretmanagerpb "google.golang.org/genproto/googleapis/cloud/secretmanager/v1"
	"html/template"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	textTemplate "text/template"
	"time"

	"cloud.google.com/go/firestore"
	"gitlab.com/polyapp-open-source/polyapp/firestoreCRUD"
	"google.golang.org/api/iterator"

	"gitlab.com/polyapp-open-source/polyapp/gen"

	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
)

type editAWebsite struct {
	Domain              string               `json:"Domain"`
	Template            string               `json:"Template"`
	BrowserIcon         []byte               `json:"Browser Icon (favicon)"`
	AnalyticsSnippet    string               `json:"Analytics Snippet"`
	Font                string               `json:"Font"`
	ExportToJSON        bool                 `json:"Export to JSON"`
	Theme               theme                `json:"Theme"`
	Pages               []page               `json:"Pages"`
	WordySections       []wordySection       `json:"Wordy Sections"`
	CoverSections       []createCover        `json:"Cover Sections"`
	GallerySections     []gallerySection     `json:"Gallery Sections"`
	ImageSections       []imageSection       `json:"Image Sections"`
	AudioSections       []audioSection       `json:"Audio Sections"`
	VideoSections       []videoSection       `json:"Video Sections"`
	InteractiveSections []interactiveSection `json:"Interactive Sections"`
}

type theme struct {
	PrimaryColor        string
	SecondaryColor      string
	SuccessColor        string
	InfoColor           string
	WarningColor        string
	DangerColor         string
	LightColor          string
	DarkColor           string
	BodyBackgroundColor string
	BodyColor           string
}

type page struct {
	PageTitle           string
	PageDescription     string
	SectionTitles       []string
	Layout              string
	SecondaryNavigation []string
	Hidden              bool
	linkPath            string // set programatically
}

type wordySection struct {
	Title           string
	Body            string
	BackgroundColor string
}

type gallerySection struct {
	Title         string
	Images        [][]byte
	Titles        []string
	ImageLayout   string
	GalleryLayout string
	FirestoreID   string
}

type imageSection struct {
	Title              string
	Picture            []byte
	Caption            string
	PictureDescription string
	PriceID            string
	OnClickLink        string
	Layout             string
	FirestoreID        string
}

type audioSection struct {
	Title       string
	Subtitle    string
	Audio       []byte
	PriceID     string
	FirestoreID string
}

type videoSection struct {
	SectionTitle string
	VideoURL     string
	OverlayTitle string
	Autoplay     bool
	Layout       string
	FirestoreID  string
}

type interactiveSection struct {
	Title                   string
	FormTemplate            string
	EmailForFormSubmissions string
	FinishURL               string
	BackgroundColor         string
}

// ActionEditAWebsite interprets the contents of this Task as a website builder and overwrites the existing website with this Task's data.
//
// This Action replaces "ActionEditWebsiteContent".
// TODO try to eliminate a lot of this code by using func DataIntoStructure
func ActionEditAWebsite(data *common.Data, request *common.POSTRequest) error {
	// if not Edit Website - User && not Edit Website - Admin
	var editWebsiteDoc common.Data
	if data.SchemaID != "vnfQkHXwRtPzBnSxwAjnPhZFx" && data.SchemaID != "ADdkaAyYPQwXOLkMfGUGqoPsl" {
		// maybe we're a full task and we need to look among sub-tasks.
		for k, v := range data.Ref {
			if strings.HasSuffix(k, "_Edit a Website - User") {
				greq, err := common.ParseRef(*v)
				if err != nil {
					return fmt.Errorf("common.ParseRef %v: %w", *v, err)
				}
				refData := &common.Data{
					FirestoreID: greq.DataID,
				}
				err = allDB.Read(refData)
				if err != nil {
					return fmt.Errorf("allDB.ReadData for greq %v: %w", greq.DataID, err)
				}
				editWebsiteDoc = *data
				data = refData
				break
			}
		}
	}
	if data.SchemaID != "vnfQkHXwRtPzBnSxwAjnPhZFx" && data.SchemaID != "ADdkaAyYPQwXOLkMfGUGqoPsl" {
		return fmt.Errorf("ActionEditAWebsite does not seem to have a compatible Data. Its schema ID: " + data.SchemaID)
	}
	e := editAWebsite{}
	for k, v := range data.S {
		if strings.HasSuffix(k, "_Domain") && v != nil {
			e.Domain = *v
		} else if strings.HasSuffix(k, "_Template") && v != nil {
			e.Template = *v
		} else if strings.HasSuffix(k, "_Analytics Snippet") && v != nil {
			e.AnalyticsSnippet = *v
		} else if strings.HasSuffix(k, "_Font Name") && v != nil {
			var familyName strings.Builder
			for _, c := range *v {
				if c == ':' || c == '&' {
					break
				} else {
					familyName.WriteRune(c)
				}
			}
			e.Font = `<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=` + strings.ReplaceAll(*v, " ", "+") + `"><style>body { font-family: '` + familyName.String() + `'; }</style>`
		}
	}
	for k, v := range data.B {
		if strings.HasSuffix(k, "_Export to JSON") && v != nil {
			e.ExportToJSON = *v
		}
	}
	if e.ExportToJSON {
		// have to export each individual data document
		// Since we are calling Init here we can omit it later too.
		err := fileCRUD.Init("")
		if err != nil {
			return fmt.Errorf("fileCRUD.Init: %w", err)
		}
	}
	for k, v := range data.Ref {
		if strings.HasSuffix(k, "_Website Demo") && v != nil && e.ExportToJSON {
			iFrameSubtaskReq, err := common.ParseRef(*v)
			if err != nil {
				return fmt.Errorf("common.ParseRef %v: %w", *v, err)
			}
			iFrameData := common.Data{
				FirestoreID: iFrameSubtaskReq.DataID,
			}
			err = allDB.Read(&iFrameData)
			if err != nil {
				return fmt.Errorf("allDB.ReadData %v: %w", iFrameSubtaskReq.DataID, err)
			}
			err = fileCRUD.Create(&iFrameData)
			if err != nil {
				return fmt.Errorf("fileCRUD.Create for iFrameData (%v): %w", iFrameData.FirestoreID, err)
			}
		}
	}
	logoKey := common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, "Browser Icon (favicon)")
	var err error
	e.BrowserIcon, err = allDB.ReadDataBytes(data.FirestoreID, logoKey)
	if err != nil {
		return fmt.Errorf("allDB.ReadDataBytes: %w", err)
	}
	chanLength := 0
	errChan := make(chan error)
	for k, v := range data.ARef {
		if strings.HasSuffix(k, "_Pages") && v != nil {
			var l sync.Mutex
			if e.Pages != nil && len(e.Pages) > 0 {
				return errors.New("more than one _Pages key")
			}
			e.Pages = make([]page, len(v))
			chanLength += len(v)
			for i := range v {
				go func(index int, ref string) {
					// I am assuming that the values of e.ExportToJSON and e.Pages do not change between goroutines.
					p, err := getPageValue(ref, e.ExportToJSON)
					l.Lock()
					e.Pages[index] = p
					l.Unlock()
					errChan <- err
				}(i, v[i])
			}
		} else if strings.HasSuffix(k, "_Wordy Sections") && v != nil {
			var l sync.Mutex
			if e.WordySections != nil && len(e.WordySections) > 0 {
				return errors.New("more than one _Wordy Sections key")
			}
			e.WordySections = make([]wordySection, len(v))
			chanLength += len(v)
			for i := range v {
				go func(i int, ref string) {
					// e.ExportToJSON and e.WordySections should only be edited here.
					w, err := getWordySectionValue(ref, e.ExportToJSON)
					l.Lock()
					e.WordySections[i] = w
					l.Unlock()
					errChan <- err
				}(i, v[i])
			}
		} else if strings.HasSuffix(k, "_Cover Section") && v != nil {
			var l sync.Mutex
			if e.CoverSections != nil && len(e.CoverSections) > 0 {
				return errors.New("more than one _Cover Sections key")
			}
			e.CoverSections = make([]createCover, len(v))
			chanLength += len(v)
			for i := range v {
				go func(i int, ref string) {
					var inter createCover
					err := RefIntoStructure(ref, &inter, e.ExportToJSON)
					l.Lock()
					e.CoverSections[i] = inter
					l.Unlock()
					errChan <- err
				}(i, v[i])
			}
		} else if strings.HasSuffix(k, "_Image Sections") && v != nil {
			var l sync.Mutex
			if e.ImageSections != nil && len(e.ImageSections) > 0 {
				return errors.New("more than one _Image Sections key")
			}
			e.ImageSections = make([]imageSection, len(v))
			chanLength += len(v)
			for i := range v {
				go func(i int, ref string) {
					images, err := getImageSectionValue(ref, e.ExportToJSON)
					l.Lock()
					e.ImageSections[i] = images
					l.Unlock()
					errChan <- err
				}(i, v[i])
			}
		} else if strings.HasSuffix(k, "_Gallery Sections") && v != nil {
			var l sync.Mutex
			if e.GallerySections != nil && len(e.GallerySections) > 0 {
				return errors.New("more than one _Gallery Sections key")
			}
			e.GallerySections = make([]gallerySection, len(v))
			chanLength += len(v)
			for i := range v {
				go func(i int, ref string) {
					gallery, err := getGallerySectionValue(ref, e.ExportToJSON)
					l.Lock()
					e.GallerySections[i] = gallery
					l.Unlock()
					errChan <- err
				}(i, v[i])
			}
		} else if strings.HasSuffix(k, "_Audio Sections") && v != nil {
			var l sync.Mutex
			if e.AudioSections != nil && len(e.AudioSections) > 0 {
				return errors.New("more than one _Audio Sections key")
			}
			e.AudioSections = make([]audioSection, len(v))
			chanLength += len(v)
			for i := range v {
				go func(i int, ref string) {
					a, err := getAudioSectionValue(ref, e.ExportToJSON)
					l.Lock()
					e.AudioSections[i] = a
					l.Unlock()
					errChan <- err
				}(i, v[i])
			}
		} else if strings.HasSuffix(k, "_Video Sections") && v != nil {
			var l sync.Mutex
			if e.VideoSections != nil && len(e.VideoSections) > 0 {
				return errors.New("more than one video section found")
			}

			e.VideoSections = make([]videoSection, len(v))
			chanLength += len(v)
			for i := range v {
				go func(i int, ref string) {
					a, err := getVideoSectionValue(ref, e.ExportToJSON)
					l.Lock()
					e.VideoSections[i] = a
					l.Unlock()
					errChan <- err
				}(i, v[i])
			}
		} else if (strings.HasSuffix(k, "_Interactive Sections") || strings.HasSuffix(k, "_Interactive Sections - Admin")) && v != nil {
			var l sync.Mutex
			if e.InteractiveSections != nil && len(e.InteractiveSections) > 0 {
				return errors.New("more than one _Interactive Sections key")
			}
			e.InteractiveSections = make([]interactiveSection, len(v))
			chanLength += len(v)
			for i := range v {
				go func(i int, ref string) {
					inter, err := getInteractiveSectionValue(ref, e.ExportToJSON)
					l.Lock()
					e.InteractiveSections[i] = inter
					l.Unlock()
					errChan <- err
				}(i, v[i])
			}
		}
	}
	combinedErr := ""
	for i := 0; i < chanLength; i++ {
		localErr := <-errChan
		if localErr != nil {
			combinedErr += "; " + localErr.Error()
		}
	}
	// Why use so many channels? When I was using indexes into arrays for some reason I was getting partially completed
	// struct outputs.
	if combinedErr != "" {
		return errors.New(combinedErr)
	}

	if e.ExportToJSON {
		err := fileCRUD.Create(data)
		if err != nil {
			return fmt.Errorf("fileCRUD.Create for data ("+data.FirestoreID+"): %w", err)
		}
	}

	var navigationBar gen.NavigationBar
	navigationBar.NavigationRefs = make([]gen.NavigationRef, 0)
	for i := range e.Pages {
		if !e.Pages[i].Hidden {
			if len(navigationBar.NavigationRefs) == 0 {
				navigationBar.NavigationRefs = append(navigationBar.NavigationRefs, gen.NavigationRef{
					Text:     e.Pages[i].PageTitle,
					LinkPath: "/",
				})
			} else {
				navigationBar.NavigationRefs = append(navigationBar.NavigationRefs, makeNavigationRef(e.Pages[i].PageTitle))
			}
			e.Pages[i].linkPath = navigationBar.NavigationRefs[len(navigationBar.NavigationRefs)-1].LinkPath
		} else {
			navRef := makeNavigationRef(e.Pages[i].PageTitle)
			e.Pages[i].linkPath = navRef.LinkPath
		}
	}
	if e.Template == "" {
		// This template UX is the "polyapp-template-default.com" website's home page.
		e.Template = "dvVqNfxkSMlkDvkyJqVhawQon"
	}
	TemplateUX, err := allDB.ReadUX(e.Template)
	if err != nil {
		return fmt.Errorf("allDB.ReadUX: %w", err)
	}

	navigationBar.LogoSrc = common.BlobURL(data.FirestoreID, logoKey)
	if strings.Contains(*TemplateUX.Navbar, "navbar-dark") {
		navigationBar.Theme = "dark"
	} else {
		navigationBar.Theme = "light"
	}
	var NavbarBuffer bytes.Buffer
	err = gen.GenNavigationBar(navigationBar, &NavbarBuffer)
	if err != nil {
		return fmt.Errorf("GenNavigationBar: %w", err)
	}

	errChanCreatePage := make(chan error)
	for i := range e.Pages {
		go func(e editAWebsite, i int, request *common.POSTRequest, navigationBar gen.NavigationBar, data *common.Data, TemplateUX common.UX, NavbarBuffer bytes.Buffer, errChan chan error) {
			err = buildPage(e, i, request, navigationBar, data, TemplateUX, NavbarBuffer)
			if err != nil {
				errChan <- fmt.Errorf("buildPage (%v): %w", e.Pages[i].SectionTitles, err)
				return
			}
			errChan <- nil
			return
		}(e, i, request, navigationBar, data, TemplateUX, NavbarBuffer, errChanCreatePage)
	}
	combinedCreatePageErr := ""
	for range e.Pages {
		localErr := <-errChanCreatePage
		if localErr != nil {
			combinedCreatePageErr += localErr.Error() + "; "
		}
	}
	if combinedCreatePageErr != "" {
		return errors.New("combinedCreatePageErr: " + combinedCreatePageErr)
	}

	// In addition to building all of the pages in the database, we also store a copy of some of the data in a way which is
	// indexed by User ID.
	if data.SchemaID == "vnfQkHXwRtPzBnSxwAjnPhZFx" {
		// Do NOT record to UserWebsiteDoc unless we are using the public website builder.
		err = createUserWebsiteDoc(request.UserID, e.Domain, data.FirestoreID, editWebsiteDoc, e.ExportToJSON)
		if err != nil {
			return fmt.Errorf("createUserWebsiteDoc: %w", err)
		}
	}

	return nil
}

// buildPage creates each page individually and stores it in its own PublicMap in the database.
func buildPage(e editAWebsite, i int, request *common.POSTRequest, navigationBar gen.NavigationBar, data *common.Data, TemplateUX common.UX, NavbarBuffer bytes.Buffer) error {
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return fmt.Errorf("firestoreCRUD.GetClient: %w", err)
	}
	var pm *common.PublicMap
	var ux common.UX
	var task common.Task
	var schema common.Schema
	pageURL := e.Domain + e.Pages[i].linkPath
	pm, err = getExistingPublicMap(ctx, client, pageURL)
	if err != nil && strings.Contains(err.Error(), "code = NotFound") {
		pm = &common.PublicMap{
			FirestoreID:  "",
			HostAndPath:  pageURL,
			IndustryID:   "Custom Computer Programming Services",
			DomainID:     "Website Builder",
			UXID:         nil,
			TaskID:       nil,
			SchemaID:     nil,
			OwningUserID: request.UserID,
		}
	} else if err != nil {
		return fmt.Errorf("getExistingPublicMap: %w", err)
	}

	// Get all existing information
	if pm.UXID != nil && *pm.UXID != "" {
		ux, err = allDB.ReadUX(*pm.UXID)
		if err != nil && strings.Contains(err.Error(), "code = NotFound") {
			pm.UXID = nil
			ux.FirestoreID = common.GetRandString(25)
			ux.Init(nil)
			ux.IndustryID = "Custom Computer Programming Services"
			ux.DomainID = "Website Builder"
		} else if err != nil {
			return fmt.Errorf("allDB.ReadUX %v: %w", *pm.UXID, err)
		}
		// Clear out old values. We'll re-build them.
		ux.SelectFields = make(map[string]string)
	} else {
		ux.FirestoreID = common.GetRandString(25)
		ux.Init(nil)
		ux.IndustryID = "Custom Computer Programming Services"
		ux.DomainID = "Website Builder"
	}
	if pm.SchemaID != nil && *pm.SchemaID != "" {
		schema, err = allDB.ReadSchema(*pm.SchemaID)
		if err != nil && strings.Contains(err.Error(), "code = NotFound") {
			pm.SchemaID = nil
			schema.FirestoreID = common.GetRandString(25)
			schema.Init(nil)
			schema.IndustryID = "Custom Computer Programming Services"
			schema.DomainID = "Website Builder"
			schema.SchemaID = schema.FirestoreID
		} else if err != nil {
			return fmt.Errorf("allDB.ReadSchema %v: %w", *pm.SchemaID, err)
		}
		// Clear out old values. We'll re-build them.
		schema.DataTypes = make(map[string]string)
		schema.DataHelpText = make(map[string]string)
		schema.DataKeys = make([]string, 0)
	} else {
		schema.FirestoreID = common.GetRandString(25)
		schema.Init(nil)
		schema.IndustryID = "Custom Computer Programming Services"
		schema.DomainID = "Website Builder"
		schema.SchemaID = schema.FirestoreID
	}
	if pm.TaskID != nil && *pm.TaskID != "" {
		task, err = allDB.ReadTask(*pm.TaskID)
		if err != nil && strings.Contains(err.Error(), "code = NotFound") {
			pm.TaskID = nil
			task.FirestoreID = common.GetRandString(25)
			task.Init(nil)
			task.IndustryID = "Custom Computer Programming Services"
			task.DomainID = "Website Builder"
		} else if err != nil {
			return fmt.Errorf("allDB.ReadTask %v: %w", *pm.TaskID, err)
		}
		// clear out old values from arrays. We must do this to delete old values. As the inputs to the website builder are
		// run through again the information which was stored here will be re-added.
		// If we didn't do this, you would only ever append to BotsTriggered... & never remove.
		task.BotStaticData = make(map[string]string)
		task.BotsTriggeredAtLoad = make([]string, 0)
		task.BotsTriggeredContinuously = make([]string, 0)
		task.BotsTriggeredAtDone = make([]string, 0)
		task.TaskGoals = make(map[string]string)
		task.FieldSecurity = make(map[string]*common.FieldSecurityOptions)
	} else {
		task.FirestoreID = common.GetRandString(25)
		task.Init(nil)
		task.IndustryID = "Custom Computer Programming Services"
		task.DomainID = "Website Builder"
	}
	ux.SchemaID = schema.FirestoreID

	var combinedHTML bytes.Buffer

	if e.Pages[i].SecondaryNavigation != nil && len(e.Pages[i].SecondaryNavigation) > 0 {
		combinedHTML.WriteString(`<div class="container-fluid bg-gray-1 flex-grow-1"><div class="row"><div class="col-lg p-0 bg-gray-1">`)
	}

	// Default as a light background since dark backgrounds often require changes to Theme / are not great with
	// default Bootstrap.
	sectionStart := gen.SectionStart{
		SectionRow: 0,
		IsLastRow:  false,
		Layout:     "",
	}
	if navigationBar.Theme == "dark" {
		// don't ignore true black
		sectionStart.BackgroundColor = "#000001"
	}
	switch e.Pages[i].Layout {
	case "Standard":
	case "2 Column":
		sectionStart.Layout = "2 Column"
	default:
		sectionStart.Layout = e.Pages[i].Layout
	}
	gallerySectionStarted := false
	for ii, sectionTitle := range e.Pages[i].SectionTitles {
		addedToHTML := false
		if len(e.Pages[i].SectionTitles) == ii+1 {
			sectionStart.IsLastRow = true
		}
		sectionStart.SectionRow = ii
		for _, section := range e.WordySections {
			if section.Title == sectionTitle {
				if gallerySectionStarted {
					gen.GenGalleryEnd(&combinedHTML)
					gen.GenSectionEnd(sectionStart, &combinedHTML)
					gallerySectionStarted = false
				}
				// Help Text says we ignore true black as a background color.
				if section.BackgroundColor != "" && section.BackgroundColor != "#000000" {
					sectionStart.BackgroundColor = section.BackgroundColor
				}
				gen.GenSectionStart(sectionStart, &combinedHTML)
				if navigationBar.Theme == "dark" {
					section.Body = strings.ReplaceAll(section.Body, `<table class="table `, `<table class="table-dark `)
				}
				combinedHTML.WriteString(section.Body)
				gen.GenSectionEnd(sectionStart, &combinedHTML)
				addedToHTML = true
				break
			}
		}
		if addedToHTML {
			continue
		}
		for _, section := range e.CoverSections {
			if section.HeadingText == sectionTitle {
				if gallerySectionStarted {
					gen.GenGalleryEnd(&combinedHTML)
					gen.GenSectionEnd(sectionStart, &combinedHTML)
					gallerySectionStarted = false
				}
				oldLayout := sectionStart.Layout
				sectionStart.Layout = "Max Width"
				gen.GenSectionStart(sectionStart, &combinedHTML)
				mediaURL := "/blob/assets/" + section.PolyappFirestoreID + "/" + common.AddFieldPrefix(url.PathEscape(section.PolyappIndustryID), url.PathEscape(section.PolyappDomainID), url.PathEscape(section.PolyappSchemaID), url.PathEscape("Media Upload"))
				links, err := gen.GenCover(gen.Cover{
					Scaling:         section.Scaling,
					MediaURL:        mediaURL,
					AltText:         section.MediaAltText,
					BackgroundColor: section.BackgroundColor,
					TextColor:       section.TextColor,
					Heading:         section.HeadingText,
					HeadingFont:     section.HeadingFont,
					Subheading:      section.SubHeadingText,
					SubheadingFont:  section.SubHeadingFont,
				}, &combinedHTML)
				if err != nil {
					return fmt.Errorf("gen.GenCover: %w", err)
				}
				if ux.HeadTag == nil {
					ux.HeadTag = common.String("")
				}
				ux.HeadTag = common.String(*ux.HeadTag + links)
				gen.GenSectionEnd(sectionStart, &combinedHTML)
				sectionStart.Layout = oldLayout
				addedToHTML = true
				break
			}
		}
		if addedToHTML {
			continue
		}
		for _, section := range e.AudioSections {
			if section.Title == sectionTitle {
				if gallerySectionStarted {
					gen.GenGalleryEnd(&combinedHTML)
					gen.GenSectionEnd(sectionStart, &combinedHTML)
					gallerySectionStarted = false
				}
				gen.GenSectionStart(sectionStart, &combinedHTML)
				s := "dark"
				if navigationBar.Theme == "light" {
					s = "light"
				}
				err = gen.GenAudioPlayer(gen.AudioPlayer{
					Title:    section.Title,
					Subtitle: section.Subtitle,
					Style:    s,
					Src:      common.BlobURL(section.FirestoreID, "Custom Computer Programming Services_Website Builder_uPLvitRcSMqElxwwIkffrwEpS_Audio"),
				}, &combinedHTML)
				if err != nil {
					return fmt.Errorf("gen.GenAudioPlayer %v: %w", section.FirestoreID, err)
				}
				gen.GenSectionEnd(sectionStart, &combinedHTML)
				addedToHTML = true
				break
			}
		}
		if addedToHTML {
			continue
		}
		for _, section := range e.VideoSections {
			if section.SectionTitle == sectionTitle {
				if gallerySectionStarted {
					gen.GenGalleryEnd(&combinedHTML)
					gen.GenSectionEnd(sectionStart, &combinedHTML)
					gallerySectionStarted = false
				}
				combinedHTML.WriteString(`<section class="w-100">`)
				vid := gen.Video{
					ID:          "a" + common.GetRandString(15),
					VideoURL:    section.VideoURL,
					OverlayText: section.OverlayTitle,
					Autoplay:    section.Autoplay,
				}
				switch section.Layout {
				case "Full Screen":
					vid.Height = "100%"
					vid.Width = "100%"
					vid.Fullscreen = true
				default:
					vid.Width = "100%"
					vid.Height = "56.25vw" // 16 : 9 Aspect ratio
				}
				err = gen.GenVideo(vid, &combinedHTML)
				if err != nil {
					return fmt.Errorf("gen.GenVideo: %w", err)
				}
				combinedHTML.WriteString(`</section>`)
				addedToHTML = true
				break
			}
		}
		if addedToHTML {
			continue
		}
		for _, section := range e.InteractiveSections {
			if section.Title == sectionTitle {
				if gallerySectionStarted {
					gen.GenGalleryEnd(&combinedHTML)
					gen.GenSectionEnd(sectionStart, &combinedHTML)
					gallerySectionStarted = false
				}
				var templateSchemaID string
				var templateUXID string
				var templateTaskID string
				switch section.FormTemplate {
				case "Contact":
					templateTaskID = "gVjHrOYrUmXhFOvnnpVUwMBct"
					templateSchemaID = "ldnnbDqEBWjQIrClXGTesexbD"
					templateUXID = "NKCjCeCMqQofMJXPkIdZsukqh"
				case "Polyapp Builder Home Page":
					templateTaskID = "LFbbGyCbHxtYzxDtEmnTSbHdn"
					templateSchemaID = "OsXHxOwclkYHRhwjJEJDnwmMg"
					templateUXID = "QHNyluuXXRYhVcQeYGbOPmtwT"
				case "Polyapp Change Website":
					templateTaskID = "FSrHRkFLhBYInmVCGdwxkDmOQ"
					templateSchemaID = "qPvZukjcWuWXUEkqbtVGZDhrB"
					templateUXID = "WhlvmiCWKFhigqAQPxMEkfRIo"
				case "Polyapp Template Chooser":
					templateTaskID = "sXIroBiXYzzgSYfJJWJDCcTau"
					templateSchemaID = "fnsLThdCWvBLaCMSdtlYHcrTk"
					templateUXID = "KFfbgGNblAtsZnyHzRypZhwJM"
				case "Polyapp Favorite Templates":
					templateTaskID = "tooFabGcVPUOpsFIpOkmTyLcw"
					templateSchemaID = "XJvQlbrSwLMIgellEXWBsOYSo"
					templateUXID = "WZrgvhxMyKnNOVFhDnUTMOFlA"
				case "Polyapp Edit Website":
					templateTaskID = "TUkwjbFhGLLpGTYMxXnDDhywA"
					templateSchemaID = "vnfQkHXwRtPzBnSxwAjnPhZFx"
					templateUXID = "SwfoPdXxHXVcOJjxihIrhEoLN"
				case "Polyapp Request Billing Reminders and Website Tips":
					templateTaskID = "hcaTrFqbtxUkZJxmjTdtMuHky"
					templateSchemaID = "GcrEytQDIbVVdsAdaTfwsmWbf"
					templateUXID = "CXmjazKZAHmiXIxgfLwtOJdzF"
				case "Request a Conversion":
					templateTaskID = "kXNxXtlZEvsolvGGMyDrkWngV"
					templateSchemaID = "xzgrfGjDvhRhtzytIccaRDsLH"
					templateUXID = "mnIRrMcJRwbbWeUizfBanQXnk"
				default:
					return fmt.Errorf("invalid Interactive Section Form Template: %v Title: %v EmailForFormSubmissions: %v", section.FormTemplate, section.Title, section.EmailForFormSubmissions)
				}

				templateUX, err := allDB.ReadUX(templateUXID)
				if err != nil {
					return fmt.Errorf("allDB.ReadUX in Interactive Section %v: %w", templateUXID, err)
				}
				templateTask, err := allDB.ReadTask(templateTaskID)
				if err != nil {
					return fmt.Errorf("allDB.ReadTask for templateTaskID ("+templateTaskID+"): %w", err)
				}
				//templateSchema, err := allDB.ReadSchema(ux.IndustryID, ux.DomainID, templateSchemaID)
				//if err != nil {
				//	return fmt.Errorf("allDB.ReadSchema for templateSchemaID %v: %w", templateSchemaID, err)
				//}
				fieldInParentTask := "_" + schema.IndustryID + "_" + schema.DomainID + "_" + schema.FirestoreID + "_" + templateTask.Name

				// Now add the subtask
				if section.BackgroundColor != "" && section.BackgroundColor != "#000000" {
					sectionStart.BackgroundColor = section.BackgroundColor
				}
				gen.GenSectionTaskStart(sectionStart, common.CreateRef(templateTask.IndustryID, templateTask.DomainID, templateTaskID, templateUXID, templateSchemaID, "polyappShouldBeOverwritten"),
					url.PathEscape(fieldInParentTask), &combinedHTML)
				// Note: Previously there was handling here for moving Done buttons out of subtasks and into the parent.
				// Unfortunately sections are styled together so that leads to some odd-looking Done buttons.
				// To fix, I've removed the code moving the Done button and I'll add some handling to redirect all
				// Done button sets to the mainDataRef in main.js
				buttonIDStartIndex := strings.LastIndex(*templateUX.HTML, `<button type="button" id="`)
				wroteOut := false
				if buttonIDStartIndex > -1 {
					buttonIDStartIndex += len(`<button type="button" id="`)
					buttonIDEndIndex := strings.Index((*templateUX.HTML)[buttonIDStartIndex:], `"`) + buttonIDStartIndex
					if buttonIDEndIndex > -1 {
						doneID := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, "Done")
						combinedHTML.WriteString((*templateUX.HTML)[:buttonIDStartIndex])
						combinedHTML.WriteString(doneID)
						combinedHTML.WriteString((*templateUX.HTML)[buttonIDEndIndex:])
						_, ok := schema.DataHelpText[doneID]
						if !ok {
							schema.DataHelpText[doneID] = "When a user clicks \"Done\" on a form, send an email to the person who created this webpage."
							schema.DataKeys = append(schema.DataKeys, doneID)
							schema.DataTypes[doneID] = "B"
						}
						wroteOut = true
					}
				}
				if !wroteOut {
					// This should be fairly rare since forms generated with the form generator are going to include
					// Done buttons.
					combinedHTML.WriteString(*templateUX.HTML)
				}
				gen.GenSectionEnd(sectionStart, &combinedHTML)

				// We probably want to run any bots associated with this Subtask. Since subtask bots do not run,
				// we must move the bots in this subtask into the parent Task.
				allFound := true
				for _, v := range templateTask.BotsTriggeredAtDone {
					found := false
					for _, v2 := range task.BotsTriggeredAtDone {
						if v2 == v {
							found = true
							break
						}
					}
					if !found {
						allFound = false
						break
					}
				}
				if !allFound {
					// not all bots were found. Please keep in mind that this is definitely not going to work with multiple
					// subtasks having the same bots but associated with different Tasks or a bunch of other different combinations.
					task.BotsTriggeredAtDone = append(task.BotsTriggeredAtDone, templateTask.BotsTriggeredAtDone...)
				}
				allFound = true
				for _, v := range templateTask.BotsTriggeredAtLoad {
					found := false
					for _, v2 := range task.BotsTriggeredAtLoad {
						if v2 == v {
							found = true
							break
						}
					}
					if !found {
						allFound = false
						break
					}
				}
				if !allFound {
					task.BotsTriggeredAtLoad = append(task.BotsTriggeredAtLoad, templateTask.BotsTriggeredAtLoad...)
				}
				allFound = true
				for _, v := range templateTask.BotsTriggeredContinuously {
					found := false
					for _, v2 := range task.BotsTriggeredContinuously {
						if v2 == v {
							found = true
							break
						}
					}
					if !found {
						allFound = false
						break
					}
				}
				if !allFound {
					task.BotsTriggeredContinuously = append(task.BotsTriggeredContinuously, templateTask.BotsTriggeredContinuously...)
				}
				// We can perform this op multiple times since they overwrite instead of appending
				for k, v := range templateTask.BotStaticData {
					task.BotStaticData[k] = v
				}

				schema.DataHelpText[fieldInParentTask] = templateTask.HelpText
				alreadyExists := false
				for _, v := range schema.DataKeys {
					if v == fieldInParentTask {
						alreadyExists = true
						break
					}
				}
				if !alreadyExists {
					schema.DataKeys = append(schema.DataKeys, fieldInParentTask)
				}
				schema.DataTypes[fieldInParentTask] = "Ref"

				task.BotStaticData["_On Form Submission Email"] = section.EmailForFormSubmissions
				task.BotStaticData["Finish URL"] = section.FinishURL

				addedToHTML = true
				break
			}
		}
		if addedToHTML {
			continue
		}
		for _, section := range e.ImageSections {
			if section.Title == sectionTitle {
				if !gallerySectionStarted && strings.Contains(sectionStart.Layout, "Gallery") {
					gen.GenSectionStart(sectionStart, &combinedHTML)
					gen.GenGalleryStart(&combinedHTML)
					gallerySectionStarted = true
					// gallery sections will either be ended when all sections have been processed or when a non-image
					// section is processed.
				}
				gallerySectionStarted = true
				link := section.OnClickLink
				if link == "" {
					link = "polyappEnlarge"
				}
				sectionImg := gen.Image{
					ID:      section.FirestoreID,
					URL:     common.BlobURL(section.FirestoreID, "Custom Computer Programming Services_Website Builder_YPfjpWdFOFiAaNCeDfwTWazBO_Picture"),
					AltText: section.PictureDescription,
					Width:   "",
					Height:  "",
					Title:   section.Title,
					Caption: section.Caption,
					Link:    link,
					Packed:  strings.HasPrefix(e.Pages[i].Layout, "Gallery - Packed"),
					Layout:  section.Layout,
				}
				switch sectionStart.Layout {
				case "Gallery - Padded, Large Images":
					err = gen.GenImage(sectionImg, &combinedHTML)
					if err != nil {
						return fmt.Errorf("gen.GenImage: %w", err)
					}
				case "Gallery - Padded, Medium Images":
					err = gen.GenImageMedium(sectionImg, &combinedHTML)
					if err != nil {
						return fmt.Errorf("gen.GenImageMedium: %w", err)
					}
				case "Gallery - Padded, Small Images":
					err = gen.GenImageSmall(sectionImg, &combinedHTML)
					if err != nil {
						return fmt.Errorf("gen.GenImageSmall: %w", err)
					}
				case "Gallery - Packed, Large Images":
					err = gen.GenImage(sectionImg, &combinedHTML)
					if err != nil {
						return fmt.Errorf("gen.GenImage: %w", err)
					}
				case "Gallery - Packed, Medium Images":
					err = gen.GenImageMedium(sectionImg, &combinedHTML)
					if err != nil {
						return fmt.Errorf("gen.GenImageMedium: %w", err)
					}
				case "Gallery - Packed, Small Images":
					err = gen.GenImageSmall(sectionImg, &combinedHTML)
					if err != nil {
						return fmt.Errorf("gen.GenImageSmall: %w", err)
					}
				default:
					err = gen.GenImage(sectionImg, &combinedHTML)
					if err != nil {
						return fmt.Errorf("gen.GenImage: %w", err)
					}
					gen.GenSectionEnd(sectionStart, &combinedHTML)
				}
				addedToHTML = true
				break
			}
		}
		if addedToHTML {
			continue
		}
		for _, section := range e.GallerySections {
			if section.Title == sectionTitle {
				if gallerySectionStarted {
					gen.GenGalleryEnd(&combinedHTML)
					gen.GenSectionEnd(sectionStart, &combinedHTML)
					gallerySectionStarted = false
				}

				gen.GenSectionStart(sectionStart, &combinedHTML)
				gen.GenGalleryStart(&combinedHTML)

				for imageIndex := range section.Images {
					title := section.Titles[imageIndex]
					if title == "" {
						title = "No Title"
					}
					sectionImg := gen.Image{
						ID:      section.FirestoreID,
						URL:     common.BlobURL(section.FirestoreID, "Custom Computer Programming Services_Website Builder_FBLQYzYVMsiggWfFiowaYPaAb_Images"+"%2F"+strconv.Itoa(imageIndex)),
						Title:   title,
						Packed:  strings.HasPrefix(e.Pages[i].Layout, "Gallery - Packed"),
						Layout:  section.ImageLayout,
						AltText: title,
					}
					switch section.GalleryLayout {
					case "Padded, Large Images":
						err = gen.GenImage(sectionImg, &combinedHTML)
						if err != nil {
							return fmt.Errorf("gen.GenImage: %w", err)
						}
					case "Padded, Medium Images":
						err = gen.GenImageMedium(sectionImg, &combinedHTML)
						if err != nil {
							return fmt.Errorf("gen.GenImageMedium: %w", err)
						}
					case "Padded, Small Images":
						err = gen.GenImageSmall(sectionImg, &combinedHTML)
						if err != nil {
							return fmt.Errorf("gen.GenImageSmall: %w", err)
						}
					case "Packed, Large Images":
						err = gen.GenImage(sectionImg, &combinedHTML)
						if err != nil {
							return fmt.Errorf("gen.GenImage: %w", err)
						}
					case "Packed, Medium Images":
						err = gen.GenImageMedium(sectionImg, &combinedHTML)
						if err != nil {
							return fmt.Errorf("gen.GenImageMedium: %w", err)
						}
					case "Packed, Small Images":
						err = gen.GenImageSmall(sectionImg, &combinedHTML)
						if err != nil {
							return fmt.Errorf("gen.GenImageSmall: %w", err)
						}
					default:
						return errors.New("Unrecognized section.GalleryLayout for a GallerySection: " + section.GalleryLayout)
					}
				}
				gen.GenGalleryEnd(&combinedHTML)
				gen.GenSectionEnd(sectionStart, &combinedHTML)
				addedToHTML = true
				break
			}
		}
	}
	if gallerySectionStarted {
		gen.GenGalleryEnd(&combinedHTML)
		gen.GenSectionEnd(gen.SectionStart{
			SectionRow: 0,
			IsLastRow:  false,
			Layout:     "",
		}, &combinedHTML)
		gallerySectionStarted = false
	}

	if e.Pages[i].SecondaryNavigation != nil && len(e.Pages[i].SecondaryNavigation) > 0 {
		combinedHTML.WriteString(`</div>`) // close last column
		combinedHTML.WriteString(`<div class="col-lg-2 p-0 flex-grow-1 bg-gray-1">`)
		navRefs := make([]gen.NavigationRef, len(e.Pages[i].SecondaryNavigation))
		for ii := range e.Pages[i].SecondaryNavigation {
			navRefs[ii] = makeNavigationRef(e.Pages[i].SecondaryNavigation[ii])
		}
		err = gen.GenWorkflowNavigation(gen.WorkflowNavigation{
			Title:          "",
			NavigationRefs: navRefs,
			Theme:          navigationBar.Theme,
		}, &combinedHTML)
		if err != nil {
			return fmt.Errorf("gen.GenWorkflowNavigation: %w", err)
		}
		combinedHTML.WriteString(`</div></div>`) // close nav column and the row
		combinedHTML.WriteString(`</div>`)       // close container-fluid
	}

	if combinedHTML.Len() == 0 {
		// I'm not going to throw an error for this. Why should I?
		//return errors.New("combinedHTML length was 0 so there is no content for this page")
	}
	ux.HTML = common.String(combinedHTML.String())
	ux.Title = common.String(e.Pages[i].PageTitle)
	ux.MetaDescription = common.String(e.Pages[i].PageDescription)
	ux.IconPath = common.String(navigationBar.LogoSrc)
	ux.CSSPath = TemplateUX.CSSPath
	ux.SelectFields = make(map[string]string)
	ux.Navbar = common.String(NavbarBuffer.String())
	ux.HeadTag = common.String(e.AnalyticsSnippet + e.Font)
	task.Name = e.Pages[i].PageTitle
	task.HelpText = e.Pages[i].PageDescription

	// must set UX, Task, and Schema IsPublic to True so that both any user can access this page (even if anonymous)
	// and so that
	ux.IsPublic = true
	task.IsPublic = true
	schema.IsPublic = true

	// Store everything
	if pm.UXID == nil || *pm.UXID == "" {
		err = allDB.CreateUX(&ux)
		if err != nil {
			return fmt.Errorf("allDB.CreateUX: %w", err)
		}
	} else {
		err = allDB.DeleteUX(ux.FirestoreID)
		if err != nil {
			return fmt.Errorf("allDB.DeleteUX %v: %w", ux.FirestoreID, err)
		}
		if e.ExportToJSON {
			err = fileCRUD.Delete(&ux)
			if err != nil {
				return fmt.Errorf("fileCRUD.Delete for UX %v: %w", ux.FirestoreID, err)
			}
		}
		err = allDB.CreateUX(&ux)
		if err != nil {
			return fmt.Errorf("allDB.CreateUX %v: %w", ux.FirestoreID, err)
		}
	}
	if pm.SchemaID == nil || *pm.SchemaID == "" {
		err = allDB.CreateSchema(&schema)
		if err != nil {
			return fmt.Errorf("allDB.CreateSchema: %w", err)
		}
	} else {
		err = allDB.DeleteSchema(schema.FirestoreID)
		if err != nil {
			return fmt.Errorf("allDB.DeleteSchema %v: %w", schema.FirestoreID, err)
		}
		if e.ExportToJSON {
			err = fileCRUD.Delete(&schema)
			if err != nil {
				return fmt.Errorf("fileCRUD.Delete for Schema %v: %w", schema.FirestoreID, err)
			}
		}
		err = allDB.CreateSchema(&schema)
		if err != nil {
			return fmt.Errorf("allDB.CreateSchema %v: %w", schema.FirestoreID, err)
		}
	}
	if pm.TaskID == nil || *pm.TaskID == "" {
		err = allDB.CreateTask(&task)
		if err != nil {
			return fmt.Errorf("allDB.CreateTask: %w", err)
		}
	} else {
		err = allDB.DeleteTask(task.FirestoreID)
		if err != nil {
			return fmt.Errorf("allDB.CreateTask %v: %w", task.FirestoreID, err)
		}
		if e.ExportToJSON {
			err = fileCRUD.Delete(&task)
			if err != nil {
				return fmt.Errorf("fileCRUD.Delete for Task %v: %w", task.FirestoreID, err)
			}
		}
		err = allDB.CreateTask(&task)
		if err != nil {
			return fmt.Errorf("allDB.CreateTask %v: %w", task.FirestoreID, err)
		}
	}
	pm.UXID = common.String(ux.FirestoreID)
	pm.SchemaID = common.String(schema.FirestoreID)
	pm.TaskID = common.String(task.FirestoreID)
	if pm.FirestoreID == "" {
		pm.FirestoreID = common.GetRandString(25)
		err = allDB.CreatePublicMap(pm)
		if err != nil {
			return fmt.Errorf("allDB.CreatePublicMap %v: %w", pm.FirestoreID, err)
		}
	} else {
		err = allDB.DeletePublicMap(pm.FirestoreID)
		if err != nil {
			return fmt.Errorf("allDB.UpdatePublicMap %v: %w", pm.FirestoreID, err)
		}
		if e.ExportToJSON {
			err = fileCRUD.Delete(pm)
			if err != nil {
				return fmt.Errorf("fileCRUD.Delete for PM %v: %w", pm.FirestoreID, err)
			}
		}
		err = allDB.CreatePublicMap(pm)
		if err != nil {
			return fmt.Errorf("allDB.UpdatePublicMap %v: %w", pm.FirestoreID, err)
		}
	}
	if e.ExportToJSON {
		// We must have already deleted any duplicates which exist since the FirestoreID is likely overwritten by Create() methods.
		err := fileCRUD.Create(&ux)
		if err != nil {
			return fmt.Errorf("fileCRUD.Create for data (%v): %w", ux.FirestoreID, err)
		}
		err = fileCRUD.Create(&task)
		if err != nil {
			return fmt.Errorf("fileCRUD.Create for data (%v): %w", task.FirestoreID, err)
		}
		err = fileCRUD.Create(&schema)
		if err != nil {
			return fmt.Errorf("fileCRUD.Create for data (%v): %w", schema.FirestoreID, err)
		}
		err = fileCRUD.Create(pm)
		if err != nil {
			return fmt.Errorf("fileCRUD.Create for data (%v): %w", pm.FirestoreID, err)
		}
	}
	return nil
}

func getThemeValue(ref string, themeChan chan theme, exportToJSON bool, errChan chan error) {
	fieldURL, err := url.Parse(ref)
	if err != nil {
		errChan <- fmt.Errorf("url.Parse for (%v): %w", ref, err)
		themeChan <- theme{}
		return
	}
	getReq, err := common.CreateGETRequest(*fieldURL)
	if err != nil {
		errChan <- fmt.Errorf("common.CreateGETRequest for (%v): %w", ref, err)
		themeChan <- theme{}
		return
	}
	id := getReq.DataID
	go func(id string, errChan chan error) {
		d := common.Data{
			FirestoreID: id,
		}
		err = allDB.Read(&d)
		if err != nil {
			errChan <- fmt.Errorf("allDB.ReadData %v: %w", id, err)
			themeChan <- theme{}
			return
		}
		var t theme
		for k, v := range d.S {
			if strings.HasSuffix(k, "_Primary Color") && v != nil {
				t.PrimaryColor = *v
			} else if strings.HasSuffix(k, "_Secondary Color") && v != nil {
				t.SecondaryColor = *v
			} else if strings.HasSuffix(k, "_Success Color") && v != nil {
				t.SuccessColor = *v
			} else if strings.HasSuffix(k, "_Info Color") && v != nil {
				t.InfoColor = *v
			} else if strings.HasSuffix(k, "_Warning Color") && v != nil {
				t.WarningColor = *v
			} else if strings.HasSuffix(k, "_Danger Color") && v != nil {
				t.DangerColor = *v
			} else if strings.HasSuffix(k, "_Light Color") && v != nil {
				t.LightColor = *v
			} else if strings.HasSuffix(k, "_Dark Color") && v != nil {
				t.DarkColor = *v
			} else if strings.HasSuffix(k, "_Body Background Color") && v != nil {
				t.BodyBackgroundColor = *v
			} else if strings.HasSuffix(k, "_Body Color") && v != nil {
				t.BodyColor = *v
			}
		}
		if exportToJSON {
			err := fileCRUD.Create(&d)
			if err != nil {
				errChan <- fmt.Errorf("fileCRUD.Create for data (%v): %w", d.FirestoreID, err)
				themeChan <- theme{}
				return
			}
		}
		errChan <- nil
		themeChan <- t
	}(id, errChan)
}

func getPageValue(ref string, exportToJSON bool) (page, error) {
	var p page
	fieldURL, err := url.Parse(ref)
	if err != nil {
		return p, fmt.Errorf("url.Parse for (%v): %w", ref, err)
	}
	getReq, err := common.CreateGETRequest(*fieldURL)
	if err != nil {
		return p, fmt.Errorf("common.CreateGETRequest for (%v): %w", ref, err)
	}
	id := getReq.DataID
	d := common.Data{
		FirestoreID: id,
	}
	err = allDB.Read(&d)
	if err != nil {
		return p, fmt.Errorf("allDB.ReadData %v: %w", id, err)
	}
	for k, v := range d.S {
		if strings.HasSuffix(k, "_Page Title") && v != nil {
			p.PageTitle = *v
		} else if strings.HasSuffix(k, "_Layout") && v != nil {
			p.Layout = *v
		} else if strings.HasSuffix(k, "_Page Description") && v != nil {
			p.PageDescription = *v
		}
	}
	for k, v := range d.AS {
		if strings.HasSuffix(k, "_Section Titles") && v != nil {
			p.SectionTitles = v
		} else if strings.HasSuffix(k, "_Secondary Navigation") && v != nil {
			p.SecondaryNavigation = v
		}
	}
	for k, v := range d.B {
		if strings.HasSuffix(k, "_Hidden") && v != nil {
			p.Hidden = *v
		}
	}
	if exportToJSON {
		err := fileCRUD.Create(&d)
		if err != nil {
			return p, fmt.Errorf("fileCRUD.Create for data (%v): %w", d.FirestoreID, err)
		}
	}
	return p, nil
}
func getWordySectionValue(ref string, exportToJSON bool) (wordySection, error) {
	fieldURL, err := url.Parse(ref)
	if err != nil {
		return wordySection{}, fmt.Errorf("url.Parse for (%v): %w", ref, err)
	}
	getReq, err := common.CreateGETRequest(*fieldURL)
	if err != nil {
		return wordySection{}, fmt.Errorf("common.CreateGETRequest for (%v): %w", ref, err)
	}
	id := getReq.DataID
	d := common.Data{
		FirestoreID: id,
	}
	err = allDB.Read(&d)
	if err != nil {
		return wordySection{}, fmt.Errorf("allDB.ReadData %v: %w", id, err)
	}
	var w wordySection
	for k, v := range d.S {
		if strings.HasSuffix(k, "_Title") && v != nil {
			w.Title = *v
		} else if strings.HasSuffix(k, "_Body") && v != nil {
			w.Body = *v
		} else if strings.HasSuffix(k, "_Background Color") && v != nil {
			w.BackgroundColor = *v
		}
	}
	if exportToJSON {
		err := fileCRUD.Create(&d)
		if err != nil {
			return wordySection{}, fmt.Errorf("fileCRUD.Create for data (%v): %w", d.FirestoreID, err)
		}
	}
	return w, nil
}

func getGallerySectionValue(ref string, exportToJSON bool) (gallerySection, error) {
	fieldURL, err := url.Parse(ref)
	if err != nil {
		return gallerySection{}, fmt.Errorf("url.Parse for (%v): %w", ref, err)
	}
	getReq, err := common.CreateGETRequest(*fieldURL)
	if err != nil {
		return gallerySection{}, fmt.Errorf("common.CreateGETRequest for (%v): %w", ref, err)
	}
	id := getReq.DataID
	var i gallerySection
	d := common.Data{
		FirestoreID: id,
	}
	err = allDB.Read(&d)
	if err != nil {
		return gallerySection{}, fmt.Errorf("allDB.ReadData %v: %w", id, err)
	}
	for k, v := range d.S {
		if strings.HasSuffix(k, "_Title") && v != nil {
			i.Title = *v
		} else if strings.HasSuffix(k, "_Gallery Layout") && v != nil {
			i.GalleryLayout = *v
		} else if strings.HasSuffix(k, "_Image Layout") && v != nil {
			i.ImageLayout = *v
		}
	}
	i.Images, err = allDB.ReadDataABytes(d.FirestoreID, common.AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, "Images"))
	i.Titles = make([]string, len(i.Images))
	for imageIndex := 0; imageIndex < len(i.Images); imageIndex++ {
		metadata, err := firestoreCRUD.GetStorageObjectMetadata(firestoreCRUD.GetBlobBucket(), firestoreCRUD.GetStorageObjectArrayName(common.CollectionData, id, url.PathEscape(common.AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, "Images")), imageIndex))
		if err != nil {
			return gallerySection{}, fmt.Errorf("firestoreCRUD.GetStorageObjectMetadata: %w", err)
		}
		i.Titles[imageIndex] = metadata["Title"]
	}
	for k, v := range d.ABytes {
		if strings.HasSuffix(k, "_Images") && v != nil {

			for imageIndex := 0; imageIndex < len(i.Images); imageIndex++ {

			}
		}
	}
	if exportToJSON {
		err := fileCRUD.Create(&d)
		if err != nil {
			return gallerySection{}, fmt.Errorf("fileCRUD.Create for data (%v): %w", d.FirestoreID, err)
		}
	}
	i.FirestoreID = id
	return i, nil
}
func getImageSectionValue(ref string, exportToJSON bool) (imageSection, error) {
	fieldURL, err := url.Parse(ref)
	if err != nil {
		return imageSection{}, fmt.Errorf("url.Parse for (%v): %w", ref, err)
	}
	getReq, err := common.CreateGETRequest(*fieldURL)
	if err != nil {
		return imageSection{}, fmt.Errorf("common.CreateGETRequest for (%v): %w", ref, err)
	}
	id := getReq.DataID
	var i imageSection
	d := common.Data{
		FirestoreID: id,
	}
	err = allDB.Read(&d)
	if err != nil {
		return imageSection{}, fmt.Errorf("allDB.ReadData %v: %w", id, err)
	}
	for k, v := range d.S {
		if strings.HasSuffix(k, "_Title") && v != nil {
			i.Title = *v
		} else if strings.HasSuffix(k, "_Price ID") && v != nil {
			i.PriceID = *v
		} else if strings.HasSuffix(k, "_Caption") && v != nil {
			i.Caption = *v
		} else if strings.HasSuffix(k, "_Picture Description") && v != nil {
			i.PictureDescription = *v
		} else if strings.HasSuffix(k, "_On Click Link") && v != nil {
			i.OnClickLink = *v
		} else if strings.HasSuffix(k, "_Layout") && v != nil {
			i.Layout = *v
		}
	}
	if exportToJSON {
		err := fileCRUD.Create(&d)
		if err != nil {
			return imageSection{}, fmt.Errorf("fileCRUD.Create for data (%v): %w", d.FirestoreID, err)
		}
	}
	i.FirestoreID = id
	return i, nil
}
func getAudioSectionValue(ref string, exportToJSON bool) (audioSection, error) {
	fieldURL, err := url.Parse(ref)
	if err != nil {
		return audioSection{}, fmt.Errorf("url.Parse for (%v): %w", ref, err)
	}
	getReq, err := common.CreateGETRequest(*fieldURL)
	if err != nil {
		return audioSection{}, fmt.Errorf("common.CreateGETRequest for (%v): %w", ref, err)
	}
	id := getReq.DataID
	var a audioSection
	d := common.Data{
		FirestoreID: id,
	}
	err = allDB.Read(&d)
	if err != nil {
		return audioSection{}, fmt.Errorf("allDB.ReadData %v: %w", id, err)
	}
	for k, v := range d.S {
		if strings.HasSuffix(k, "_Price ID") && v != nil {
			a.PriceID = *v
		} else if strings.HasSuffix(k, "_Title") && v != nil {
			a.Title = *v
		} else if strings.HasSuffix(k, "_Subtitle") && v != nil {
			a.Subtitle = *v
		}
	}
	a.Audio, err = allDB.ReadDataBytes(d.FirestoreID, common.AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, "Audio"))
	if err != nil {
		return audioSection{}, fmt.Errorf("allDB.ReadDataBytes: %w", err)
	}
	if exportToJSON {
		err := fileCRUD.Create(&d)
		if err != nil {
			return audioSection{}, fmt.Errorf("fileCRUD.Create for data (%v): %w", d.FirestoreID, err)
		}
	}
	a.FirestoreID = id
	return a, nil
}
func getVideoSectionValue(ref string, exportToJSON bool) (videoSection, error) {
	fieldURL, err := url.Parse(ref)
	if err != nil {
		return videoSection{}, fmt.Errorf("url.Parse for (%v): %w", ref, err)
	}
	getReq, err := common.CreateGETRequest(*fieldURL)
	if err != nil {
		return videoSection{}, fmt.Errorf("common.CreateGETRequest for (%v): %w", ref, err)
	}
	id := getReq.DataID
	var vidSection videoSection
	d := common.Data{
		FirestoreID: id,
	}
	err = allDB.Read(&d)
	if err != nil {
		return videoSection{}, fmt.Errorf("allDB.ReadData %v: %w", id, err)
	}
	for k, v := range d.S {
		if strings.HasSuffix(k, "_Video URL") && v != nil {
			vidSection.VideoURL = *v
		} else if strings.HasSuffix(k, "_Section Title") && v != nil {
			vidSection.SectionTitle = *v
		} else if strings.HasSuffix(k, "_Overlay Title") && v != nil {
			vidSection.OverlayTitle = *v
		} else if strings.HasSuffix(k, "_Layout") && v != nil {
			vidSection.Layout = *v
		}
	}
	for k, v := range d.B {
		if strings.HasSuffix(k, "_Autoplay") && v != nil {
			vidSection.Autoplay = *v
		}
	}
	if exportToJSON {
		err := fileCRUD.Create(&d)
		if err != nil {
			return videoSection{}, fmt.Errorf("fileCRUD.Create for data (%v): %w", d.FirestoreID, err)
		}
	}
	vidSection.FirestoreID = id
	return vidSection, nil
}
func getInteractiveSectionValue(ref string, exportToJSON bool) (interactiveSection, error) {
	fieldURL, err := url.Parse(ref)
	if err != nil {
		return interactiveSection{}, fmt.Errorf("url.Parse for (%v): %w", ref, err)
	}
	getReq, err := common.CreateGETRequest(*fieldURL)
	if err != nil {
		return interactiveSection{}, fmt.Errorf("common.CreateGETRequest for (%v): %w", ref, err)
	}
	id := getReq.DataID
	var i interactiveSection
	d := common.Data{
		FirestoreID: id,
	}
	err = allDB.Read(&d)
	if err != nil {
		return interactiveSection{}, fmt.Errorf("allDB.ReadData %v: %w", id, err)
	}
	for k, v := range d.S {
		if strings.HasSuffix(k, "_Title") && v != nil {
			i.Title = *v
		} else if strings.HasSuffix(k, "_Form Template") && v != nil {
			i.FormTemplate = *v
		} else if strings.HasSuffix(k, "_Email for Form Submissions") && v != nil {
			i.EmailForFormSubmissions = *v
		} else if strings.HasSuffix(k, "_Finish URL") && v != nil {
			i.FinishURL = *v
		} else if strings.HasSuffix(k, "_Background Color") && v != nil {
			i.BackgroundColor = *v
		}
	}
	if exportToJSON {
		err := fileCRUD.Create(&d)
		if err != nil {
			return interactiveSection{}, fmt.Errorf("fileCRUD.Create for data (%v): %w", d.FirestoreID, err)
		}
	}
	return i, nil
}

// createUserWebsiteDoc creates a document which will be indexed by User ID which describes this website.
// This index can be queried to find all Data IDs belonging to this user, which is helpful for access control and other things.
//
// userID will be used to add or update a document which is indexed by user and lists all the websites for that user.
//
// domain is the domain of the website. When a customer sees their list of websites they can edit they'll want to know
// which website is which and the simplest way to do that is to display their domain.
//
// dataID is the ID of the Data document which holds the website information. This document is a subtask if the website
// is being edited at polyappbuilder.com or it is a full task if being edited directly.
//
// editWebsiteDoc is the doc which is pointed to by polyappbuilder.com/Edit-Website. Its Data ID must be listed alongside
// the domain of the website so that when a user clicks a row in the Change Website table they are brought to Edit-Website
// with the correct information for their website displayed.
//
// exportToJSON exports a row in the list of websites and potentially other information and then moves them across environments.
func createUserWebsiteDoc(userID string, domain string, dataID string, editWebsiteDoc common.Data, exportToJSON bool) error {
	// We can hard-code the Schema ID, although it (like the Task & Schema & UX) might change if you had to completely
	// re-generate the website. For now, I AM hard-coding the SchemaID to avoid the extra lookup.
	if editWebsiteDoc.FirestoreID == "" {
		_ = editWebsiteDoc.Init(nil)
		editWebsiteDoc.IndustryID = "Custom Computer Programming Services"
		editWebsiteDoc.DomainID = "Website Builder"
		editWebsiteDoc.SchemaID = "vzHsnDitaaEZexwrJfWxtDjtD"
		editWebsiteDoc.FirestoreID = common.GetRandString(25)
		// point from Edit-Website to Website's Data.
		createWebsiteRef := common.CreateRef("Custom Computer Programming Services", "Website Builder", "TUkwjbFhGLLpGTYMxXnDDhywA", "SwfoPdXxHXVcOJjxihIrhEoLN", "vnfQkHXwRtPzBnSxwAjnPhZFx", dataID)
		editWebsiteDoc.Ref["Custom Computer Programming Services_Website Builder_vzHsnDitaaEZexwrJfWxtDjtD_Edit a Website - User"] = common.String(createWebsiteRef)
	}

	// userWebsiteDoc is the doc in the table indexed by user. It contains the domain and a reference to the Edit Website
	// Doc and is supposed to have enough information for someone to choose a website to edit.
	userWebsiteDoc := common.Data{}
	_ = userWebsiteDoc.Init(nil)
	userWebsiteDoc.IndustryID = "Custom Computer Programming Services"
	userWebsiteDoc.DomainID = "Website Builder"
	userWebsiteDoc.SchemaID = userID + "schema"
	fullPrefix := common.AddFieldPrefix(userWebsiteDoc.IndustryID, userWebsiteDoc.DomainID, userWebsiteDoc.SchemaID, "")
	userWebsiteDoc.S[fullPrefix+"Domain"] = common.String(domain)
	userWebsiteDoc.S[fullPrefix+"Data ID"] = common.String(editWebsiteDoc.FirestoreID)
	firestoreCtx, firestoreClient, err := firestoreCRUD.GetClient()
	if err != nil {
		return fmt.Errorf("firestoreCRUD.GetClient: %w", err)
	}
	potentialDuplicates, err := firestoreClient.Collection(common.CollectionData).Where(fullPrefix+"Data ID", "==", editWebsiteDoc.FirestoreID).Limit(2).Documents(firestoreCtx).GetAll()
	if err != nil {
		return fmt.Errorf("firestore query for "+fullPrefix+"Data ID threw err: %w", err)
	}

	if len(potentialDuplicates) == 0 {
		// There is no existing record tying this user to the Task which surrounds the website.
		userWebsiteSchema := createUserWebsiteSchema(fullPrefix, userID)
		err = allDB.DeleteSchema(userWebsiteSchema.FirestoreID)
		if err != nil {
			// ignore all errors since we don't totally care if it was deleted.
		}
		err = allDB.CreateSchema(&userWebsiteSchema)
		if err != nil {
			return fmt.Errorf("allDB.CreateSchema for %v: %w", userWebsiteSchema.FirestoreID, err)
		}

		userWebsiteDoc.FirestoreID = common.GetRandString(25)
		err = allDB.CreateData(&userWebsiteDoc)
		if err != nil {
			return fmt.Errorf("allDB.CreateData for userWebsiteDoc %v: %w", userWebsiteDoc.FirestoreID, err)
		}
		err := allDB.CreateData(&editWebsiteDoc)
		if err != nil {
			return fmt.Errorf("allDB.CreateData for editWebsiteDoc %v: %w", editWebsiteDoc.FirestoreID, err)
		}
		if exportToJSON {
			err = fileCRUD.Create(&userWebsiteSchema)
			if err != nil {
				return fmt.Errorf("fileCRUD.Create UserWebsiteSchema %v: %w", userWebsiteSchema.FirestoreID, err)
			}
			err = fileCRUD.Create(&userWebsiteDoc)
			if err != nil {
				return fmt.Errorf("fileCRUD.Create userWebsiteDoc %v: %w", userWebsiteDoc.FirestoreID, err)
			}
			err = fileCRUD.Create(&editWebsiteDoc)
			if err != nil {
				return fmt.Errorf("fileCRUD.Create editWebsiteDoc %v: %w", editWebsiteDoc.FirestoreID, err)
			}
		}
	} else if len(potentialDuplicates) > 1 {
		return errors.New("more than one document for this user at this Data ID. Field: " + fullPrefix + "Data ID" + " Data ID: " + editWebsiteDoc.FirestoreID)
	} else if len(potentialDuplicates) == 1 {
		userWebsiteSchema := createUserWebsiteSchema(fullPrefix, userID)
		err = allDB.DeleteSchema(userWebsiteSchema.FirestoreID)
		if err != nil {
			// ignore all errors since we don't totally care if it was deleted.
		}
		err = allDB.CreateSchema(&userWebsiteSchema)
		if err != nil && strings.Contains(err.Error(), "code = AlreadyExists") {
			// this means this user has > 1 website or has created a website before. Aka this is OK.
		} else if err != nil {
			return fmt.Errorf("allDB.CreateSchema for userWebsiteSchema %v: %w", userWebsiteSchema.FirestoreID, err)
		}

		readDoc := common.Data{}
		_ = readDoc.Init(potentialDuplicates[0].Data())
		userWebsiteDoc.FirestoreID = potentialDuplicates[0].Ref.ID
		if userWebsiteDoc.S[fullPrefix+"Domain"] == nil || readDoc.S[fullPrefix+"Domain"] == nil || *userWebsiteDoc.S[fullPrefix+"Domain"] != *readDoc.S[fullPrefix+"Domain"] {
			// Someone changed the Domain so let's put out an update
			err = allDB.UpdateData(&userWebsiteDoc)
			if err != nil {
				return fmt.Errorf("allDB.UpdateData for userWebsiteDoc %v: %w", userWebsiteDoc.FirestoreID, err)
			}
			err := allDB.UpdateData(&editWebsiteDoc)
			if err != nil {
				return fmt.Errorf("allDB.CreateData for editWebsiteDoc %v: %w", editWebsiteDoc.FirestoreID, err)
			}
			if exportToJSON {
				err = fileCRUD.Update(&userWebsiteDoc)
				if err != nil && strings.Contains(err.Error(), "error Reading:") {
					err = fileCRUD.Create(&userWebsiteDoc)
				}
				if err != nil {
					return fmt.Errorf("fileCRUD.Update userWebsiteDoc %v: %w", userWebsiteDoc.FirestoreID, err)
				}
				err = fileCRUD.Update(&editWebsiteDoc)
				if err != nil && strings.Contains(err.Error(), "error Reading") {
					err = fileCRUD.Create(&editWebsiteDoc)
				}
				if err != nil {
					return fmt.Errorf("fileCRUD.Update editWebsiteDoc %v: %w", editWebsiteDoc.FirestoreID, err)
				}
			}
		}
	}
	return nil
}

func createUserWebsiteSchema(fullPrefix string, userID string) common.Schema {
	userWebsiteSchema := common.Schema{}
	userWebsiteSchema.Init(nil)
	userWebsiteSchema.FirestoreID = userID + "schema"
	userWebsiteSchema.IndustryID = "Custom Computer Programming Services"
	userWebsiteSchema.DomainID = "Website Builder"
	userWebsiteSchema.SchemaID = userID + "schema"
	userWebsiteSchema.DataKeys = append(userWebsiteSchema.DataKeys, fullPrefix+"Domain", fullPrefix+"Data ID")
	userWebsiteSchema.DataHelpText[fullPrefix+"Domain"] = "Domain of the website created or owned by this user."
	userWebsiteSchema.DataHelpText[fullPrefix+"Data ID"] = "Data ID of the Task which can edit this website."

	userWebsiteSchema.DataTypes = make(map[string]string)
	userWebsiteSchema.DataTypes[fullPrefix+"Domain"] = "S"
	userWebsiteSchema.DataTypes[fullPrefix+"Data ID"] = "S"
	return userWebsiteSchema
}

type navMenuOption struct {
	// DataID is metadata; it is the Data document ID which contains this Nav Menu Option.
	DataID string
	Name   string
	URL    string
}

type wordyPage struct {
	Title       string
	URL         string
	Description string
	Body        string
	MakePrivate bool
}

type interactivePage struct {
	URL           string
	Template      string
	EmailToSendTo string
	FinishURL     string
}

// ActionEditWebsiteContent interprets the contents of this Task as a website builder and overwrite the existing website with this Task's data.
//
// Unlike most bots created so far, it does not assume you are using a particular schema ID / schema.
func ActionEditWebsiteContent(data *common.Data, request *common.POSTRequest) error {
	domainValue := ""
	logoKey := ""
	wordyPagesKey := ""
	navMenuStyle := "Light theme without a browser icon"
	// paymentPage generates pages at two URLs: /pay and /account
	// for now, both link back to Polyapp's subscription setup, making this a boolean.
	exportToJSON := false
	for k, v := range data.S {
		if strings.HasSuffix(k, "_Domain") && v != nil {
			domainValue = *v
			domainValue = strings.TrimPrefix(domainValue, "https://")
			domainValue = strings.TrimPrefix(domainValue, "http://")
			if strings.Contains(domainValue, "/") {
				domainValue = strings.Split(domainValue, "/")[0]
			}
		} else if strings.HasSuffix(k, "_Navigation Bar Style") && v != nil {
			navMenuStyle = *v
		}
	}
	for k, v := range data.B {
		if strings.HasSuffix(k, "_Export to JSON") && v != nil {
			exportToJSON = *v
		}
	}
	ok, err := allDB.ReadDataBytes(data.FirestoreID, common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, "Browser Icon (favicon)"))
	if err != nil {
		return fmt.Errorf("allDB.ReadDataBytes: %w", err)
	}
	if len(ok) > 0 {
		logoKey = common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, "Browser Icon (favicon)")
	}

	if exportToJSON {
		// have to export each individual data document
		// Since we are calling Init here we can omit it later too.
		err := fileCRUD.Init("")
		if err != nil {
			return fmt.Errorf("fileCRUD.Init: %w", err)
		}
		err = fileCRUD.Create(data)
		if err != nil {
			return fmt.Errorf("fileCRUD.Create for data ("+data.FirestoreID+"): %w", err)
		}
	}

	var navMenuOptionsValue []navMenuOption
	errChan := make(chan error)
	chanNavMenuOption := make(chan navMenuOption)
	chanWordyPage := make(chan wordyPage)
	var interactivePages []interactivePage
	for k, refs := range data.ARef {
		if strings.HasSuffix(k, "_Navigation Menu Option") {
			navMenuOptionsValue = make([]navMenuOption, len(refs))
			for i, ref := range refs {
				URL, err := url.Parse(ref)
				if err != nil {
					errChan <- fmt.Errorf("url.Parse for ("+ref+"): %w", err)
					chanNavMenuOption <- navMenuOption{}
					continue
				}
				getReq, err := common.CreateGETRequest(*URL)
				if err != nil {
					errChan <- fmt.Errorf("common.CreateGETRequest for ("+ref+"): %w", err)
					chanNavMenuOption <- navMenuOption{}
					continue
				}
				navMenuOptionsValue[i] = navMenuOption{
					DataID: getReq.DataID,
				}
				go func(getReq common.GETRequest, chanNavMenuOption chan navMenuOption, errChan chan error) {
					d := common.Data{
						FirestoreID: getReq.DataID,
					}
					err = allDB.Read(&d)
					if err != nil {
						errChan <- fmt.Errorf("allDB.ReadData for ("+getReq.DataID+"): %w", err)
						chanNavMenuOption <- navMenuOption{}
						return
					}
					namePtr := d.S[common.AddFieldPrefix(d.IndustryID, d.DomainID, getReq.SchemaID, "Name")]
					URLPtr := d.S[common.AddFieldPrefix(d.IndustryID, d.DomainID, getReq.SchemaID, "URL")]
					if exportToJSON && common.GetCloudProvider() == "" {
						// have to export each individual data document
						err = fileCRUD.Create(&d)
						if err != nil {
							errChan <- fmt.Errorf("fileCRUD.Create for data ("+d.FirestoreID+"): %w", err)
							chanNavMenuOption <- navMenuOption{}
						}
					}
					errChan <- nil
					chanNavMenuOption <- navMenuOption{
						DataID: getReq.DataID,
						Name:   *namePtr,
						URL:    *URLPtr,
					}
				}(getReq, chanNavMenuOption, errChan)
			}
		} else if strings.HasSuffix(k, "_Wordy Pages") {
			wordyPagesKey = k
			for _, ref := range refs {
				go func(ref string, chanWordyPage chan wordyPage, errChan chan error) {
					URL, err := url.Parse(ref)
					if err != nil {
						errChan <- fmt.Errorf("url.Parse for ("+ref+"): %w", err)
						chanWordyPage <- wordyPage{}
						return
					}
					getReq, err := common.CreateGETRequest(*URL)
					if err != nil {
						errChan <- fmt.Errorf("common.CreateGETRequest for ("+ref+"): %w", err)
						chanWordyPage <- wordyPage{}
						return
					}
					d := common.Data{
						FirestoreID: getReq.DataID,
					}
					err = allDB.Read(&d)
					if err != nil {
						errChan <- fmt.Errorf("allDB.ReadData for ("+ref+"): %w", err)
						chanWordyPage <- wordyPage{}
						return
					}
					titlePtr := d.S[common.AddFieldPrefix("Custom Computer Programming Services", "Website Builder", getReq.SchemaID, "Title")]
					urlPtr := d.S[common.AddFieldPrefix("Custom Computer Programming Services", "Website Builder", getReq.SchemaID, "URL")]
					descriptionPtr := d.S[common.AddFieldPrefix("Custom Computer Programming Services", "Website Builder", getReq.SchemaID, "Description")]
					bodyPtr := d.S[common.AddFieldPrefix("Custom Computer Programming Services", "Website Builder", getReq.SchemaID, "Body")]
					privatePtr := d.B[common.AddFieldPrefix("Custom Computer Programming Services", "Website Builder", getReq.SchemaID, "Make Private")]
					if exportToJSON && common.GetCloudProvider() == "" {
						// have to export each individual data document
						err = fileCRUD.Create(&d)
						if err != nil {
							errChan <- fmt.Errorf("fileCRUD.Create for data ("+d.FirestoreID+"): %w", err)
							chanWordyPage <- wordyPage{}
						}
					}
					errChan <- nil
					chanWordyPage <- wordyPage{
						Title:       *titlePtr,
						URL:         *urlPtr,
						Description: *descriptionPtr,
						Body:        *bodyPtr,
						MakePrivate: *privatePtr,
					}
				}(ref, chanWordyPage, errChan)
			}
		} else if strings.HasSuffix(k, "_Interactive Pages") {
			if interactivePages != nil {
				continue
			}
			interactivePages = make([]interactivePage, len(refs))
			for formPageIndex, ref := range refs {
				go func(ref string, formPageIndex int, errChan chan error) {
					URL, err := url.Parse(ref)
					if err != nil {
						errChan <- fmt.Errorf("url.Parse for ("+ref+"): %w", err)
						return
					}
					getReq, err := common.CreateGETRequest(*URL)
					if err != nil {
						errChan <- fmt.Errorf("common.CreateGETRequest for ("+ref+"): %w", err)
						return
					}
					d := common.Data{
						FirestoreID: getReq.DataID,
					}
					err = allDB.Read(&d)
					if err != nil {
						errChan <- fmt.Errorf("allDB.ReadData for ("+ref+"): %w", err)
						return
					}
					urlPtr := d.S[common.AddFieldPrefix(getReq.GetIndustry(), getReq.GetDomain(), getReq.SchemaID, "URL")]
					if urlPtr == nil {
						urlPtr = common.String("")
					}
					formTemplatePtr := d.S[common.AddFieldPrefix(getReq.GetIndustry(), getReq.GetDomain(), getReq.SchemaID, "Form Template")]
					if formTemplatePtr == nil {
						formTemplatePtr = common.String("")
					}
					emailForFormSubmissionsPtr := d.S[common.AddFieldPrefix(getReq.GetIndustry(), getReq.GetDomain(), getReq.SchemaID, "Email for Form Submissions")]
					if emailForFormSubmissionsPtr == nil {
						emailForFormSubmissionsPtr = common.String("")
					}
					interactivePages[formPageIndex] = interactivePage{
						URL:           *urlPtr,
						Template:      *formTemplatePtr,
						EmailToSendTo: *emailForFormSubmissionsPtr,
					}
					if exportToJSON && common.GetCloudProvider() == "" {
						// have to export each individual data document
						err = fileCRUD.Create(&d)
						if err != nil {
							errChan <- fmt.Errorf("fileCRUD.Create for data ("+d.FirestoreID+"): %w", err)
						}
					}
					errChan <- nil
				}(ref, formPageIndex, errChan)
			}
		}
	}
	combinedErr := ""
	wordyPagesValue := make([]wordyPage, len(data.ARef[wordyPagesKey]))
	for i := 0; i < len(navMenuOptionsValue)+len(wordyPagesValue)+len(interactivePages); i++ {
		err := <-errChan
		if err != nil {
			combinedErr += err.Error() + "; "
			continue
		}
	}
	for range navMenuOptionsValue {
		navMenuOptionLocal := <-chanNavMenuOption
		for navMenuOptionsIndex, _ := range navMenuOptionsValue {
			// ensures we keep the user's ordering of navMenuOptions even though the channels will deliver results out of order.
			if navMenuOptionsValue[navMenuOptionsIndex].DataID == navMenuOptionLocal.DataID {
				navMenuOptionsValue[navMenuOptionsIndex].Name = navMenuOptionLocal.Name
				navMenuOptionsValue[navMenuOptionsIndex].URL = navMenuOptionLocal.URL
				break
			}
		}
	}
	for i := range wordyPagesValue {
		wordyPageLocal := <-chanWordyPage
		wordyPagesValue[i] = wordyPageLocal
	}
	if combinedErr != "" {
		return errors.New(combinedErr)
	}

	err = createWebsite(request, data.FirestoreID, domainValue, logoKey, navMenuStyle, navMenuOptionsValue, wordyPagesValue, interactivePages, exportToJSON)
	if err != nil {
		return fmt.Errorf("createWebsite: %w", err)
	}

	return nil
}

// createWebsite creates a new website, linking all of the pages so they are accessible at public URLs.
func createWebsite(request *common.POSTRequest, dataID string, domain string, logoField string, navMenuStyle string, navMenuOptions []navMenuOption, wordyPages []wordyPage, interactivePages []interactivePage, exportToJSON bool) error {
	publicMaps := make([]*common.PublicMap, 0)
	uxs := make([]*common.UX, 0)
	tasks := make([]*common.Task, 0)

	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return fmt.Errorf("firestoreCRUD.GetClient: %w", err)
	}
	var navigationBar gen.NavigationBar
	navigationBar.NavigationRefs = make([]gen.NavigationRef, len(navMenuOptions))
	for i, n := range navMenuOptions {
		navigationBar.NavigationRefs[i].Text = n.Name
		navigationBar.NavigationRefs[i].LinkPath = n.URL
	}
	switch navMenuStyle {
	case "Light theme with a browser icon":
		navigationBar.LogoSrc = common.BlobURL(dataID, logoField)
		navigationBar.Theme = "light"
	case "Dark theme with a browser icon":
		navigationBar.LogoSrc = common.BlobURL(dataID, logoField)
		navigationBar.Theme = "dark"
	case "Light theme without a browser icon":
		navigationBar.Theme = "light"
	case "Dark theme without a browser icon":
		navigationBar.Theme = "dark"
	}
	var NavbarBuffer bytes.Buffer
	err = gen.GenNavigationBar(navigationBar, &NavbarBuffer)
	if err != nil {
		return fmt.Errorf("GenNavigationBar: %w", err)
	}
	for _, wordyPage := range wordyPages {
		// We want to:
		// 1. Figure out if this webpage already exists. If it does, we want to edit it. If it does not, we want to create
		// a brand new page.
		// 2. To create a webpage we just need to save two records: the PublicMap and the UX. The rest of the webpage display
		// is handled by PublicMapSiteGET in defaultservice/main.go
		isPublic := true
		if wordyPage.MakePrivate {
			isPublic = false
			wordyPage.Body = wordyPage.Body + `<div id="polyappJSNeedFullAuthCanary"></div>`
		}
		ux := &common.UX{
			FirestoreID:     "",
			IndustryID:      "Custom Computer Programming Services",
			DomainID:        "Website Builder",
			SchemaID:        "",
			HTML:            common.String(wordyPage.Body),
			Title:           common.String(wordyPage.Title),
			MetaDescription: common.String(wordyPage.Description),
			MetaKeywords:    nil,
			MetaAuthor:      nil,
			HeadTag:         nil,
			IconPath:        common.String(common.BlobURL(dataID, logoField)),
			CSSPath:         nil, // TODO generate CSS based on the inputs to this Task.
			SelectFields:    make(map[string]string),
			IsPublic:        isPublic,
		}
		hostAndPath := domain + wordyPage.URL
		if hostAndPath[len(hostAndPath)-1] != '/' {
			hostAndPath += "/"
		}
		existingPM, err := getExistingPublicMap(ctx, client, hostAndPath)
		if err != nil {
			return fmt.Errorf("getExistingPublicMap: %w", err)
		}

		newUX := false
		if existingPM == nil {
			existingPM = &common.PublicMap{
				FirestoreID:  common.GetRandString(25),
				HostAndPath:  hostAndPath,
				IndustryID:   "Custom Computer Programming Services",
				DomainID:     "Website Builder",
				UXID:         common.String(common.GetRandString(25)),
				SchemaID:     common.String("polyappNone"), // needed by UX, although this Doc does not exist.
				OwningUserID: request.UserID,               // assume that the current User is the owner of this site for now. In the future we should allow admins to edit this value in the UI.
			}
			newUX = true
			// there is not an existing PM, so let's create one ASAP
			err = allDB.CreatePublicMap(existingPM)
			if err != nil {
				return fmt.Errorf("CreatePublicMap: %w", err)
			}
		} else if existingPM.UXID == nil || *existingPM.UXID == "" {
			if existingPM.OwningUserID == "" {
				// this is a conversion. TODO remove this 'if' in September of 2020 (conversion should be done by then).
				existingPM.OwningUserID = request.UserID
			}
			existingPM.UXID = common.String(common.GetRandString(25))
			err = allDB.UpdatePublicMap(existingPM)
			if err != nil {
				return fmt.Errorf("UpdatePublicMap: %w", err)
			}
			newUX = true
		} else if existingPM.OwningUserID == "" {
			// this is a conversion. TODO remove this 'if' in September of 2020 (conversion should be done by then).
			existingPM.OwningUserID = request.UserID
			err = allDB.UpdatePublicMap(existingPM)
			if err != nil {
				return fmt.Errorf("UpdatePublicMap: %w", err)
			}
		}

		pmToStore, err := allDB.ReadPublicMap(existingPM.FirestoreID)
		if err != nil {
			return fmt.Errorf("allDB.ReadPublicMap pmToStore: %w", err)
		}
		publicMaps = append(publicMaps, &pmToStore)

		ux.FirestoreID = *existingPM.UXID
		ux.SchemaID = *existingPM.SchemaID // required by UX
		ux.Navbar = common.String(NavbarBuffer.String())
		if newUX {
			err = allDB.CreateUX(ux)
			if err != nil {
				return fmt.Errorf("CreateUX: %w", err)
			}
		} else {
			err = allDB.UpdateUX(ux)
			if err != nil {
				return fmt.Errorf("UpdateUX: %w", err)
			}
		}
		uxToStore, err := allDB.ReadUX(ux.FirestoreID)
		if err != nil {
			return fmt.Errorf("allDB.ReadUX uxToStore: %w", err)
		}
		uxs = append(uxs, &uxToStore)
	}
	for _, interactivePage := range interactivePages {
		// We want to:
		// 1. Figure out if this webpage already exists. If it does, we want to edit it. If it does not, we want to create
		// a brand new page.
		// 2. To create a webpage we just need to save two records: the PublicMap and the UX. The rest of the webpage display
		// is handled by PublicMapSiteGET in defaultservice/main.go
		formSchemaID := ""
		formUXID := ""
		templateTaskID := ""
		switch interactivePage.Template {
		case "Contact":
			formSchemaID = "ldnnbDqEBWjQIrClXGTesexbD"
			formUXID = "NKCjCeCMqQofMJXPkIdZsukqh"
			templateTaskID = "gVjHrOYrUmXhFOvnnpVUwMBct"
			// taskID must be created because we need to set interactivePage.EmailToSendTo as "_On Form Submission Email"
		default:
			return errors.New("Invalid interactivePage.Template: " + interactivePage.Template)
		}
		formUX, err := allDB.ReadUX(formUXID)
		if err != nil {
			return fmt.Errorf("allDB.ReadUX in interactivePage "+formUXID+": %w", err)
		}

		ux := &common.UX{
			FirestoreID:     "",
			IndustryID:      "Custom Computer Programming Services",
			DomainID:        "Website Builder",
			SchemaID:        "",
			HTML:            formUX.HTML,
			Title:           common.String("Contact"),
			MetaDescription: common.String("Contact the owners of this website"),
			MetaKeywords:    nil,
			MetaAuthor:      nil,
			HeadTag:         nil,
			IconPath:        common.String(common.BlobURL(dataID, logoField)),
			CSSPath:         nil, // TODO generate CSS based on the inputs to this Task.
			SelectFields:    make(map[string]string),
			IsPublic:        formUX.IsPublic,
		}
		hostAndPath := domain + interactivePage.URL
		if hostAndPath[len(hostAndPath)-1] != '/' {
			hostAndPath += "/"
		}
		existingPM, err := getExistingPublicMap(ctx, client, hostAndPath)
		if err != nil {
			return fmt.Errorf("getExistingPublicMap: %w", err)
		}

		formTaskID := ""
		if existingPM == nil || existingPM.TaskID == nil || *existingPM.TaskID == "" {
			// if there is no existingPM or no TaskID we must create one by duplicating the "Template" task.
			// The goal here is to allow us to use the same UX and Schema, but varying Task which hold different BotStaticData.
			templateTask, err := allDB.ReadTask(templateTaskID)
			if err != nil {
				return fmt.Errorf("allDB.ReadTask for templateTaskID ("+templateTaskID+"): %w", err)
			}
			botStaticData := templateTask.BotStaticData
			if botStaticData == nil {
				botStaticData = make(map[string]string)
			}
			botStaticData["_On Form Submission Email"] = interactivePage.EmailToSendTo
			newTask := common.Task{
				FirestoreID:               common.GetRandString(25),
				IndustryID:                templateTask.IndustryID,
				DomainID:                  templateTask.DomainID,
				Name:                      templateTask.Name,
				HelpText:                  templateTask.HelpText,
				TaskGoals:                 templateTask.TaskGoals,
				BotsTriggeredAtLoad:       templateTask.BotsTriggeredAtLoad,
				BotsTriggeredAtDone:       templateTask.BotsTriggeredAtDone,
				BotsTriggeredContinuously: templateTask.BotsTriggeredContinuously,
				BotStaticData:             botStaticData,
				IsPublic:                  templateTask.IsPublic,
				Deprecated:                templateTask.Deprecated,
			}
			err = allDB.CreateTask(&newTask)
			if err != nil {
				return fmt.Errorf("allDB.CreateTask for newTask ("+newTask.FirestoreID+"): %w", err)
			}
			formTaskID = newTask.FirestoreID
		} else {
			formTaskID = *existingPM.TaskID
		}

		taskToStore, err := allDB.ReadTask(formTaskID)
		if err != nil {
			return fmt.Errorf("allDB.ReadUX uxToStore: %w", err)
		}
		tasks = append(tasks, &taskToStore)

		newUX := false
		if existingPM == nil {
			existingPM = &common.PublicMap{
				FirestoreID: common.GetRandString(25),
				HostAndPath: hostAndPath,
				IndustryID:  "Custom Computer Programming Services",
				DomainID:    "Website Builder",
				// the thought here is that we do not want to use the Form's UX; we want to use our fancy wrapped UX
				// we are creating. Therefore if an existing UX is not available we still must create a new one.
				UXID:         common.String(common.GetRandString(25)),
				TaskID:       common.String(formTaskID),
				SchemaID:     common.String(formSchemaID),
				OwningUserID: request.UserID, // see other comments above about this
			}
			newUX = true
			// there is not an existing PM, so let's create one ASAP
			err = allDB.CreatePublicMap(existingPM)
			if err != nil {
				return fmt.Errorf("CreatePublicMap: %w", err)
			}
		} else if existingPM.UXID == nil || *existingPM.UXID == "" {
			if existingPM.OwningUserID == "" {
				// this is a conversion. TODO remove this 'if' in August of 2020 (conversion should be done by then).
				existingPM.OwningUserID = request.UserID
			}
			existingPM.UXID = common.String(common.GetRandString(25))
			err = allDB.UpdatePublicMap(existingPM)
			if err != nil {
				return fmt.Errorf("UpdatePublicMap: %w", err)
			}
			newUX = true
		} else {
			updatePM := false
			if existingPM.SchemaID == nil || *existingPM.SchemaID == "" || existingPM.TaskID == nil || *existingPM.TaskID == "" || existingPM.OwningUserID == "" {
				updatePM = true
			}
			// in addition to eliminating any old, bad data in Schema, let's also set the TaskID
			existingPM.SchemaID = common.String(formSchemaID)
			existingPM.TaskID = common.String(formTaskID)
			// this is a conversion. TODO remove this in August of 2020 (conversion should be done by then).
			existingPM.OwningUserID = request.UserID
			if updatePM {
				err = allDB.UpdatePublicMap(existingPM)
				if err != nil {
					return fmt.Errorf("UpdatePublicMap for existing: %w", err)
				}
			}
		}

		pmToStore, err := allDB.ReadPublicMap(existingPM.FirestoreID)
		if err != nil {
			return fmt.Errorf("allDB.ReadPublicMap pmToStore: %w", err)
		}
		publicMaps = append(publicMaps, &pmToStore)

		ux.FirestoreID = *existingPM.UXID
		ux.SchemaID = *existingPM.SchemaID
		ux.Navbar = common.String(NavbarBuffer.String())
		if newUX {
			err = allDB.CreateUX(ux)
			if err != nil {
				return fmt.Errorf("CreateUX: %w", err)
			}
		} else {
			err = allDB.UpdateUX(ux)
			if err != nil {
				return fmt.Errorf("UpdateUX: %w", err)
			}
		}
		uxToStore, err := allDB.ReadUX(ux.FirestoreID)
		if err != nil {
			return fmt.Errorf("allDB.ReadUX uxToStore: %w", err)
		}
		uxs = append(uxs, &uxToStore)
	}

	if exportToJSON {
		err = fileCRUD.Init("")
		if err != nil {
			return fmt.Errorf("fileCRUD.INit: %w", err)
		}
		for _, pm := range publicMaps {
			err = fileCRUD.Create(pm)
			if err != nil {
				return fmt.Errorf("fileCRUD.Create for pm ("+pm.FirestoreID+"): %w", err)
			}
		}
		for _, ux := range uxs {
			err = fileCRUD.Create(ux)
			if err != nil {
				return fmt.Errorf("fileCRUD.Create for ux ("+ux.FirestoreID+"): %w", err)
			}
		}
		for _, task := range tasks {
			err = fileCRUD.Create(task)
			if err != nil {
				return fmt.Errorf("fileCRUD.Create for task ("+task.FirestoreID+"): %w", err)
			}
		}
	}

	return nil
}

// getExistingPublicMap for a HostAndPath. Helps prevent duplicates from being created by using this lookup to then Update
// existing data.
//
// HostAndPath is calculable from the Domain.
func getExistingPublicMap(ctx context.Context, client *firestore.Client, HostAndPath string) (*common.PublicMap, error) {
	if len(HostAndPath) > 0 && HostAndPath[len(HostAndPath)-1] != '/' {
		HostAndPath += "/"
	}
	q := client.Collection(common.CollectionPublicMap).Where("HostAndPath", "==", HostAndPath)
	q.Limit(2)
	iter := q.Documents(ctx)
	doc, err := iter.Next()
	if err == iterator.Done {
		return nil, errors.New("code = NotFound")
	}
	if err != nil {
		return nil, fmt.Errorf("iter.Next: %w", err)
	}
	_, err = iter.Next()
	if err != iterator.Done {
		return nil, errors.New("there must be duplicate entries in Firestore for HostAndPath: " + HostAndPath)
	}
	pmExisting := &common.PublicMap{}
	err = pmExisting.Init(doc.Data())
	if err != nil {
		return nil, fmt.Errorf("pmExisting.Init: %w", err)
	}
	return pmExisting, nil
}

func makeNavigationRef(title string) gen.NavigationRef {
	return gen.NavigationRef{
		Text:     title,
		LinkPath: "/" + strings.ReplaceAll(title, " ", "-"),
	}
}

// ActionTable is the Action taken continuously with Table tasks. It redirects users to a new URL with a variable Data ID
// if you select a row. If you click the Done button it redirects you to a random ID for that Data ID.
// Right now there is no support for creating a Data from the available information.
//
// Side effect: This Action deletes the Data document. See in-function comments for 'why'.
func ActionTable(data *common.Data, botStaticData map[string]string, request *common.POSTRequest) error {
	if request.IsDone {
		// we need to create a new one of whatever this table holds and then redirect there.
		newID := common.GetRandString(25)
		request.FinishURL = strings.TrimSpace(botStaticData["Create URL"])
		if request.FinishURL == "" {
			return errors.New("No Create URL provided")
		}
		if strings.Contains(request.FinishURL, "?") {
			request.FinishURL += "& " + newID
		} else {
			request.FinishURL += "? " + newID
		}
		return nil
	}

	tableRef := ""
	for _, v := range data.Ref {
		// I'm not sure how to handle multiple Refs on the page except reading all of them, which isn't a great solution.
		if v != nil {
			tableRef = *v
		}
		break
	}
	u, err := url.Parse(tableRef)
	if err != nil {
		return fmt.Errorf("url.Parse: %w", err)
	}
	if u == nil {
		// there was nothing selected
		return nil
	}
	getReq, err := common.CreateGETRequest(*u)
	if err != nil {
		return fmt.Errorf("common.CreateGETRequest: %w", err)
	}
	tableData := common.Data{
		FirestoreID: getReq.DataID,
	}
	err = allDB.Read(&tableData)
	if err != nil {
		return fmt.Errorf("allDB.ReadData %v: %w", getReq.DataID, err)
	}
	if len(tableData.AS) > 1 {
		return errors.New("Unsure which array of strings is the selected row")
	}
	if len(tableData.AS) == 0 {
		// no row has been selected
		return nil
	}
	var selectedRow []string
	for _, v := range tableData.AS {
		selectedRow = v
	}
	if len(selectedRow) < 1 || selectedRow == nil {
		return errors.New("Selected row must have something in it")
	}

	newID := selectedRow[len(selectedRow)-1]
	request.FinishURL = strings.TrimSpace(botStaticData["Row Select URL"])
	if request.FinishURL == "" {
		return errors.New("No Row Select URL provided")
	}
	if strings.Contains(request.FinishURL, "?") {
		request.FinishURL += "&data=" + newID
	} else {
		request.FinishURL += "?data=" + newID
	}

	// I want to delete the source documents for 2 reasons.
	// 1. I do not want to slowly build up a bunch of useless documents in the database which simply record the last state
	// of the application when they pressed a button.
	// 2. When a user navigates 'back' to the previous page and the Data document was not deleted then the row they
	// selected will have persisted in the Data. That means that the page will first have a delay and then automatically
	// redirect to where the page directed users to the last time.
	_ = allDB.DeleteData(tableData.FirestoreID)
	_ = allDB.DeleteData(data.FirestoreID)

	return nil
}

// EditBot interprets the current Data into a Bot and updates the Bot if a Bot ID is included or creates a new Bot.
// Also creates / updates child Actions.
//
// Works with Schema snYQbrlFiAdTgLEuJJcOAxksU
func EditBot(data *common.Data) error {
	var b common.Bot
	_ = b.Init(nil)
	for k, v := range data.S {
		if strings.HasSuffix(k, "_Bot ID") {
			b.FirestoreID = *v
		} else if strings.HasSuffix(k, "_Name") {
			b.Name = *v
		} else if strings.HasSuffix(k, "_Help Text") {
			b.HelpText = *v
		}
	}
	err := fileCRUD.Init("")
	if err != nil {
		return fmt.Errorf("fileCRUD.Init: %w", err)
	}
	allActions := make([]common.Action, 0)
	actionDatas := make([]common.Data, 0)
	for k, v := range data.ARef {
		if strings.HasSuffix(k, "_Action") {
			for i := range v {
				getReq, err := common.ParseRef(v[i])
				if err != nil {
					return fmt.Errorf("common.ParseRef (%v): %w", v[i], err)
				}
				actionData := common.Data{
					FirestoreID: getReq.DataID,
				}
				err = allDB.Read(&actionData)
				if err != nil {
					return fmt.Errorf("allDB.ReadData for actionData (%v): %w", getReq.DataID, err)
				}
				var a common.Action
				_ = a.Init(nil)
				for actionDataK, actionDataV := range actionData.S {
					if strings.HasSuffix(actionDataK, "_Action ID") {
						a.FirestoreID = *actionDataV
					} else if strings.HasSuffix(actionDataK, "_Name") {
						a.Name = *actionDataV
					} else if strings.HasSuffix(actionDataK, "_Help Text") {
						a.HelpText = *actionDataV
					}
				}
				allActions = append(allActions, a)
				actionDatas = append(actionDatas, actionData)
			}
		}
	}
	for i := range allActions {
		if allActions[i].FirestoreID == "" {
			allActions[i].FirestoreID = common.GetRandString(25)
			err := allDB.CreateAction(&allActions[i])
			if err != nil {
				return fmt.Errorf("allDB.CreateAction (%v): %w", allActions[i].FirestoreID, err)
			}
			actionDatas[i].S[common.AddFieldPrefix(actionDatas[i].IndustryID, actionDatas[i].DomainID, actionDatas[i].SchemaID, "Action ID")] = common.String(allActions[i].FirestoreID)
			err = allDB.UpdateData(&actionDatas[i])
			if err != nil {
				return fmt.Errorf("allDB.UpdateData (%v): %w", actionDatas[i].FirestoreID, err)
			}
		} else {
			err := allDB.UpdateAction(&allActions[i])
			if err != nil {
				return fmt.Errorf("allDB.UpdateAction (%v): %w", allActions[i].FirestoreID, err)
			}
		}
		// append ActionIDs in the correct order
		b.ActionIDs = append(b.ActionIDs, allActions[i].FirestoreID)
		// always export to JSON
		err := fileCRUD.Create(&allActions[i])
		if err != nil {
			return fmt.Errorf("fileCRUD.Create %v: %w", allActions[i].FirestoreID, err)
		}
		err = fileCRUD.CreateData(&actionDatas[i])
		if err != nil {
			return fmt.Errorf("fileCRUD.CreateData %v: %w", actionDatas[i].FirestoreID, err)
		}
	}
	if b.FirestoreID == "" {
		b.FirestoreID = common.GetRandString(25)
		err := allDB.CreateBot(&b)
		if err != nil {
			return fmt.Errorf("allDB.CreateBot (%v): %w", b.FirestoreID, err)
		}
		data.S[common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, "Bot ID")] = common.String(b.FirestoreID)
		err = allDB.UpdateData(data)
		if err != nil {
			return fmt.Errorf("allDB.UpdateData (%v): %w", data.FirestoreID, err)
		}
	} else {
		err := allDB.UpdateBot(&b)
		if err != nil {
			return fmt.Errorf("allDB.UpdateBot (%v): %w", b.FirestoreID, err)
		}
	}
	// always export to JSON
	err = fileCRUD.Create(&b)
	if err != nil {
		return fmt.Errorf("fileCRUD.Create %v: %w", b.FirestoreID, err)
	}
	err = fileCRUD.CreateData(data)
	if err != nil {
		return fmt.Errorf("fileCRUD.CreateData %v: %w", data.FirestoreID, err)
	}
	return nil
}

// ActionNextData opens the current Task with the next Data. By default the next Data is the next ID for a query on this
// SchemaID. For example if you have ID 'a' and the schema contains IDs: '0', 'a', 'b', 'c',
// then the current Task would be opened with Data 'b'.
// TODO right now this is very similar to calling the GETDataTables. It would be better to actually call that func.
func ActionNextData(currentData *common.Data, request *common.POSTRequest, response *common.POSTResponse, botStaticData map[string]string) error {
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return fmt.Errorf("firestoreCRUD.GetClient: %w", err)
	}
	schema := request.SchemaCache
	if schema == nil {
		schema = &currentData.SchemaCache
	}
	if schema.FirestoreID == "" {
		s, err := allDB.ReadSchema(request.SchemaID)
		if err != nil {
			return fmt.Errorf("allDB.ReadSchema(%v): %w", request.SchemaID, err)
		}
		schema = &s
	}
	startAfterID := request.DataID
	for {
		startAfterDoc, err := client.Collection(common.CollectionData).Doc(startAfterID).Get(ctx)
		if err != nil {
			return fmt.Errorf("Get Start After Doc %v: %w", request.DataID, err)
		}
		query := client.Collection(common.CollectionData).StartAfter(startAfterDoc).Limit(1)
		sortedByColumn := ""
		sortedDir := firestore.Asc
		var dataTableRequest common.DataTableRequest
		if botStaticData["Query"] != "" {
			parsedURL, err := url.Parse(botStaticData["Query"])
			if err != nil {
				return fmt.Errorf("url.Parse: %w", err)
			}
			dataTableRequest, err = common.CreateDataTableRequest(*parsedURL)
			if err != nil {
				return fmt.Errorf("common.CreateDataTableRequest: %w", err)
			}
			initialOrderColumnInt, err := strconv.Atoi(dataTableRequest.InitialOrderColumn)
			if err == nil && initialOrderColumnInt < len(schema.DataKeys) {
				sortedByColumn = schema.DataKeys[initialOrderColumnInt]
			}
			if dataTableRequest.InitialSearch != "" && sortedByColumn != "" {
				query = query.Where(sortedByColumn, "==", dataTableRequest.InitialSearch)
			} else if sortedByColumn != "" {
				query = query.OrderBy(sortedByColumn, sortedDir)
			}
		} else {
			// use the current request as a basis for defaults. Which is to say we know nothing about the sorting or
			// searching for how this Task was opened from the Data Table, so we're not going to change the defaults there.
		}

		allDocs, err := query.Documents(ctx).GetAll()
		if len(allDocs) < 1 {
			// We've gone through the whole list of things to process so return the person to the Change Data table to
			// view all of the work they have finished.
			response.NewURL = "/polyappChangeData?industry=" + request.GetIndustry() + "&domain=" + request.GetDomain() +
				"&task=" + request.GetTask() + "&ux=" + request.UXID + "&schema=" + request.SchemaID + "&user=" + request.UserID +
				"&role=" + request.RoleID
			if dataTableRequest.InitialSearch != "" {
				response.NewURL += "&table-initial-search=" + dataTableRequest.InitialSearch
			}
			if dataTableRequest.InitialOrderColumn != "" {
				response.NewURL += "&table-initial-order-column=" + dataTableRequest.InitialOrderColumn
			}
			return nil
		}
		startAfterID = allDocs[0].Ref.ID

		// TODO consider adding filters here which reject some documents & prevent them from being shown. Perhaps
		// you could filter if someField == "some value".

		response.NewURL = common.CreateURL(request.IndustryID, request.DomainID, request.TaskID, request.UXID, request.SchemaID,
			allDocs[0].Ref.ID, request.UserID, request.RoleID, request.OverrideIndustryID, request.OverrideDomainID)
		return nil
	}
}

type createCover struct {
	PolyappFirestoreID    string
	PolyappIndustryID     string
	PolyappDomainID       string
	PolyappSchemaID       string
	MediaUpload           []byte `suffix:"Media Upload"`
	MediaAltText          string `suffix:"Media Alt Text"`
	BackgroundColor       string `suffix:"Background Color"`
	Scaling               string `suffix:"Scaling"`
	HeadingText           string `suffix:"Heading"`
	HeadingFont           string `suffix:"Heading Font"`
	SubHeadingText        string `suffix:"Subheading"`
	SubHeadingFont        string `suffix:"Subheading Font"`
	TextColor             string `suffix:"Text Color"`
	DownloadHTMLAndImages bool   `suffix:"Download HTML and Images"`
	HTMLOutput            string `suffix:"HTML Output"`
}

// ActionCreateCover creates a Cover, which is a UI element comprising an Image and some text overlaying that image.
// This is based on the zEKYTPojAIxhu6K2hedp Schema. Run this continuously.
func ActionCreateCover(data *common.Data, response *common.POSTResponse) error {
	var err error
	c := createCover{}
	err = common.DataIntoStructure(data, &c)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}

	if c.MediaUpload == nil || len(c.MediaUpload) == 0 {
		// Not enough information to create the Cover
		return nil
	}

	var b bytes.Buffer
	mediaURL := "/blob/assets/" + data.FirestoreID + "/" + common.AddFieldPrefix(url.PathEscape(data.IndustryID), url.PathEscape(data.DomainID), url.PathEscape(data.SchemaID), url.PathEscape("Media Upload"))
	links, err := gen.GenCover(gen.Cover{
		Scaling:         c.Scaling,
		MediaURL:        mediaURL,
		AltText:         c.MediaAltText,
		BackgroundColor: c.BackgroundColor,
		TextColor:       c.TextColor,
		Heading:         c.HeadingText,
		HeadingFont:     c.HeadingFont,
		Subheading:      c.SubHeadingText,
		SubheadingFont:  c.SubHeadingFont,
	}, &b)
	if err != nil {
		return fmt.Errorf("gen.GenCover: %w", err)
	}
	c.HTMLOutput = links + `
` + b.String()
	if c.DownloadHTMLAndImages {
		// TODO redirect the user to a downloads url which includes a Zip file containing the desired information.
		response.NewURL = ""
	}
	imgContentType := http.DetectContentType(c.MediaUpload)
	encodedMedia := "data:" + imgContentType + ";base64, " + base64.StdEncoding.EncodeToString(c.MediaUpload)
	c.HTMLOutput = strings.ReplaceAll(c.HTMLOutput, mediaURL, encodedMedia)
	c.HTMLOutput = bootstrapInlineCSS() + c.HTMLOutput
	id := common.AddFieldPrefix(url.PathEscape(data.IndustryID), url.PathEscape(data.DomainID), url.PathEscape(data.SchemaID), url.PathEscape("HTML Output"))
	response.ModDOMs = append(response.ModDOMs, common.ModDOM{
		DeleteSelector: "#" + common.CSSEscape(id),
		InsertSelector: "#" + common.CSSEscape("polyappError"+id),
		Action:         "beforebegin",
		HTML:           `<textarea type="text" class="form-control" rows="5" id="` + id + `">` + c.HTMLOutput + `</textarea>`,
	})

	response.ModDOMs = append(response.ModDOMs, common.ModDOM{
		DeleteSelector: "#" + common.CSSEscape("polyappError"+id) + " + div",
		InsertSelector: "#" + common.CSSEscape("polyappError"+id),
		Action:         "afterend",
		HTML:           `<div>` + c.HTMLOutput + `</div>`,
	})

	return nil
}

// bootstrapInlineCSS returns a <style> tag which contains all CSS needed to get the Cover to work on any webpage.
func bootstrapInlineCSS() string {
	return `<style>
*, ::after, ::before {
    box-sizing: border-box;
}
.align-items-center {
    -ms-flex-align: center!important;
    align-items: center!important;
}
/* @media directive is necessary to prevent these styles overriding other @media directive in Chrome. */
@media (min-width: 1px) {
	.col, .col-1, .col-10, .col-11, .col-12, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-auto, .col-lg, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-auto, .col-md, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-auto, .col-sm, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-auto, .col-xl, .col-xl-1, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-auto {
		position: relative;
		width: 100%;
		padding-right: 15px;
		padding-left: 15px;
	}
	.col-12 {
		-ms-flex: 0 0 100%;
		flex: 0 0 100%;
		max-width: 100%;
	}
}

@media (min-width: 576px) {
	.col-sm-4 {
		-ms-flex: 0 0 33.333333%;
		flex: 0 0 33.333333%;
		max-width: 33.333333%;
	}
	.col-sm-6 {
		-ms-flex: 0 0 50%;
		flex: 0 0 50%;
		max-width: 50%;
	}
	.col-sm-8 {
		-ms-flex: 0 0 66.666667%;
		flex: 0 0 66.666667%;
		max-width: 66.666667%;
	}
}

.container-fluid, .container-lg, .container-md, .container-sm, .container-xl {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
}
img {
    vertical-align: middle;
    border-style: none;
}
.img-fluid {
    max-width: 100%;
    height: auto;
}
.justify-content-between {
    -ms-flex-pack: justify!important;
    justify-content: space-between!important;
}
.p-0 {
    padding: 0!important;
}
.row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}
.text-center {
    text-align: center!important;
}
</style>`
}

// ActionBrowserActionOpenURL opens a new tab with the URL field.
func ActionBrowserActionOpenURL(d *common.Data, response *common.POSTResponse) error {
	var f struct {
		URL string `suffix:"URL"`
	}
	err := common.DataIntoStructure(d, &f)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}
	// BrowserActions would be used if this bot was run continuously.
	//response.BrowserActions = append(response.BrowserActions, common.BrowserAction{
	//	Name: common.BrowserActionOpenNewTab,
	//	Data: f.URL,
	//})
	response.ModDOMs = append(response.ModDOMs, common.ModDOM{
		InsertSelector: "body",
		Action:         "afterbegin",
		HTML:           `<script>window.open('` + f.URL + `', '_blank');</script>`,
	})
	return nil
}

// ActionEditRole edits a Role document in the database.
//
// Note: It would be nice if this could be replaced with a generic function.
func ActionEditRole(d *common.Data) error {
	exportAsJSONToFile := d.B[common.AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, "Export as JSON To File")]
	if exportAsJSONToFile != nil && *exportAsJSONToFile {
		err := fileCRUD.Init("")
		if err != nil {
			return fmt.Errorf("fileCRUD.Init: %w", err)
		}
	}
	role := common.Role{
		Access: make([]common.Access, 0),
	}
	err := common.DataIntoStructure(d, &role)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}
	for k, v := range d.ARef {
		if strings.HasSuffix(k, "_Access") {
			for i := range v {
				ref, err := common.ParseRef(v[i])
				if err != nil {
					return fmt.Errorf("common.ParseRef: %w", err)
				}
				innerData := common.Data{
					FirestoreID: ref.DataID,
				}
				err = allDB.Read(&innerData)
				if err != nil {
					return fmt.Errorf("allDB.ReadData: %w", err)
				}
				a := common.Access{}
				err = common.DataIntoStructure(&innerData, &a)
				if err != nil {
					return fmt.Errorf("DataIntoStructure for Access: %w", err)
				}
				keyValueMap := make(map[string]string)
				keyValue := innerData.AS["polyapp_Role_AckJPcqmSVmgumHxFUveOzCQc_Key Value"]
				for _, s := range keyValue {
					split := strings.Split(s, " ")
					if len(split) != 2 || len(split[0]) < 1 || len(split[1]) < 1 {
						continue
					}
					keyValueMap[split[0]] = split[1]
				}
				a.KeyValue = keyValueMap
				role.Access = append(role.Access, a)
				if exportAsJSONToFile != nil && *exportAsJSONToFile {
					err = fileCRUD.CreateData(&innerData)
					if err != nil {
						return fmt.Errorf("fileCRUD.CreateData for inner data: %w", err)
					}
				}
			}
		}
	}
	if role.FirestoreID != "" {
		// update
		err = allDB.UpdateRole(&role)
		if err != nil {
			return fmt.Errorf("allDB.UpdateRole: %w", err)
		}
	} else {
		// create
		role.FirestoreID = common.GetRandString(20)
		err = allDB.CreateRole(&role)
		if err != nil {
			return fmt.Errorf("allDb.CreateRole: %w", err)
		}
		d.S[common.AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, "Role ID")] = common.String(role.FirestoreID)
		err = allDB.UpdateData(d)
		if err != nil {
			return fmt.Errorf("allDB.UpdateData: %w", err)
		}
	}
	if exportAsJSONToFile != nil && *exportAsJSONToFile {
		err = fileCRUD.Create(d)
		if err != nil {
			return fmt.Errorf("fileCRUD.Create for data (%v): %w", d.FirestoreID, err)
		}
		err = fileCRUD.Create(&role)
		if err != nil {
			return fmt.Errorf("fileCRUD.Create for Role (%v): %w", role.FirestoreID, err)
		}
	}
	return nil
}

// ActionEditUser edits a User document in the database.
//
// Note: It would be nice if this could be replaced with a generic function.
func ActionEditUser(d *common.Data) error {
	var err error
	user := common.User{}
	err = common.DataIntoStructure(d, &user)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}
	// Unfortunately DataIntoStructure does not handle *string which means most data is not translated into the structure.
	// We can do a manual search of d *common.Data to try to find values to put into common.User
	//
	// TODO make DataIntoStructure support struct fields which are pointers to base types like string, bool, etc.
	user.UID = common.String("")
	user.FullName = common.String("")
	user.PhotoURL = common.String("")
	user.PhoneNumber = common.String("")
	user.Email = common.String("")
	user.EmailVerified = common.Bool(false)
	newPassword := ""
	for k, v := range d.S {
		if strings.HasSuffix(k, "UID") {
			user.UID = v
		} else if strings.HasSuffix(k, "Full Name") {
			user.FullName = v
		} else if strings.HasSuffix(k, "Photo URL") {
			user.PhotoURL = v
		} else if strings.HasSuffix(k, "Phone Number") {
			user.PhoneNumber = v
		} else if strings.HasSuffix(k, "Email") {
			user.Email = v
		} else if strings.HasSuffix(k, "Theme Path") {
			user.ThemePath = v
		}
		// This field is not part of the User collection on purpose.
		if strings.HasSuffix(k, "_New Password") && v != nil {
			newPassword = *v
			d.S[k] = common.String("")
		}
	}
	for k, v := range d.B {
		if strings.HasSuffix(k, "Email Verified") {
			user.EmailVerified = v
		}
	}
	if user.FirestoreID == "" {
		// create
		user.FirestoreID = common.GetRandString(20)
		err = allDB.CreateUser(&user)
		if err != nil {
			return fmt.Errorf("allDb.CreateRole: %w", err)
		}
		d.S[common.AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, "UID")] = user.UID
		d.S[common.AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, "User ID")] = common.String(user.FirestoreID)
	} else {
		// update
		err = allDB.UpdateUser(&user)
		if err != nil {
			return fmt.Errorf("allDB.UpdateUser: %w", err)
		}
		d.S[common.AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, "UID")] = user.UID
	}

	if newPassword != "" {
		err = allDB.UpdateUserPassword(&user, newPassword)
		if err != nil {
			return fmt.Errorf("allDB.UpdateUserPassword: %w", err)
		}
	}

	// Critical to remove the New Password and update other data, like including a new UID.
	err = allDB.UpdateData(d)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}
	exportAsJSONToFile := d.B[common.AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, "Export as JSON To File")]
	if exportAsJSONToFile != nil && *exportAsJSONToFile {
		err = fileCRUD.Init("")
		if err != nil {
			return fmt.Errorf("fileCRUD.Init: %w", err)
		}
		// Do not save UID, which links the user to Firebase, because it will be different in different projects.
		uid := d.S[common.AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, "UID")]
		delete(d.S, common.AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, "UID"))
		user.UID = common.String("")
		err = fileCRUD.Create(d)
		if err != nil {
			return fmt.Errorf("fileCRUD.Create for data (%v): %w", d.FirestoreID, err)
		}
		err = fileCRUD.Create(&user)
		if err != nil {
			return fmt.Errorf("fileCRUD.Create for User (%v): %w", user.FirestoreID, err)
		}
		d.S[common.AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, "UID")] = uid
	}
	return nil
}

type UserActivity struct {
	UserID    string  `suffix:"User ID"`
	EventURL  string  `suffix:"Event URL"`
	EventTime float64 `suffix:"Event Time"`
	Event     string
}

// ActionUserActivity records user activity when a Task is loaded or when a Task is Done.
// It is designed to run as a User Bot. It knows if the Task is loading or Done based on the Data provided.
func ActionUserActivity(request *common.POSTRequest) error {
	userActivity := UserActivity{
		UserID:    request.UserID,
		EventURL:  common.CreateURL(request.IndustryID, request.DomainID, request.TaskID, request.UXID, request.SchemaID, request.DataID, request.UserID, request.RoleID, request.OverrideIndustryID, request.OverrideDomainID),
		EventTime: float64(time.Now().UTC().Unix()),
		Event:     "Load",
	}
	if request.MessageID != "" {
		userActivity.Event = "Done"
	}
	newData := common.Data{
		FirestoreID: common.GetRandString(30),
	}
	err := newData.Init(nil)
	if err != nil {
		return fmt.Errorf("newData.Init: %w", err)
	}
	err = common.StructureIntoData("polyapp", "User", "iRQJHSitiUYNJJFfNSNCiFxwo", &userActivity, &newData)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}
	err = allDB.CreateData(&newData)
	if err != nil {
		return fmt.Errorf("allDB.CreateData: %w", err)
	}
	return nil
}

// ActionHomeLoad is triggered when the Home Page is loaded. It adds some static content to the page. This is usually
// the wrong approach to use when adding static content to Polyapp, but it is also the most flexible since it does not
// require any components and lets you directly put information into the DOM.
func ActionHomeLoad(request *common.POSTRequest, response *common.POSTResponse) error {
	var html bytes.Buffer
	html.WriteString(`<div class="container-fluid px-5">
<h1>Polyapp</h1>`)
	useractivities := make([]UserActivity, 0)
	q := allDB.Query{}
	q.Init("polyapp", "User", "iRQJHSitiUYNJJFfNSNCiFxwo", common.CollectionData)
	q.AddEquals("polyapp_User_iRQJHSitiUYNJJFfNSNCiFxwo_User ID", request.UserID)
	err := q.AddSortBy("polyapp_User_iRQJHSitiUYNJJFfNSNCiFxwo_Event Time", "desc")
	if err != nil {
		return fmt.Errorf("q.AddSortBy: %w", err)
	}
	q.SetLength(20)
	iter, err := q.QueryRead()
	if err != nil {
		return fmt.Errorf("q.QueryRead: %w", err)
	}
	for {
		queryable, err := iter.Next()
		if err != nil && err == common.IterDone {
			break
		}
		if err != nil {
			return fmt.Errorf("iter.Next: %w", err)
		}
		d := queryable.(*common.Data)
		ua := UserActivity{}
		err = common.DataIntoStructure(d, &ua)
		if err != nil {
			return fmt.Errorf("DataIntoStructure: %w", err)
		}
		useractivities = append(useractivities, ua)
	}

	html.WriteString(`<h3>Recent Tasks</h3>`)
	refs := make([]common.GETRequest, 0)
	for i := range useractivities {
		getRef, err := common.ParseRef(useractivities[i].EventURL)
		if err != nil {
			return fmt.Errorf("common.parseref %v: %w", useractivities[i].EventURL, err)
		}
		if getRef.TaskID == common.HomeTaskID && getRef.SchemaID == common.HomeSchemaID && getRef.UXID == common.HomeUXID {
			// Nobody cares that we recently visited the Home Task.
			continue
		}
		seen := false
		for _, ref := range refs {
			if ref.GetTask() == getRef.GetTask() {
				seen = true
				break
			}
		}
		if !seen {
			refs = append(refs, getRef)
		}
	}
	taskListItems := make([]gen.TaskListItem, len(refs))
	for i := range refs {
		t, err := allDB.ReadTask(refs[i].GetTask())
		if err != nil && strings.Contains(err.Error(), "code = NotFound") {
			// Happens when you delete a Task someone recently viewed
			taskListItems[i] = gen.TaskListItem{
				ID:             "",
				Name:           "[Deleted]",
				HelpText:       "[Deleted]",
				ChangeDataLink: "/",
				NewDataLink:    "/",
				EditTaskLink:   "/",
			}
			continue
		}
		if err != nil {
			return fmt.Errorf("allDB.ReadTask: %w", err)
		}

		queryable, err := allDB.QueryEquals(common.CollectionData, common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Task ID"), t.FirestoreID)
		if err != nil {
			return fmt.Errorf("allDB.QueryEquals %v: %w", i, err)
		}
		if queryable == nil {
			return fmt.Errorf("QueryEquals returned nil when searching at key %v value %v", common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Task ID"), t.FirestoreID)
		}
		d := queryable.(*common.Data)
		taskListItems[i] = gen.TaskListItem{
			ID:             "",
			Name:           t.Name,
			HelpText:       t.HelpText,
			ChangeDataLink: `/polyappChangeData?industry=` + t.IndustryID + `&domain=` + t.DomainID + `&task=` + t.FirestoreID + `&schema=` + refs[i].SchemaID + `&ux=` + refs[i].UXID,
			NewDataLink:    common.CreateURL(t.IndustryID, t.DomainID, t.FirestoreID, refs[i].UXID, refs[i].SchemaID, "new", "", "", "", ""),
			EditTaskLink:   common.CreateURL("polyapp", "TaskSchemaUX", "gqyLWclKrvEaWHalHqUdHghrt", "hRpIEGPgKvxSzvnCeDpAVcwgJ", "gjGLMlcNApJyvRjmgQbopsXPI", d.FirestoreID, "", "", "", ""),
		}
	}

	err = gen.GenTaskList(taskListItems, &html)
	if err != nil {
		return fmt.Errorf("gen.GenTaskList: %w", err)
	}

	err = gen.GenFullTextSearch(gen.FullTextSearch{
		ID:         "polyapp_polyapp_polyapp_Full Text Search",
		Label:      "Search for Tasks",
		HelpText:   "When you type into this search bar we automatically search for Tasks which contain the words you type in either their Name or Help Text fields.",
		Readonly:   false,
		Collection: common.CollectionTask,
	}, &html)
	if err != nil {
		return fmt.Errorf("gen.GenFullTextSearch: %w", err)
	}

	html.WriteString(`</div>`) // closes the div with class="container-fluid px-5"

	response.ModDOMs = append(response.ModDOMs,
		common.ModDOM{
			DeleteSelector: "h2",
		},
		common.ModDOM{
			DeleteSelector: "h6",
		},
		common.ModDOM{
			InsertSelector: "#polyapp_User_BJVHCgzBvaahJFBMOGTZSniwK_Done",
			Action:         "beforebegin",
			HTML:           html.String(),
		},
		common.ModDOM{
			DeleteSelector: "#polyapp_User_BJVHCgzBvaahJFBMOGTZSniwK_Done",
		})
	return nil
}

type deleteData struct {
	DataID string `suffix:"Data ID"`
}

// ActionDeleteData deletes a Data document from the database. After deletion there is no way to recover the deleted
// data. This will also delete any child documents (documents referenced by this document). For instance, if you have an
// Edit Task Data document and you delete it, it would delete itself and all of the Fields documents and all of the
// Subtasks documents associated with the Edit Task Data document.
func ActionDeleteData(data *common.Data, request *common.POSTRequest) error {
	deleteData := deleteData{}
	err := common.DataIntoStructure(data, &deleteData)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}
	dataToDelete := common.Data{
		FirestoreID: deleteData.DataID,
	}
	err = allDB.Read(&dataToDelete)
	if err != nil {
		return fmt.Errorf("allDB.ReadData: %w", err)
	}
	var user common.User
	if request.UserCache != nil && request.UserCache.FirestoreID != "" {
		user = *request.UserCache
	}
	if user.FirestoreID == "" && request.UserID != "" {
		user, err = allDB.ReadUser(request.UserID)
		if err != nil {
			return fmt.Errorf("allDB.ReadUser: %w", err)
		}
	}
	if user.FirestoreID == "" {
		return errors.New("User could not be found so we can't authorize their access to Delete Data")
	}
	isAuthorized := false
	for _, roleID := range user.Roles {
		role, err := allDB.ReadRole(roleID)
		if err != nil {
			return fmt.Errorf("allDB.ReadRole: %w", err)
		}
		hasAccess, err := common.RoleHasAccess(&role, dataToDelete.IndustryID, dataToDelete.DomainID, dataToDelete.SchemaID, 'd')
		if err != nil {
			return fmt.Errorf("common.RoleHasAccess for role %v: %w", role.FirestoreID, err)
		}
		if hasAccess {
			isAuthorized = true
			break
		}
	}
	if !isAuthorized {
		return errors.New("user is not authorized for accessing the Data being deleted so they are not allowed to delete it")
	}
	allChildDocuments := make([]string, 0)
	for _, ref := range dataToDelete.Ref {
		getReq, err := common.ParseRef(*ref)
		if err != nil {
			return fmt.Errorf("common.ParseRef %v: %w", ref, err)
		}
		allChildDocuments = append(allChildDocuments, getReq.DataID)
	}
	for k, refs := range dataToDelete.ARef {
		for _, ref := range refs {
			getReq, err := common.ParseRef(ref)
			if err != nil {
				return fmt.Errorf("common.ParseRef %v in ARef key %v: %w", ref, k, err)
			}
			allChildDocuments = append(allChildDocuments, getReq.DataID)
		}
	}
	// TODO create allDB.MassDelete, MassRead, etc. functions which use a transaction to do this and fail if any of them fail
	for _, dataID := range allChildDocuments {
		err = allDB.DeleteData(dataID)
		if err != nil {
			return fmt.Errorf("allDB.DeleteData %v: %w", dataID, err)
		}
	}
	err = allDB.DeleteData(dataToDelete.FirestoreID)
	if err != nil {
		return fmt.Errorf("allDB.DeleteData %v: %w", dataToDelete.FirestoreID, err)
	}
	return nil
}

type adminDelete struct {
	Schema     string
	DocumentID string `suffix:"Document ID"`
}

// ActionDeleteAdmin deletes a document from a non-Data collection from the Database. After deletion there is
// no way to recover the deleted document.
//
// Warning: there is no access control which prevents someone from deleting documents from every collection.
func ActionDeleteAdmin(data *common.Data) error {
	adminDelete := adminDelete{}
	err := common.DataIntoStructure(data, &adminDelete)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}
	switch adminDelete.Schema {
	case "Task":
		err = allDB.DeleteTask(adminDelete.DocumentID)
		if err != nil {
			return fmt.Errorf("allDB.DeleteTask: %w", err)
		}
	case "Schema":
		err = allDB.DeleteSchema(adminDelete.DocumentID)
		if err != nil {
			return fmt.Errorf("allDB.DeleteSchema: %w", err)
		}
	case "UX":
		err = allDB.DeleteUX(adminDelete.DocumentID)
		if err != nil {
			return fmt.Errorf("allDB.DeleteUX: %w", err)
		}
	case "Bot":
		err = allDB.DeleteBot(adminDelete.DocumentID)
		if err != nil {
			return fmt.Errorf("allDB.DeleteBot: %w", err)
		}
	case "Action":
		err = allDB.DeleteAction(adminDelete.DocumentID)
		if err != nil {
			return fmt.Errorf("allDB.DeleteAction: %w", err)
		}
	case "User":
		err = allDB.DeleteUser(adminDelete.DocumentID)
		if err != nil {
			return fmt.Errorf("allDB.DeleteUser: %w", err)
		}
	case "Role":
		err = allDB.DeleteRole(adminDelete.DocumentID)
		if err != nil {
			return fmt.Errorf("allDB.DeleteRole: %w", err)
		}
	default:
		return errors.New("Unhandled admin delete schema: " + adminDelete.Schema)
	}
	return nil
}

type HTTPSCallRequest struct {
	CurrentData map[string]interface{} `json:"CurrentData"`
	// ReferencedData is made up of simplified Data documents which were referenced in CurrentData via data.Ref or data.ARef.
	ReferencedData map[string]map[string]interface{} `json:"ReferencedData"`
	Request        *common.POSTRequest               `json:"Request"`
	Response       *common.POSTResponse              `json:"Response"`
	BotStaticData  map[string]string                 `json:"BotStaticData"`
}

type HTTPSCallResponse struct {
	Request  *common.POSTRequest  `json:"Request"`
	Response *common.POSTResponse `json:"Response"`
	// CreateDatas will only be created if the Data being created passes validation and the User has access to the Industry and Domain or the Datas ID.
	CreateDatas []common.Data `json:"CreateDatas"`
	// UpdateDatas will only be updated if the Data being created passes validation and the User has access to the Industry and Domain or the Datas ID.
	UpdateDatas []common.Data `json:"UpdateDatas"`
	// DeleteDatas will only be deleted if the Data being deleted exists and the User has access to the Industry and Domain or the Datas ID.
	DeleteDatas []string `json:"DeleteDatas"`
}

// ActionHTTPSCall makes a call for an HTTP verb to a configurable URL. The request location is specified by the HTTPSCall
// bot static data. The body of the request conforms to the type HTTPSCall.
//
// Supported HTTP verbs: GET, HEAD, POST, PUT, PATCH, DELETE
//
// Example configuration in BotStaticData: HTTPSCall GET https://example.com?aParam=value
// Example 2: HTTPSCall POST https://example.com?aParam=value
//
// You can also configure acceptable response codes. If you do not set this, it is set to []int{200}
//
// Example configuration: HTTPSCallAcceptStatusCodes 200,404
// Example Configuration: HTTPSCallAcceptStatusCodes 200
func ActionHTTPSCall(currentData *common.Data, request *common.POSTRequest, response *common.POSTResponse, botStaticData map[string]string) error {
	var err error
	var httpsCall string
	var acceptStatusCodesString string
	for k, v := range botStaticData {
		if k == "HTTPSCall" {
			httpsCall = v
		} else if k == "HTTPSCallAcceptStatusCodes" {
			acceptStatusCodesString = v
		}
	}
	if httpsCall == "" {
		return errors.New("HTTPSCall was not set so we do not know where to make the HTTPS Call in ActionStatusCall")
	}
	acceptStatusCodes := []int{200}
	if len(acceptStatusCodesString) > 0 {
		acceptStatusCodes = make([]int, 0)
		splitAcceptStatusCodesString := strings.Split(acceptStatusCodesString, ",")
		for i := range splitAcceptStatusCodesString {
			convertedStatusCode, err := strconv.Atoi(splitAcceptStatusCodesString[i])
			if err != nil {
				return fmt.Errorf("strconv.Atoi(splitAcceptStatusCodesString[i]) for string %v: %w", splitAcceptStatusCodesString[i], err)
			}
			acceptStatusCodes = append(acceptStatusCodes, convertedStatusCode)
		}
	}
	splitHTTPSCall := strings.Split(httpsCall, " ")
	if len(splitHTTPSCall) < 2 {
		return errors.New("splitHTTPSCall length was less than 2 so a HTTP verb was not included")
	}
	if splitHTTPSCall[0] == "" || splitHTTPSCall[1] == "" {
		return errors.New("splitHTTPSCall resulted in one part of the call being empty string")
	}

	httpVerb := strings.ToUpper(splitHTTPSCall[0])
	switch httpVerb {
	case http.MethodGet, http.MethodHead, http.MethodPost, http.MethodPut, http.MethodPatch, http.MethodDelete:
	default:
		return fmt.Errorf("HTTP verb must precede the HTTPS URL to use and be GET, HEAD, POST, PUT, PATCH, or DELETE. Verb seen: %v", httpVerb)
	}

	URL := strings.ToLower(splitHTTPSCall[1])
	parsedURL, err := url.Parse(URL)
	if err != nil {
		return fmt.Errorf("url.Parse failure for URL %v : %w", URL, err)
	}
	if parsedURL.Scheme != "https" {
		return fmt.Errorf("url.Parse Scheme was not https; it was: %v", parsedURL.Scheme)
	}

	referencedData := make(map[string]map[string]interface{})
	var referencedDataLock sync.Mutex
	errChan := make(chan error)
	for _, v := range currentData.Ref {
		go func(v string, errChan chan error) {
			currentDataRef, err := common.ParseRef(v)
			if err != nil {
				errChan <- fmt.Errorf("common.ParseRef %v: %w", v, err)
				return
			}
			refData := common.Data{
				FirestoreID: currentDataRef.DataID,
			}
			err = allDB.Read(&refData)
			if err != nil {
				errChan <- fmt.Errorf("allDB.ReadData for currentDataRef %v: %w", currentDataRef.DataID, err)
				return
			}
			simplifiedRefData, err := refData.Simplify()
			if err != nil {
				errChan <- fmt.Errorf("refData.Simplify for currentDataRef %v: %w", currentDataRef.DataID, err)
				return
			}
			referencedDataLock.Lock()
			referencedData[refData.FirestoreID] = simplifiedRefData
			referencedDataLock.Unlock()
			errChan <- nil
		}(*v, errChan)
	}
	combinedErr := ""
	for range currentData.Ref {
		err := <-errChan
		if err != nil {
			combinedErr += err.Error() + "; "
		}
	}
	if len(combinedErr) > 0 {
		return errors.New("combinedErr from reading currentData.Ref: " + combinedErr)
	}
	numElements := 0
	for _, vArray := range currentData.ARef {
		for _, v := range vArray {
			go func(v string) {
				currentDataRef, err := common.ParseRef(v)
				if err != nil {
					errChan <- fmt.Errorf("common.ParseRef %v: %w", v, err)
					return
				}
				refData := common.Data{
					FirestoreID: currentDataRef.DataID,
				}
				err = allDB.Read(&refData)
				if err != nil {
					errChan <- fmt.Errorf("allDB.ReadData for currentDataRef %v: %w", currentDataRef.DataID, err)
					return
				}
				simplifiedRefData, err := refData.Simplify()
				if err != nil {
					errChan <- fmt.Errorf("refData.Simplify for currentDataRef %v: %w", currentDataRef.DataID, err)
					return
				}
				referencedDataLock.Lock()
				referencedData[refData.FirestoreID] = simplifiedRefData
				referencedDataLock.Unlock()
				errChan <- nil
			}(v)
			numElements++
		}
	}
	combinedErr = ""
	for i := 0; i < numElements; i++ {
		err := <-errChan
		if err != nil {
			combinedErr += err.Error() + "; "
		}
	}
	if len(combinedErr) > 0 {
		return errors.New("combinedErr from reading currentData.ARef: " + combinedErr)
	}

	dataSimplified, err := currentData.Simplify()
	if err != nil {
		return fmt.Errorf("currentData.Simplify: %w", err)
	}
	HTTPSCallBody := HTTPSCallRequest{
		CurrentData:    dataSimplified,
		ReferencedData: referencedData,
		Request:        request,
		Response:       response,
		BotStaticData:  botStaticData,
	}
	body, err := json.Marshal(HTTPSCallBody)
	if err != nil {
		return fmt.Errorf("json.Marshal: %w", err)
	}
	outgoingRequest, err := http.NewRequest(httpVerb, URL, bytes.NewReader(body))
	if err != nil {
		return fmt.Errorf("http.NewRequest: %w", err)
	}
	outgoingRequest.Header.Set("Content-Type", "application/json")
	outgoingRequest.Close = true
	incomingResponse, err := http.DefaultClient.Do(outgoingRequest)
	if err != nil {
		return fmt.Errorf("http.DefaultClient.Do: %w", err)
	}
	defer incomingResponse.Body.Close()
	ok := false
	for i := range acceptStatusCodes {
		if incomingResponse.StatusCode == acceptStatusCodes[i] {
			ok = true
			break
		}
	}
	if !ok {
		errorResponse, err := ioutil.ReadAll(incomingResponse.Body)
		if err != nil {
			return fmt.Errorf("incomingResponse Status Code (%v) was not acceptable. Only these are acceptable: %v; response message not provided", incomingResponse.StatusCode, acceptStatusCodes)
		}
		return fmt.Errorf("incomingResponse Status Code (%v) was not acceptable. Based on the configuration provided only these status codes are acceptable: %v; response message: %v", incomingResponse.StatusCode, acceptStatusCodes, string(errorResponse))
	}
	if incomingResponse.StatusCode < 200 || incomingResponse.StatusCode > 299 {
		// Response codes in this range are either Informational, Redirects, Client Errors, or Server Errors.
		// Informational responses are not likely to contain a body which we should be parsing.
		// Redirects are ignored by this code to help simplify it.
		// Client errors and server errors are unlikely to return a response body which we want to store somewhere.
		// I'm also slightly concerned that an address will return a response which coincidentally partially matches
		// the response struct and it'll end up causing unexpected behavior. This is unlikely, but it's better to be safe than sorry.
		return nil
	}
	responseBody, err := ioutil.ReadAll(incomingResponse.Body)
	if err != nil {
		return fmt.Errorf("ioutil.ReadAll for incomingResponse.Body: %w", err)
	}
	if len(responseBody) == 0 {
		return nil
	}
	var HTTPSCallResponse HTTPSCallResponse
	err = json.Unmarshal(responseBody, &HTTPSCallResponse)
	if err != nil {
		return fmt.Errorf("json.Unmarshal for responseBody: %w", err)
	}

	var user common.User
	if request.UserCache != nil && request.UserCache.FirestoreID != "" {
		user = *request.UserCache
	}
	if user.FirestoreID == "" && request.UserID != "" {
		user, err = allDB.ReadUser(request.UserID)
		if err != nil {
			return fmt.Errorf("allDB.ReadUser: %w", err)
		}
	}
	if user.FirestoreID == "" {
		return errors.New("User could not be found so we can't authorize their access to Delete Data")
	}
	allRoles := make([]common.Role, 0)
	for _, roleID := range user.Roles {
		role, err := allDB.ReadRole(roleID)
		if err != nil {
			return fmt.Errorf("allDB.ReadRole: %w", err)
		}
		allRoles = append(allRoles, role)
	}

	for i := range HTTPSCallResponse.CreateDatas {
		isAuthorized := false
		for i := range allRoles {
			isAuthorized, err = common.RoleHasAccess(&allRoles[i], HTTPSCallResponse.CreateDatas[i].IndustryID, HTTPSCallResponse.CreateDatas[i].DomainID, HTTPSCallResponse.CreateDatas[i].SchemaID, 'c')
			if err != nil {
				return fmt.Errorf("common.RoleHasAccess for role %v: %w", allRoles[i].FirestoreID, err)
			}
			if isAuthorized {
				break
			}
		}
		if !isAuthorized {
			return fmt.Errorf("user is not authorized for accessing the Data %v being deleted so they are not allowed to delete it", HTTPSCallResponse.CreateDatas[i].FirestoreID)
		}
	}
	for i := range HTTPSCallResponse.UpdateDatas {
		isAuthorized := false
		for i := range allRoles {
			isAuthorized, err = common.RoleHasAccess(&allRoles[i], HTTPSCallResponse.UpdateDatas[i].IndustryID, HTTPSCallResponse.UpdateDatas[i].DomainID, HTTPSCallResponse.UpdateDatas[i].SchemaID, 'u')
			if err != nil {
				return fmt.Errorf("common.RoleHasAccess for role %v: %w", HTTPSCallResponse.UpdateDatas[i].FirestoreID, err)
			}
			if isAuthorized {
				break
			}
		}
		if !isAuthorized {
			return fmt.Errorf("user is not authorized for accessing the Data %v being deleted so they are not allowed to delete it", HTTPSCallResponse.UpdateDatas[i].FirestoreID)
		}
	}
	for i := range HTTPSCallResponse.DeleteDatas {
		isAuthorized := false
		for i := range allRoles {
			d := common.Data{
				FirestoreID: HTTPSCallResponse.DeleteDatas[i],
			}
			err = allDB.Read(&d)
			if err != nil {
				return fmt.Errorf("allDB.ReadDatas: %w", err)
			}
			isAuthorized, err = common.RoleHasAccess(&allRoles[i], d.IndustryID, d.DomainID, d.SchemaID, 'd')
			if err != nil {
				return fmt.Errorf("common.RoleHasAccess for role %v: %w", allRoles[i].FirestoreID, err)
			}
			if isAuthorized {
				break
			}
		}
		if !isAuthorized {
			return fmt.Errorf("user is not authorized for accessing the Data %v being deleted so they are not allowed to delete it", HTTPSCallResponse.DeleteDatas[i])
		}
	}
	for i := range HTTPSCallResponse.CreateDatas {
		err = allDB.CreateData(&HTTPSCallResponse.CreateDatas[i])
		if err != nil {
			return fmt.Errorf("allDB.CreateDatas %v: %w", HTTPSCallResponse.CreateDatas[i].FirestoreID, err)
		}
	}
	for i := range HTTPSCallResponse.UpdateDatas {
		err = allDB.UpdateData(&HTTPSCallResponse.UpdateDatas[i])
		if err != nil {
			return fmt.Errorf("allDB.UpdateDatas %v: %w", HTTPSCallResponse.UpdateDatas[i].FirestoreID, err)
		}
	}
	for i := range HTTPSCallResponse.DeleteDatas {
		err = allDB.DeleteData(HTTPSCallResponse.DeleteDatas[i])
		if err != nil {
			return fmt.Errorf("allDB.DeleteDatas %v: %w", HTTPSCallResponse.DeleteDatas[i], err)
		}
	}
	if HTTPSCallResponse.Response != nil {
		// It is currently safe to completely overwrite the Response
		response.NewURL = HTTPSCallResponse.Response.NewURL
		if len(HTTPSCallResponse.Response.ModDOMs) > 0 {
			response.ModDOMs = append(response.ModDOMs, HTTPSCallResponse.Response.ModDOMs...)
		}
		if HTTPSCallResponse.Response.DataUpdates != nil {
			response.DataUpdates = HTTPSCallResponse.Response.DataUpdates
		}
	}
	if HTTPSCallResponse.Request != nil {
		// On the other hand we can't trust a remote server to change anything but the most basic parts of Request
		request.Data = HTTPSCallResponse.Request.Data
		request.IsDone = HTTPSCallResponse.Request.IsDone
		request.FinishURL = HTTPSCallResponse.Request.FinishURL
		// For example, being able to set request.UserID remotely would be a security disaster.
	}

	// Unlike most Actions which set currentData in the course of their execution,
	// currentData may be out of sync with the database because another server modified it.
	currentData = &common.Data{
		FirestoreID: currentData.FirestoreID,
	}
	err = allDB.Read(currentData)
	if err != nil {
		return fmt.Errorf("allDB.ReadData: %w", err)
	}

	return nil
}

type importS struct {
	DataFormat            string   `suffix:"Data Format"`
	FileToImport          []byte   `suffix:"File to Import"`
	SchemaID              string   `suffix:"Schema ID"`
	ImportedDataIDs       []string `suffix:"Imported Data IDs"`
	OverwriteExistingData bool     `suffix:"Overwrite Existing Data"`
}

// ActionImportData is used by the Import Task to help import Data into Polyapp.
func ActionImportData(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	importS := importS{}
	err = common.DataIntoStructure(data, &importS)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}
	if importS.FileToImport == nil || len(importS.FileToImport) == 0 {
		importS.FileToImport, err = allDB.ReadDataBytes(data.FirestoreID, common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, "File to Import"))
		if err != nil {
			return fmt.Errorf("allDB.ReadDataBytes: %w", err)
		}
	}
	if len(importS.ImportedDataIDs) > 0 {
		return errors.New("already imported according to ImportedDataIDs")
	}
	if len(importS.FileToImport) < 1 {
		return errors.New("File To Import is required")
	}

	schema, err := allDB.ReadSchema(importS.SchemaID)
	if err != nil {
		return fmt.Errorf("allDB.ReadSchema: %w", err)
	}

	allData := make([]*common.Data, 0)
	switch importS.DataFormat {
	case "CSV":
		csvReader := csv.NewReader(bytes.NewReader(importS.FileToImport))
		firstRow, err := csvReader.Read()
		if err == io.EOF {
			return errors.New("data to import was an empty file")
		}
		if err != nil {
			return fmt.Errorf("csvReader.Read first row: %w", err)
		}
		expectedRowLength := len(firstRow)
		keys := make([]string, len(firstRow))
		types := make([]string, len(firstRow))
		for i := range firstRow {
			keys[i] = common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, firstRow[i])
			types[i] = schema.DataTypes[keys[i]]
		}
		for {
			importedDataRow, err := csvReader.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				return fmt.Errorf("csvReader.Read(): %w", err)
			}
			if expectedRowLength == 0 {
				expectedRowLength = len(importedDataRow)
			}
			if len(importedDataRow) != expectedRowLength {
				continue
			}
			importedData := common.Data{}
			err = importedData.Init(nil)
			if err != nil {
				return fmt.Errorf("importedData.Init: %w", err)
			}
			importedData.FirestoreID = common.GetRandString(25)
			importedData.IndustryID = schema.IndustryID
			importedData.DomainID = schema.DomainID
			importedData.SchemaID = schema.SchemaID
			for i := range importedDataRow {
				err = putStringInData(importedDataRow[i], &importedData, types[i], keys[i])
				if err != nil {
					return fmt.Errorf("putStringInData: %w", err)
				}
			}
			allData = append(allData, &importedData)
		}
	case "JSON (from Polyapp)":
		fullMap := make(map[string]map[string]interface{})
		err = json.Unmarshal(importS.FileToImport, &fullMap)
		if err != nil {
			return fmt.Errorf("couldn't unmarshal while reading: %w", err)
		}
		for k := range fullMap {
			newData := common.Data{}
			err = newData.Init(fullMap[k])
			if err != nil {
				return fmt.Errorf("Init %v: %w", k, err)
			}
			newData.FirestoreID = k
			if !importS.OverwriteExistingData || newData.FirestoreID == "" {
				newData.FirestoreID = common.GetRandString(25)
			}
			allData = append(allData, &newData)
		}
	case "JSON (arbitrary)":
		fullMap := make(map[string]interface{})
		err = json.Unmarshal(importS.FileToImport, &fullMap)
		if err != nil {
			return fmt.Errorf("json.Unmarshal: %w", err)
		}
		allData, err = ImportArbitraryJSONRecursively(fullMap, schema)
		if err != nil {
			return fmt.Errorf("ImportArbitraryJSONRecursively: %w", err)
		}
	default:
		return fmt.Errorf("unhandled DataFormat: %v", importS.DataFormat)
	}

	// I am intentionally delaying starting saving the data until we know the entire input file is A OK.
	errChan := make(chan error)
	for i := range allData {
		go func(d *common.Data, i int, errChan chan error) {
			if importS.OverwriteExistingData {
				err := allDB.DeleteData(d.FirestoreID)
				if err != nil && strings.Contains(err.Error(), "code = NotFound") {
					// OK
				} else if err != nil {
					errChan <- fmt.Errorf("allDB.DeleteData %v: %w", d.FirestoreID, err)
					return
				}
			}
			err := allDB.Create(d)
			if err != nil {
				errChan <- fmt.Errorf("allDB.CreateData %v index %v: %w", d.FirestoreID, i, err)
				return
			}
			errChan <- nil
		}(allData[i], i, errChan)
	}
	combinedErr := ""
	for range allData {
		err := <-errChan
		if err != nil {
			combinedErr += err.Error() + "; "
		}
	}
	if len(combinedErr) > 0 {
		return fmt.Errorf("errors creating data: %v", combinedErr)
	}

	for i := range allData {
		importS.ImportedDataIDs = append(importS.ImportedDataIDs, allData[i].FirestoreID)
	}

	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &importS, data)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}

	if data.FirestoreID == "" {
		// happens if the inputs to this function are from a dummy Data.
	} else {
		err = allDB.UpdateData(data)
		if err != nil && strings.Contains(err.Error(), "code = NotFound") {
			err = allDB.Create(data)
			if err != nil {
				return fmt.Errorf("allDB.Create: %w", err)
			}
		} else if err != nil {
			return fmt.Errorf("allDB.UpdateData: %w", err)
		}
	}

	response.NewURL = common.CreateURL(request.IndustryID, request.DomainID, request.TaskID, request.UXID, request.SchemaID,
		request.DataID, request.UserID, request.RoleID, request.OverrideIndustryID, request.OverrideDomainID)

	return nil
}

type importDataAndCreateTask struct {
	Industry        string   `suffix:"Industry"`
	Domain          string   `suffix:"Domain"`
	DataFormat      string   `suffix:"Data Format"`
	FileToImport    []byte   `suffix:"File to Import"`
	ImportedDataIDs []string `suffix:"Imported Data IDs"`
	EditTaskDataID  string   `suffix:"Edit Task Data ID"`
}

// ActionImportDataAndCreateTask imports data from the specified source and examines it to determine a schema for the Data.
// Once a schema is determined, a new entry in Edit Task is created. This Edit Task is then used to create a new Task, Schema, and UX.
// The final step is to import the source data into the newly created Schema.
//
// This bot should be triggered at Done.
func ActionImportDataAndCreateTask(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	importDataAndCreateTask := importDataAndCreateTask{}
	err = common.DataIntoStructure(data, &importDataAndCreateTask)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}
	importDataAndCreateTask.FileToImport, err = allDB.ReadDataBytes(data.FirestoreID, common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, "File to Import"))
	if err != nil {
		return fmt.Errorf("allDB.ReadDataBytes: %w", err)
	}
	if len(importDataAndCreateTask.FileToImport) < 1 {
		return errors.New("File To Import is required")
	}
	if len(importDataAndCreateTask.ImportedDataIDs) > 0 {
		return errors.New("already imported according to ImportedDataIDs")
	}
	if importDataAndCreateTask.Industry == "" {
		return errors.New("Industry not populated")
	}
	if importDataAndCreateTask.Domain == "" {
		return errors.New("Domain not populated")
	}

	var fieldNames []string
	var firstDataRow []string
	switch importDataAndCreateTask.DataFormat {
	case "CSV":
		csvReader := csv.NewReader(bytes.NewReader(importDataAndCreateTask.FileToImport))
		fieldNames, err = csvReader.Read()
		if err == io.EOF {
			return errors.New("data to import was an empty file")
		}
		if err != nil {
			return fmt.Errorf("csvReader.Read first row: %w", err)
		}
		firstDataRow, err = csvReader.Read()
		if err == io.EOF {
			return errors.New("data to import did not have at least one data row")
		}
		if err != nil {
			return fmt.Errorf("csvReader.Read first data row: %w", err)
		}
	default:
		return errors.New("unsupported import DataFormat: " + importDataAndCreateTask.DataFormat)
	}

	if len(firstDataRow) < 1 || len(fieldNames) < 1 {
		return errors.New("input file did not have parseable column names and/or parseable first row of data")
	}

	editTask, editTaskData, err := CreateEditTaskAndData(importDataAndCreateTask.Industry, importDataAndCreateTask.Domain, fieldNames, firstDataRow)
	if err != nil {
		return fmt.Errorf("CreateEditTaskAndData: %w", err)
	}

	// Now that the Edit Task Datas have been created, we can call the Edit Task Action.
	err = ActionEditTask(&editTaskData, &common.POSTRequest{
		IndustryID: "polyapp",
		DomainID:   "TaskSchemaUX",
		UserID:     request.UserID,
		RoleID:     request.RoleID,
	}, nil)
	if err != nil {
		return fmt.Errorf("ActionEditTask: %w", err)
	}

	err = common.DataIntoStructure(&editTaskData, &editTask)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}

	// We can set the new Edit Task Data into the current Data.
	importDataAndCreateTask.EditTaskDataID = editTaskData.FirestoreID

	// We still need to import the data into our new Schema.
	importS := importS{
		DataFormat:      importDataAndCreateTask.DataFormat,
		FileToImport:    importDataAndCreateTask.FileToImport,
		SchemaID:        editTask.SchemaID,
		ImportedDataIDs: importDataAndCreateTask.ImportedDataIDs,
	}
	importData := common.Data{
		FirestoreID: common.GetRandString(25),
	}
	_ = importData.Init(nil)
	err = common.StructureIntoData("polyapp", "Import", "GpHQudsjtVMmyLgurwuIJUMcY", &importS, &importData)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}
	importData.S[common.AddFieldPrefix("polyapp", "Import", "GpHQudsjtVMmyLgurwuIJUMcY", "Schema ID")] = common.String(importS.SchemaID)
	err = ActionImportData(&importData, &common.POSTRequest{}, &common.POSTResponse{})
	if err != nil {
		return fmt.Errorf("ActionImportData: %w", err)
	}
	importDataAndCreateTask.ImportedDataIDs = importData.AS["polyapp_Import_GpHQudsjtVMmyLgurwuIJUMcY_Imported Data IDs"]

	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &importDataAndCreateTask, data)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}

	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}

	response.NewURL = common.CreateURL(request.IndustryID, request.DomainID, request.TaskID, request.UXID, request.SchemaID,
		request.DataID, request.UserID, request.RoleID, request.OverrideIndustryID, request.OverrideDomainID)

	return nil
}

// CreateEditTaskFromJSON uses allDB.CreateData to create Fields, EditTask, and Subtasks which are represented by
// the provided exampleJSONMap. It returns these as Data in the first return argument.
//
// For a simple JSON structure like this one:
// ```
// {
//   "field": "value",
//   "array": [
//     "array value 1",
//     "array value 2"
//   ]
// }
// ```
//
// The provided map would be translated into a single Task which has a "field" string field and an "array" array field
// composed of strings.
//
// If the exampleJSONMap contains an embedded JSON object, that JSON object is parsed to create a new Task.
// In this way, we can convert the entire JSON object into one or more Tasks, each with their own Schema and UX.
//
// Rather than storing the Data ourselves, the Data are returned after all parsing is complete with the first common.Data
// being the head Data. The Tree may be used to determine dependencies in the returned slice of common.Data.
//
// Known Problem: {"a":{"A":[{"AA":""}]},"b":{"A":[{"AA":"","BB":""}]}}
// Even if we iterated over every option in the first "A" array, we haven't seen every option in "AA" under "b".
// Because we assume that "A"'s schema in "a" is independent from "A"'s schema in "b" we won't assign all possible property values to the subtask under "A".
// To fix this sort of error, try importing only 2 layers of your application at a time.
// Then go into "Edit Task" and edit the subtasks until they work the correct way.
func CreateEditTaskFromJSON(industry, domain, name string, illustrativeJSON map[string]interface{}) ([]common.Data, *Tree, error) {
	var err error
	if industry == "" || domain == "" || illustrativeJSON == nil || len(illustrativeJSON) == 0 {
		return nil, nil, errors.New("not all inputs were provided")
	}
	tree := Tree{
		Children: make([]*Tree, 0),
	}
	editTask := EditTask{
		Name:                      name,
		HelpText:                  "TODO",
		AgreeToMakePublic:         false,
		ExportAsJSONToFile:        false,
		TaskID:                    "",
		SchemaID:                  "",
		UXID:                      "",
		Industry:                  industry,
		Domain:                    domain,
		BotsTriggeredAtLoad:       []string{},
		BotsTriggeredContinuously: []string{},
		BotsTriggeredAtDone:       []string{},
		BotStaticData:             []string{},
		Done:                      false,
		Fields:                    []string{},
		Subtasks:                  []string{},
	}
	subtasks := make([]Subtask, 0)
	subtaskDatas := make([]common.Data, 0)
	fields := make([]Field, 0)
	fieldDatas := make([]common.Data, 0)
	// The biggest problem I'm seeing is that the ordering is random. We aren't given the ordering of the fields in the
	// map illustrativeJSON because ordering in JSON is supposed to be random: https://github.com/golang/go/issues/27179
	// Because I am unwilling to add a non-stdlib package to handle this I'm going to order the fields by key.
	sortedIllustrativeJSONKeys := make([]string, len(illustrativeJSON))
	i := 0
	for k := range illustrativeJSON {
		sortedIllustrativeJSONKeys[i] = k
		i++
	}
	sort.Strings(sortedIllustrativeJSONKeys)
	for _, k := range sortedIllustrativeJSONKeys {
		safeKey := strings.ReplaceAll(k, "_", " ")
		isSubtask := false
		switch v := illustrativeJSON[safeKey].(type) {
		case string:
			userInterface := "text"
			if len(v) > 50 {
				userInterface = "textarea"
			}
			fields = append(fields, Field{
				Name:          safeKey,
				HelpText:      fmt.Sprintf("Example: %v", v),
				UserInterface: userInterface,
				SelectOptions: []string{},
			})
		case float64, float32, int, int64, int32, int16, int8:
			fields = append(fields, Field{
				Name:          safeKey,
				HelpText:      fmt.Sprintf("Example: %v", v),
				UserInterface: "number", // TODO support integer only fields
				SelectOptions: []string{},
			})
		case bool:
			fields = append(fields, Field{
				Name:          safeKey,
				HelpText:      fmt.Sprintf("Example: %v", v),
				UserInterface: "checkbox",
				SelectOptions: []string{},
			})
		case []string:
			fields = append(fields, Field{
				Name:          safeKey,
				HelpText:      fmt.Sprintf("Example: %v", v),
				UserInterface: "list of single line text inputs",
				SelectOptions: []string{},
			})
		case []bool:
			fields = append(fields, Field{
				Name:          safeKey,
				HelpText:      fmt.Sprintf("Example: %v", v),
				UserInterface: "list of checkboxes",
				SelectOptions: []string{},
			})
		case []float64, []float32, []int, []int64, []int32, []int16, []int8:
			fields = append(fields, Field{
				Name:          safeKey,
				HelpText:      fmt.Sprintf("Example: %v", v),
				UserInterface: "list of numbers",
				SelectOptions: []string{},
			})
		case []interface{}:
			if len(v) == 0 {
				// assume an empty array is []string
				fields = append(fields, Field{
					Name:          safeKey,
					HelpText:      fmt.Sprintf("Example: %v", v),
					UserInterface: "list of single line text inputs",
					SelectOptions: []string{},
				})
			} else {
				switch v[0].(type) {
				case string:
					fields = append(fields, Field{
						Name:          safeKey,
						HelpText:      fmt.Sprintf("Example: %v", v),
						UserInterface: "list of single line text inputs",
						SelectOptions: []string{},
					})
				case bool:
					fields = append(fields, Field{
						Name:          safeKey,
						HelpText:      fmt.Sprintf("Example: %v", v),
						UserInterface: "list of checkboxes",
						SelectOptions: []string{},
					})
				case float64, float32, int, int64, int32, int16, int8:
					fields = append(fields, Field{
						Name:          safeKey,
						HelpText:      fmt.Sprintf("Example: %v", v),
						UserInterface: "list of numbers",
						SelectOptions: []string{},
					})
				case map[string]interface{}:
					// If you have: [{"a":"b"},{"a":"b","b":"c"}]
					// And you only check v[0] to find the schema of the ARef Task, then you will think the correct schema
					// is one field named "a", when in fact it should be the superset: 2 fields, "a" and "b".
					// To fix this problem, we should iterate over every map[string]interface{} and create a superset
					// of all of the fields.
					supersetArrayValue := make(map[string]interface{})
					for supersetIndex := range v {
						castVAtIndex, ok := v[supersetIndex].(map[string]interface{})
						if ok && v[supersetIndex] != nil {
							for castVAtIndexKey := range castVAtIndex {
								if supersetArrayValue[castVAtIndexKey] == nil {
									supersetArrayValue[castVAtIndexKey] = castVAtIndex[castVAtIndexKey]
								}
							}
						}
					}

					datas, childTree, err := CreateEditTaskFromJSON(industry, domain, safeKey, supersetArrayValue)
					if err != nil {
						return nil, nil, fmt.Errorf("CreateEditTaskFromJSON map[string]interface{} for key %v: %w", safeKey, err)
					}
					fieldDatas = append(fieldDatas, datas...)
					isSubtask = true
					subtasks = append(subtasks, Subtask{
						TaskID:          *datas[0].S["polyapp_TaskSchemaUX_gjGLMlcNApJyvRjmgQbopsXPI_Task ID"],
						SingletonOrList: "list",
					})
					tree.Children = append(tree.Children, childTree)
				default:
					return nil, nil, errors.New("TODO unhandled array type")
				}
			}
		case interface{}:
			m := v.(map[string]interface{})
			datas, childTree, err := CreateEditTaskFromJSON(industry, domain, safeKey, m)
			if err != nil {
				return nil, nil, fmt.Errorf("CreateEditTaskFromJSON map[string]interface{} for key %v: %w", safeKey, err)
			}
			fieldDatas = append(fieldDatas, datas...)
			subtasks = append(subtasks, Subtask{
				TaskID:          *datas[0].S["polyapp_TaskSchemaUX_gjGLMlcNApJyvRjmgQbopsXPI_Task ID"],
				SingletonOrList: "singleton",
			})
			isSubtask = true
			tree.Children = append(tree.Children, childTree)
		case nil:
			// "null" in JSON could be anything. Let's assume it is a string simply because strings are easy.
			fields = append(fields, Field{
				Name:          safeKey,
				HelpText:      fmt.Sprintf("Example: %v", v),
				UserInterface: "text",
				SelectOptions: []string{},
			})
		default:
			return nil, nil, fmt.Errorf("unhandled type in illustrative JSON key %v value: %v", safeKey, illustrativeJSON[safeKey])
		}
		if isSubtask {
			newSubtaskData := common.Data{}
			_ = newSubtaskData.Init(nil)
			err = common.StructureIntoData("polyapp", "TaskSchemaUX", "SYfKUkHjtbesOmcBsPVyimJjk", &subtasks[len(subtasks)-1], &newSubtaskData)
			if err != nil {
				return nil, nil, fmt.Errorf("StructureIntoData: %w", err)
			}
			newSubtaskData.FirestoreID = common.GetRandString(25)
			subtaskDatas = append(subtaskDatas, newSubtaskData)
			editTask.Subtasks = append(editTask.Subtasks, common.CreateRef("polyapp", "TaskSchemaUX", "MFAzIytaMcyKmeIqoXjkVGhpP", "HOXSNlWYKJycecTKcTUCYZngH", "SYfKUkHjtbesOmcBsPVyimJjk", newSubtaskData.FirestoreID))
		} else {
			newFieldData := common.Data{}
			_ = newFieldData.Init(nil)
			err = common.StructureIntoData("polyapp", "TaskSchemaUX", "SszMuOVRaMIOUteoWiXClbpSC", &fields[len(fields)-1], &newFieldData)
			if err != nil {
				return nil, nil, fmt.Errorf("StructureIntoData: %w", err)
			}
			newFieldData.FirestoreID = common.GetRandString(25)
			fieldDatas = append(fieldDatas, newFieldData)
			editTask.Fields = append(editTask.Fields, common.CreateRef("polyapp", "TaskSchemaUX", "dssHRRryWueHfJDfcCCMgCAsq",
				"tBrxmDfyKRTsOwlXVUgwqgfdo", "SszMuOVRaMIOUteoWiXClbpSC", newFieldData.FirestoreID))
		}
	}

	editTaskData := common.Data{}
	_ = editTaskData.Init(nil)
	editTaskData.SchemaID = "gjGLMlcNApJyvRjmgQbopsXPI"
	err = integrity.BackPopulate(&editTaskData)
	if err != nil {
		return nil, nil, fmt.Errorf("integrity.BackPopulate: %w", err)
	}
	err = common.StructureIntoData("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", &editTask, &editTaskData)
	if err != nil {
		return nil, nil, fmt.Errorf("StructureIntoData: %w", err)
	}
	editTaskData.FirestoreID = common.GetRandString(25)

	tree.FirestoreID = editTaskData.FirestoreID
	datas := []common.Data{editTaskData}
	datas = append(datas, fieldDatas...)
	datas = append(datas, subtaskDatas...)
	return datas, &tree, nil
}

// CreateEditTaskAndData uses allDB.CreateData to create Fields and the EditTask Data which are represented by the provided
// fieldNames and the illustrativeDataRow. len(fieldNames) must == len(illustrativeDataRow)
func CreateEditTaskAndData(industry, domain string, fieldNames []string, illustrativeDataRow []string) (EditTask, common.Data, error) {
	var err error
	if len(fieldNames) != len(illustrativeDataRow) {
		return EditTask{}, common.Data{}, errors.New("len(fieldNames) != len(illustrativeDataRow) which means we can't guess the types of some of the fieldNames or we can't match some data with a field name.")
	}
	editTask := EditTask{
		Name:                      "generated task " + common.GetRandString(25),
		HelpText:                  "TODO",
		AgreeToMakePublic:         false,
		ExportAsJSONToFile:        false,
		TaskID:                    "",
		SchemaID:                  "",
		UXID:                      "",
		Industry:                  industry,
		Domain:                    domain,
		BotsTriggeredAtLoad:       []string{},
		BotsTriggeredContinuously: []string{},
		BotsTriggeredAtDone:       []string{},
		BotStaticData:             []string{},
		Done:                      false,
		Fields:                    []string{},
		Subtasks:                  []string{},
	}
	fields := make([]Field, len(fieldNames))
	for i := range fieldNames {
		userInterface := guessUserInterface(illustrativeDataRow[i])
		fields[i] = Field{
			Name:            fieldNames[i],
			HelpText:        "TODO",
			UserInterface:   userInterface,
			Validator:       "",
			SelectOptions:   []string{},
			UseAsDoneButton: false,
			ReadOnly:        false,
		}
	}
	fieldDatas := make([]common.Data, len(fields))
	for i := range fields {
		_ = fieldDatas[i].Init(nil)
		err = common.StructureIntoData("polyapp", "TaskSchemaUX", "SszMuOVRaMIOUteoWiXClbpSC", &fields[i], &fieldDatas[i])
		if err != nil {
			return EditTask{}, common.Data{}, fmt.Errorf("StructureIntoData: %w", err)
		}
		fieldDatas[i].FirestoreID = common.GetRandString(25)
		editTask.Fields = append(editTask.Fields, common.CreateRef("polyapp", "TaskSchemaUX", "dssHRRryWueHfJDfcCCMgCAsq",
			"tBrxmDfyKRTsOwlXVUgwqgfdo", "SszMuOVRaMIOUteoWiXClbpSC", fieldDatas[i].FirestoreID))
		err = allDB.CreateData(&fieldDatas[i])
		if err != nil {
			return EditTask{}, common.Data{}, fmt.Errorf("allDB.CreateData %v %v: %w", i, fieldDatas[i].FirestoreID, err)
		}
	}
	editTaskData := common.Data{}
	_ = editTaskData.Init(nil)
	editTaskData.SchemaID = "gjGLMlcNApJyvRjmgQbopsXPI"
	err = integrity.BackPopulate(&editTaskData)
	if err != nil {
		return EditTask{}, common.Data{}, fmt.Errorf("integrity.BackPopulate: %w", err)
	}
	err = common.StructureIntoData("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", &editTask, &editTaskData)
	if err != nil {
		return EditTask{}, common.Data{}, fmt.Errorf("StructureIntoData: %w", err)
	}
	editTaskData.FirestoreID = common.GetRandString(25)
	err = allDB.CreateData(&editTaskData)
	if err != nil {
		return EditTask{}, common.Data{}, fmt.Errorf("allDB.CreateData editTaskData: %w", err)
	}
	return editTask, editTaskData, nil
}

func guessUserInterface(s string) string {
	var err error
	_, err = strconv.ParseFloat(s, 64)
	if err == nil {
		return "number"
	}
	_, err = strconv.ParseInt(s, 10, 64)
	if err == nil {
		return "number"
	}
	_, err = strconv.ParseBool(s)
	if err == nil {
		return "checkbox"
	}
	if 50 < len(s) {
		return "multiple line input"
	}
	return "single line input"
}

// putStringInData takes the string, the data you are importing the string into, the string's type, and the string's key.
// TODO handle arrays some how? It is unclear how we should do so
func putStringInData(s string, data *common.Data, t string, k string) error {
	switch t {
	case "S":
		data.S[k] = common.String(s)
	case "B":
		b, err := strconv.ParseBool(s)
		if err != nil {
			return fmt.Errorf("strconv.ParseBool: %w", err)
		}
		data.B[k] = common.Bool(b)
	case "F":
		parsedFloat, err := strconv.ParseFloat(s, 64)
		if err != nil {
			return fmt.Errorf("strconv.ParseFloat: %w", err)
		}
		data.F[k] = common.Float64(parsedFloat)
	case "I":
		parsedInt, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			return fmt.Errorf("strconv.ParseInt: %w", err)
		}
		data.I[k] = common.Int64(parsedInt)
	case "Ref":
		data.Ref[k] = common.String(s)
	case "Bytes":
		data.Bytes[k] = []byte(s)
	default:
		return errors.New("putStringInData can't handle array types, only Strings, Booleans, Floats, Ints, refs, and []byte")
	}
	return nil
}

type exportData struct {
	SchemaID       string `suffix:"Schema ID"`
	DataFormat     string `suffix:"Data Format"`
	RemoveMetadata bool   `suffix:"Remove Metadata"`
	ExportedData   []byte `suffix:"Exported Data"`
}

// ActionExportData exports all the data from one of Polyapp's Schemas into one of several different supported formats.
func ActionExportData(data *common.Data, response *common.POSTResponse) error {
	var err error
	var buf bytes.Buffer
	exportData := exportData{}
	err = common.DataIntoStructure(data, &exportData)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}

	if exportData.SchemaID == "" || exportData.DataFormat == "" {
		return fmt.Errorf("exportData.SchmeaID %v or exportData.DataFormat %v were not populated", exportData.SchemaID, exportData.DataFormat)
	}

	schema, err := allDB.ReadSchema(exportData.SchemaID)
	if err != nil {
		return fmt.Errorf("allDB.ReadSchema: %w", err)
	}
	switch exportData.DataFormat {
	case "CSV":
		w := csv.NewWriter(&buf)
		firstRow := make([]string, len(schema.DataKeys))
		for i := range schema.DataKeys {
			firstRow[i] = common.RemoveFieldPrefix(schema.DataKeys[i])
		}
		err = w.Write(firstRow)
		if err != nil {
			return fmt.Errorf("w.Write for firstRow: %w", err)
		}

		q := allDB.Query{}
		q.Init(schema.IndustryID, schema.DomainID, schema.SchemaID, common.CollectionData)
		q.AddEquals(common.PolyappSchemaID, schema.FirestoreID)
		iter, err := q.QueryRead()
		if err != nil {
			return fmt.Errorf("q.QueryRead: %w", err)
		}
		for {
			d, err := iter.Next()
			if err != nil && err == common.IterDone {
				break
			}
			if err != nil {
				return fmt.Errorf("iter.Next: %w", err)
			}
			dCast := d.(*common.Data)
			// Ignore all bytes
			dCast.Bytes = nil
			dCast.ABytes = nil
			allValues := make([]string, len(schema.DataKeys))
			for i, key := range schema.DataKeys {
				allValues[i] = castValue(dCast, key)
			}
			err = w.Write(allValues)
			if err != nil {
				return fmt.Errorf("w.Write: %w", err)
			}
		}
		w.Flush()
		err = w.Error()
		if err != nil {
			return fmt.Errorf("w.Flush error: %w", err)
		}
		exportData.ExportedData = buf.Bytes()
	case "JSON":
		q := allDB.Query{}
		q.Init(schema.IndustryID, schema.DomainID, schema.SchemaID, common.CollectionData)
		q.AddEquals(common.PolyappSchemaID, schema.FirestoreID)
		iter, err := q.QueryRead()
		if err != nil {
			return fmt.Errorf("q.QueryRead: %w", err)
		}
		fullMap := make(map[string]map[string]interface{})
		for {
			d, err := iter.Next()
			if err != nil && err == common.IterDone {
				break
			}
			if err != nil {
				return fmt.Errorf("iter.Next: %w", err)
			}
			dCast := d.(*common.Data)
			// Ignore all bytes
			dCast.Bytes = nil
			dCast.ABytes = nil

			m, err := dCast.Simplify()
			if err != nil {
				return fmt.Errorf("dCast.Simplify %v: %w", dCast.FirestoreID, err)
			}
			fullMap[dCast.FirestoreID] = m
		}
		marshalled, err := json.Marshal(fullMap)
		if err != nil {
			return fmt.Errorf("json.Marshal: %w", err)
		}
		exportData.ExportedData = marshalled
	case "JSON no prefix":
		// TODO
	case "JSON no prefix camelCase":
		// TODO
	case "JSON no prefix PascalCase":
		// TODO
	case "JSON no prefix underscore_case":
		// TODO
	case "JSON recursive", "JSON recursive no prefix", "JSON recursive no prefix camelCase", "JSON recursive no prefix PascalCase", "JSON recursive no prefix underscore_case":
		keyStyle := ""
		if strings.HasSuffix(exportData.DataFormat, "camelCase") {
			keyStyle = "camelCase"
		} else if strings.HasSuffix(exportData.DataFormat, "PascalCase") {
			keyStyle = "PascalCase"
		} else if strings.HasSuffix(exportData.DataFormat, "underscore_case") {
			keyStyle = "underscore_case"
		}
		exportedData, err := ExportJSONRecursively(schema, false, keyStyle, exportData.RemoveMetadata)
		if err != nil {
			return fmt.Errorf("ExportJSONRecursively: %w", err)
		}
		exportData.ExportedData = exportedData
	default:
		return errors.New("unsupported Data Format: " + exportData.DataFormat)
	}

	err = common.StructureIntoData("polyapp", "Export", "uGhKgwWImpQhdXIPssYLfuxaO", &exportData, data)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}

	// /blob/assets/downloads/ allows you to get a single blob file from the server via main.go's BlobDownloadHandler
	response.NewURL = "/blob/assets/" + data.FirestoreID + "/" + common.AddFieldPrefix("polyapp", "Export", "uGhKgwWImpQhdXIPssYLfuxaO", "Exported Data") + "?cacheBuster=" + common.GetRandString(15)

	return nil
}

// ExportJSONRecursively returns []byte JSON document containing not only every Data in this Schema,
// but also every Subtask associated with those Data documents.
//
// schema is the Schema from which you wish to export all Data.
//
// includePrefix if set to true sets field keys to include the prefix like "ind_dom_sch_key", false field keys are "key".
//
// keyStyle is one of: "", camelCase, PascalCase, underscore_case
func ExportJSONRecursively(schema common.Schema, includePrefix bool, keyStyle string, removeMetadata bool) ([]byte, error) {
	q := allDB.Query{}
	q.Init(schema.IndustryID, schema.DomainID, schema.SchemaID, common.CollectionData)
	q.AddEquals(common.PolyappSchemaID, schema.FirestoreID)
	iter, err := q.QueryRead()
	if err != nil {
		return nil, fmt.Errorf("q.QueryRead: %w", err)
	}
	mainKey := schema.FirestoreID
	if schema.Name != nil {
		mainKey = *schema.Name
	}
	fullMap := make(map[string][]interface{})
	for {
		d, err := iter.Next()
		if err != nil && err == common.IterDone {
			break
		}
		if err != nil {
			return nil, fmt.Errorf("iter.Next: %w", err)
		}
		dCast := d.(*common.Data)
		// Ignore all bytes
		dCast.Bytes = nil
		dCast.ABytes = nil

		m, err := dataToMapRecursive(dCast, includePrefix, keyStyle, removeMetadata)
		fullMap[mainKey] = append(fullMap[mainKey], m)
	}
	marshalled, err := json.Marshal(fullMap)
	if err != nil {
		return nil, fmt.Errorf("json.Marshal: %w", err)
	}
	return marshalled, nil
}

func dataToMapRecursive(d *common.Data, includePrefix bool, keyStyle string, removeMetadata bool) (map[string]interface{}, error) {
	m, err := d.Simplify()
	if err != nil {
		return nil, fmt.Errorf("dCast.Simplify %v: %w", d.FirestoreID, err)
	}
	if removeMetadata {
		for k := range m {
			switch k {
			case common.PolyappIndustryID, common.PolyappDomainID, common.PolyappSchemaID, common.PolyappDeprecated, common.PolyappFirestoreID:
				delete(m, k)
				continue
			}
			if strings.HasSuffix(k, "Done") || strings.HasSuffix(k, "Done Timestamps") || strings.HasSuffix(k, "Done User IDs") ||
				strings.HasSuffix(k, "Load Timestamps") || strings.HasSuffix(k, "Load User IDs") {
				delete(m, k)
				continue
			}
		}
	}
	mWithKeyStyle := make(map[string]interface{})
	for k := range m {
		if strings.HasPrefix(k, "_") {
			// Either Ref or ARef
			switch v := m[k].(type) {
			case *string:
				if v == nil || *v == "" {
					continue
				}
				getDataReq, err := common.ParseRef(*v)
				if err != nil {
					return nil, fmt.Errorf("common.ParseRef: %w", err)
				}
				subtaskData := common.Data{
					FirestoreID: getDataReq.DataID,
				}
				err = allDB.Read(&subtaskData)
				if err != nil {
					return nil, fmt.Errorf("allDB.ReadData: %w", err)
				}
				// Ref
				subtaskMap, err := dataToMapRecursive(&subtaskData, includePrefix, keyStyle, removeMetadata)
				if err != nil {
					return nil, fmt.Errorf("dataToMapRecursive: %w", err)
				}
				mWithKeyStyle[convertKeyToKeyStyle(k, includePrefix, keyStyle)] = subtaskMap
			case []string:
				mWithKeyStyle[convertKeyToKeyStyle(k, includePrefix, keyStyle)] = make([]map[string]interface{}, len(v))
				for i := range v {
					getDataReq, err := common.ParseRef(v[i])
					if err != nil {
						return nil, fmt.Errorf("common.ParseRef: %w", err)
					}
					subtaskData := common.Data{
						FirestoreID: getDataReq.DataID,
					}
					err = allDB.Read(&subtaskData)
					if err != nil {
						return nil, fmt.Errorf("allDB.ReadData: %w", err)
					}
					subtaskMap, err := dataToMapRecursive(&subtaskData, includePrefix, keyStyle, removeMetadata)
					if err != nil {
						return nil, fmt.Errorf("dataToMapRecursive: %w", err)
					}
					mWithKeyStyle[convertKeyToKeyStyle(k, includePrefix, keyStyle)].([]map[string]interface{})[i] = subtaskMap
				}
			}
		} else {
			mWithKeyStyle[convertKeyToKeyStyle(k, includePrefix, keyStyle)] = m[k]
		}
	}
	return mWithKeyStyle, nil
}

func convertKeyToKeyStyle(key string, includePrefix bool, keyStyle string) string {
	if !includePrefix {
		keySplit := strings.Split(key, "_")
		key = keySplit[len(keySplit)-1]
	}
	switch keyStyle {
	case "camelCase":
		almostCamelCase := strings.ReplaceAll(strings.Title(key), " ", "")
		for _, r := range almostCamelCase {
			// I'm not 100% sure this is correct for non-ASCII characters
			return common.RemoveNonAlphaNumeric(string(r) + almostCamelCase[1:])
		}
		return ""
	case "PascalCase":
		return common.RemoveNonAlphaNumeric(strings.ReplaceAll(strings.Title(key), " ", ""))
	case "underscore_case":
		return strings.ToLower(strings.ReplaceAll(common.RemoveNonAlphaNumeric(key), " ", "_"))
	default:
		return key
	}
}

// castValue from data at key into a string
func castValue(data *common.Data, key string) string {
	if data.S[key] != nil {
		return *data.S[key]
	} else if data.B[key] != nil {
		return strconv.FormatBool(*data.B[key])
	} else if data.F[key] != nil {
		return strconv.FormatFloat(*data.F[key], 'f', -1, 64)
	} else if data.I[key] != nil {
		return strconv.FormatInt(*data.I[key], 10)
	} else if data.Ref[key] != nil {
		return *data.Ref[key]
	}
	//} else if data.AS[key] != nil {
	//	var s strings.Builder
	//	s.WriteString("[")
	//	return "["
	//	for arrayI := range data.AS[key] {
	//		s.WriteString(data.AS[key][arrayI])
	//	}
	//	s.WriteString("]")
	//	return s.String()
	//} else if data.AB[key] != nil {
	//	var s strings.Builder
	//	s.WriteString("[")
	//	return "["
	//	for arrayI := range data.AB[key] {
	//		s.WriteString(strconv.FormatBool(data.AB[key][arrayI]))
	//	}
	//	s.WriteString("]")
	//	return s.String()
	//} else if data.ARef[key] != nil {
	//	var s strings.Builder
	//	s.WriteString("[")
	//	return "["
	//	for arrayI := range data.ARef[key] {
	//		s.WriteString(data.ARef[key][arrayI])
	//	}
	//	s.WriteString("]")
	//	return s.String()
	//} else if data.AF[key] != nil {
	//	var s strings.Builder
	//	s.WriteString("[")
	//	return "["
	//	for arrayI := range data.AF[key] {
	//		s.WriteString(strconv.FormatFloat(data.AF[key][arrayI], 'f', -1, 64))
	//	}
	//	s.WriteString("]")
	//	return s.String()
	//} else if data.AI[key] != nil {
	//	var s strings.Builder
	//	s.WriteString("[")
	//	return "["
	//	for arrayI := range data.AI[key] {
	//		s.WriteString(strconv.FormatInt(data.AI[key][arrayI], 10))
	//	}
	//	s.WriteString("]")
	//	return s.String()
	return ""
}

type BotScaffold struct {
	TaskID   string `suffix:"Task ID"`
	SchemaID string `suffix:"Schema ID"`
}

type genBotField struct {
	FieldName         string
	FieldNameNoSpaces string
	TypeString        string
}
type genBotScaffold struct {
	BotNameWithSpaces string
	BotName           string
	BotHelpText       string
	F                 []genBotField
	Industry          string
	Domain            string
	Schema            string
}

// GenerateBotScaffold is a Task which can help you get started creating new Bots in Polyapp.
// It will ask you for a Bot ID and a Schema ID you are operating on. By examining these two documents, it can
// generate code snippets which you can copy and paste into the code in the places indicated in the Help Text.
func GenerateBotScaffold(data *common.Data) error {
	var err error
	b := BotScaffold{}
	err = common.DataIntoStructure(data, &b)
	if err != nil {
		return fmt.Errorf("common.DataIntoStructure: %w", err)
	}

	g, err := createBotScaffold(b.TaskID, b.SchemaID)
	if err != nil {
		return fmt.Errorf("createBotScaffold: %w", err)
	}

	caseTemplate := `	case "{{.BotNameWithSpaces}}":
		err = actions.Action{{.BotName}}(currentData, request, response)
		if err != nil {
			return fmt.Errorf("actions.{{.BotName}}: %w", err)
		}
`
	var w bytes.Buffer
	t, err := template.New("t").Parse(caseTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(&w, g)
	if err != nil {
		return err
	}
	botgoFile, err := ioutil.ReadFile(common.GetPublicPath() + "/../handlers/bot.go")
	if err != nil {
		return fmt.Errorf("ioutil.ReadFile bot.go: %w", err)
	}
	splitBotGoFile := bytes.Split(botgoFile, []byte(`	default:
		return errors.New("unhandled action: " + action.Name)
	}`))
	var outBotGoFile bytes.Buffer
	outBotGoFile.Write(splitBotGoFile[0])
	outBotGoFile.Write(w.Bytes())
	outBotGoFile.Write([]byte(`	default:
		return errors.New("unhandled action: " + action.Name)
	}`))
	outBotGoFile.Write(splitBotGoFile[1])
	err = ioutil.WriteFile(common.GetPublicPath()+"/../handlers/bot.go", outBotGoFile.Bytes(), 0600)
	if err != nil {
		return fmt.Errorf("ioutil.WriteFile bot.go: %w", err)
	}
	actionFuncTemplate := `
// Action{{.BotName}} {{.BotHelpText}}
func Action{{.BotName}}(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	{{.BotName}} := {{.BotName}}{}
	err = common.DataIntoStructure(data, &{{.BotName}})
	if err != nil {
		return fmt.Errorf("common.DataIntoStructure: %w", err)
	}
	return nil
}
`
	w.Reset()
	err = createTypeFromBotScaffold(g, &w)
	if err != nil {
		return fmt.Errorf("createTypeFromBotScaffold: %w", err)
	}
	t, err = template.New("t").Parse(actionFuncTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(&w, g)
	if err != nil {
		return err
	}
	fActions, err := os.OpenFile(common.GetPublicPath()+"/../actions/actions.go", os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		return fmt.Errorf("os.OpenFile: %w", err)
	}
	defer fActions.Close()
	_, err = fActions.Write(w.Bytes())
	if err != nil {
		return fmt.Errorf("fActions.Write in bot.go: %w", err)
	}
	return nil
}

func createTypeFromBotScaffold(g genBotScaffold, w io.Writer) error {
	typeTemplate := `
type {{.BotName}} struct {
{{range .F}}	{{.FieldNameNoSpaces}} {{.TypeString}} ` + "`" + `suffix:"{{.FieldName}}"` + "`\n" + `{{end}}
}
`
	t, err := template.New("t").Parse(typeTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(w, g)
	if err != nil {
		return err
	}
	return nil
}

// createBotScaffold returns a genBotScaffold after looking up the necessary data from the database.
func createBotScaffold(TaskID string, SchemaID string) (genBotScaffold, error) {
	g := genBotScaffold{
		F: make([]genBotField, 0),
	}
	task, err := allDB.ReadTask(TaskID)
	if err != nil {
		return genBotScaffold{}, fmt.Errorf("allDB.ReadTask: %w", err)
	}
	g.BotNameWithSpaces = strings.TrimSpace(task.Name)
	g.BotName = strings.Title(strings.ReplaceAll(g.BotNameWithSpaces, " ", ""))
	g.BotHelpText = task.HelpText
	schema, err := allDB.ReadSchema(SchemaID)
	if err != nil {
		return genBotScaffold{}, fmt.Errorf("allDB.ReadSchema: %w", err)
	}
	for k, v := range schema.DataTypes {
		f := genBotField{
			FieldName:         common.RemoveFieldPrefix(k),
			FieldNameNoSpaces: strings.Title(strings.ReplaceAll(common.RemoveFieldPrefix(k), " ", "")),
		}
		switch v {
		case "S", "Ref":
			f.TypeString = "string"
		case "B":
			f.TypeString = "bool"
		case "F":
			f.TypeString = "float64"
		case "I":
			f.TypeString = "int64"
		case "Bytes":
			f.TypeString = "[]byte"
		case "AB":
			f.TypeString = "[]bool"
		case "AS", "ARef":
			f.TypeString = "[]string"
		case "AF":
			f.TypeString = "[]float64"
		case "AI":
			f.TypeString = "[]int64"
		case "ABytes":
			f.TypeString = "[][]byte"
		}
		g.F = append(g.F, f)
	}
	return g, nil
}

// ActionNewData opens a new Data in this Task. Usually used "At Done", this Bot creates a new Data and directs you to
// view it. This is extremely useful if the Task represents a contact like a phone call and when you press "Done" you
// need to open a new contact for the next phone call you receive. Keep in mind that using this Bot can mess with
// reporting. For example, if you were using the time the contact was opened as the "start time" for your call tracking
// and the "Last Done" for your "finish time", then when you install this Bot you will get "start time"s which start prior
// to a new call being received. This problem could be mitigated with some custom code or by integrating call logging
// into your Polyapp Task.
func ActionNewData(request *common.POSTRequest, response *common.POSTResponse) error {
	d := common.Data{}
	d.Init(nil)
	d.IndustryID = request.IndustryID
	d.DomainID = request.DomainID
	d.SchemaID = request.SchemaID
	d.FirestoreID = common.GetRandString(25)
	err := allDB.CreateData(&d)
	if err != nil {
		return fmt.Errorf("allDB.CreateData: %w", err)
	}

	response.NewURL = common.CreateURL(request.IndustryID, request.DomainID, request.TaskID, request.UXID, request.SchemaID, d.FirestoreID, request.UserID, request.RoleID, request.OverrideIndustryID, request.OverrideDomainID)
	return nil
}

// getSecret from secret manager. If the secret returns "PermissionDenied" that can indicate the secret does not
// exist. In that case the returned values are "", nil
func getSecret(ctx context.Context, client *secretmanager.Client, secretId string) (string, error) {
	req := &secretmanagerpb.AccessSecretVersionRequest{
		Name: "projects/" + common.GetGoogleProjectID() + "/secrets/" + secretId + "/versions/latest",
	}
	result, err := client.AccessSecretVersion(ctx, req)
	if err != nil && strings.Contains(err.Error(), "PermissionDenied") {
		// No access to this secret, or it does not exist
		return "", nil
	} else if err != nil && strings.Contains(err.Error(), "NotFound") {
		// access to this secret and it does not exist
		return "", nil
	} else if err != nil {
		return "", fmt.Errorf("client.AccessSecretVersion %v: %w", secretId, err)
	}
	return string(result.Payload.Data), nil
}

// addSecret to secret manager. Creates a new Secret if it does not exist.
// WARNING: Secrets cost $0.06 cents / version / month so limiting the number of secrets is important.
//
// secretID == the part of the URL after /secrets/
func addSecret(ctx context.Context, client *secretmanager.Client, secretID string, value string) error {
	secretValue, err := getSecret(ctx, client, secretID)
	if err != nil {
		return fmt.Errorf("getSecret: %w", err)
	}
	if secretValue == "" {
		// we need to create a new Secret since the existing one was not found
		createSecretRequest := &secretmanagerpb.CreateSecretRequest{
			Parent:   "projects/" + common.GetGoogleProjectID(),
			SecretId: secretID,
			Secret: &secretmanagerpb.Secret{
				Replication: &secretmanagerpb.Replication{
					Replication: &secretmanagerpb.Replication_Automatic_{
						Automatic: &secretmanagerpb.Replication_Automatic{},
					},
				},
			},
		}
		_, err = client.CreateSecret(ctx, createSecretRequest)
		if err != nil && !strings.Contains(err.Error(), "already exists") {
			return fmt.Errorf("client.CreateSecret: %w", err)
		}
	} else {
		// we ought to delete the current secret version (if there IS a current secret version) so we are not paying for secrets we are not using
		//DestroySecretVersionRequest := secretmanagerpb.DestroySecretVersionRequest{
		//	Name: "projects/" + common.GetGoogleProjectID() + "/secrets/" + secretID + "/versions/###need to get from listing current secret versions###",
		//}
		//_, err = client.DestroySecretVersion(client, DestroySecretVersionRequest)
		//if err != nil {
		//	return fmt.Errorf("client.DestroySecretVersion: %w", err)
		//}
	}
	addSecretRequest := &secretmanagerpb.AddSecretVersionRequest{
		Parent: "projects/" + common.GetGoogleProjectID() + "/secrets/" + secretID,
		Payload: &secretmanagerpb.SecretPayload{
			Data: []byte(value),
		},
	}
	_, err = client.AddSecretVersion(ctx, addSecretRequest)
	if err != nil {
		return fmt.Errorf("client.AddSecret: %w", err)
	}
	return nil
}

func deleteSecret(ctx context.Context, client *secretmanager.Client, secretID string) error {
	var err error
	req := &secretmanagerpb.DeleteSecretRequest{
		Name: "projects/" + common.GetGoogleProjectID() + "/secrets/" + secretID,
	}
	err = client.DeleteSecret(ctx, req)
	if err != nil {
		return fmt.Errorf("client.DeleteSecret: %w", err)
	}
	return nil
}

// ActionLoadTracking is triggered when a Task loads.
func ActionLoadTracking(data *common.Data, request *common.POSTRequest) error {
	var err error
	if request.SchemaCache == nil {
		request.SchemaCache = &data.SchemaCache
	}
	if request.SchemaCache.FirestoreID == "" {
		s, err := allDB.ReadSchema(request.SchemaID)
		if err != nil {
			return fmt.Errorf("allDB.ReadSchema(%v): %w", request.SchemaID, err)
		}
		request.SchemaCache = &s
	}
	loadTimestampKey := common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, "Load Timestamps")
	loadTimestampUserKey := common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, "Load User IDs")
	if request.SchemaCache.DataTypes[loadTimestampKey] == "AF" && request.SchemaCache.DataTypes[loadTimestampUserKey] == "AS" {
		data.AF[loadTimestampKey] = append(data.AF[loadTimestampKey], float64(time.Now().UTC().Unix()))
		data.AS[loadTimestampUserKey] = append(data.AS[loadTimestampUserKey], request.UserID)
		err = allDB.UpdateData(data)
		if err != nil {
			return fmt.Errorf("allDB.UpdateData %v: %w", data.FirestoreID, err)
		}
	}
	return nil
}

// ActionDoneTracking is triggered when a Task is Done.
func ActionDoneTracking(data *common.Data, request *common.POSTRequest) error {
	var err error
	if request.SchemaCache == nil {
		request.SchemaCache = &data.SchemaCache
	}
	if request.SchemaCache.FirestoreID == "" {
		s, err := allDB.ReadSchema(request.SchemaID)
		if err != nil {
			return fmt.Errorf("allDB.ReadSchema(%v): %w", request.SchemaID, err)
		}
		request.SchemaCache = &s
	}
	doneTimestampsField := common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, "Done Timestamps")
	doneTimestampUserKey := common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, "Done User IDs")
	if request.SchemaCache.DataTypes[doneTimestampsField] == "AF" && request.SchemaCache.DataTypes[doneTimestampUserKey] == "AS" {
		data.AF[doneTimestampsField] = append(data.AF[doneTimestampsField], float64(time.Now().UTC().Unix()))
		data.AS[doneTimestampUserKey] = append(data.AS[doneTimestampUserKey], request.UserID)
		err = allDB.UpdateData(data)
		if err != nil {
			return fmt.Errorf("allDB.UpdateData: %w", err)
		}
	}
	return nil
}

type exportApp struct {
	TaskIDs      []string `suffix:"Task IDs"`
	DataIDs      []string `suffix:"Data IDs"`
	ExportedData []byte   `suffix:"Exported Data"`
}

// ExportApp exports Data used to create Tasks, Schemas, UXs, Bots, and Actions. This Task does NOT export any Data.
// This Task does NOT export the underlying Task, Bot, etc. documents. After export, the "App" can be imported into
// another instance of Polyapp using Import App.
func ActionExportApp(data *common.Data, response *common.POSTResponse) error {
	var err error
	e := exportApp{}
	err = common.DataIntoStructure(data, &e)
	if err != nil {
		return fmt.Errorf("common.DataIntoStructure: %w", err)
	}
	allData := make(map[string]map[string]interface{})
	botIDs := make(map[string]bool)
	for _, taskID := range e.TaskIDs {
		q := allDB.Query{}
		q.Init("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", common.CollectionData)
		q.AddEquals(common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Task ID"), taskID)
		q.SetLength(1)
		iter, err := q.QueryRead()
		if err != nil {
			return fmt.Errorf("q.QueryRead: %w", err)
		}
		for {
			o, err := iter.Next()
			if err != nil && err == common.IterDone {
				break
			}
			if err != nil {
				return fmt.Errorf("iter.Next: %w", err)
			}
			d := o.(*common.Data)
			allData[d.FirestoreID], err = d.Simplify()
			if err != nil {
				return fmt.Errorf("d.Simplify: %w", err)
			}
			editTask := EditTask{}
			err = common.DataIntoStructure(d, &editTask)
			if err != nil {
				return fmt.Errorf("common.DataIntoStructure: %w", err)
			}
			for _, ref := range editTask.Subtasks {
				parsedRef, err := common.ParseRef(ref)
				if err != nil {
					return fmt.Errorf("common.ParseRef: %w", err)
				}
				additionalData := common.Data{
					FirestoreID: parsedRef.DataID,
				}
				err = allDB.Read(&additionalData)
				if err != nil {
					return fmt.Errorf("allDB.ReadData: %w", err)
				}
				allData[additionalData.FirestoreID], err = additionalData.Simplify()
				if err != nil {
					return fmt.Errorf("d.Simplify: %w", err)
				}
			}
			for _, ref := range editTask.Fields {
				parsedRef, err := common.ParseRef(ref)
				if err != nil {
					return fmt.Errorf("common.ParseRef: %w", err)
				}
				additionalData := common.Data{
					FirestoreID: parsedRef.DataID,
				}
				err = allDB.Read(&additionalData)
				if err != nil {
					return fmt.Errorf("allDB.ReadData: %w", err)
				}
				allData[additionalData.FirestoreID], err = additionalData.Simplify()
				if err != nil {
					return fmt.Errorf("d.Simplify: %w", err)
				}
			}
			for _, id := range editTask.BotsTriggeredAtLoad {
				botIDs[id] = true
			}
			for _, id := range editTask.BotsTriggeredContinuously {
				botIDs[id] = true
			}
			for _, id := range editTask.BotsTriggeredAtDone {
				botIDs[id] = true
			}
		}
	}
	for botID := range botIDs {
		q := allDB.Query{}
		q.Init("polyapp", "Bot", "veKWxEWtCEnCDLknytuUPzMHY", common.CollectionData)
		q.AddEquals("polyapp_Bot_veKWxEWtCEnCDLknytuUPzMHY_Bot ID", botID)
		q.SetLength(1)
		it, err := q.QueryRead()
		if err != nil {
			return fmt.Errorf("q.QueryRead: %w", err)
		}
		for {
			doc, err := it.Next()
			if err != nil && err == common.IterDone {
				break
			} else if err != nil {
				return fmt.Errorf("it.Next: %w", err)
			}
			d := doc.(*common.Data)
			allData[d.FirestoreID], err = d.Simplify()
			if err != nil {
				return fmt.Errorf("d.Simplify: %w", err)
			}
			for _, v := range d.ARef {
				for _, ref := range v {
					parsedRef, err := common.ParseRef(ref)
					if err != nil {
						return fmt.Errorf("common.ParseRef: %w", err)
					}
					additionalData := common.Data{
						FirestoreID: parsedRef.DataID,
					}
					err = allDB.Read(&additionalData)
					if err != nil {
						return fmt.Errorf("allDB.ReadData: %w", err)
					}
					allData[additionalData.FirestoreID], err = additionalData.Simplify()
					if err != nil {
						return fmt.Errorf("additionalData.Simplify: %w", err)
					}
				}
			}
		}
	}
	for i := range e.DataIDs {
		additions, err := allDB.ReadDataAndChildren(e.DataIDs[i])
		if err != nil {
			return fmt.Errorf("allDB.ReadDataAndChildren: %w", err)
		}
		for dataI := range additions {
			allData[additions[dataI].FirestoreID], err = additions[dataI].Simplify()
			if err != nil {
				return fmt.Errorf("additions[%v].Simplify: %w", dataI, err)
			}
		}
	}
	marshalled, err := json.Marshal(allData)
	if err != nil {
		return fmt.Errorf("json.Marshal: %w", err)
	}
	e.ExportedData = marshalled
	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &e, data)
	if err != nil {
		return fmt.Errorf("common.StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}

	// /blob/assets/downloads/ allows you to get a single blob file from the server via main.go's BlobDownloadHandler
	response.NewURL = "/blob/assets/" + data.FirestoreID + "/" + common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, "Exported Data") +
		"?cacheBuster=" + common.GetRandString(10)

	return nil
}

type importApp struct {
	FileToImport          []byte   `suffix:"File to Import"`
	OverwriteExistingData bool     `suffix:"Overwrite Existing Data"`
	ImportedDataIDs       []string `suffix:"Imported Data IDs"`
}

// ImportApp import Data used to create Tasks, Schemas, UXs, Bots, and Actions then run EditTask's Action and EditBot's
// Action to create the Task, Bot, etc. documents. It is assumed that the documents were exported using "Export App".
func ActionImportApp(data *common.Data, request *common.POSTRequest) error {
	var err error
	im := importApp{}
	err = common.DataIntoStructure(data, &im)
	if err != nil {
		return fmt.Errorf("common.DataIntoStructure: %w", err)
	}
	im.FileToImport, err = allDB.ReadDataBytes(data.FirestoreID, common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, "File to Import"))
	if err != nil {
		return fmt.Errorf("allDB.ReadDataBytes: %w", err)
	}
	allData := make([]*common.Data, 0)
	fullMap := make(map[string]map[string]interface{})
	err = json.Unmarshal(im.FileToImport, &fullMap)
	if err != nil {
		return fmt.Errorf("json.Unmarshal: %w", err)
	}
	for k := range fullMap {
		newData := common.Data{}
		err = newData.Init(fullMap[k])
		if err != nil {
			return fmt.Errorf("Init %v: %w", k, err)
		}
		newData.FirestoreID = k
		if !im.OverwriteExistingData || newData.FirestoreID == "" {
			newData.FirestoreID = common.GetRandString(25)
		}
		allData = append(allData, &newData)
	}
	editTaskDatas := make([]*common.Data, 0)
	botDatas := make([]*common.Data, 0)
	im.ImportedDataIDs = make([]string, 0)
	for i := range allData {
		if im.OverwriteExistingData {
			err := allDB.DeleteData(allData[i].FirestoreID)
			if err != nil && strings.Contains(err.Error(), "code = NotFound") {
				// OK
			} else if err != nil {
				return fmt.Errorf("allDB.DeleteData: %w", err)
			}
		}
		err = allDB.CreateData(allData[i])
		if err != nil {
			return fmt.Errorf("allDB.CreateData: %w", err)
		}
		im.ImportedDataIDs = append(im.ImportedDataIDs, allData[i].FirestoreID)
		if allData[i].SchemaID == "gjGLMlcNApJyvRjmgQbopsXPI" {
			editTaskDatas = append(editTaskDatas, allData[i])
		}
		if allData[i].SchemaID == "veKWxEWtCEnCDLknytuUPzMHY" {
			botDatas = append(botDatas, allData[i])
		}
	}
	for _, botData := range botDatas {
		err = EditBot(botData)
		if err != nil {
			return fmt.Errorf("EditBot for Data ID %v: %w", botData.FirestoreID, err)
		}
	}
	for _, editTaskData := range editTaskDatas {
		err = ActionEditTask(editTaskData, &common.POSTRequest{
			IndustryID: "polyapp",
			DomainID:   "TaskSchemaUX",
			UserID:     request.UserID,
			RoleID:     request.RoleID,
		}, nil)
		if err != nil {
			return fmt.Errorf("ActionEditTask for Data ID %v: %w", editTaskData.FirestoreID, err)
		}
	}
	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &im, data)
	if err != nil {
		return fmt.Errorf("common.StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}
	return nil
}

// ImportArbitraryJSONRecursively takes a map which was unmarshalled from JSON, converts each object in the JSON into
// a common.Data, and returns them. If an object has a child object then a reference is added from the parent common.Data
// to the child common.Data.
//
// Key conversions: This function assumes keys are either all prefixed or none of them are prefixed. A poll is taken of
// fullMap's top level keys and majority wins. A key is thought to be prefixed if it follows the format: _i_d_s_k or i_d_s_k
// If it is a tie then keys are assumed to not be prefixed.
//
// Schema determination: The top-level map much follow the schema of the schema parameter. Child objects in the JSON
// must be from one of the parent object's Schemas. If an object's Schema is not detected an error is returned.
// This prevents partial imports and partial import rollbacks.
func ImportArbitraryJSONRecursively(fullMap map[string]interface{}, schema common.Schema) ([]*common.Data, error) {
	keysArePrefixed := areKeysPrefixed(fullMap)
	return importArbitraryJSONRecursively(fullMap, schema, keysArePrefixed)
}

func areKeysPrefixed(fullMap map[string]interface{}) bool {
	numPrefixed := 0
	numNotPrefixed := 0
	for k := range fullMap {
		if len(k) > 0 && k[0] == '_' {
			if strings.Count(k, "_") == 4 {
				numPrefixed++
			} else {
				numNotPrefixed++
			}
		} else if strings.Count(k, "_") == 3 {
			numPrefixed++
		} else {
			numNotPrefixed++
		}
	}
	if numPrefixed > numNotPrefixed {
		return true
	}
	return false
}

// importArbitraryJSONRecursively requires keysArePrefixed. It calls itself recursively.
func importArbitraryJSONRecursively(fullMap map[string]interface{}, schema common.Schema, keysArePrefixed bool) ([]*common.Data, error) {
	allDatas := make([]*common.Data, 1)
	d := common.Data{}
	// adding keys to a map while iterating over it doesn't seem to be supported in Go.
	toAddMap := make(map[string]interface{})
	for k := range fullMap {
		safeKey := strings.ReplaceAll(k, "_", " ")
		if safeKey != k {
			toAddMap[safeKey] = fullMap[k]
			delete(fullMap, k)
		}
	}
	for k := range toAddMap {
		fullMap[k] = toAddMap[k]
	}
	prefixedKeyMap := fullMap
	if !keysArePrefixed {
		prefixedKeyMap = make(map[string]interface{})
		for k := range fullMap {
			switch fullMap[k].(type) {
			case map[string]interface{}:
				// subtask
				m, ok := fullMap[k].(map[string]interface{})
				if !ok {
					prefixedKeyMap[common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, k)] = fullMap[k]
					continue
				}
				subtaskSchema, err := getSubtaskSchemaFromMap(m, schema, false, k)
				if err != nil && strings.Contains(err.Error(), "no matching Schema found") {
					continue
				}
				if err != nil {
					return nil, fmt.Errorf("getSubtaskSchemaFromMap: %w", err)
				}
				queryable, err := allDB.QueryEquals(common.CollectionData,
					common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Schema ID"), subtaskSchema.FirestoreID)
				if err != nil {
					return nil, fmt.Errorf("allDB.QueryEquals: %w", err)
				}
				editTaskData := queryable.(*common.Data)
				subtaskTaskID := editTaskData.S[common.AddFieldPrefix(editTaskData.IndustryID, editTaskData.DomainID, editTaskData.SchemaID, "Task ID")]
				subtaskUXID := editTaskData.S[common.AddFieldPrefix(editTaskData.IndustryID, editTaskData.DomainID, editTaskData.SchemaID, "UX ID")]
				subtaskDatas, err := importArbitraryJSONRecursively(m, subtaskSchema, keysArePrefixed)
				if err != nil {
					return nil, fmt.Errorf("importArbitraryJSONRecursively for key %v: %w", k, err)
				}
				allDatas = append(allDatas, subtaskDatas...)
				prefixedKeyMap["_"+common.AddFieldPrefix(subtaskSchema.IndustryID, subtaskSchema.DomainID, subtaskSchema.FirestoreID, k)] = common.CreateRef(
					subtaskSchema.IndustryID, subtaskSchema.DomainID, *subtaskTaskID, *subtaskUXID, subtaskSchema.FirestoreID, subtaskDatas[0].FirestoreID)
				continue
			case []interface{}:
				if len(fullMap[k].([]interface{})) > 0 {
					v := fullMap[k].([]interface{})
					switch arrayValue := v[0].(type) {
					case map[string]interface{}:
						// array of subtasks
						subtaskSchema, err := getSubtaskSchemaFromMap(arrayValue, schema, true, k)
						if err != nil && strings.Contains(err.Error(), "no matching Schema found") {
							continue
						}
						if err != nil {
							return nil, fmt.Errorf("getSubtaskSchemaFromMap: %w", err)
						}
						queryable, err := allDB.QueryEquals(common.CollectionData,
							common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Schema ID"), subtaskSchema.FirestoreID)
						if err != nil {
							return nil, fmt.Errorf("allDB.QueryEquals: %w", err)
						}
						editTaskData := queryable.(*common.Data)
						subtaskTaskID := editTaskData.S[common.AddFieldPrefix(editTaskData.IndustryID, editTaskData.DomainID, editTaskData.SchemaID, "Task ID")]
						subtaskUXID := editTaskData.S[common.AddFieldPrefix(editTaskData.IndustryID, editTaskData.DomainID, editTaskData.SchemaID, "UX ID")]
						prefixedKeyMap["_"+common.AddFieldPrefix(subtaskSchema.IndustryID, subtaskSchema.DomainID, subtaskSchema.FirestoreID, k)] = make([]string, len(v))
						for mapIndex := range v {
							subtaskDatas, err := importArbitraryJSONRecursively(v[mapIndex].(map[string]interface{}), subtaskSchema, keysArePrefixed)
							if err != nil {
								return nil, fmt.Errorf("importArbitraryJSONRecursively for key %v in []: %w", k, err)
							}
							allDatas = append(allDatas, subtaskDatas...)
							prefixedKeyMap["_"+common.AddFieldPrefix(subtaskSchema.IndustryID, subtaskSchema.DomainID, subtaskSchema.FirestoreID, k)].([]string)[mapIndex] = common.CreateRef(
								subtaskSchema.IndustryID, subtaskSchema.DomainID, *subtaskTaskID, *subtaskUXID, subtaskSchema.FirestoreID, subtaskDatas[0].FirestoreID)
						}
						continue
					default:
						// ignore - will be handled by d.Init
					}
				} // else ignore - will be handled by d.Init
			default:
				// ignore - will be handled by d.Init
			}
			prefixedKeyMap[common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, k)] = fullMap[k]
		}
	}
	d.Init(prefixedKeyMap)
	d.FirestoreID = common.GetRandString(25)
	d.IndustryID = schema.IndustryID
	d.DomainID = schema.DomainID
	d.SchemaID = schema.FirestoreID
	allDatas[0] = &d
	return allDatas, nil
}

// getSubtaskSchemaFromMap finds a matching Schema from among a common.Schema's subtask Schemas.
//
// If more than one schema matches it returns the one which matches favorSchemaNamed. If more than one matching Schema
// has the same Name, then it returns the Schema which sorts first by Schema ID. If favorSchemaNamed does not match
// any of the names of the retrieved Schemas, it matches the one which sorts first by Schema ID. If none of the
// Schemas is compatible with the Data, or if there were no Refs or ARefs under parentSchema, it returns an error.
func getSubtaskSchemaFromMap(m map[string]interface{}, parentSchema common.Schema, isArray bool, favorSchemaNamed string) (common.Schema, error) {
	var s common.Schema
	var err error
	if m[common.PolyappSchemaID] != nil {
		schemaID, ok := m[common.PolyappSchemaID].(string)
		if !ok {
			return s, errors.New("contained key polyappSchemaID but key was not a string")
		}
		s, err = allDB.ReadSchema(schemaID)
		if err != nil {
			return s, fmt.Errorf("allDB.ReadSchema: %w", err)
		}
		return s, nil
	}

	keyOptions := make([]string, 0)
	if isArray {
		for k, v := range parentSchema.DataTypes {
			if v == "ARef" {
				keyOptions = append(keyOptions, k)
			}
		}
	} else {
		for k, v := range parentSchema.DataTypes {
			if v == "Ref" {
				keyOptions = append(keyOptions, k)
			}
		}
	}
	if len(keyOptions) == 0 {
		return common.Schema{}, errors.New("len(keyOptions) == 0 so there are no known subtask Schemas which could match this map value")
	}
	sort.Strings(keyOptions)

	var firstMatchingSchema common.Schema
	for _, key := range keyOptions {
		_, _, schemaID, _ := common.SplitField(key)
		s, err = allDB.ReadSchema(schemaID)
		if err != nil {
			return s, fmt.Errorf("allDB.ReadSchema: %w", err)
		}

		prefixedKeyMap := make(map[string]interface{})
		for k := range m {
			prefixedKeyMap[common.AddFieldPrefix(s.IndustryID, s.DomainID, s.FirestoreID, k)] = m[k]
		}
		d := common.Data{}
		d.Init(prefixedKeyMap)
		d.FirestoreID = "irrelevant"
		d.IndustryID = s.IndustryID
		d.DomainID = s.DomainID
		d.SchemaID = s.SchemaID

		err = common.DataSatisfiesSchema(&d, &s)
		if err != nil {
			continue
		}
		if s.Name != nil && *s.Name == favorSchemaNamed {
			return s, nil
		}
	}
	if firstMatchingSchema.FirestoreID != "" {
		// return the first matching Schema. If more than one matches that's tough luck.
		return firstMatchingSchema, nil
	}
	k1 := ""
	k2 := ""
	for k := range m {
		if k1 == "" {
			k1 = k
		} else if k2 == "" {
			k2 = k
		} else {
			break
		}
	}
	return common.Schema{}, fmt.Errorf("no matching Schema found for this map %v sample key 1: (%v) value: (%v) sample key 2: (%v) value: (%v)", m, k1, m[k1], k2, m[k2])
}

type ConfigureAPICall struct {
	FirestoreID         string
	AdditionalHeaders   []string `suffix:"Additional Headers"`
	AcceptEncoding      string   `suffix:"Accept-Encoding"`
	UserAgentHeader     string   `suffix:"User-Agent Header"`
	MaxRetries          float64  `suffix:"Max Retries"`
	Body                string   `suffix:"Body"`
	ContentLengthHeader float64  `suffix:"Content-Length Header"`
	Cookies             []string `suffix:"Cookies"`
	AcceptHeader        string   `suffix:"Accept Header"`
	HTTPVerb            string   `suffix:"HTTP Verb"`
	RequestURL          string   `suffix:"Request URL"`
	ConnectionHeader    string   `suffix:"Connection Header"`
	HostHeader          string   `suffix:"Host Header"`
	ContentTypeHeader   string   `suffix:"Content-Type Header"`
	Test                bool
	APICallResponse     string `suffix:"Test Response"`
	APICallStatusCode   string `suffix:"Test Status Code"`
}

// ActionConfigureAPICall. Integrating with an external API requires a few steps: (1) Call the API,
// perhaps with specific headers or authentication information; (2) Store the API response; (3) Do something with
// the API response. This Task allows you to configure an API call.
//
// This func may verify the data and can return a validation error. TODO do this.
//
// If "Test" (bool) is set to true, then set it to false and make an API request based on the configuration provided. Refresh the page when done.
func ActionConfigureAPICall(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	ConfigureAPICall := ConfigureAPICall{}
	err = common.DataIntoStructure(data, &ConfigureAPICall)
	if err != nil {
		return fmt.Errorf("common.DataIntoStructure: %w", err)
	}

	if !ConfigureAPICall.Test {
		return nil
	}
	ConfigureAPICall.Test = false

	err = DoConfigureAPICallRequest(&ConfigureAPICall, map[string]string{}, map[string]string{})
	if err != nil {
		return fmt.Errorf("doConfigureAPICallRequest: %w", err)
	}

	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &ConfigureAPICall, data)
	if err != nil {
		return fmt.Errorf("common.StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}

	response.NewURL = common.CreateURL(request.IndustryID, request.DomainID, request.TaskID, request.UXID, request.SchemaID,
		request.DataID, request.UserID, request.RoleID, request.OverrideIndustryID, request.OverrideDomainID)

	return nil
}

// DoConfigureAPICallRequest performs the work of making a request based on the ConfigureAPICall structure.
// It stores the response in the ConfigureAPICall structure which was passed in.
//
// This function exists to make sure when we run "Test" in the "Configure API Call" Task the same code runs as when we
// make the request in reality.
func DoConfigureAPICallRequest(ConfigureAPICall *ConfigureAPICall, bodyMappingMap map[string]string, queryMappingMap map[string]string) error {
	bodyTemplate, err := textTemplate.New("").Parse(ConfigureAPICall.Body)
	if err != nil {
		return fmt.Errorf("textTemplate.New.Parse (%v): %w", ConfigureAPICall.Body, err)
	}
	var requestBody bytes.Buffer
	err = bodyTemplate.Execute(&requestBody, bodyMappingMap)
	if err != nil {
		return fmt.Errorf("bodyTemplate.Execute for Body: %w", err)
	}
	if strings.Contains(requestBody.String(), "<no value>") {
		return fmt.Errorf("requestURL template did not have all inputs filled by Data. Result of running the template: %v", requestBody.String())
	}
	urlTemplate, err := textTemplate.New("").Parse(ConfigureAPICall.RequestURL)
	if err != nil {
		return fmt.Errorf("textTemplate.New.Parse (%v): %w", ConfigureAPICall.RequestURL, err)
	}
	var requestURL bytes.Buffer
	err = urlTemplate.Execute(&requestURL, queryMappingMap)
	if err != nil {
		return fmt.Errorf("bodyTemplate.Execute for URL: %w", err)
	}
	if strings.Contains(requestURL.String(), "<no value>") {
		return fmt.Errorf("requestURL template did not have all inputs filled by Data. Result of running the template: %v", requestURL.String())
	}
	req, err := http.NewRequest(ConfigureAPICall.HTTPVerb, requestURL.String(), &requestBody)
	if err != nil {
		return fmt.Errorf("http.NewRequest: %w", err)
	}
	// Ignore Max Retries
	if ConfigureAPICall.ContentTypeHeader != "" {
		req.Header.Set("Content-Type", ConfigureAPICall.ContentTypeHeader)
	}
	if ConfigureAPICall.ContentLengthHeader != 0 {
		req.Header.Set("Content-Length", strconv.Itoa(int(ConfigureAPICall.ContentLengthHeader)))
	}
	if ConfigureAPICall.HostHeader != "" {
		req.Header.Set("Host", ConfigureAPICall.HostHeader)
	}
	if ConfigureAPICall.UserAgentHeader != "" {
		req.Header.Set("User-Agent", ConfigureAPICall.UserAgentHeader)
	}
	if ConfigureAPICall.AcceptHeader != "" {
		req.Header.Set("Accept", ConfigureAPICall.AcceptHeader)
	}
	if ConfigureAPICall.AcceptEncoding != "" {
		req.Header.Set("Accept-Encoding", ConfigureAPICall.AcceptEncoding)
	}
	if ConfigureAPICall.ConnectionHeader != "" {
		req.Header.Set("Connection", ConfigureAPICall.ConnectionHeader)
	}
	for _, headerLine := range ConfigureAPICall.AdditionalHeaders {
		headerLineSplit := strings.Split(headerLine, ": ")
		if len(headerLineSplit) < 2 {
			continue
		}
		headerName := headerLineSplit[0]
		headerValue := strings.Join(headerLineSplit[1:], ": ")
		req.Header.Set(headerName, headerValue)
	}
	for _, cookieLine := range ConfigureAPICall.Cookies {
		cookieLineSplit := strings.Split(cookieLine, ": ")
		if len(cookieLineSplit) < 2 {
			continue
		}
		cookieDomain := cookieLineSplit[0]
		cookieValue := strings.Join(cookieLineSplit[1:], ": ")
		// https://stackoverflow.com/a/33926065/12713117
		cookieHeader := http.Header{}
		cookieHeader.Add("Cookie", cookieValue)
		cookieRequest := http.Request{Header: cookieHeader}
		cookies := cookieRequest.Cookies()
		for i := range cookies {
			cookies[i].Domain = cookieDomain
			req.AddCookie(cookies[i])
		}
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("http.DefaultClient.Do: %w", err)
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("ioutil.ReadAll: %w", err)
	}
	ConfigureAPICall.APICallStatusCode = resp.Status
	ConfigureAPICall.APICallResponse = string(respBody)

	return nil
}

// StoreAPIResponse can parse a ConfigureAPICall's Body field into a common.Data by using a recursive data import.
//
// The API Response is assumed to be JSON.
func StoreAPIResponse(ConfigureAPICall ConfigureAPICall, s common.Schema) error {
	var err error
	if len(ConfigureAPICall.Body) < 1 {
		return nil
	}
	fullMap := make(map[string]interface{})
	err = json.Unmarshal([]byte(ConfigureAPICall.Body), &fullMap)
	if err != nil && strings.Contains(err.Error(), "cannot unmarshal array into Go value") {
		// Some APIs seem to think it is OK to return arrays of objects which aren't wrapped in a containing object.
		// Can you imagine? Their creator's audacity knows no bounds.
		err = json.Unmarshal(append(append([]byte(`{"r":`), []byte(ConfigureAPICall.Body)...), []byte(`}`)...), &fullMap)
	}
	if err != nil {
		return fmt.Errorf("could not parse response into JSON: %w", err)
	}
	allData, err := ImportArbitraryJSONRecursively(fullMap, s)
	if err != nil {
		return fmt.Errorf("ImportArbitraryJSONRecursively: %w", err)
	}
	errChan := make(chan error)
	for i := range allData {
		go func(d *common.Data, errChan chan error) {
			err := allDB.CreateData(d)
			if err != nil {
				errChan <- fmt.Errorf("allDB.CreateData %v: %w", d.FirestoreID, err)
				return
			}
			errChan <- nil
		}(allData[i], errChan)
	}
	combinedErr := ""
	for range allData {
		err := <-errChan
		if err != nil {
			combinedErr += err.Error() + "; "
		}
	}
	if len(combinedErr) > 0 {
		return fmt.Errorf("errors creating data: %v", combinedErr)
	}
	return nil
}

type APICall struct {
	APIResponseSchemaID       string   `suffix:"API Response Schema ID"`
	ConfigureAPICall          string   `suffix:"Configure API Call"`
	GenerateAPIResponseSchema bool     `suffix:"Generate API Response Schema"`
	IngressBotID              string   `suffix:"Ingress Bot ID"`
	EgressBotID               string   `suffix:"Egress Bot ID"`
	Name                      string   `suffix:"Name"`
	BotStaticDataToUse        []string `suffix:"Bot Static Data To Use"`
}

// ActionAPICall This Task helps you integrate a Task with an external API. To use it, first use this Task's "Configure API Call" Subtask to set up your call to the external API and verify that you can call it successfully.
//
// Then use the listed "Bot ID To Use" and "Bot Static Data To Use" in the Task with which you wish to integrate.
func ActionAPICall(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	APICall, ConfigureAPICall, _, bodyMapping, queryMapping, err := getAPICallInfo(data.FirestoreID)
	if err != nil {
		return fmt.Errorf("getAPICallInfo: %w", err)
	}
	bodyMappingMap := make(map[string]string)
	for i := range bodyMapping {
		bodyMappingMap[bodyMapping[i].ExternalAPIFieldName] = "TestBodyValue"
	}
	queryMappingMap := make(map[string]string)
	for i := range queryMapping {
		queryMappingMap[queryMapping[i].ExternalAPIFieldName] = "TestQueryValue"
	}

	generateAPIResponse := APICall.GenerateAPIResponseSchema
	APICall.GenerateAPIResponseSchema = false

	doReload := false

	if ConfigureAPICall.Test || (generateAPIResponse && ConfigureAPICall.APICallResponse == "") {
		ConfigureAPICall.Test = false
		err = DoConfigureAPICallRequest(&ConfigureAPICall, bodyMappingMap, queryMappingMap)
		if err != nil {
			return fmt.Errorf("doConfigureAPICallRequest: %w", err)
		}

		ConfigureAPICallData := common.Data{}
		ConfigureAPICallData.Init(nil)
		ConfigureAPICallData.IndustryID = data.IndustryID
		ConfigureAPICallData.DomainID = data.DomainID
		ConfigureAPICallData.SchemaID = "LpMdSjNKmHKKeSpnYYijzxJFW"
		ConfigureAPICallData.FirestoreID = ConfigureAPICall.FirestoreID
		err = common.StructureIntoData(ConfigureAPICallData.IndustryID, ConfigureAPICallData.DomainID, "LpMdSjNKmHKKeSpnYYijzxJFW", &ConfigureAPICall, &ConfigureAPICallData)
		if err != nil {
			return fmt.Errorf("common.StructureIntoData: %w", err)
		}
		err = allDB.UpdateData(&ConfigureAPICallData)
		if err != nil {
			return fmt.Errorf("allDB.UpdateData for ConfigureAPICallData: %w", err)
		}
		doReload = true
	}

	if generateAPIResponse && ConfigureAPICall.APICallResponse == "" {
		return errors.New("Generate API Response Schema was clicked, but \"Test Response\" was empty. A response is needed to generate a Schema from the response.")
	}
	if generateAPIResponse {
		mapIn := make(map[string]interface{})
		err := json.Unmarshal([]byte(ConfigureAPICall.APICallResponse), &mapIn)
		if err != nil {
			return fmt.Errorf("json.Unmarshal failed indicating the API Response was not JSON but we can only handle JSON responses in ActionAPICall: %w", err)
		}
		datas, dependencyTree, err := CreateEditTaskFromJSON("polyapp", "External API Responses", APICall.Name, fixInvalidKeys(mapIn))
		if err != nil {
			return fmt.Errorf("actions.CreateEditTaskFromJSON: %w", err)
		}
		errChan := make(chan error)
		for i := range datas {
			go func(d common.Data, errChan chan error) {
				err = allDB.CreateData(&d)
				if err != nil {
					errChan <- fmt.Errorf("allDB.CreateData: %w", err)
					return
				}
				errChan <- nil
			}(datas[i], errChan)
		}
		combinedErr := ""
		for range datas {
			err := <-errChan
			if err != nil {
				combinedErr += err.Error() + "; "
			}
		}
		if len(combinedErr) > 0 {
			return errors.New("combinedErr from storing Datas: " + combinedErr)
		}
		err = createTaskRecursively(dependencyTree, datas, request.UserID, request.RoleID)
		if err != nil {
			return fmt.Errorf("createTaskRecursively: %w", err)
		}
		schemaID := datas[0].S[common.AddFieldPrefix(datas[0].IndustryID, datas[0].DomainID, datas[0].SchemaID, "Schema ID")]
		if schemaID == nil {
			return errors.New("head Schema did not have an ID in its Edit Task Data")
		}
		APICall.APIResponseSchemaID = *schemaID
		doReload = true
	}

	APICall.IngressBotID = "VdSyROckwzkoqwPshDLmBzEuE"
	APICall.EgressBotID = "kiTAYFaOWeaUDQZwUEbSdnyzG"
	APICall.BotStaticDataToUse = []string{"APICallData " + data.FirestoreID}

	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &APICall, data)
	if err != nil {
		return fmt.Errorf("common.StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}

	if doReload {
		response.NewURL = common.CreateURL(request.IndustryID, request.DomainID, request.TaskID, request.UXID, request.SchemaID,
			request.DataID, request.UserID, request.RoleID, request.OverrideIndustryID, request.OverrideDomainID)
	}

	return nil
}

// createTaskRecursively checks if a Task has dependencies. If it does, it calls createTaskRecursively.
func createTaskRecursively(dependencyTree *Tree, datas []common.Data, userID string, roleID string) error {
	var err error
	for i := range dependencyTree.Children {
		err = createTaskRecursively(dependencyTree.Children[i], datas, userID, roleID)
		if err != nil {
			return fmt.Errorf("createTaskRecursively: %w", err)
		}
	}
	var headTask *common.Data
	for i := range datas {
		if datas[i].FirestoreID == dependencyTree.FirestoreID {
			headTask = &datas[i]
			break
		}
	}
	if headTask == nil {
		return errors.New("headTask was nil for FirestoreID: " + dependencyTree.FirestoreID)
	}

	// update this Data's subtasks with the Task, Schema, and UX IDs from its children.
	for dependencyTreeChildrenIndex, child := range dependencyTree.Children {
		// there is a 1:1 between dependencyTree.Children and Subtasks
		childTaskID := ""
		childSchemaID := ""
		childUXID := ""
		for i := range datas {
			if datas[i].FirestoreID == child.FirestoreID {
				// This is the Subtask's "Edit Task" Data.
				childTaskID = *datas[i].S[common.AddFieldPrefix(datas[i].IndustryID, datas[i].DomainID, datas[i].SchemaID, "Task ID")]
				childSchemaID = *datas[i].S[common.AddFieldPrefix(datas[i].IndustryID, datas[i].DomainID, datas[i].SchemaID, "Schema ID")]
				childUXID = *datas[i].S[common.AddFieldPrefix(datas[i].IndustryID, datas[i].DomainID, datas[i].SchemaID, "UX ID")]
				break
			}
		}
		if childTaskID == "" || childSchemaID == "" || childUXID == "" {
			return errors.New("a dependencyTree.Children was not found in datas")
		}

		// we assume the order of Subtasks == the order of dependencyTree.Children
		subtasks := headTask.ARef["polyapp_TaskSchemaUX_SYfKUkHjtbesOmcBsPVyimJjk_Subtasks"]
		if len(subtasks) != len(dependencyTree.Children) {
			return errors.New("len(subtasks) != len(dependencyTree.Children) at ID: " + child.FirestoreID)
		}
		subtaskRef, err := common.ParseRef(subtasks[dependencyTreeChildrenIndex])
		if err != nil {
			return fmt.Errorf("common.ParseRef: %w", err)
		}

		// We now know the ID of the Subtask. We could read it from the database, but instead we'll use our convenient
		// cache of common.Data stored in datas.
		var updatedSubtask common.Data
		subtaskIndexInDatas := 0
		for i := range datas {
			if datas[i].FirestoreID == subtaskRef.DataID {
				updatedSubtask = datas[i]
				subtaskIndexInDatas = i
				break
			}
		}
		if updatedSubtask.FirestoreID == "" {
			return errors.New("subtask was not found in datas: " + subtaskRef.DataID)
		}
		updatedSubtask.S[common.AddFieldPrefix(updatedSubtask.IndustryID, updatedSubtask.DomainID, updatedSubtask.SchemaID, "Task ID")] = common.String(childTaskID)
		updatedSubtask.S[common.AddFieldPrefix(updatedSubtask.IndustryID, updatedSubtask.DomainID, updatedSubtask.SchemaID, "Schema ID")] = common.String(childSchemaID)
		updatedSubtask.S[common.AddFieldPrefix(updatedSubtask.IndustryID, updatedSubtask.DomainID, updatedSubtask.SchemaID, "UX ID")] = common.String(childUXID)
		err = allDB.UpdateData(&updatedSubtask)
		if err != nil {
			return fmt.Errorf("allDB.UpdateData: %w", err)
		}
		// Must update datas in addition to the database.
		datas[subtaskIndexInDatas] = updatedSubtask
	}
	err = ActionEditTask(headTask, &common.POSTRequest{
		IndustryID: "polyapp",
		DomainID:   "TaskSchemaUX",
		UserID:     userID,
		RoleID:     roleID,
	}, nil)
	if err != nil {
		return fmt.Errorf("actions.ActionEditTask: %w", err)
	}
	for i := range datas {
		if datas[i].FirestoreID == dependencyTree.FirestoreID {
			datas[i] = *headTask
			break
		}
	}
	return nil
}

// fixInvalidKeys handles invalid key names. We shouldn't rely upon people to paste in valid key names, and we should
// correct invalid key names for them. This function does that for you.
func fixInvalidKeys(providedJSONMap map[string]interface{}) map[string]interface{} {
	fixedMap := make(map[string]interface{})
	for k := range providedJSONMap {
		switch v := providedJSONMap[k].(type) {
		case map[string]interface{}:
			fixedMap[fixOneKey(k)] = fixInvalidKeys(v)
		case []map[string]interface{}:
			fixedMap[fixOneKey(k)] = make([]interface{}, len(v))
			for i := range v {
				fixedMap[fixOneKey(k)].([]interface{})[i] = fixInvalidKeys(v[i])
			}
		case []interface{}:
			m := make([]map[string]interface{}, len(v))
			var ok bool
			for i := range v {
				m[i], ok = v[i].(map[string]interface{})
				if !ok {
					break
				}
			}
			if !ok {
				fixedMap[fixOneKey(k)] = providedJSONMap[k]
				continue
			}
			fixedMap[fixOneKey(k)] = make([]interface{}, len(m))
			for i := range m {
				fixedMap[fixOneKey(k)].([]interface{})[i] = fixInvalidKeys(m[i])
			}
		default:
			fixedMap[fixOneKey(k)] = v
		}
	}
	return fixedMap
}

func fixOneKey(s string) string {
	return strings.ReplaceAll(s, "_", " ")
}

// ActionPopulateFromUser takes information stored in the User document and sets it into the current, top-level Data.
// All information must be mapped in a "Bot Static Data" for any setting to take place. The format is:
// "PopulateFromUser {UserProperty}-{FieldName}" Examples: "PopulateFromUser UID-field name",
// "PopulateFromUser Full Name-field name" "PopulateFromUser Phone Number-phone number".
// Full list of possible User fields includes: UID, Full Name, Photo URL, Email Verified, Phone Number, Email,
// Roles, Bots Triggered At Load, Bots Triggered At Done, Home Task, Home UX, Home Schema, Home Data, polyappFirestoreID.
func ActionPopulateFromUser(data *common.Data, request *common.POSTRequest, response *common.POSTResponse, botStaticData map[string]string) error {
	var err error
	translateMap := make(map[string]string)
	for k, v := range botStaticData {
		if k == "PopulateFromUser" {
			split := strings.Split(v, "-")
			if len(split) < 2 {
				return errors.New("splitting PopulateFromUser on - produced less than 2 halves: " + v)
			}
			joinedSecondHalf := ""
			for i := 1; i < len(split); i++ {
				joinedSecondHalf += split[i]
			}
			translateMap[split[0]] = common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, joinedSecondHalf)
		}
	}

	if request.UserCache == nil {
		u, err := allDB.ReadUser(request.UserID)
		if err != nil {
			return fmt.Errorf("allDB.ReadUser: %w", err)
		}
		request.UserCache = &u
	}

	for userProperty, fieldName := range translateMap {
		if (data.S[fieldName] == nil || *data.S[fieldName] == "") &&
			(data.B[fieldName] == nil || *data.B[fieldName] == false) &&
			(data.AS[fieldName] == nil || len(data.AS[fieldName]) == 0) {
			// ok, because this value isn't currently set & therefore we are allowed to overwrite it.
		} else {
			continue
		}
		switch userProperty {
		case "UID":
			data.S[fieldName] = request.UserCache.UID
		case "Full Name":
			data.S[fieldName] = request.UserCache.FullName
		case "Photo URL":
			data.S[fieldName] = request.UserCache.PhotoURL
		case "Email Verified":
			data.B[fieldName] = request.UserCache.EmailVerified
		case "Phone Number":
			data.S[fieldName] = request.UserCache.PhoneNumber
		case "Email":
			data.S[fieldName] = request.UserCache.Email
		case "Roles":
			data.AS[fieldName] = request.UserCache.Roles
		case "Bots Triggered At Load":
			data.AS[fieldName] = request.UserCache.BotsTriggeredAtLoad
		case "Bots Triggered At Done":
			data.AS[fieldName] = request.UserCache.BotsTriggeredAtDone
		case "Home Task":
			data.S[fieldName] = common.String(request.UserCache.HomeTask)
		case "Home UX":
			data.S[fieldName] = common.String(request.UserCache.HomeUX)
		case "Home Schema":
			data.S[fieldName] = common.String(request.UserCache.HomeSchema)
		case "Home Data":
			data.S[fieldName] = common.String(request.UserCache.HomeData)
		case "polyappFirestoreID":
			data.S[fieldName] = common.String(request.UserCache.FirestoreID)
		}
	}

	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}

	return nil
}

type copyTaskDataByID struct {
	NewIndustry  string `suffix:"New Industry"`
	CopySchemaID string `suffix:"Copy Schema ID"`
	NewDomain    string `suffix:"New Domain"`
	CopyTaskID   string `suffix:"Copy Task ID"`
	DataID       string `suffix:"Data ID"`
	CopyUXID     string `suffix:"Copy UX ID"`
	CopyDataID   string `suffix:"Copy Data ID"`
}

// ActionCopyTaskByDataID copies a Data and its children. It also assumes that Data works with ActionEditTask and it
// generates a new Task, Schema, and UX for that Data.
func ActionCopyTaskByDataID(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	copyTaskDataByID := copyTaskDataByID{}
	err = common.DataIntoStructure(data, &copyTaskDataByID)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}
	dataToCopy := &common.Data{
		FirestoreID: copyTaskDataByID.DataID,
	}
	err = allDB.Read(dataToCopy)
	if err != nil {
		return fmt.Errorf("allDB.ReadData: %w", err)
	}
	r := common.POSTRequest{
		UserID: request.UserID,
		RoleID: request.RoleID,
	}
	copyData, err := copyTaskSchemaUXFromData(dataToCopy, &r, copyTaskDataByID.NewIndustry, copyTaskDataByID.NewDomain,
		copyTaskDataByID.CopyTaskID, copyTaskDataByID.CopySchemaID, copyTaskDataByID.CopyUXID)
	if err != nil {
		return fmt.Errorf("copyTaskSchemaUXFromData: %w", err)
	}
	copyTaskDataByID.CopyDataID = copyData.FirestoreID
	for k, v := range copyData.S {
		if strings.HasSuffix(k, "_Task ID") {
			copyTaskDataByID.CopyTaskID = *v
		} else if strings.HasSuffix(k, "_UX ID") {
			copyTaskDataByID.CopyUXID = *v
		} else if strings.HasSuffix(k, "_Schema ID") {
			copyTaskDataByID.CopySchemaID = *v
		}
	}

	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &copyTaskDataByID, data)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}
	response.NewURL = "/t/polyapp/TaskSchemaUX/gqyLWclKrvEaWHalHqUdHghrt?ux=hRpIEGPgKvxSzvnCeDpAVcwgJ&schema=gjGLMlcNApJyvRjmgQbopsXPI&data=" + copyTaskDataByID.CopyDataID
	return nil
}

// copyTaskSchemaUXFromData takes the head Data in "Edit Task" and copies it into new Data in Firestore. It also
// uses that Data to generate a new Task, Schema, and UX and stores that in Firestore.
//
// The new Data will set Agree To Make Public to false.
//
// request should set UserID and RoleID and security will be validated against them. The caches will also be used.
//
// newIndustry and newDomain are optional. They will change the Industry and Domain of the Data.
//
// newTask, newSchema, newUX are optional. If set they will be used for the new Task, Schema, and UX. You can use this
// to update an existing Task/Schema/UX which is "downstream" from the Task/Schema/UX you are copying.
func copyTaskSchemaUXFromData(data *common.Data, request *common.POSTRequest, newIndustry, newDomain string, newTask, newSchema, newUX string) (CopyDataID common.Data, err error) {
	dataAndChildren, err := allDB.CopyData(data)
	if err != nil {
		return CopyDataID, fmt.Errorf("allDB.CopyData: %w", err)
	}
	for k := range dataAndChildren[0].S {
		if strings.HasSuffix(k, "_Task ID") {
			dataAndChildren[0].S[k] = common.String(newTask)
		} else if strings.HasSuffix(k, "_UX ID") {
			dataAndChildren[0].S[k] = common.String(newUX)
		} else if strings.HasSuffix(k, "_Schema ID") {
			dataAndChildren[0].S[k] = common.String(newSchema)
		} else if strings.HasSuffix(k, "_Industry") && newIndustry != "" {
			dataAndChildren[0].S[k] = common.String(newIndustry)
		} else if strings.HasSuffix(k, "_Domain") && newDomain != "" {
			dataAndChildren[0].S[k] = common.String(newDomain)
		}
	}
	for k := range dataAndChildren[0].B {
		if strings.HasSuffix(k, "_Agree to Make Public") {
			dataAndChildren[0].B[k] = common.Bool(false)
		}
	}
	errChan := make(chan error)
	for i := range dataAndChildren {
		go func(d common.Data, i int, errChan chan error) {
			err = allDB.CreateData(&d)
			if err != nil {
				errChan <- fmt.Errorf("allDB.CreateData %v: %w", d.FirestoreID, err)
				return
			}
			errChan <- nil
		}(dataAndChildren[i], i, errChan)
	}
	combinedErr := ""
	for range dataAndChildren {
		err := <-errChan
		if err != nil {
			combinedErr += "; err: " + err.Error()
		}
	}
	if combinedErr != "" {
		return CopyDataID, fmt.Errorf("combinedErr from setting allDB.CreateData: %w", err)
	}

	err = ActionEditTask(&dataAndChildren[0], request, nil)
	if err != nil {
		return CopyDataID, fmt.Errorf("ActionEditTask: %w", err)
	}
	return dataAndChildren[0], nil
}

type ViewTaskHelpText struct {
	HeadTaskID string `suffix:"Head Task ID"`
}

// ActionViewTaskHelpText This Task helps you view a particular Task's Help Text. It uses a hidden, read-only field
// which holds the Head Task ID. When the page is loaded, it reads the Help Text of the Task documents and displays it
// via its Bot, the "View Task Help Text" Bot.
func ActionViewTaskHelpText(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	ViewTaskHelpText := ViewTaskHelpText{}
	err = common.DataIntoStructure(data, &ViewTaskHelpText)
	if err != nil {
		return fmt.Errorf("common.DataIntoStructure: %w", err)
	}

	headTask, err := allDB.ReadTask(ViewTaskHelpText.HeadTaskID)
	if err != nil {
		return fmt.Errorf("allDB.ReadTask: %w", err)
	}
	queryable, err := allDB.QueryEquals(common.CollectionData,
		common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Task ID"), headTask.FirestoreID)
	if err != nil {
		return fmt.Errorf("allDB.QueryEquals: %w", err)
	}
	editTaskData := queryable.(*common.Data)
	schemaID := editTaskData.S[common.AddFieldPrefix(editTaskData.IndustryID, editTaskData.DomainID, editTaskData.SchemaID, "Schema ID")]
	if schemaID == nil {
		return errors.New("Schema ID for this Task ID was nil; perhaps the Task hasn't been generated yet")
	}
	headSchema, err := allDB.ReadSchema(*schemaID)
	if err != nil {
		return fmt.Errorf("allDB.ReadSchema: %w", err)
	}
	childTasks := make([]common.Task, 0)
	for k, v := range headSchema.DataTypes {
		_, _, schemaID, _ := common.SplitField(k)
		if v == "Ref" || v == "ARef" {
			queryable, err := allDB.QueryEquals(common.CollectionData,
				common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Schema ID"), schemaID)
			if err != nil {
				return fmt.Errorf("allDB.QueryEquals: %w", err)
			}
			editTaskData := queryable.(*common.Data)
			taskID := editTaskData.S[common.AddFieldPrefix(editTaskData.IndustryID, editTaskData.DomainID, editTaskData.SchemaID, "Task ID")]
			childTask, err := allDB.ReadTask(*taskID)
			if err != nil {
				return fmt.Errorf("allDB.ReadSchema: %w", err)
			}
			childTasks = append(childTasks, childTask)
		}
	}
	type taskHelpTextInfo struct {
		Name     string
		HelpText string
	}
	var taskHelpTextS struct {
		Name             string
		HelpText         string
		TaskHelpTextInfo []taskHelpTextInfo
	}
	taskHelpTextS.Name = headTask.Name
	taskHelpTextS.HelpText = headTask.HelpText
	taskHelpTextS.TaskHelpTextInfo = make([]taskHelpTextInfo, 0)
	for _, task := range childTasks {
		taskHelpTextS.TaskHelpTextInfo = append(taskHelpTextS.TaskHelpTextInfo, taskHelpTextInfo{
			Name:     task.Name,
			HelpText: task.HelpText,
		})
	}
	var htmlBuffer bytes.Buffer
	viewTaskHelpTextTemplate := `<h1>{{.Name}}</h1>
<p>{{.HelpText}}</p>
{{range .TaskHelpTextInfo}}
<h2>{{.Name}}</h2>
<p>{{.HelpText}}</p>
{{end}}
`
	t, err := template.New("").Parse(viewTaskHelpTextTemplate)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	err = t.Execute(&htmlBuffer, taskHelpTextS)
	if err != nil {
		return fmt.Errorf("t.Execute: %w", err)
	}

	response.ModDOMs = append(response.ModDOMs, common.ModDOM{
		InsertSelector: "#polyapp_TaskSchemaUX_nEwbjkzzZiJjLhUOWYBhVEdzI_Done",
		Action:         "beforebegin",
		HTML:           htmlBuffer.String(),
	})
	return nil
}

type ImmediateSimpleConversion struct {
	DeleteOldFieldValue bool      `suffix:"Delete Old Field Value"`
	DoneTimestamps      []float64 `suffix:"Done Timestamps"`
	NewFieldName        string    `suffix:"New Field Name"`
	DoneUserIDs         []string  `suffix:"Done User IDs"`
	LoadTimestamps      []float64 `suffix:"Load Timestamps"`
	OldFieldName        string    `suffix:"Old Field Name"`
	Done                bool      `suffix:"Done"`
	LoadUserIDs         []string  `suffix:"Load User IDs"`
}

// ActionImmediateSimpleConversion An Immediate Conversion goes through all Data associated with a Task and immediately
// runs the conversion on each Data when you click Done. A Simple Conversion takes the value in one field and places it into another field.
func ActionImmediateSimpleConversion(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	ImmediateSimpleConversion := ImmediateSimpleConversion{}
	err = common.DataIntoStructure(data, &ImmediateSimpleConversion)
	if err != nil {
		return fmt.Errorf("common.DataIntoStructure: %w", err)
	}

	industry, domain, schema, _ := common.SplitField(ImmediateSimpleConversion.OldFieldName)
	q := allDB.Query{}
	q.Init(industry, domain, schema, common.CollectionData)
	err = q.AddSortBy(ImmediateSimpleConversion.OldFieldName, "asc")
	if err != nil {
		return fmt.Errorf("q.AddSortBy: %w", err)
	}
	iter, err := q.QueryRead()
	if err != nil {
		return fmt.Errorf("q.QueryRead: %w", err)
	}
	newDatas := make([]*common.Data, 0)
	for {
		d, err := iter.Next()
		if err != nil && err == common.IterDone {
			break
		} else if err != nil {
			return fmt.Errorf("iter.Next: %w", err)
		}
		matchingData := d.(*common.Data)
		if matchingData.S[ImmediateSimpleConversion.OldFieldName] != nil {
			if ImmediateSimpleConversion.NewFieldName == "delete" {
				// do nothing
			} else {
				matchingData.S[ImmediateSimpleConversion.NewFieldName] = common.String(*matchingData.S[ImmediateSimpleConversion.OldFieldName])
			}
			if ImmediateSimpleConversion.DeleteOldFieldValue {
				matchingData.S[ImmediateSimpleConversion.OldFieldName] = nil
			}
		}
		if matchingData.F[ImmediateSimpleConversion.OldFieldName] != nil {
			if ImmediateSimpleConversion.NewFieldName == "delete" {
				// do nothing
			} else {
				matchingData.F[ImmediateSimpleConversion.NewFieldName] = common.Float64(*matchingData.F[ImmediateSimpleConversion.OldFieldName])
			}
			if ImmediateSimpleConversion.DeleteOldFieldValue {
				matchingData.F[ImmediateSimpleConversion.OldFieldName] = nil
			}
		}
		if matchingData.I[ImmediateSimpleConversion.OldFieldName] != nil {
			if ImmediateSimpleConversion.NewFieldName == "delete" {
				// do nothing
			} else {
				matchingData.I[ImmediateSimpleConversion.NewFieldName] = common.Int64(*matchingData.I[ImmediateSimpleConversion.OldFieldName])
			}
			if ImmediateSimpleConversion.DeleteOldFieldValue {
				matchingData.S[ImmediateSimpleConversion.OldFieldName] = nil
			}
		}
		if matchingData.B[ImmediateSimpleConversion.OldFieldName] != nil {
			if ImmediateSimpleConversion.NewFieldName == "delete" {
				// do nothing
			} else {
				matchingData.B[ImmediateSimpleConversion.NewFieldName] = common.Bool(*matchingData.B[ImmediateSimpleConversion.OldFieldName])
			}
			if ImmediateSimpleConversion.DeleteOldFieldValue {
				matchingData.B[ImmediateSimpleConversion.OldFieldName] = nil
			}
		}
		if matchingData.Ref[ImmediateSimpleConversion.OldFieldName] != nil {
			if ImmediateSimpleConversion.NewFieldName == "delete" {
				// do nothing
			} else {
				matchingData.Ref[ImmediateSimpleConversion.NewFieldName] = common.String(*matchingData.Ref[ImmediateSimpleConversion.OldFieldName])
			}
			if ImmediateSimpleConversion.DeleteOldFieldValue {
				matchingData.Ref[ImmediateSimpleConversion.OldFieldName] = nil
			}
		}
		if matchingData.AS[ImmediateSimpleConversion.OldFieldName] != nil {
			if ImmediateSimpleConversion.NewFieldName == "delete" {
				// do nothing
			} else {
				matchingData.AS[ImmediateSimpleConversion.NewFieldName] = matchingData.AS[ImmediateSimpleConversion.OldFieldName]
			}
			if ImmediateSimpleConversion.DeleteOldFieldValue {
				matchingData.AS[ImmediateSimpleConversion.OldFieldName] = nil
			}
		}
		if matchingData.AF[ImmediateSimpleConversion.OldFieldName] != nil {
			if ImmediateSimpleConversion.NewFieldName == "delete" {
				// do nothing
			} else {
				matchingData.AF[ImmediateSimpleConversion.NewFieldName] = matchingData.AF[ImmediateSimpleConversion.OldFieldName]
			}
			if ImmediateSimpleConversion.DeleteOldFieldValue {
				matchingData.AF[ImmediateSimpleConversion.OldFieldName] = nil
			}
		}
		if matchingData.AI[ImmediateSimpleConversion.OldFieldName] != nil {
			if ImmediateSimpleConversion.NewFieldName == "delete" {
				// do nothing
			} else {
				matchingData.AI[ImmediateSimpleConversion.NewFieldName] = matchingData.AI[ImmediateSimpleConversion.OldFieldName]
			}
			if ImmediateSimpleConversion.DeleteOldFieldValue {
				matchingData.AI[ImmediateSimpleConversion.OldFieldName] = nil
			}
		}
		if matchingData.AB[ImmediateSimpleConversion.OldFieldName] != nil {
			if ImmediateSimpleConversion.NewFieldName == "delete" {
				// do nothing
			} else {
				matchingData.AB[ImmediateSimpleConversion.NewFieldName] = matchingData.AB[ImmediateSimpleConversion.OldFieldName]
			}
			if ImmediateSimpleConversion.DeleteOldFieldValue {
				matchingData.AB[ImmediateSimpleConversion.OldFieldName] = nil
			}
		}
		if matchingData.ARef[ImmediateSimpleConversion.OldFieldName] != nil {
			if ImmediateSimpleConversion.NewFieldName == "delete" {
				// do nothing
			} else {
				matchingData.ARef[ImmediateSimpleConversion.NewFieldName] = matchingData.ARef[ImmediateSimpleConversion.OldFieldName]
			}
			if ImmediateSimpleConversion.DeleteOldFieldValue {
				matchingData.ARef[ImmediateSimpleConversion.OldFieldName] = nil
			}
		}
		newDatas = append(newDatas, matchingData)
	}

	errChan := make(chan error)
	for i := range newDatas {
		go func(errChan chan error, d *common.Data) {
			err = allDB.UpdateData(d)
			if err != nil {
				errChan <- fmt.Errorf("allDB.UpdateData for Data ID (%v): %w", d.FirestoreID, err)
				return
			}
			errChan <- nil
		}(errChan, newDatas[i])
	}

	combinedErr := ""
	for range newDatas {
		err := <-errChan
		if err != nil {
			combinedErr += err.Error() + "; "
		}
	}
	if combinedErr != "" {
		return fmt.Errorf("combinedErr while updating data during conversion: %v", combinedErr)
	}

	return nil
}

type CreateTheme struct {
	Description    string `suffix:"Description"`
	SecondaryColor string `suffix:"Secondary Color"`
	PrimaryColor   string `suffix:"Primary Color"`
	LightColor     string `suffix:"Light Color"`
	DarkColor      string `suffix:"Dark Color"`
	LogoforIcon    []byte `suffix:"Logo for Icon"`
	Name           string `suffix:"Name"`
	BootstrapTheme []byte `suffix:"Bootstrap Theme"`
	SetAsMyTheme   bool   `suffix:"Set As My Theme"`
	ExportToJSON   bool   `suffix:"Export to JSON"`
}

// ActionCreateTheme A theme stylizes Polyapp with your preferred colors, lets you set a logo into the address bar, and generally lets you control the look and feel of the application.
func ActionCreateTheme(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	CreateTheme := CreateTheme{}
	err = common.DataIntoStructure(data, &CreateTheme)
	if err != nil {
		return fmt.Errorf("common.DataIntoStructure: %w", err)
	}
	var buf bytes.Buffer
	err = gen.GenBootstrapThemeDirectToCSS(gen.BootstrapThemeDirectToCSS{
		Primary:        CreateTheme.PrimaryColor,
		Secondary:      CreateTheme.SecondaryColor,
		Light:          CreateTheme.LightColor,
		Dark:           CreateTheme.DarkColor,
		BodyBackground: "",
	}, &buf)
	if err != nil {
		return fmt.Errorf("gen.GenBootstrapTheme: %w", err)
	}

	CreateTheme.BootstrapTheme = buf.Bytes()

	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &CreateTheme, data)
	if err != nil {
		return fmt.Errorf("common.StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}

	if CreateTheme.SetAsMyTheme {
		themeURL := common.BlobURL(data.FirestoreID, common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, "Bootstrap Theme")) + "?cacheBuster=" + common.GetRandString(5)
		if request.UserCache == nil {
			u, err := allDB.ReadUser(request.UserID)
			if err != nil {
				return fmt.Errorf("allDB.ReadUser: %w", err)
			}
			request.UserCache = &u
		}
		request.UserCache.ThemePath = common.String(themeURL)
		userDataI, err := allDB.QueryEquals(common.CollectionData, "polyapp_User_TkfMDeIjfSaXCMSByYRsEbubp_User ID", request.UserCache.FirestoreID)
		if err != nil {
			return fmt.Errorf("allDB.QueryEquals: %w", err)
		}
		userData := userDataI.(*common.Data)
		userData.S[common.AddFieldPrefix(userData.IndustryID, userData.DomainID, userData.SchemaID, "Theme Path")] = common.String(themeURL)
		err = allDB.UpdateData(userData)
		if err != nil {
			return fmt.Errorf("allDB.UpdateData for User %v: %w", userData.FirestoreID, err)
		}
		err = allDB.UpdateUser(request.UserCache)
		if err != nil {
			return fmt.Errorf("allDB.UpdateUser: %w", err)
		}
	}

	if CreateTheme.ExportToJSON {
		err = fileCRUD.Init("")
		if err != nil {
			return fmt.Errorf("fileCRUD.Init: %w", err)
		}
		err = fileCRUD.Create(data)
		if err != nil {
			return fmt.Errorf("fileCRUD.Create: %w", err)
		}
	}

	return nil
}
