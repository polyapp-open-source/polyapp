package actions

import (
	"bytes"
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"gitlab.com/polyapp-open-source/polyapp/fileCRUD"
	"gitlab.com/polyapp-open-source/polyapp/firestoreCRUD"
	"gitlab.com/polyapp-open-source/polyapp/gen"
	"gitlab.com/polyapp-open-source/polyapp/integrity"
	"net/url"
	"strings"
	"sync"
	"time"
)

// EditTask Schema is gjGLMlcNApJyvRjmgQbopsXPI
type EditTask struct {
	Name     string `suffix:"Name"`
	HelpText string `suffix:"Help Text"`
	// AgreeToMakePublic, if true, sends the Data, Task, Schema, UX, Bot, Action documents to a remote server.
	AgreeToMakePublic         bool   `suffix:"Agree to Make Public"`
	ExportAsJSONToFile        bool   `suffix:"Export As JSON To File"`
	TaskID                    string `suffix:"Task ID"`
	SchemaID                  string `suffix:"Schema ID"`
	UXID                      string `suffix:"UX ID"`
	ViewTaskHelpTextDataID    string `suffix:"View Task Help Text Data ID"`
	Industry                  string `suffix:"Industry"`
	Domain                    string
	BotsTriggeredAtLoad       []string `suffix:"Bots Triggered At Load"`
	BotsTriggeredContinuously []string `suffix:"Bots Triggered Continuously"`
	BotsTriggeredAtDone       []string `suffix:"Bots Triggered At Done"`
	BotStaticData             []string `suffix:"Bot Static Data"`
	Theme                     string   `suffix:"Theme"`
	Done                      bool     `suffix:"Done"`
	Fields                    []string `suffix:"Fields" dataType:"ARef" industry:"polyapp" domain:"TaskSchemaUX" schema:"SszMuOVRaMIOUteoWiXClbpSC"`
	Subtasks                  []string `suffix:"Subtasks" dataType:"ARef" industry:"polyapp" domain:"TaskSchemaUX" schema:"SYfKUkHjtbesOmcBsPVyimJjk"`
	ImmediateSimpleConversion []string `suffix:"Immediate Simple Conversion" dataType:"ARef" industry:"polyapp" domain:"TaskSchemaUX" schema:"XmjaaekTsoQjsFHwTBBACmQzY"`
}

// Field Schema is SszMuOVRaMIOUteoWiXClbpSC
type Field struct {
	Name     string `suffix:"Name"`
	HelpText string `suffix:"Help Text"`
	// UserInterface can be one of: "single line input", "multiple line input", "multiple line text editor", "select", "image upload", etc.
	UserInterface string `suffix:"User Interface"`
	// Validator can be one of: "", "Not Empty (also known as a Required Field)"
	Validator string `suffix:"Validator"`
	// SelectOptions is only set if you are creating a Select.
	SelectOptions []string `suffix:"Select Options"`
	// UseAsDoneButton is usually ignored unless the UserInterface is a "button". If set to True, this will make this Field ID _Done instead of the default.
	UseAsDoneButton bool `suffix:"Use as Done Button"`
	// ReadOnly is true if this field should be read-only. This means that both the UI should show it as Read-Only and the server (Task) should enforce that status.
	ReadOnly bool `suffix:"Read Only"`
	// Hidden is set to True if you want the field to exist in the Schema and Task but not in the UX.
	Hidden      bool   `suffix:"Hidden"`
	FirestoreID string `suffix:"PolyappFirestoreID"`
}

func (f Field) Validate() error {
	if err := integrity.ValidatePolyappKey("ind_dom_sch_" + f.Name); err != nil {
		// we could wait to try to catch this error when we try to save the Data or save the Schema, but it would be
		// better to catch the error earlier.
		return fmt.Errorf("integrity.ValidatePolyappKey (%v) (Help Text: %v): %w", f.Name, f.HelpText, err)
	}
	return nil
}

// Subtask Schema is SYfKUkHjtbesOmcBsPVyimJjk
type Subtask struct {
	TaskID          string `suffix:"Task ID"`
	SingletonOrList string `suffix:"Singleton or List"`
}

type mappingLocalDataIDToHubDataID struct {
	LocalDataID string `suffix:"Local Data ID"`
	HubDataID   string `suffix:"Hub Data ID"`
}

type Tree struct {
	FirestoreID string
	Children    []*Tree
}

// ActionLoadEditTask performs some one-time setup for the Edit Task Task.
func ActionLoadEditTask(currentData *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	q := allDB.Query{}
	q.Init("polyapp", "TaskSchemaUX", "KxZRoxrRpyECTbCudVfXKnmHB", common.CollectionData)
	err = q.AddSortBy(common.AddFieldPrefix("polyapp", "TaskSchemaUX", "KxZRoxrRpyECTbCudVfXKnmHB", "Name"), "asc")
	if err != nil {
		return fmt.Errorf("q.AddSortBy: %w", err)
	}
	iter, err := q.QueryRead()
	if err != nil {
		return fmt.Errorf("q.QueryRead: %w", err)
	}
	themeOptions := make(map[string]bool)
	for {
		queryable, err := iter.Next()
		if err != nil && err == common.IterDone {
			break
		}
		if err != nil {
			return fmt.Errorf("iter.Next: %w", err)
		}
		themeData := queryable.(*common.Data)
		if themeData.S[common.AddFieldPrefix("polyapp", "TaskSchemaUX", "KxZRoxrRpyECTbCudVfXKnmHB", "Name")] != nil {
			themeOptions[*themeData.S[common.AddFieldPrefix("polyapp", "TaskSchemaUX", "KxZRoxrRpyECTbCudVfXKnmHB", "Name")]] = true
		}
	}
	delete(themeOptions, "")
	for k := range themeOptions {
		response.ModDOMs = append(response.ModDOMs, common.ModDOM{
			InsertSelector: "#" + common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Theme"),
			Action:         "afterbegin",
			HTML:           `<option>` + k + `</option>`,
		})
	}
	return nil
}

// ActionEditTask is, as of September 1, 2020, the best way to create an Interactive Section or other Task.
// It is used to create all other Task, Schema, and UX documents. It is also used to edit the Task which edits Tasks.
//
// Warnings and caveats:
// 1. Adding a field to a Schema is easy. Modifying an existing field's name is not. When you modify a field's name
// you are making all previous Data stored using that field's name as the key in the JSON in the Data collection in the
// Database incompatible since that Data will now not have a key in Schema. Ex.: Field "A" renamed to field "B".
// When you open the Data to edit it, it will throw an error because field "A" was present in the Data, but it is not present
// in the new Schema.
// 2. Removing an existing field's name is not easy for the same reason as (1). If there is a Data document and you go
// back to read or edit it, the Schema will not contain the field and it will throw an error.
// TODO implement convert in integrity/convert and the NextSchemaID field in type Schema.
//
// Data documents are never deleted when the schema changes. So when you export to JSON a Schema with fields "A" and "B"
// and then update it to only contain Field "A", the JSON document containing "B" will still be there on the server.
// TODO we want to delete this document in all of the databases (local JSON files & Firestore) when this occurs but this hasn't been implemented.
//
// Schema: mywkQhPF1qIT979zOn6J
func ActionEditTask(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	var editTask EditTask
	err = common.DataIntoStructure(data, &editTask)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}
	if editTask.Name == "" || editTask.HelpText == "" || data.FirestoreID == "" || editTask.Industry == "" || editTask.Domain == "" {
		return errors.New("ActionEditTask required fields were not set")
	}

	if request.UserCache == nil {
		user, err := allDB.ReadUser(request.UserID)
		if err != nil {
			return fmt.Errorf("allDB.Readuser: %w", err)
		}
		request.UserCache = &user
	}
	hasAccess := false
	for _, roleID := range request.UserCache.Roles {
		role, err := allDB.ReadRole(roleID)
		if err != nil {
			return fmt.Errorf("allDB.ReadRole: %w", err)
		}
		hasAccess, err = common.RoleHasAccessKeyValues(&role, editTask.Industry, editTask.Domain, editTask.SchemaID, "EditTask", "true")
		if hasAccess {
			break
		}
	}
	if !hasAccess {
		return fmt.Errorf("User %v did not have access to EditTask for industry %v domain %v schema %v", request.UserID, editTask.Industry, editTask.Domain, editTask.SchemaID)
	}

	if editTask.ExportAsJSONToFile {
		// have to export each individual data document
		// Since we are calling Init here we can omit it later too.
		err = fileCRUD.Init("")
		if err != nil {
			return fmt.Errorf("fileCRUD.Init: %w", err)
		}
	}

	// Gather data about the Fields.
	allFields := make([]Field, len(editTask.Fields))
	fieldDatas := make([]common.Data, len(editTask.Fields))
	errChanFields := make(chan error)
	for i, fieldRef := range editTask.Fields {
		go func(errChan chan error, fieldRef string, i int) {
			fieldURL, err := url.Parse(fieldRef)
			if err != nil {
				errChan <- fmt.Errorf("url.Parse for ("+fieldRef+"): %w", err)
				return
			}
			getReq, err := common.CreateGETRequest(*fieldURL)
			if err != nil {
				errChan <- fmt.Errorf("common.CreateGETRequest for ("+fieldRef+"): %w", err)
				return
			}
			dataForField := &common.Data{
				FirestoreID: getReq.DataID,
			}
			err = allDB.Read(dataForField)
			if err != nil {
				errChan <- fmt.Errorf("allDB.ReadData for ("+fieldRef+"): %w", err)
				return
			}
			if editTask.ExportAsJSONToFile {
				err = fileCRUD.Create(dataForField)
				if err != nil {
					errChan <- fmt.Errorf("exportAsJSONToFile for fileCRUD.Create %v: %w", dataForField.FirestoreID, err)
					return
				}
			}
			err = common.DataIntoStructure(dataForField, &allFields[i])
			if err != nil {
				errChan <- fmt.Errorf("DataIntoStructure for allFields[%v]: %w", i, err)
				return
			}
			fieldDatas[i] = *dataForField
			err = allFields[i].Validate()
			if err != nil {
				errChan <- fmt.Errorf("allFields[%v].Validate: %w", i, err)
				return
			}
			errChan <- nil
		}(errChanFields, fieldRef, i)
	}

	errChanImmediateSimpleConversions := make(chan error)
	var immediateSimpleConversionsMux sync.Mutex
	immediateSimpleConversionDatas := make([]common.Data, len(editTask.ImmediateSimpleConversion))
	immediateSimpleConversions := make([]ImmediateSimpleConversion, len(editTask.ImmediateSimpleConversion))
	for i := range editTask.ImmediateSimpleConversion {
		parsedRef, err := common.ParseRef(editTask.ImmediateSimpleConversion[i])
		if err != nil {
			return fmt.Errorf("common.ParseRef %v: %w", editTask.ImmediateSimpleConversion[i], err)
		}
		imcData := common.Data{
			FirestoreID: parsedRef.DataID,
		}
		go func(imcData common.Data, i int) {
			err = allDB.Read(&imcData)
			if err != nil {
				errChanImmediateSimpleConversions <- fmt.Errorf("allDB.Read %v index %v: %w", imcData.FirestoreID, i, err)
				return
			}
			immediateSimpleConversionsMux.Lock()
			defer immediateSimpleConversionsMux.Unlock()
			immediateSimpleConversionDatas[i] = imcData
			err = common.DataIntoStructure(&imcData, &immediateSimpleConversions[i])
			if err != nil {
				errChanImmediateSimpleConversions <- fmt.Errorf("common.DataIntoStructure %v index %v: %w", imcData.FirestoreID, i, err)
				return
			}
			errChanImmediateSimpleConversions <- nil
		}(imcData, i)
	}
	combinedErr := ""
	for range editTask.ImmediateSimpleConversion {
		err = <-errChanImmediateSimpleConversions
		if err != nil {
			combinedErr += err.Error() + "; "
		}
	}
	for range editTask.Fields {
		err := <-errChanFields
		if err != nil {
			combinedErr += err.Error() + "; "
		}
	}

	if combinedErr != "" {
		return errors.New("combinedErr ImmediateSimpleConversion and Fields: " + combinedErr)
	}

	// Add default fields
	allFields = append(allFields, Field{
		Name:            "Load Timestamps",
		HelpText:        "Times this Task was opened",
		UserInterface:   "list of numbers",
		Validator:       "",
		SelectOptions:   nil,
		UseAsDoneButton: false,
		ReadOnly:        true,
		Hidden:          true,
	}, Field{
		Name:            "Done Timestamps",
		HelpText:        "Times Task was Done",
		UserInterface:   "list of numbers",
		Validator:       "",
		SelectOptions:   nil,
		UseAsDoneButton: false,
		ReadOnly:        true,
		Hidden:          true,
	}, Field{
		Name:            "Load User IDs",
		HelpText:        "ID of the User who loaded this Task. This array should be the same length as the Load Timestamps array.",
		UserInterface:   "list of single line text inputs",
		Validator:       "",
		UseAsDoneButton: false,
		ReadOnly:        true,
		Hidden:          true,
	}, Field{
		Name:          "Done User IDs",
		HelpText:      "ID of the User who clicked Done on this Task. This array should be the same length as the Done Timestamps array.",
		UserInterface: "list of single line text inputs",
		Validator:     "",
		ReadOnly:      true,
		Hidden:        true,
	})

	// Set defaults
	newTask := false
	newSchema := false
	var oldSchema common.Schema
	newUX := false
	newViewTaskHelpText := false
	if editTask.TaskID == "" {
		editTask.TaskID = common.GetRandString(25)
		newTask = true
	}
	if editTask.SchemaID == "" {
		editTask.SchemaID = common.GetRandString(25)
		newSchema = true
	} else {
		oldSchema, err = allDB.ReadSchema(editTask.SchemaID)
		if err != nil {
			return fmt.Errorf("allDB.ReadSchema oldSchema (%v): %w", editTask.SchemaID, err)
		}
	}
	if editTask.UXID == "" {
		editTask.UXID = common.GetRandString(25)
		newUX = true
	}
	if editTask.ViewTaskHelpTextDataID == "" {
		editTask.ViewTaskHelpTextDataID = common.GetRandString(25)
		newViewTaskHelpText = true
	}
	isPublic := false

	task := &common.Task{
		FirestoreID:               editTask.TaskID,
		IndustryID:                editTask.Industry,
		DomainID:                  editTask.Domain,
		Name:                      editTask.Name,
		HelpText:                  editTask.HelpText,
		TaskGoals:                 make(map[string]string),
		BotStaticData:             make(map[string]string),
		BotsTriggeredAtLoad:       append(editTask.BotsTriggeredAtLoad, "acfKuypDwkuzhLmRXoOCWUxrI"),
		BotsTriggeredAtDone:       append(editTask.BotsTriggeredAtDone, "LOEDMRbHPbSUKljIHGnHaGIrX"),
		BotsTriggeredContinuously: editTask.BotsTriggeredContinuously,
		IsPublic:                  false,
		Deprecated:                nil,
	}
	if len(editTask.BotStaticData) > 0 {
		for _, staticData := range editTask.BotStaticData {
			spaceIndex := strings.Index(staticData, " ")
			if spaceIndex > 0 && spaceIndex+1 < len(staticData) {
				task.BotStaticData[staticData[:spaceIndex]] = staticData[spaceIndex+1:]
			}
		}
	}

	dataTypes := make(map[string]string)
	dataHelpText := make(map[string]string)
	dataKeys := make([]string, len(allFields))
	for i, thisField := range allFields {
		prefixedDataKey := common.AddFieldPrefix(editTask.Industry, editTask.Domain, editTask.SchemaID, thisField.Name)
		dataTypes[prefixedDataKey] = "S"

		dataHelpText[prefixedDataKey] = thisField.HelpText
		dataKeys[i] = prefixedDataKey
	}
	schema := &common.Schema{
		FirestoreID:  editTask.SchemaID,
		NextSchemaID: nil,
		IndustryID:   editTask.Industry,
		DomainID:     editTask.Domain,
		SchemaID:     editTask.SchemaID,
		DataTypes:    dataTypes,
		DataHelpText: dataHelpText,
		DataKeys:     dataKeys,
		IsPublic:     isPublic,
		Deprecated:   nil,
		Name:         common.String(editTask.Name),
	}

	if !newSchema {
		// old schema so let's check to see if we need conversions
		fieldsWhichNeedConversions, err := FindFieldsWhichNeedConversions(allFields, oldSchema)
		if err != nil {
			return fmt.Errorf("FindFieldsWhichNeedConversions: %w", err)
		}
		needConversions, ConversionsOut := ValidateConversionCoverage(immediateSimpleConversions, fieldsWhichNeedConversions)
		if needConversions {
			// Although the Task & its Datas passed validation, it is not actually valid because we are missing come conversions.
			// If we edited the Task right now the user might unintentionally lose some Data.
			newConversionDatas := make([]*common.Data, 0)
			for i := range ConversionsOut {
				newConversionData := common.Data{
					FirestoreID: common.GetRandString(25),
				}
				newConversionData.Init(nil)
				err = common.StructureIntoData("polyapp", "TaskSchemaUX", "XmjaaekTsoQjsFHwTBBACmQzY", &ConversionsOut[i], &newConversionData)
				if err != nil {
					return fmt.Errorf("common.StructureIntoData for ConversionsOut %v: %w", i, err)
				}
				newConversionDatas = append(newConversionDatas, &newConversionData)
			}
			// Delete the old conversion Datas to make it easier to write this code
			for i := range immediateSimpleConversionDatas {
				err = allDB.DeleteData(immediateSimpleConversionDatas[i].FirestoreID)
				if err != nil {
					return fmt.Errorf("allDB.DeleteData for immediateSimpleConversions (%v) index %v: %w", immediateSimpleConversionDatas[i].FirestoreID, i, err)
				}
			}
			editTask.ImmediateSimpleConversion = make([]string, len(newConversionDatas))
			for i := range newConversionDatas {
				err = allDB.Create(newConversionDatas[i])
				if err != nil {
					return fmt.Errorf("allDB.Create (%v) index %v: %w", newConversionDatas[i].FirestoreID, i, err)
				}
				editTask.ImmediateSimpleConversion[i] = common.CreateRef("polyapp", "TaskSchemaUX", "HCrXgiLFKYdKfhxzKYqzPIZXw", "xODjpnRoXquWhwmcRmjyTeQpz", "XmjaaekTsoQjsFHwTBBACmQzY", newConversionDatas[i].FirestoreID)
			}
			err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &editTask, data)
			if err != nil {
				return fmt.Errorf("common.StructureIntoData: %w", err)
			}
			err = allDB.UpdateData(data)
			if err != nil {
				return fmt.Errorf("allDB.Updatedata prior to redirection: %w", err)
			}
			response.NewURL = common.CreateURLFromRequest(*request)
			return nil
		}
	}

	ux, err := getUXFromFields(allFields, task, schema, common.CreateURL("polyapp", "TaskSchemaUX", "GBWKfLRqZqcEepMjDJchkIhex", "pBEGavjujYrMqIbWcgQZHJLfK", "nEwbjkzzZiJjLhUOWYBhVEdzI", editTask.ViewTaskHelpTextDataID, request.UserID, request.RoleID, "", ""))
	if err != nil {
		return fmt.Errorf("getUXFromFields: %w", err)
	}
	ux.FirestoreID = editTask.UXID
	ux.IndustryID = editTask.Industry
	ux.DomainID = editTask.Domain
	ux.SchemaID = editTask.SchemaID
	if ux.SelectFields == nil {
		ux.SelectFields = map[string]string{}
	}
	ux.IsPublic = isPublic
	ux.MetaDescription = common.String(task.HelpText)
	ux.Title = common.String(task.Name)
	if editTask.Theme != "" {
		q, err := allDB.QueryEquals(common.CollectionData, common.AddFieldPrefix("polyapp", "TaskSchemaUX", "KxZRoxrRpyECTbCudVfXKnmHB", "Name"), editTask.Theme)
		if err != nil {
			return fmt.Errorf("allDB.QueryEquals: %w", err)
		}
		ux.CSSPath = common.String(common.BlobURL(q.GetFirestoreID(), common.AddFieldPrefix("polyapp", "TaskSchemaUX", "KxZRoxrRpyECTbCudVfXKnmHB", "Bootstrap Theme")))
		themeData := q.(*common.Data)
		logoForIcon, err := allDB.ReadDataBytes(themeData.FirestoreID, common.AddFieldPrefix("polyapp", "TaskSchemaUX", "KxZRoxrRpyECTbCudVfXKnmHB", "Logo for Icon"))
		if err != nil && strings.Contains(err.Error(), "NotFound") {
			ux.IconPath = common.String("")
		} else if err != nil {
			return fmt.Errorf("allDB.ReadDataBytes for %v: %w", themeData.FirestoreID, err)
		} else if logoForIcon != nil && len(logoForIcon) > 0 {
			ux.IconPath = common.String(common.BlobURL(q.GetFirestoreID(), common.AddFieldPrefix("polyapp", "TaskSchemaUX", "KxZRoxrRpyECTbCudVfXKnmHB", "Logo for Icon")))
		} else {
			ux.IconPath = common.String("")
		}
	} else {
		ux.CSSPath = nil // important to clear out the CSS Path variable
	}
	if !newUX {
		oldUX, err := allDB.ReadUX(ux.FirestoreID)
		if err != nil && strings.Contains(err.Error(), "code = NotFound") {
			// Right now this gets hit if you are trying to re-generate a broken Task.
			// We should allow re-generation of a broken Task without throwing an error.
		} else if err != nil {
			return fmt.Errorf("ReadUX of oldUX: %w", err)
		} else if ux.IconPath == nil {
			ux.IconPath = oldUX.IconPath
			ux.Navbar = oldUX.Navbar
		}
	}

	// Validation needs access to type information which is inferred from the UX being used to edit the Fields.
	for _, thisField := range allFields {
		prefixedDataKey := common.AddFieldPrefix(editTask.Industry, editTask.Domain, editTask.SchemaID, thisField.Name)
		if thisField.Validator != "" {
			switch thisField.Validator {
			case "Not Empty (also known as a Required Field)":
				op := "!="
				var op2Constant interface{}
				switch dataTypes[prefixedDataKey] {
				case "S":
					op2Constant = ""
				case "I":
					op2Constant = int64(0)
				case "F":
					op2Constant = float64(0)
				case "B":
					op2Constant = false
				case "AS", "AB", "AI", "AF", "Bytes", "ABytes":
					op2Constant = []interface{}{}
					op = "len>0"
					//case "AS":
					//	op2Constant = []string{}
					//case "AB":
					//	op2Constant = []bool{}
					//case "AI":
					//	op2Constant = []int64{}
					//case "AF":
					//	op2Constant = []float64{}
					//case "Bytes":
					//	op2Constant = []byte{}
					//case "ABytes":
					//	op2Constant = [][]byte{}
				}
				err := task.TaskGoalAdd(prefixedDataKey, "Nonempty", "Field must not be empty", prefixedDataKey, op, "", op2Constant, thisField.Name+" is required")
				if err != nil {
					return fmt.Errorf("TaskGoalAdd for key "+prefixedDataKey+" fail: %w", err)
				}
			case "Email":
				err := task.TaskGoalAdd(prefixedDataKey, "Email", "Field must be a valid email", prefixedDataKey, "==", "", "", thisField.Name+" must be a valid email")
				if err != nil {
					return fmt.Errorf("TaskGoalAdd for key "+prefixedDataKey+" fail: %w", err)
				}
			case "URL":
				err := task.TaskGoalAdd(prefixedDataKey, "URL", "Field must be a valid URL", prefixedDataKey, "==", "", "", thisField.Name+" must be a valid URL")
				if err != nil {
					return fmt.Errorf("TaskGoalAdd for key "+prefixedDataKey+" fail: %w", err)
				}
			case "Phone Number":
				err := task.TaskGoalAdd(prefixedDataKey, "Phone Number", "Field must be a valid phone number", prefixedDataKey, "==", "", "", thisField.Name+" must be a valid phone number")
				if err != nil {
					return fmt.Errorf("TaskGoalAdd for key "+prefixedDataKey+" fail: %w", err)
				}
			case "Alphanumeric Characters Only":
				// TODO
				err := task.TaskGoalAdd(prefixedDataKey, "Alphanumeric Characters Only", "Field may only contain alphanumeric characters", prefixedDataKey, "==", "", "", thisField.Name+" may only contain alphanumeric characters")
				if err != nil {
					return fmt.Errorf("TaskGoalAdd for key "+prefixedDataKey+" fail: %w", err)
				}
			case "Alphanumeric Unicode Characters Only":
				// TODO
			case "Alpha Characters Only":
				// TODO
			case "Alpha Unicode Characters Only":
				// TODO
			case "Numeric Only":
				// TODO
			case "Numeric Unicode Only":
				// TODO
			case "Integer Only":
				// TODO
			}
		}
	}

	// Next step is to gather UX from the subtasks and place that within this larger Task.
	subTaskErrChan := make(chan error, len(editTask.Subtasks))
	subTaskUXs := make([]common.UX, len(editTask.Subtasks))
	subTaskTasks := make([]common.Task, len(editTask.Subtasks))
	subTaskSchemas := make([]common.Schema, len(editTask.Subtasks))
	subTaskDatas := make([]common.Data, len(editTask.Subtasks))
	for subTaskUXIndex, subTaskRef := range editTask.Subtasks {
		go func(subTaskRef string, subTaskUXIndex int, parentSchema *common.Schema, errChan chan error) {
			URL, err := url.Parse(subTaskRef)
			if err != nil {
				errChan <- fmt.Errorf("url.Parse: %w", err)
				return
			}
			req, err := common.CreateGETRequest(*URL)
			if err != nil {
				errChan <- fmt.Errorf("common.CreateGETRequest: %w", err)
				return
			}
			var s Subtask
			data := &common.Data{
				FirestoreID: req.DataID,
			}
			err = allDB.Read(data)
			if err != nil {
				errChan <- fmt.Errorf("allDB.ReadData: %w", err)
				return
			}
			if editTask.ExportAsJSONToFile {
				err = fileCRUD.Create(data)
				if err != nil {
					errChan <- fmt.Errorf("exportAsJSONToFile for fileCRUD.Create %v: %w", data.FirestoreID, err)
					return
				}
			}
			err = common.DataIntoStructure(data, &s)
			if err != nil {
				errChan <- fmt.Errorf("DataIntoStructure: %w", err)
				return
			}
			queryable, err := allDB.QueryEquals(common.CollectionData, common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Task ID"), s.TaskID)
			if err != nil && strings.Contains(err.Error(), "code = NotFound") {
				// Ignore Tasks with no corresponding Data
				errChan <- fmt.Errorf("allDB.QueryEquals: %w", err)
				return
			}
			if err != nil {
				errChan <- fmt.Errorf("allDB.QueryEquals %v: %w", s.TaskID, err)
				return
			}
			uxID := *queryable.(*common.Data).S[common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "UX ID")]
			schemaID := *queryable.(*common.Data).S[common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Schema ID")]
			task, err := allDB.ReadTask(s.TaskID)
			if err != nil {
				errChan <- fmt.Errorf("allDB.ReadTask: %w", err)
				return
			}
			if editTask.ExportAsJSONToFile {
				err = fileCRUD.Create(&task)
				if err != nil {
					errChan <- fmt.Errorf("exportAsJSONToFile for fileCRUD.Create %v: %w", task.FirestoreID, err)
					return
				}
			}
			ux, err := allDB.ReadUX(uxID)
			if err != nil {
				errChan <- fmt.Errorf("allDB.ReadUX: %w", err)
				return
			}
			if editTask.ExportAsJSONToFile {
				err = fileCRUD.Create(&ux)
				if err != nil {
					errChan <- fmt.Errorf("exportAsJSONToFile for fileCRUD.Create %v: %w", ux.FirestoreID, err)
					return
				}
			}
			schema, err := allDB.ReadSchema(schemaID)
			if err != nil {
				errChan <- fmt.Errorf("allDB.ReadTask: %w", err)
				return
			}
			if editTask.ExportAsJSONToFile {
				err = fileCRUD.Create(&schema)
				if err != nil {
					errChan <- fmt.Errorf("exportAsJSONToFile for fileCRUD.Create %v: %w", schema.FirestoreID, err)
					return
				}
			}

			// Remove any subtasks' HTML. If a Task has a subtask and it is added as a subtask to another Task, we don't want
			// to write to the subtask's subtask. This limit has a variety of reasons behind it, but basically it
			// exists because I want the software to be as simple as possible & recursive task nesting != simple.
			//
			// Replace the subtasks with read only links.
			buttonStartIndex := strings.LastIndex(*ux.HTML, `<button type="button" id="`)
			nestedARefStart := strings.Index(*ux.HTML, `<div class="form-group" id="_`)
			nestedRefStart := strings.Index(*ux.HTML, `<div class="mb-3 nestedRefStart`)
			nestedStart := nestedRefStart
			if nestedStart < nestedARefStart && nestedARefStart > 0 {
				nestedStart = nestedARefStart
			}
			if nestedStart > 0 && nestedStart < buttonStartIndex {
				var sb strings.Builder
				sb.Grow(len(*ux.HTML))
				sb.WriteString((*ux.HTML)[:nestedStart])
				for fieldName, fieldType := range schema.DataTypes {
					if fieldType == "Ref" {
						splitFieldName := strings.Split(fieldName, "_")
						err = gen.GenLabeledInput([]gen.LabeledInput{
							{
								HelpText:     "Embedded Task - Help Text is hidden. Click the link to open this Subtask to learn more.",
								Label:        splitFieldName[len(splitFieldName)-1],
								ID:           fieldName,
								Type:         "ref",
								InitialValue: "",
								Readonly:     true,
							},
						}, &sb)
						if err != nil {
							errChan <- fmt.Errorf("gen.GenLabeledInput: %w", err)
							return
						}
					} else if fieldType == "ARef" {
						splitFieldName := strings.Split(fieldName, "_")
						err = gen.GenListLabeledInput(gen.ListLabeledInput{
							HelpText:      "Embedded Task - Help Text is hidden. Click the link to open this Subtask to learn more.",
							Label:         splitFieldName[len(splitFieldName)-1],
							ID:            fieldName,
							InitialValues: nil,
							DefaultValue:  common.CreateURL(schema.IndustryID, schema.DomainID, task.FirestoreID, ux.FirestoreID, schema.FirestoreID, "new", "", "", "", ""),
							Type:          "ref",
							Readonly:      true,
						}, &sb)
						if err != nil {
							errChan <- fmt.Errorf("gen.GenListLabeledInput: %w", err)
							return
						}
					}
				}
				sb.WriteString((*ux.HTML)[buttonStartIndex:])
				ux.HTML = common.String(sb.String())
			}

			// remove the header and Done button from the HTML since this will be in an embedded Task
			indexToRemoveUpTo := strings.Index(*ux.HTML, `</h2>`)
			buttonStartIndex = strings.LastIndex(*ux.HTML, `<button type="button" id="`)
			buttonEndIndex := strings.LastIndex(*ux.HTML, `>Done</button>`)
			buttonEndIndex += len(`>Done</button>`)
			if indexToRemoveUpTo > 0 && buttonStartIndex > 0 && buttonEndIndex > 0 {
				var sb strings.Builder
				sb.Grow(len(*ux.HTML))
				sb.WriteString((*ux.HTML)[indexToRemoveUpTo+len(`</h2>`) : buttonStartIndex])
				sb.WriteString((*ux.HTML)[buttonEndIndex:])
				ux.HTML = common.String(sb.String())
			} else if indexToRemoveUpTo > 0 {
				var sb strings.Builder
				sb.Grow(len(*ux.HTML))
				sb.WriteString((*ux.HTML)[indexToRemoveUpTo+len(`</h2>`):])
				ux.HTML = common.String(sb.String())
			} else if buttonStartIndex > 0 && buttonEndIndex > 0 {
				var sb strings.Builder
				sb.Grow(len(*ux.HTML))
				sb.WriteString((*ux.HTML)[:buttonStartIndex])
				sb.WriteString((*ux.HTML)[buttonEndIndex:])
				ux.HTML = common.String(sb.String())
			}

			var newHTML bytes.Buffer
			// When generating subtasks, we are currently generating them not in the parentSchema's Industry and Domain, but
			// in the polyapp_TaskSchemaUX. This is DEFINITELY not what I want to do because it means that I can't split the
			// polyapp_TaskSchemaUX domain into a differet place than any of the generated websites.
			// That being said, I currently do not want to go through and find all references to polyapp_TaskSchemaUX and
			// update 100% of them to the new Custom Computer Programming Services_Website Builder industry / domain.
			if s.SingletonOrList == "singleton" {
				newHTML.WriteString(`<div class="mb-3 nestedRefStart">`) // Exact string `<div class="mb-3 nestedRefStart` is coupled to other places
				err = gen.GenNestedAccordion([]gen.NestedAccordion{{
					TitleStatic: task.Name,
					// polyappShouldBeOverwritten should be found when inserting data into the DOM during GET and overwritten
					Ref:          common.CreateRef(ux.IndustryID, ux.DomainID, s.TaskID, uxID, schemaID, "polyappShouldBeOverwritten"),
					InnerContent: *ux.HTML,
				}}, &newHTML)
				newHTML.WriteString(`</div>`)
				if err != nil {
					errChan <- fmt.Errorf("gen.GenNestedAccordion: %w", err)
					return
				}
				fieldKey := "_" + common.AddFieldPrefix(ux.IndustryID, ux.DomainID, schemaID, task.Name)
				parentSchema.DataTypes[fieldKey] = "Ref"
				parentSchema.DataKeys = append(parentSchema.DataKeys, fieldKey)
				parentSchema.DataHelpText[fieldKey] = task.HelpText
			} else if s.SingletonOrList == "section" {
				start := gen.SectionStart{
					SectionRow: 0,
					IsLastRow:  true,
				}
				gen.GenSectionStart(start, &newHTML)
				newHTML.WriteString(*ux.HTML)
				gen.GenSectionEnd(start, &newHTML)
				fieldKey := "_" + common.AddFieldPrefix(ux.IndustryID, ux.DomainID, schemaID, task.Name)
				parentSchema.DataTypes[fieldKey] = "Ref"
				parentSchema.DataKeys = append(parentSchema.DataKeys, fieldKey)
				parentSchema.DataHelpText[fieldKey] = task.HelpText
			} else {
				err = gen.GenListAccordion(gen.ListAccordion{
					ID:                         "_" + common.AddFieldPrefix(ux.IndustryID, ux.DomainID, schemaID, task.Name),
					Label:                      task.Name,
					HelpText:                   task.HelpText,
					DisplayNoneRenderedSubTask: *ux.HTML,
					DisplayNoneTitleField:      schema.DataKeys[0],
					DisplayNoneSubTaskRef:      common.CreateRef(ux.IndustryID, ux.DomainID, s.TaskID, uxID, schemaID, "polyappShouldBeOverwritten"),
				}, &newHTML)
				if err != nil {
					errChan <- fmt.Errorf("gen.GenListAccordion: %w", err)
					return
				}
				fieldKey := "_" + common.AddFieldPrefix(ux.IndustryID, ux.DomainID, schemaID, task.Name)
				parentSchema.DataTypes[fieldKey] = "ARef"
				parentSchema.DataKeys = append(parentSchema.DataKeys, fieldKey)
				parentSchema.DataHelpText[fieldKey] = task.HelpText
			}
			ux.HTML = common.String(newHTML.String())
			subTaskUXs[subTaskUXIndex] = ux
			subTaskTasks[subTaskUXIndex] = task
			subTaskSchemas[subTaskUXIndex] = schema
			subTaskDatas[subTaskUXIndex] = *data
			errChan <- nil
		}(subTaskRef, subTaskUXIndex, schema, subTaskErrChan)
	}
	combinedErr = ""

	layoutCol2 := false
	if data.FirestoreID == "hzLDZfqVrNejUysEMrqSUukEJ" || data.FirestoreID == "LVLakQwsHlcqfZNLEhcIRvxjN" {
		// Edit a Website
		layoutCol2 = true
	}

	var combinedHTML bytes.Buffer
	if layoutCol2 {
		combinedHTML.WriteString(`<div class="row">`)
		combinedHTML.WriteString(`<div class="col-md">`)
	}
	combinedHTML.WriteString(*ux.HTML)
	for range editTask.Subtasks {
		subTaskErr := <-subTaskErrChan
		if subTaskErr != nil {
			combinedErr += subTaskErr.Error() + "; "
			continue
		}
	}
	if combinedErr != "" {
		return errors.New("processing subtasks: " + combinedErr)
	}
	for i := range editTask.Subtasks {
		if i == (len(editTask.Subtasks)-1) && layoutCol2 {
			combinedHTML.WriteString(`</div>`)
			combinedHTML.WriteString(`<div class="col-md">`)
		}
		if subTaskUXs[i].HTML != nil {
			combinedHTML.WriteString(*subTaskUXs[i].HTML)
		}
		if i == (len(editTask.Subtasks)-1) && layoutCol2 {
			combinedHTML.WriteString(`</div>`)
		}
	}
	if layoutCol2 {
		if len(editTask.Subtasks) == 0 {
			combinedHTML.WriteString(`</div>`)
		}
		combinedHTML.WriteString(`</div>`)
	}

	doneID := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, "Done")
	if schema.DataTypes[doneID] == "" {
		// there is no existing doneID, so that means no Button was created in the Form which was checked as the Done button.
		err = gen.GenTextButton(gen.TextButton{
			ID:        doneID,
			Text:      "Done",
			Styling:   "success",
			Recaptcha: false, // This should be a switch in the Task Designer. Previous value: ux.IsPublic
		}, &combinedHTML)
		if err != nil {
			return fmt.Errorf("GenTextButton for Done button: %w", err)
		}
		schema.DataTypes[doneID] = "B"
		schema.DataHelpText[doneID] = "Press when finished with the Task"
		schema.DataKeys = append(schema.DataKeys, doneID)
	}

	// Ensure we try to do full authentication when this page loads.
	combinedHTML.WriteString(`<div id="polyappJSNeedFullAuthCanary"></div>`)
	ux.HTML = common.String(combinedHTML.String())

	errChan := make(chan error, 3)

	go func() {
		if !newTask {
			err := allDB.DeleteTask(task.FirestoreID)
			if err != nil {
				errChan <- fmt.Errorf("DeleteTask ("+task.FirestoreID+"): %w", err)
				return
			}
		}
		err := allDB.CreateTask(task)
		if err != nil {
			errChan <- fmt.Errorf("CreateTask ("+task.FirestoreID+"): %w", err)
			return
		}
		if editTask.ExportAsJSONToFile {
			err = fileCRUD.Create(task)
			if err != nil {
				errChan <- fmt.Errorf("exportAsJSONToFile for fileCRUD.Create %v: %w", task.FirestoreID, err)
				return
			}
		}
		errChan <- nil
	}()
	go func() {
		if !newSchema {
			// Must delete the schema, UX and Task first because if you don't and the Industry / Schema changes,
			// you would end up with fields like a_b_c_d when the new Industry / Domain are "q" and "z".
			// For the uninitiated, Industry / Domain "q" and "z" would imply the field should be q_z_c_d.
			err := allDB.DeleteSchema(schema.FirestoreID)
			if err != nil {
				errChan <- fmt.Errorf("UpdateSchema ("+schema.FirestoreID+"): %w", err)
				return
			}
		}
		err := allDB.CreateSchema(schema)
		if err != nil {
			errChan <- fmt.Errorf("CreateSchema ("+schema.FirestoreID+"): %w", err)
			return
		}
		if editTask.ExportAsJSONToFile {
			err = fileCRUD.Create(schema)
			if err != nil {
				errChan <- fmt.Errorf("exportAsJSONToFile for fileCRUD.Create %v: %w", schema.FirestoreID, err)
				return
			}
		}
		errChan <- nil
	}()
	go func() {
		if !newUX {
			err := allDB.DeleteUX(ux.FirestoreID)
			if err != nil {
				errChan <- fmt.Errorf("DeleteUX ("+ux.FirestoreID+"): %w", err)
				return
			}
		}
		err := allDB.CreateUX(ux)
		if err != nil {
			errChan <- fmt.Errorf("CreateUx ("+ux.FirestoreID+"): %w", err)
			return
		}
		if editTask.ExportAsJSONToFile {
			err = fileCRUD.Create(ux)
			if err != nil {
				errChan <- fmt.Errorf("exportAsJSONToFile for fileCRUD.Create %v: %w", ux.FirestoreID, err)
				return
			}
		}
		errChan <- nil
	}()
	go func() {
		if !newViewTaskHelpText {
			err := allDB.DeleteData(editTask.ViewTaskHelpTextDataID)
			if err != nil {
				errChan <- fmt.Errorf("DeleteData (%v): %w", editTask.ViewTaskHelpTextDataID, err)
				return
			}
		}
		vd := common.Data{}
		_ = vd.Init(nil)
		vd.FirestoreID = editTask.ViewTaskHelpTextDataID
		vd.IndustryID = "polyapp"
		vd.DomainID = "TaskSchemaUX"
		vd.SchemaID = "nEwbjkzzZiJjLhUOWYBhVEdzI"
		vd.S[common.AddFieldPrefix(vd.IndustryID, vd.DomainID, vd.SchemaID, "Head Task ID")] = common.String(editTask.TaskID)
		err := allDB.CreateData(&vd)
		if err != nil {
			errChan <- fmt.Errorf("CreateData (%v): %w", vd.FirestoreID, err)
			return
		}
		if editTask.ExportAsJSONToFile {
			err = fileCRUD.CreateData(&vd)
			if err != nil {
				errChan <- fmt.Errorf("exportAsJSONToFile for fileCRUD.Create %v: %w", vd.FirestoreID, err)
				return
			}
		}
		errChan <- nil
	}()

	combinedErr = ""
	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &editTask, data)
	if err != nil {
		combinedErr += "StructureIntoData: " + err.Error() + "; "
	}

	for i := 0; i < 4; i++ {
		err := <-errChan
		if err != nil {
			combinedErr += err.Error() + "; "
		}
	}

	// I think we should only do conversions if we're certain it's safe to do so
	if combinedErr == "" {
		for i := range immediateSimpleConversionDatas {
			// I want to run these conversions in parallel but the code isn't written to support that right now.
			// TODO write conversion code which can handle multiple changes to one Data and refactor this to use that code.
			err = ActionImmediateSimpleConversion(&immediateSimpleConversionDatas[i], nil, nil)
			if err != nil {
				combinedErr += fmt.Sprintf("; ActionImmediateSimpleConversion %v: %v", i, err.Error())
			}
		}
		// There's no need to re-run a successful conversion the next time the Task is edited. Doing so will merely slow
		// down the code with little to no benefit. I guess the conversion could have failed & re-running would fix that,
		// but then the Task won't navigate away and the user will have a chance fix the problem by finishing the conversion by clicking "Done".
		for i := range immediateSimpleConversionDatas {
			err = allDB.DeleteData(immediateSimpleConversionDatas[i].FirestoreID)
			if err != nil {
				// Don't care; the parent Task won't reference it anyway
			}
		}
		data.ARef[common.AddFieldPrefix("polyapp", "TaskSchemaUX", "XmjaaekTsoQjsFHwTBBACmQzY", "Immediate Simple Conversion")] = nil
	}

	// Necessary or else new Tasks will not have their IDs populated
	// Must do this after CreateSchema finishes or else UpdateData will fail since the Schema will not exist in the DB.
	err = allDB.UpdateData(data)
	if err != nil {
		combinedErr += "UpdateData (" + data.FirestoreID + "): " + err.Error() + "; "
	}

	if editTask.ExportAsJSONToFile {
		err = fileCRUD.CreateData(data)
		if err != nil {
			combinedErr += "fileCRUD.CreateData (" + data.FirestoreID + "): " + err.Error() + "; "
		}
	}

	// TODO Handle removing a ARef entry. When this happens we should Delete the Data document from the database and
	// also Delete it from the file storage. We know if a Data document has been deleted by querying for all Fields and
	// Subtasks associated with this Task.
	if combinedErr != "" {
		return errors.New("saving Task, Schema, UX, and conversions combinedErr: " + combinedErr)
	}

	request.FinishURL = common.CreateURL(task.IndustryID, task.DomainID, task.FirestoreID, ux.FirestoreID, schema.FirestoreID,
		"", request.UserID, request.RoleID, "", "")
	return nil
}

// getUXFromFields takes an array of Field and creates a HTML string usable in the *common.UX object.
func getUXFromFields(fields []Field, task *common.Task, schema *common.Schema, taskHelpURL string) (*common.UX, error) {
	var err error
	knownFieldNames := make(map[string]bool)
	var allContent bytes.Buffer
	// Must write <h2> because if this is used as a subtask we remove everything before the first </h2> on the page
	// when we "remove the header and Done button"
	allContent.WriteString(`<h2>`)
	allContent.WriteString(task.Name)
	allContent.WriteString(`<h6 class="text-muted"><a href="` + taskHelpURL + `">View Help Page</a></h6>`)
	allContent.WriteString(`</h2>`)
	for _, field := range fields {
		if knownFieldNames[field.Name] {
			return nil, errors.New("field.Name was a duplicate: " + field.Name)
		}
		knownFieldNames[field.Name] = true

		if field.ReadOnly {
			if task.FieldSecurity == nil {
				task.FieldSecurity = make(map[string]*common.FieldSecurityOptions)
			}
			task.FieldSecurity[common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)] = &common.FieldSecurityOptions{
				Readonly: true,
			}
		}
		err = writeFieldToBuffer(&allContent, field, schema, task.BotStaticData)
		if err != nil {
			return nil, fmt.Errorf("writeFieldToBuffer: %w", err)
		}
	}
	return &common.UX{
		HTML:            common.String(allContent.String()),
		SelectFields:    map[string]string{},
		Title:           common.String(task.Name),
		MetaDescription: common.String(task.HelpText),
	}, nil
}

func writeFieldToBuffer(allContent *bytes.Buffer, field Field, schema *common.Schema, botStaticData map[string]string) error {
	var err error
	var oldAllContent bytes.Buffer
	if field.Hidden {
		oldAllContent = *allContent
		allContent = bytes.NewBuffer([]byte{})
	}

	switch field.UserInterface {
	case "single line input", "text":
		// https://stackoverflow.com/a/30976223/12713117
		// Bascially the "New Password" field is autofilling. We then autosave the password to the server, where it is stored.
		// Obviously that is bad.
		autocompleteOff := false
		if strings.Contains(strings.ToLower(field.Name), "password") {
			autocompleteOff = true
		}
		err = gen.GenLabeledInput([]gen.LabeledInput{
			{
				Label:           field.Name,
				HelpText:        field.HelpText,
				ID:              common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name),
				Type:            "text",
				Readonly:        field.ReadOnly,
				AutocompleteOff: autocompleteOff,
			},
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenLabeledInput: %w", err)
		}
	case "multiple line input", "text area", "textarea":
		err = gen.GenLabeledInput([]gen.LabeledInput{
			{
				Label:    field.Name,
				HelpText: field.HelpText,
				ID:       common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name),
				Type:     "textarea",
				Readonly: field.ReadOnly,
			},
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenLabeledInput: %w", err)
		}
	case "full-fledged text editor", "summernote", "rich text":
		err = gen.GenSummernote(gen.Summernote{
			Label:    field.Name,
			HelpText: field.HelpText,
			ID:       common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name),
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenSummernote: %w", err)
		}
	case "select between static options", "select":
		selectOptions := make([]gen.SelectOption, len(field.SelectOptions)+1)
		selectOptions[0] = gen.SelectOption{
			Value:    "",
			Selected: true,
		}
		for i, o := range field.SelectOptions {
			selectOptions[i+1] = gen.SelectOption{
				Value: o,
			}
		}
		err = gen.GenSelectInput(gen.SelectInput{
			HelpText:      field.HelpText,
			ID:            common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name),
			Label:         field.Name,
			SelectOptions: selectOptions,
			MultiSelect:   false,
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenSelectInput: %w", err)
		}
	case "multiple select":
		selectOptions := make([]gen.SelectOption, len(field.SelectOptions))
		for i, o := range field.SelectOptions {
			selectOptions[i] = gen.SelectOption{
				Value: o,
			}
		}
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenSelectInput(gen.SelectInput{
			HelpText:      field.HelpText,
			ID:            id,
			Label:         field.Name,
			SelectOptions: selectOptions,
			MultiSelect:   true,
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenSelectInput: %w", err)
		}
		schema.DataTypes[id] = "AS"
	case "image upload":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenImageUpload(gen.ImageUpload{
			Label:    field.Name,
			ID:       id,
			HelpText: field.HelpText,
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenImageUpload: %w", err)
		}
		schema.DataTypes[id] = "Bytes"
	case "multiple image upload":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenImageUpload(gen.ImageUpload{
			Label:         field.Name,
			ID:            id,
			HelpText:      field.HelpText,
			AllowMultiple: true,
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenImageUpload: %w", err)
		}
		schema.DataTypes[id] = "Bytes"
	case "audio upload":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenAudioUpload(gen.AudioUpload{
			Label:    field.Name,
			ID:       id,
			HelpText: field.HelpText,
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenImageUpload: %w", err)
		}
		schema.DataTypes[id] = "Bytes"
	case "upload", "file upload", "attachment":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenUpload(gen.Upload{
			Label:    field.Name,
			ID:       id,
			HelpText: field.HelpText,
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenUpload: %w", err)
		}
		schema.DataTypes[id] = "Bytes"
	case "checkbox":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenLabeledInput([]gen.LabeledInput{
			{
				Label:    field.Name,
				HelpText: field.HelpText,
				ID:       id,
				Type:     "checkbox",
				Readonly: field.ReadOnly,
			},
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenLabeledInput for checkbox: %w", err)
		}
		schema.DataTypes[id] = "B"
	case "number":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenLabeledInput([]gen.LabeledInput{
			{
				Label:    field.Name,
				HelpText: field.HelpText,
				ID:       id,
				Type:     "number",
				Readonly: field.ReadOnly,
			},
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenLabeledInput for number: %w", err)
		}
		schema.DataTypes[id] = "F"
	case "time", "date time":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenTime(gen.Time{
			ID:           id,
			Label:        field.Name,
			HelpText:     field.HelpText,
			InitialValue: 0,
			Readonly:     field.ReadOnly,
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenTime: %w", err)
		}
		schema.DataTypes[id] = "I"
	case "date":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenTime(gen.Time{
			ID:           id,
			Label:        field.Name,
			HelpText:     field.HelpText,
			InitialValue: 0,
			Readonly:     field.ReadOnly,
			Format:       "L",
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenTime: %w", err)
		}
		schema.DataTypes[id] = "I"
	case "time only":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenTime(gen.Time{
			ID:           id,
			Label:        field.Name,
			HelpText:     field.HelpText,
			InitialValue: 0,
			Readonly:     field.ReadOnly,
			Format:       "LT",
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenTime: %w", err)
		}
		schema.DataTypes[id] = "I"
	case "time (init from current time)", "date time (init from current time)":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenTime(gen.Time{
			ID:                id,
			Label:             field.Name,
			HelpText:          field.HelpText,
			InitialValue:      0,
			Readonly:          field.ReadOnly,
			InitializeWithNow: true,
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenTime: %w", err)
		}
		schema.DataTypes[id] = "I"
	case "date (init from current time)":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenTime(gen.Time{
			ID:                id,
			Label:             field.Name,
			HelpText:          field.HelpText,
			InitialValue:      0,
			Readonly:          field.ReadOnly,
			Format:            "L",
			InitializeWithNow: true,
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenTime: %w", err)
		}
		schema.DataTypes[id] = "I"
	case "time only (init from current time)":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenTime(gen.Time{
			ID:                id,
			Label:             field.Name,
			HelpText:          field.HelpText,
			InitialValue:      0,
			Readonly:          field.ReadOnly,
			Format:            "LT",
			InitializeWithNow: true,
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenTime: %w", err)
		}
		schema.DataTypes[id] = "I"
	case "list of single line text inputs":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenListLabeledInput(gen.ListLabeledInput{
			HelpText: field.HelpText,
			Label:    field.Name,
			ID:       id,
			Type:     "text",
			Readonly: field.ReadOnly,
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenListLabeledInput for list of single line text inputs: %w", err)
		}
		schema.DataTypes[id] = "AS"
	case "list of numbers":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenListLabeledInput(gen.ListLabeledInput{
			HelpText: field.HelpText,
			Label:    field.Name,
			ID:       id,
			Type:     "number",
			Readonly: field.ReadOnly,
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenListLabeledInput for list of numbers: %w", err)
		}
		schema.DataTypes[id] = "AF"
	case "list of checkboxes":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenListLabeledInput(gen.ListLabeledInput{
			HelpText: field.HelpText,
			Label:    field.Name,
			ID:       id,
			Type:     "checkbox",
			Readonly: field.ReadOnly,
		}, allContent)
		if err != nil {
			return fmt.Errorf("genListLabeledInput for list of checkboxes: %w", err)
		}
		schema.DataTypes[id] = "AB"
	case "button":
		var id string
		if field.UseAsDoneButton {
			// remove the button which would have been at field.Name
			unusedID := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
			id = common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, "Done")
			schema.DataTypes[id] = "B"
			schema.DataHelpText[id] = schema.DataHelpText[unusedID]
			schema.DataKeys = append(schema.DataKeys, id)
			delete(schema.DataTypes, unusedID)
			delete(schema.DataHelpText, unusedID)
			for i, v := range schema.DataKeys {
				if v == unusedID {
					schema.DataKeys[i] = schema.DataKeys[len(schema.DataKeys)-1]
					schema.DataKeys[len(schema.DataKeys)-1] = ""
					schema.DataKeys = schema.DataKeys[:len(schema.DataKeys)-1]
					break
				}
			}
		} else {
			id = common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
			schema.DataTypes[id] = "B"
		}
		if !field.ReadOnly {
			// this is pretty hidden behavior, but basically I need a way to avoid include a Done button in the UI for
			// a limited number of Tasks.
			err = gen.GenTextButton(gen.TextButton{
				ID:        id,
				Text:      field.Name,
				Styling:   "primary",
				Recaptcha: false, // previous value: schema.IsPublic,
			}, allContent)
			if err != nil {
				return fmt.Errorf("GenTextButton: %w", err)
			}
		}
	case "image button":
		var id string
		if field.UseAsDoneButton {
			// remove the button which would have been at field.Name
			unusedID := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
			id = common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, "Done")
			schema.DataTypes[id] = "B"
			schema.DataHelpText[id] = schema.DataHelpText[unusedID]
			schema.DataKeys = append(schema.DataKeys, id)
			delete(schema.DataTypes, unusedID)
			delete(schema.DataHelpText, unusedID)
			for i, v := range schema.DataKeys {
				if v == unusedID {
					schema.DataKeys[i] = schema.DataKeys[len(schema.DataKeys)-1]
					schema.DataKeys[len(schema.DataKeys)-1] = ""
					schema.DataKeys = schema.DataKeys[:len(schema.DataKeys)-1]
					break
				}
			}
		} else {
			id = common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
			schema.DataTypes[id] = "B"
		}
		iconButton := gen.IconButton{
			ID:        id,
			Text:      "",
			Inline:    false,
			AltText:   field.HelpText,
			ImagePath: "/assets/giraffe192.jpg",
		}
		for _, so := range field.SelectOptions {
			if so == "inline" {
				iconButton.Inline = true
			} else if strings.HasPrefix(so, "image path ") {
				iconButton.ImagePath = strings.TrimPrefix(so, "image path ")
			} else if strings.HasPrefix(so, "text ") {
				iconButton.Text = strings.TrimPrefix(so, "text ")
			}
		}
		err = gen.GenIconButton(iconButton, allContent)
		if err != nil {
			return fmt.Errorf("GenTextButton: %w", err)
		}
	case "header":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenStaticContent(gen.StaticContent{
			Content: field.Name,
			Tag:     "h1",
		}, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenStaticContent for header: %w", err)
		}
		delete(schema.DataTypes, id)
		delete(schema.DataHelpText, id)
		for i, v := range schema.DataKeys {
			if v == id {
				schema.DataKeys[i] = schema.DataKeys[len(schema.DataKeys)-1]
				schema.DataKeys[len(schema.DataKeys)-1] = ""
				schema.DataKeys = schema.DataKeys[:len(schema.DataKeys)-1]
				break
			}
		}
	case "display help text":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenStaticContent(gen.StaticContent{
			Content: field.Name,
			Tag:     "h3",
		}, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenStaticContent for header: %w", err)
		}
		markedUpHelpText := strings.ReplaceAll(field.HelpText, "\n", "<br>")
		allContent.WriteString(`<p>` + markedUpHelpText + `</p>`)
		if err != nil {
			return fmt.Errorf("gen.GenStaticContent for body: %w", err)
		}
		delete(schema.DataTypes, id)
		delete(schema.DataHelpText, id)
		for i, v := range schema.DataKeys {
			if v == id {
				schema.DataKeys[i] = schema.DataKeys[len(schema.DataKeys)-1]
				schema.DataKeys[len(schema.DataKeys)-1] = ""
				schema.DataKeys = schema.DataKeys[:len(schema.DataKeys)-1]
				break
			}
		}
	case "image":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		image := gen.Image{
			ID:      "a" + common.GetRandString(20),
			URL:     "/assets/giraffe192.jpg",
			AltText: "Polyapp's Logo, a big, multi-colored giraffe.",
			Width:   "",
			Height:  "",
			Caption: "",
			Layout:  "Show Title and Caption on Bottom on Hover",
		}
		for _, so := range field.SelectOptions {
			if strings.HasPrefix(so, "URL ") {
				image.URL = strings.TrimPrefix(so, "URL ")
			}
			if strings.HasPrefix(so, "ALT ") {
				image.AltText = strings.TrimPrefix(so, "ALT ")
			}
		}
		err = gen.GenImage(image, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenStaticContent for image: %w", err)
		}
		delete(schema.DataTypes, id)
		delete(schema.DataHelpText, id)
		for i, v := range schema.DataKeys {
			if v == id {
				schema.DataKeys[i] = schema.DataKeys[len(schema.DataKeys)-1]
				schema.DataKeys[len(schema.DataKeys)-1] = ""
				schema.DataKeys = schema.DataKeys[:len(schema.DataKeys)-1]
				break
			}
		}
	case "color picker":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenLabeledInput([]gen.LabeledInput{
			{
				Label:    field.Name,
				HelpText: field.HelpText,
				ID:       id,
				Type:     "color",
				Readonly: field.ReadOnly,
			},
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenLabeledInput for number: %w", err)
		}
	case "iFrame":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		iframe := gen.IFrame{
			ID:     id,
			Title:  field.Name,
			Source: "/demo/polyappbuilder.com/404.html",
		}
		for _, so := range field.SelectOptions {
			if strings.HasPrefix(so, "src ") {
				iframe.Source = strings.TrimPrefix(so, "src ")
			}
		}
		err = gen.GenIFrame(iframe, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenIFrame: %w", err)
		}
	case "Side Navigation":
		sideNav := gen.WorkflowNavigation{
			Title:          field.Name,
			NavigationRefs: make([]gen.NavigationRef, 0),
			Theme:          "light", // TODO make this variable based on the provided template UX
		}
		for _, so := range field.SelectOptions {
			sideNav.NavigationRefs = append(sideNav.NavigationRefs, makeNavigationRef(so))
		}
		err = gen.GenWorkflowNavigation(sideNav, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenWorkflowNavigation: %w", err)
		}
	case "Table":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		table := gen.Table{
			ID:       id,
			Industry: schema.IndustryID,
			Domain:   schema.DomainID,
			Schema:   schema.SchemaID,
			Columns:  make([]gen.Column, 0),
		}
		for _, so := range field.SelectOptions {
			if strings.HasPrefix(so, "col ") {
				c := gen.Column{
					ID: url.PathEscape(strings.TrimPrefix(so, "col ")),
				}
				splitColID := strings.Split(c.ID, "_")
				if len(splitColID) < 1 {
					return errors.New("col was set in Table but it did not have a length > 0 or did not include _")
				}
				c.Header, _ = url.PathUnescape(splitColID[len(splitColID)-1])
				table.Columns = append(table.Columns, c)
			} else if strings.HasPrefix(so, "industry ") {
				table.Industry = strings.TrimPrefix(so, "industry ")
			} else if strings.HasPrefix(so, "domain ") {
				table.Domain = strings.TrimPrefix(so, "domain ")
			} else if strings.HasPrefix(so, "schema ") {
				table.Schema = strings.TrimPrefix(so, "schema ")
			} else if strings.HasPrefix(so, "rowselectURL ") {
				botStaticData["Row Select URL"] = strings.TrimPrefix(so, "rowselectURL ")
			} else if strings.HasPrefix(so, "createURL ") {
				botStaticData["Create URL"] = strings.TrimPrefix(so, "createURL ")
			}
		}
		if len(table.Columns) < 1 {
			table.Columns = append(table.Columns, gen.Column{Header: "Done", ID: url.PathEscape(common.AddFieldPrefix(table.Industry, table.Domain, table.Schema, "Done"))})
		}
		err = gen.GenTable(table, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenTable: %w", err)
		}
		schema.DataTypes[id] = "Ref"
	case "discussion or review thread":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenDiscussionThread(gen.DiscussionThread{
			ID: id,
		}, allContent)
		if err != nil {
			return fmt.Errorf("GenSummernote: %w", err)
		}
		schema.DataTypes[id] = "AS"
	case "Select List of Data IDs":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		selectID := gen.ListSelectID{
			Label:      field.Name,
			HelpText:   field.HelpText,
			ID:         id,
			Collection: common.CollectionData,
			Field:      common.PolyappFirestoreID,
			Readonly:   field.ReadOnly,
		}
		// Allow someone to set a Field to choose via adding a SelectOption.
		// TODO rename "SelectOptions" to "Options" and document this behavior.
		for _, option := range field.SelectOptions {
			splitOption := strings.Split(option, " ")
			if len(splitOption) >= 2 && splitOption[0] == "Field" {
				joinedFieldName := strings.Join(splitOption[1:], " ")
				if len(strings.Split(joinedFieldName, "_")) == 4 {
					// industry_domain_schema_field name
					selectID.Field = joinedFieldName
				}
			}
		}
		err = gen.GenListSelectID(selectID, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenListSelectID: %w", err)
		}
		schema.DataTypes[id] = "AS"
	case "Select Data ID":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		selectID := gen.SelectID{
			Label:             field.Name,
			HelpText:          field.HelpText,
			ID:                id,
			InitialHumanValue: "",
			InitialIDValue:    "",
			Collection:        common.CollectionData,
			Field:             common.PolyappFirestoreID,
			Readonly:          field.ReadOnly,
		}
		// Allow someone to set a Field to choose via adding a SelectOption.
		// TODO rename "SelectOptions" to "Options" and document this behavior.
		for _, option := range field.SelectOptions {
			splitOption := strings.Split(option, " ")
			if len(splitOption) >= 2 && splitOption[0] == "Field" {
				joinedFieldName := strings.Join(splitOption[1:], " ")
				if len(strings.Split(joinedFieldName, "_")) == 4 {
					// industry_domain_schema_field name
					selectID.Field = joinedFieldName
				}
			}
		}
		err = gen.GenSelectID(selectID, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenSelectID: %w", err)
		}
	case "Select Schema ID":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		selectID := gen.SelectID{
			Label:             field.Name,
			HelpText:          field.HelpText,
			ID:                id,
			InitialHumanValue: "",
			InitialIDValue:    "",
			Collection:        common.CollectionSchema,
			Field:             "Name",
			Readonly:          field.ReadOnly,
		}
		err = gen.GenSelectID(selectID, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenSelectID: %w", err)
		}
	case "Select Task ID":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		selectID := gen.SelectID{
			Label:             field.Name,
			HelpText:          field.HelpText,
			ID:                id,
			InitialHumanValue: "",
			InitialIDValue:    "",
			Collection:        common.CollectionTask,
			Field:             "Name",
			Readonly:          field.ReadOnly,
		}
		err = gen.GenSelectID(selectID, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenSelectID: %w", err)
		}
	case "Select UX ID":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		selectID := gen.SelectID{
			Label:             field.Name,
			HelpText:          field.HelpText,
			ID:                id,
			InitialHumanValue: "",
			InitialIDValue:    "",
			Collection:        common.CollectionUX,
			Field:             "Title",
			Readonly:          field.ReadOnly,
		}
		err = gen.GenSelectID(selectID, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenSelectID: %w", err)
		}
	case "Select User ID":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		selectID := gen.SelectID{
			Label:             field.Name,
			HelpText:          field.HelpText,
			ID:                id,
			InitialHumanValue: "",
			InitialIDValue:    "",
			Collection:        common.CollectionUser,
			Field:             "FullName",
			Readonly:          field.ReadOnly,
		}
		err = gen.GenSelectID(selectID, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenSelectID: %w", err)
		}
	case "Select Role ID":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		selectID := gen.SelectID{
			Label:             field.Name,
			HelpText:          field.HelpText,
			ID:                id,
			InitialHumanValue: "",
			InitialIDValue:    "",
			Collection:        common.CollectionRole,
			Field:             "Name",
			Readonly:          field.ReadOnly,
		}
		err = gen.GenSelectID(selectID, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenSelectID: %w", err)
		}
	case "Select Bot ID":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		selectID := gen.SelectID{
			Label:             field.Name,
			HelpText:          field.HelpText,
			ID:                id,
			InitialHumanValue: "",
			InitialIDValue:    "",
			Collection:        common.CollectionBot,
			Field:             "Name",
			Readonly:          field.ReadOnly,
		}
		err = gen.GenSelectID(selectID, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenSelectID: %w", err)
		}
	case "Select Action ID":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		selectID := gen.SelectID{
			Label:             field.Name,
			HelpText:          field.HelpText,
			ID:                id,
			InitialHumanValue: "",
			InitialIDValue:    "",
			Collection:        common.CollectionAction,
			Field:             "Name",
			Readonly:          field.ReadOnly,
		}
		err = gen.GenSelectID(selectID, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenSelectID: %w", err)
		}
	case "Geolocation":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		err = gen.GenGeolocationAPI(gen.GeolocationAPI{
			ID:       id,
			Label:    field.Name,
			HelpText: field.HelpText,
		}, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenGeolocationAPI: %w", err)
		}
	case "Hidden Text", "Hidden Ref":
	case "Hidden List of Text", "Hidden List of Refs":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		schema.DataTypes[id] = "AS"
	case "Hidden Integer":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		schema.DataTypes[id] = "I"
	case "Hidden Float", "Hidden Number":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		schema.DataTypes[id] = "F"
	case "Hidden List of Integers":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		schema.DataTypes[id] = "AI"
	case "Hidden List of Floats", "Hidden List of Numbers":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		schema.DataTypes[id] = "AF"
	case "Hidden Bytes":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		schema.DataTypes[id] = "Bytes"
	case "Hidden List of Bytes":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		schema.DataTypes[id] = "ABytes"
	case "Hidden Boolean", "Hidden Button":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		schema.DataTypes[id] = "B"
		if field.UseAsDoneButton {
			// remove the button which would have been at field.Name
			unusedID := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
			id = common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, "Done")
			schema.DataTypes[id] = "B"
			schema.DataHelpText[id] = schema.DataHelpText[unusedID]
			schema.DataKeys = append(schema.DataKeys, id)
			delete(schema.DataTypes, unusedID)
			delete(schema.DataHelpText, unusedID)
			for i, v := range schema.DataKeys {
				if v == unusedID {
					schema.DataKeys[i] = schema.DataKeys[len(schema.DataKeys)-1]
					schema.DataKeys[len(schema.DataKeys)-1] = ""
					schema.DataKeys = schema.DataKeys[:len(schema.DataKeys)-1]
					break
				}
			}
		}
	case "Full Text Search Task", "Full Text Search Schema", "Full Text Search UX":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		fullTextSearch := gen.FullTextSearch{
			ID:       id,
			Label:    field.Name,
			HelpText: field.HelpText,
			Readonly: field.ReadOnly,
		}
		if strings.Contains(field.UserInterface, "Task") {
			fullTextSearch.Collection = common.CollectionTask
		} else if strings.Contains(field.UserInterface, "Schema") {
			fullTextSearch.Collection = common.CollectionSchema
		} else if strings.Contains(field.UserInterface, "UX") {
			fullTextSearch.Collection = common.CollectionUX
		} else {
			return errors.New("fullTextSearch was not for Task, Schema, or UX, which is unhandled")
		}
		err = gen.GenFullTextSearch(fullTextSearch, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenFullTextSearch: %w", err)
		}
	case "Address":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		apiKey := ""
		for _, v := range field.SelectOptions {
			if strings.HasPrefix(v, "APIKey ") && len(v) > len("APIKey ") {
				apiKey = strings.Split(v, " ")[1]
			}
		}
		placeAutocomplete := gen.PlaceAutocomplete{
			Label:    field.Name,
			HelpText: field.HelpText,
			ID:       id,
			Readonly: field.ReadOnly,
			APIKey:   apiKey,
		}
		err = gen.GenPlaceAutocomplete(placeAutocomplete, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenPlaceAutocomplete: %w", err)
		}
	case "Predict Float":
		id := common.AddFieldPrefix(schema.IndustryID, schema.DomainID, schema.FirestoreID, field.Name)
		predictFloat := gen.PredictFloat{
			ID:    id,
			Title: field.Name,
		}
		err = gen.GenPredictFloat(predictFloat, allContent)
		if err != nil {
			return fmt.Errorf("gen.GenPredictFloat: %w", err)
		}
		field.ReadOnly = true
		schema.DataTypes[id] = "F"
	default:
		return errors.New("field did not have a valid UserInterface value: " + field.UserInterface)
	}

	if field.Hidden {
		allContent = &oldAllContent
	}

	return nil
}

var (
	regeneratingTasksMux sync.Mutex
	tasksRegeneratedMux  sync.Mutex
	tasksRegenerated     map[string]bool
)

// ActionReCreateAllTasks Other names: "Re-Generate All Tasks" or "Generate All Tasks". This Task has a Bot which runs a
// Done which loops over all Tasks in the system, finds their associated Datas (if available) and re-runs "Edit Task" on them.
//
// This is an administration tool used when the "Edit Task" Task or its Bot is updated and you want to re-generate all
// of your Tasks to take advantage of that update.
//
// Because this Task affects so much of the system, you must have total system access - Industry Polyapp and Domain
// Polyapp and EditTask on that line - access to run this code.
func ActionReCreateAllTasks(request *common.POSTRequest) error {
	regeneratingTasksMux.Lock()
	defer regeneratingTasksMux.Unlock()
	tasksRegenerated = make(map[string]bool)
	if request.UserCache == nil {
		user, err := allDB.ReadUser(request.UserID)
		if err != nil {
			return fmt.Errorf("allDB.Readuser: %w", err)
		}
		request.UserCache = &user
	}
	hasAccess := false
	for _, roleID := range request.UserCache.Roles {
		role, err := allDB.ReadRole(roleID)
		if err != nil {
			return fmt.Errorf("allDB.ReadRole: %w", err)
		}
		hasAccess, err = common.RoleHasAccessKeyValues(&role, "polyapp", "polyapp", "", "EditTask", "true")
		if hasAccess {
			break
		}
	}
	if !hasAccess {
		return fmt.Errorf("User %v did not have access to EditTask for industry %v domain %v schema %v", request.UserID, "polyapp", "polyapp", "")
	}
	ctx, client, err := firestoreCRUD.GetClient()
	if err != nil {
		return fmt.Errorf("firestoreCRUD.GetClient: %w", err)
	}
	docs, err := client.Collection(common.CollectionTask).Where("Name", "!=", "").Documents(ctx).GetAll()
	if err != nil {
		return fmt.Errorf("client.Collection.Where.documents: %w", err)
	}

	// https://gobyexample.com/worker-pools
	//type job struct {
	//	firestoreID string
	//	request *common.POSTRequest
	//}
	errChan := make(chan error)
	//jobs := make(chan job, len(docs))
	//numWorkers := 50
	//for i := 0; i < numWorkers; i++ {
	//	go func(jobs <-chan job, errChan chan <-error){
	//		for j := range jobs {
	//			errChan <- regenerateTask(j.firestoreID, j.request)
	//		}
	//	}(jobs, errChan)
	//}

	for i := range docs {
		t := common.Task{}
		err = t.Init(docs[i].Data())
		if err != nil {
			return fmt.Errorf("t.Init for %v: %w", docs[i].Ref.ID, err)
		}
		t.FirestoreID = docs[i].Ref.ID

		// don't regenerate the Edit Task Task. Doing so breaks concurrency.
		if t.FirestoreID == "lTvwFXhqipljNNulVCZceIzEG" {
			continue
		}
		go func(firestoreID string, request *common.POSTRequest) {
			errChan <- regenerateTask(firestoreID, request)
		}(t.FirestoreID, request)
	}
	combinedErr := ""
	for i := range docs {
		fmt.Printf("%v %v\n", i, docs[i].Ref.ID)
		err := <-errChan
		if err != nil {
			combinedErr += err.Error() + "; "
		}
	}
	if combinedErr != "" {
		return fmt.Errorf("combinedErr from regenerateTask: %v", combinedErr)
	}
	return nil
}

func regenerateTask(firestoreID string, request *common.POSTRequest) error {
	queryable, err := allDB.QueryEquals(common.CollectionData, common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Task ID"), firestoreID)
	if err != nil && strings.Contains(err.Error(), "code = NotFound") {
		// Ignore Tasks with no corresponding Data
		return nil
	}
	if err != nil {
		return fmt.Errorf("allDB.QueryEquals %v: %w", firestoreID, err)
	}
	editTaskData := queryable.(*common.Data)

	// Tasks are dependent upon other tasks. For example, the "Edit Task" Task will fail to generate if we are
	// simultaneously generating the "Fields" Task because it needs to examine the output of the "Fields" Task when
	// it is generating the parent Task.
	// The proper way to deal with this is to create a dependency tree and work your way from the Tasks with the most dependencies
	// up to those with the fewest. I take this approach when generating Tasks for JSON2App. Unfortunately I don't have time to
	// refactor that code so it can be used here too.
	for _, ref := range editTaskData.AS[common.AddFieldPrefix(editTaskData.IndustryID, editTaskData.DomainID, editTaskData.SchemaID, "Subtasks")] {
		parsedRef, err := common.ParseRef(ref)
		if err != nil {
			continue
		}
		// this is wrong
		waitCount := 0
		for {
			tasksRegeneratedMux.Lock()
			if tasksRegenerated[parsedRef.DataID] {
				// Dependency was created correctly
				tasksRegeneratedMux.Unlock()
				break
			}
			tasksRegeneratedMux.Unlock()
			if waitCount > 100 {
				// failed to wait long enough. Ignore the problem so we can proceed.
				break
			}
			waitCount++
			time.Sleep(time.Millisecond * 600)
		}
	}

	err = ActionEditTask(editTaskData, &common.POSTRequest{
		IndustryID: "polyapp",
		DomainID:   "TaskSchemaUX",
		UserID:     request.UserID,
		RoleID:     request.RoleID,
	}, nil)
	if err != nil {
		return fmt.Errorf("ActionEditTask %v: %w", editTaskData.FirestoreID, err)
	}
	tasksRegeneratedMux.Lock()
	tasksRegenerated[editTaskData.FirestoreID] = true
	tasksRegeneratedMux.Unlock()
	return nil
}

// FindFieldsWhichNeedConversions searched through oldSchema to find all of its fields.
// It then checks editTask to ensure it has all of those fields.
// If a field is not found, it is added to a list of prefixed field names which need conversions and returned.
//
// This function does NOT check to make sure the Field has the correct data type because we can sometimes do an automatic
// conversion with common.ConvertDataToMatchSchema
//
// Special case: "Done" field never needs a conversion.
func FindFieldsWhichNeedConversions(fields []Field, oldSchema common.Schema) ([]string, error) {
	fieldsWhichNeedConversions := make([]string, 0)
	for fieldName, dataType := range oldSchema.DataTypes {
		if dataType == "ARef" || dataType == "Ref" {
			continue
		}
		_, _, _, name := common.SplitField(fieldName)
		needsConversion := true
		for i := range fields {
			if fields[i].Name == name {
				needsConversion = false
				break
			}
		}
		if name == "Done" {
			needsConversion = false
		}
		if needsConversion {
			fieldsWhichNeedConversions = append(fieldsWhichNeedConversions, fieldName)
		}
	}
	if len(fieldsWhichNeedConversions) == 0 {
		fieldsWhichNeedConversions = nil
	}
	return fieldsWhichNeedConversions, nil
}

// ValidateConversionCoverage ensures the list of immediateSimpleConversions covers a all fieldsWhichNeedConversions.
// It may be a superset.
//
// If a field which needs a conversion does not already have a conversion then a conversion is added to immediateSimpleConversion.
// After all fields have been processed the new superset of immediateSimpleConversion is returned.
func ValidateConversionCoverage(immediateSimpleConversions []ImmediateSimpleConversion, fieldsWhichNeedConversions []string) (missingConversions bool, immediateSimpleConversionsOut []ImmediateSimpleConversion) {
	immediateSimpleConversionsOut = make([]ImmediateSimpleConversion, len(immediateSimpleConversions))
	for i := range immediateSimpleConversions {
		immediateSimpleConversionsOut[i] = immediateSimpleConversions[i]
	}
	for fieldsIndex := range fieldsWhichNeedConversions {
		hasConversion := false
		for i := range immediateSimpleConversions {
			if fieldsWhichNeedConversions[fieldsIndex] == immediateSimpleConversions[i].OldFieldName {
				hasConversion = true
				break
			}
		}
		// special cases
		if !hasConversion {
			missingConversions = true
			immediateSimpleConversionsOut = append(immediateSimpleConversionsOut, ImmediateSimpleConversion{
				DeleteOldFieldValue: true,
				NewFieldName:        "",
				OldFieldName:        fieldsWhichNeedConversions[fieldsIndex],
			})
		}
	}
	return
}
