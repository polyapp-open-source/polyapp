package actions

import (
	"bytes"
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"gitlab.com/polyapp-open-source/polyapp/fileCRUD"
	"html"
	"html/template"
	"io"
	"math"
	"net/url"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

type EditChart struct {
	Name             string
	Schema           string
	FieldName        string `suffix:"Field Name"`
	ChartType        string `suffix:"Chart Type"`
	ChartHTML        []byte `suffix:"Chart HTML"`
	ChartPreviewHTML []byte `suffix:"Chart Preview HTML"`
}

// ActionEditChartLoad makes sure each field has one or more filters attached and that those filters make sense. It also populates the list of possible Field Names.
func ActionEditChartLoad(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	ec := EditChart{}
	err = common.DataIntoStructure(data, &ec)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}
	response.ModDOMs = append(response.ModDOMs, common.ModDOM{
		InsertSelector: "#polyappJSNeedFullAuthCanary",
		Action:         "afterend",
		HTML:           `<div class="col col-12 col-lg-6"><div id="polyappJSEditChartPreview"></div></div>`,
	})
	err = updateChartOptions(ec, response)
	if err != nil {
		return fmt.Errorf("updateChartOptions: %w", err)
	}
	return nil
}

func updateChartOptions(ec EditChart, response *common.POSTResponse) error {
	if ec.Schema == "" {
		// Not ready to set up options
		return nil
	}

	schema, err := allDB.ReadSchema(ec.Schema)
	if err != nil {
		return fmt.Errorf("allDB.ReadSchema %v: %w", ec.Schema, err)
	}
	//industry := schema.IndustryID
	//domain := schema.DomainID

	var htmlToUse strings.Builder
	htmlToUse.WriteString(`<option class="pointer" selected=""></option>`)
	for _, key := range schema.DataKeys {
		htmlToUse.WriteString("<option>")
		htmlToUse.WriteString(common.RemoveFieldPrefix(key))
		htmlToUse.WriteString("</option>")
	}
	response.ModDOMs = append(response.ModDOMs, common.ModDOM{
		DeleteSelector: "#" + common.CSSEscape("polyapp_Reporting_OhrPAxfzladiHrneuWGTRTKER_Field%20Name") + " > *",
		InsertSelector: "#" + common.CSSEscape("polyapp_Reporting_OhrPAxfzladiHrneuWGTRTKER_Field%20Name"),
		Action:         "afterbegin",
		HTML:           htmlToUse.String(),
	})

	timeFields := []string{}
	for _, key := range schema.DataKeys {
		noPrefixKey := common.RemoveFieldPrefix(key)
		if common.IsTimeField(noPrefixKey) && schema.DataTypes[key] == "I" {
			timeFields = append(timeFields, key)
		}
	}

	var timeFieldSelectHTML strings.Builder
	timeFieldSelectHTML.WriteString(`<option class="pointer" selected=""></option>`)
	for _, timeField := range timeFields {
		timeFieldSelectHTML.WriteString("<option>")
		timeFieldSelectHTML.WriteString(common.RemoveFieldPrefix(timeField))
		timeFieldSelectHTML.WriteString("</option>")
	}
	response.ModDOMs = append(response.ModDOMs, common.ModDOM{
		DeleteSelector: "select[id^=" + common.CSSEscape("polyapp_Reporting_sKLVNYBpnYJQGRdlCaOtmsvBt_Time%20Field") + "] > *",
		InsertSelector: "select[id^=" + common.CSSEscape("polyapp_Reporting_sKLVNYBpnYJQGRdlCaOtmsvBt_Time%20Field") + "]",
		Action:         "afterbegin",
		HTML:           timeFieldSelectHTML.String(),
	}, common.ModDOM{
		DeleteSelector: "select[id^=" + common.CSSEscape("polyapp_Reporting_vujrVdTzLTyLbcrXMKXIVvCkq_Time%20Field") + "] > *",
		InsertSelector: "select[id^=" + common.CSSEscape("polyapp_Reporting_vujrVdTzLTyLbcrXMKXIVvCkq_Time%20Field") + "]",
		Action:         "afterbegin",
		HTML:           timeFieldSelectHTML.String(),
	})

	var numberFieldsHTML strings.Builder
	numberFieldsHTML.WriteString(`<option class="pointer" selected=""></option>`)
	for fieldName, fieldType := range schema.DataTypes {
		isTimeField := false
		for _, timeField := range timeFields {
			if fieldName == timeField {
				isTimeField = true
				break
			}
		}
		if isTimeField {
			continue
		}
		if fieldType == "I" || fieldType == "F" {
			numberFieldsHTML.WriteString(`<option>`)
			numberFieldsHTML.WriteString(common.RemoveFieldPrefix(fieldName))
			numberFieldsHTML.WriteString(`</option>`)
		}
	}
	response.ModDOMs = append(response.ModDOMs, common.ModDOM{
		DeleteSelector: "select[id^=" + common.CSSEscape("polyapp_Reporting_etAhazqUoXhSAuGKRnxtEDavk_Field%20Name") + "] > *",
		InsertSelector: "select[id^=" + common.CSSEscape("polyapp_Reporting_etAhazqUoXhSAuGKRnxtEDavk_Field%20Name") + "]",
		Action:         "afterbegin",
		HTML:           numberFieldsHTML.String(),
	})

	return nil
}

// ActionEditChartPreview creates a preview of a chart using only the first 100 data points in a Schema. It then inserts that chart into the DOM on the right hand side of the page (bottom on mobile).
func ActionEditChartPreview(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	ec := EditChart{}
	err = common.DataIntoStructure(data, &ec)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}

	err = updateChartOptions(ec, response)
	if err != nil {
		return fmt.Errorf("updateChartOptions: %w", err)
	}

	//if ec.Schema == "" || ec.Name == "" || ec.ChartType == "" || ec.FieldName == "" {
	//	// Not yet ready to generate a preview
	//	return nil
	//}
	//
	//chartFilters, err := getChartFilters(data)
	//if err != nil {
	//	return fmt.Errorf("getChartFilters: %w", err)
	//}
	//
	//if ec.Schema == "" || ec.FieldName == "" || ec.ChartType == "" {
	//	return nil
	//}
	//
	//var html bytes.Buffer
	//err = QueryAndMakeChart(industry, domain, ec.Schema,
	//	ec.FieldName, ec.ChartType, ec.Name, chartFilters,
	//	request, &html, 100)
	//if err != nil {
	//	return fmt.Errorf("makeChart: %w", err)
	//}
	//
	//ec.ChartPreviewHTML = bytes.Join([][]byte{[]byte(`<script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.1/dist/chart.min.js"></script>`), html.Bytes()[bytes.Index(html.Bytes(), []byte(`<div class="container">`)):bytes.LastIndex(html.Bytes(), []byte(`</body>`))]}, []byte(` `))
	//
	//err = common.StructureIntoData(request.IndustryID, request.DomainID, request.SchemaID, &ec, data)
	//if err != nil {
	//	return fmt.Errorf("StructureIntoData: %w", err)
	//}
	//
	//err = allDB.UpdateData(data)
	//if err != nil {
	//	return fmt.Errorf("allDB.UpdateData: %w", err)
	//}
	//
	//response.ModDOMs = append(response.ModDOMs, common.ModDOM{
	//	//DeleteSelector: "#polyappJSEditChartPreview > *",
	//	InsertSelector: "#polyapp_Reporting_OhrPAxfzladiHrneuWGTRTKER_Done",
	//	Action:         "afterend",
	//	HTML:           string(ec.ChartPreviewHTML),
	//})

	return nil
}

// ActionEditChartCreate creates a new Chart when you are done editing the existing chart.
func ActionEditChartCreate(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	ec := EditChart{}
	err = common.DataIntoStructure(data, &ec)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}

	schema, err := allDB.ReadSchema(ec.Schema)
	if err != nil {
		return fmt.Errorf("allDB.ReadSchema %v: %w", ec.Schema, err)
	}
	industry := schema.IndustryID
	domain := schema.DomainID

	chartFilters, err := getChartFilters(data)
	if err != nil {
		return fmt.Errorf("getChartFilters: %w", err)
	}

	if ec.Schema == "" || ec.FieldName == "" || ec.ChartType == "" {
		return nil
	}

	var html bytes.Buffer
	err = QueryAndMakeChart(industry, domain, ec.Schema,
		ec.FieldName, ec.ChartType, ec.Name, chartFilters,
		request, &html, 10_000)
	if err != nil {
		return fmt.Errorf("makeChart: %w", err)
	}

	ec.ChartHTML = html.Bytes()

	err = common.StructureIntoData(request.IndustryID, request.DomainID, request.SchemaID, &ec, data)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}

	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}

	// /blob/assets/downloads/ allows you to get a single blob file from the server via main.go's BlobDownloadHandler
	response.NewURL = "/blob/assets/" + data.FirestoreID + "/" +
		common.AddFieldPrefix(request.IndustryID, request.DomainID, request.SchemaID, "Chart HTML") + "?cacheBuster=" + common.GetRandString(10)

	return nil
}

// ActionLoadChartJavascript inserts code which loads the Javascript needed to display charts.
func ActionLoadChartJavascript(request *common.POSTRequest, response *common.POSTResponse) error {
	response.ModDOMs = append(response.ModDOMs, common.ModDOM{
		InsertSelector: "#" + common.AddFieldPrefix(request.IndustryID, request.DomainID, request.SchemaID, "Done"),
		Action:         "afterend",
		HTML:           `<script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.1/dist/chart.min.js"></script><canvas id="polyappJSChartID" width="400" height="400"></canvas>`,
	})
	return nil
}

type CreateChartFromData struct {
	Name         string
	Schema       string
	FieldName    string        `suffix:"Field Name"`
	ChartType    string        `suffix:"Chart Type"`
	ChartHTML    []byte        `suffix:"Chart HTML"`
	ChartFilters []chartFilter `suffix:"Chart Filters"`
}

type chartFilter struct {
	FieldName  string `suffix:"Field Name"`
	LowerLimit string `suffix:"Lower Limit"`
	UpperLimit string `suffix:"Upper Limit"`
}

type GetChartFilters struct {
	ChartFilters                 []chartFilter                  `suffix:"Chart Filters"`
	ChartUserTimeFilters         []chartTimeFilter              `suffix:"Chart User Time Filters"`
	ChartTimeRangeFilters        []chartTimeRangeFilter         `suffix:"Chart Time Range Filters"`
	ChartRelativeTimeRangeFilter []chartRelativeTimeRangeFilter `suffix:"Chart Relative Time Filters"`
}

// getChartFilters finds all supported types of chart filters and returns them in one easy to use array.
func getChartFilters(data *common.Data) ([]chartFilter, error) {
	CF := GetChartFilters{}
	err := allDB.DataIntoStructureComplex(data, &CF)
	if err != nil {
		return nil, fmt.Errorf("DataIntoStructureComplex: %w", err)
	}
	chartFilters := make([]chartFilter, len(CF.ChartFilters)+len(CF.ChartUserTimeFilters)+len(CF.ChartTimeRangeFilters)+len(CF.ChartRelativeTimeRangeFilter))
	for i := range CF.ChartFilters {
		chartFilters[i] = CF.ChartFilters[i]
	}
	for i := range CF.ChartUserTimeFilters {
		chartFilters[len(CF.ChartFilters)+i] = chartFilter{
			FieldName:  CF.ChartUserTimeFilters[i].FieldName,
			LowerLimit: strconv.FormatInt(CF.ChartUserTimeFilters[i].StartTime, 10),
			UpperLimit: strconv.FormatInt(CF.ChartUserTimeFilters[i].EndTime, 10),
		}
	}
	for i := range CF.ChartTimeRangeFilters {
		chartFilters[len(CF.ChartFilters)+len(CF.ChartUserTimeFilters)+i] = chartFilter{
			FieldName:  CF.ChartTimeRangeFilters[i].TimeField,
			LowerLimit: strconv.FormatInt(CF.ChartTimeRangeFilters[i].LowerLimit, 10),
			UpperLimit: strconv.FormatInt(CF.ChartTimeRangeFilters[i].UpperLimit, 10),
		}
	}
	for i := range CF.ChartRelativeTimeRangeFilter {
		chartFilters[len(CF.ChartFilters)+len(CF.ChartUserTimeFilters)+len(CF.ChartTimeRangeFilters)+i] = chartFilterFromRelativeTime(CF.ChartRelativeTimeRangeFilter[i], time.Now())
	}
	return chartFilters, nil
}

type chartTimeFilter struct {
	FieldName string `suffix:"Field Name"`
	StartTime int64  `suffix:"Start Time"`
	EndTime   int64  `suffix:"End Time"`
}

type chartTimeRangeFilter struct {
	TimeField  string `suffix:"Time Field"`
	LowerLimit int64  `suffix:"Lower Limit"`
	UpperLimit int64  `suffix:"Upper Limit"`
}

type chartRelativeTimeRangeFilter struct {
	TimeField          string `suffix:"Time Field"`
	RelativeTimePeriod string `suffix:"Relative Time Period"`
}

type productivityChart struct {
	Name       string
	UserID     string  `suffix:"User ID"`
	ChartHTML  []byte  `suffix:"Chart HTML"`
	LowerLimit float64 `suffix:"Lower Limit"`
	UpperLimit float64 `suffix:"Upper Limit"`
	Industry   string
	Domain     string
	Schema     string
}

// ActionProductivityChart is a custom chart task. It creates a new Chart which calculates the difference between when a
// Task was started and when it was "Done" for the last time. This value is displayed on the Y axis. The X axis displays
// when Tasks were first opened.
func ActionProductivityChart(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	p := productivityChart{
		Industry: "polyapp",
		Domain:   "User",
		Schema:   "iRQJHSitiUYNJJFfNSNCiFxwo",
	}
	err = common.DataIntoStructure(data, &p)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}
	if p.LowerLimit == 0 {
		p.LowerLimit = 1
	}
	if p.UpperLimit == 0 {
		p.UpperLimit = 57_600
	}
	user, err := allDB.ReadUser(p.UserID)
	if err != nil {
		return fmt.Errorf("allDB.ReadUser: %w", err)
	}
	p.Name = "Productivity Chart for " + *user.FullName
	chartFilters, err := getChartFilters(data)
	if err != nil {
		return fmt.Errorf("getChartFilters: %w", err)
	}

	q := allDB.Query{}
	q.Init(p.Industry, p.Domain, p.Schema, common.CollectionData)
	f := common.AddFieldPrefix(p.Industry, p.Domain, p.Schema, "User ID")
	q.AddEquals(f, p.UserID)
	q.SetLength(10_000) // limit from https://gitlab.com/polyapp-open-source/polyapp/-/issues/11
	iter, err := q.QueryRead()
	if err != nil {
		return fmt.Errorf("q.QueryRead: %w", err)
	}
	var allData []*common.Data
	for {
		d, err := iter.Next()
		if err != nil && err == common.IterDone {
			break
		}
		if err != nil {
			return fmt.Errorf("iter.Next: %w", err)
		}
		data := d.(*common.Data)
		allData = append(allData, data)
	}
	if len(allData) < 1 {
		return errors.New("User has no productivity data so we can't run this report")
	}

	c := chart{}
	var numDatas []float64
	var xAxisNums []int64
	c.Data = make([]string, 0)
	c.Labels = make([]string, 0)
	seen := make(map[string]bool)

	// perform all necessary allDB.ReadData calls in parallel or we'll be waiting too long for
	// up to 10_000 sequential calls to finish
	var mapMux sync.Mutex
	allEventData := make(map[string]common.Data)
	errChanLength := 0
	errChan := make(chan error)
	var wg sync.WaitGroup
	for i := range allData {
		wg.Add(1)
		go func(i int, errChan chan error) {
			eventURL := *allData[i].S[common.AddFieldPrefix(allData[i].IndustryID, allData[i].DomainID, allData[i].SchemaID, "Event URL")]
			// Should be [i].I but without RectifyData it is a F
			//eventTime := *allData[i].F[common.AddFieldPrefix(allData[i].IndustryID, allData[i].DomainID, allData[i].SchemaID, "Event Time")]
			eventURLParsed, err := common.ParseRef(eventURL)
			if err != nil {
				errChan <- fmt.Errorf("common.ParseRef %v: %w", eventURL, err)
				errChanLength++
				wg.Done()
				return
			}
			eventType := *allData[i].S[common.AddFieldPrefix(allData[i].IndustryID, allData[i].DomainID, allData[i].SchemaID, "Event")]

			// Only consider Load events and only consider those events if they are not already in the eventData dataset
			mapMux.Lock()
			if seen[eventURLParsed.DataID] || eventType != "Load" {
				mapMux.Unlock()
				wg.Done()
				return
			}
			seen[eventURLParsed.DataID] = true
			mapMux.Unlock()
			eventData := common.Data{
				FirestoreID: eventURLParsed.DataID,
			}
			err = allDB.Read(&eventData)
			if err != nil && strings.Contains(err.Error(), "NotFound") {
				wg.Done()
				return
			}
			if err != nil {
				errChan <- fmt.Errorf("allDB.ReadData: %w", err)
				errChanLength++
				wg.Done()
				return
			}
			mapMux.Lock()
			allEventData[eventURLParsed.DataID] = eventData
			mapMux.Unlock()
			wg.Done()
			return
		}(i, errChan)
	}
	wg.Wait()
	combinedErr := ""
	for i := 0; i < errChanLength; i++ {
		combinedErr += (<-errChan).Error() + "; "
	}
	if len(combinedErr) > 0 {
		return errors.New(combinedErr)
	}

	for _, eventData := range allEventData {
		if len(eventData.AF[common.AddFieldPrefix(eventData.IndustryID, eventData.DomainID, eventData.SchemaID, "Done Timestamps")]) > 0 &&
			len(eventData.AF[common.AddFieldPrefix(eventData.IndustryID, eventData.DomainID, eventData.SchemaID, "Load Timestamps")]) > 0 {
			doneTimestamps := eventData.AF[common.AddFieldPrefix(eventData.IndustryID, eventData.DomainID, eventData.SchemaID, "Done Timestamps")]
			loadTimestamps := eventData.AF[common.AddFieldPrefix(eventData.IndustryID, eventData.DomainID, eventData.SchemaID, "Load Timestamps")]

			if len(loadTimestamps) < 1 || len(doneTimestamps) < 1 {
				// bad data. We should not have any productivity data from before the epoch.
				continue
			}

			if !checkFilters(data, chartFilters) {
				c.NumFiltered++
				continue
			}

			// The problem with measure time spent in this way is that it can miss when someone navigates away
			// from the page and works on something else and then comes back again later to work on it again.
			// As it stands, we can only compare the first time they opened the Task until the next time they clicked "Done".
			// This isn't an easy to fix problem because the client does not send "the page is open"-style pings.
			// It only sends pings when you type into the page. So you could open something, read it for a half hour,
			// and close it, and we would be none the wiser that you were "working" for that half hour. We would only
			// record the time you started reviewing the Task.
			loadTimestampsIndex := 0
			doneTimestampsIndex := 0
			var timeTaken float64 = 0
			startedWorkingOnTask := loadTimestamps[loadTimestampsIndex]
			taskDone := doneTimestamps[doneTimestampsIndex]
			timeTaken += taskDone - startedWorkingOnTask

			for {
				if doneTimestampsIndex >= len(doneTimestamps) {
					break
				}
				for {
					if loadTimestampsIndex >= len(loadTimestamps) {
						break
					}
					if loadTimestamps[loadTimestampsIndex] > doneTimestamps[doneTimestampsIndex] {
						startedWorkingOnTask = loadTimestamps[loadTimestampsIndex]
					}
					loadTimestampsIndex++
				}
				doneTimestampsIndex++
				timeTaken += taskDone - startedWorkingOnTask
			}

			if float64(timeTaken) < p.LowerLimit || float64(timeTaken) > p.UpperLimit {
				c.NumFiltered++
				continue
			}
			xAxisNums = append(xAxisNums, int64(loadTimestamps[0]))
			c.Labels = append(c.Labels, time.Unix(int64(loadTimestamps[0]), 0).Format(time.Stamp))
			numDatas = append(numDatas, timeTaken)
			c.Data = append(c.Data, strconv.FormatInt(int64(timeTaken), 10))
		}
	}

	s := sortableP{
		c:         c,
		xAxisNums: xAxisNums,
	}
	sort.Sort(s)
	if len(numDatas) > 0 {
		c.Mean, c.StandardDeviation, c.Median, c.Minimum, c.Maximum, c.Sum = getStats(numDatas)
	}

	c.XAxisLabel = "Load Timestamp"
	c.YAxisLabel = "Time Taken"
	c.AxisType = "cartesian"
	c.Type = "line"
	c.DatasetLabel = p.Name
	c.StatsAreTime = true
	c.EditReportLink = common.CreateURLFromRequest(*request)
	c.DataID = request.DataID

	var html bytes.Buffer
	err = makeChart(c, &html)
	if err != nil {
		return fmt.Errorf("makeChart: %w", err)
	}
	p.ChartHTML = html.Bytes()

	err = common.StructureIntoData(request.IndustryID, request.DomainID, request.SchemaID, &p, data)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}

	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}

	// /blob/assets/downloads/ allows you to get a single blob file from the server via main.go's BlobDownloadHandler
	response.NewURL = "/blob/assets/" + data.FirestoreID + "/" + common.AddFieldPrefix(request.IndustryID, request.DomainID, request.SchemaID, "Chart HTML") + "?cacheBuster=" + common.GetRandString(10)

	return nil
}

type sortableP struct {
	xAxisNums []int64
	c         chart
}

func (s sortableP) Len() int {
	return len(s.xAxisNums)
}

func (s sortableP) Less(i, j int) bool {
	if s.xAxisNums[i] < s.xAxisNums[j] {
		return true
	}
	return false
}

func (s sortableP) Swap(i, j int) {
	temp := s.xAxisNums[i]
	temp2 := s.c.Data[i]
	temp3 := s.c.Labels[i]
	s.xAxisNums[i] = s.xAxisNums[j]
	s.c.Data[i] = s.c.Data[j]
	s.c.Labels[i] = s.c.Labels[j]
	s.xAxisNums[j] = temp
	s.c.Data[j] = temp2
	s.c.Labels[j] = temp3
}

// ActionCreateChartFromData creates a new Chart which reflects information available in Data.
func ActionCreateChartFromData(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	return ActionEditChartCreate(data, request, response)
}

func QueryAndMakeChart(industry, domain, schema, fieldName, chartType, chartName string, chartFilters []chartFilter, request *common.POSTRequest, html io.Writer, length int) error {
	var err error
	q := allDB.Query{}
	q.Init(industry, domain, schema, common.CollectionData)
	f := common.AddFieldPrefix(industry, domain, schema, fieldName)
	err = q.AddSortBy(f, "asc")
	if err != nil {
		return fmt.Errorf("q.AddSortBy: %w", err)
	}
	q.SetLength(length) // limit from https://gitlab.com/polyapp-open-source/polyapp/-/issues/11
	iter, err := q.QueryRead()
	if err != nil {
		return fmt.Errorf("q.QueryRead: %w", err)
	}
	numFiltered := 0
	var allData []*common.Data
	frequencyCounts := make(map[string]int)
	for {
		d, err := iter.Next()
		if err != nil && err == common.IterDone {
			break
		}
		if err != nil {
			return fmt.Errorf("iter.Next: %w", err)
		}
		data := d.(*common.Data)
		v := castValue(data, f)
		if checkFilters(data, chartFilters) {
			allData = append(allData, data)
			frequencyCounts[v] += 1
		} else {
			numFiltered++
		}
	}
	datasetLabel := chartName
	if datasetLabel == "" {
		datasetLabel = fieldName
	}
	c := chart{}
	datasetDisplay := ""
	switch chartType {
	case "line", "bar":
		datasetDisplay = "Over Time"
	case "radar", "doughnut", "pie":
		datasetDisplay = "Response Frequency"
	}
	switch datasetDisplay {
	case "Over Time":
		c.XAxisLabel = "Done Timestamps"
		c.YAxisLabel = fieldName
		c.Labels, c.Data, c.Mean, c.StandardDeviation, c.Median, c.Minimum, c.Maximum, c.Sum = overTime(allData, f)
	case "Averaged Over Time - Daily":
		c.Labels, c.Data, c.Mean, c.StandardDeviation, c.Median, c.Minimum, c.Maximum, c.Sum = averagedOverTime(allData, f, "daily")
	case "Averaged Over Time - Weekly":
		c.Labels, c.Data, c.Mean, c.StandardDeviation, c.Median, c.Minimum, c.Maximum, c.Sum = averagedOverTime(allData, f, "weekly")
	case "Averaged Over Time - Monthly":
		c.Labels, c.Data, c.Mean, c.StandardDeviation, c.Median, c.Minimum, c.Maximum, c.Sum = averagedOverTime(allData, f, "monthly")
	case "Response Frequency":
		c.XAxisLabel = fieldName
		c.YAxisLabel = "Response Frequency"
		c.Labels, c.Data = fromFrequencyCounts(frequencyCounts)
	default:
		return fmt.Errorf("unhandled Dataset Display value: %v", datasetDisplay)
	}
	switch chartType {
	case "line", "bar":
		c.AxisType = "cartesian"
	case "radar", "doughnut", "pie":
		c.AxisType = "category"
	}

	c.NumFiltered = numFiltered
	c.Type = chartType
	c.DatasetLabel = datasetLabel
	c.EditReportLink = common.CreateURLFromRequest(*request)
	c.DataID = request.DataID

	err = makeChart(c, html)
	if err != nil {
		return fmt.Errorf("makeChart: %w", err)
	}

	return nil
}

// chartFilterFromRelativeTime translates the text in a chartRelativeTimeRangeFilter into a more usable chartFilter.
func chartFilterFromRelativeTime(filter chartRelativeTimeRangeFilter, currentTime time.Time) chartFilter {
	var upperLimit, lowerLimit time.Time
	lastYearStart := time.Date(currentTime.Year()-1, time.January, 1, 0, 0, 0, 0, currentTime.Location())
	thisYearStart := time.Date(currentTime.Year(), 1, 1, 0, 0, 0, 0, currentTime.Location())
	nextYearStart := time.Date(currentTime.Year()+1, 1, 1, 0, 0, 0, 0, currentTime.Location())
	nextYearEnd := time.Date(currentTime.Year()+2, 1, 1, 0, 0, 0, 0, currentTime.Location())
	var lastMonthStart time.Time
	if currentTime.Month()-1 == 0 {
		lastMonthStart = time.Date(currentTime.Year()-1, 12, 1, 0, 0, 0, 0, currentTime.Location())
	} else {
		lastMonthStart = time.Date(currentTime.Year(), time.January, 1, 0, 0, 0, 0, currentTime.Location())
	}
	thisMonthStart := time.Date(currentTime.Year(), currentTime.Month(), 1, 0, 0, 0, 0, currentTime.Location())
	var nextMonthStart time.Time
	if currentTime.Month()+1 > 12 {
		nextMonthStart = time.Date(currentTime.Year()+1, 1, 1, 0, 0, 0, 0, currentTime.Location())
	} else {
		nextMonthStart = time.Date(currentTime.Year(), currentTime.Month()+1, 1, 0, 0, 0, 0, currentTime.Location())
	}
	var nextMonthEnd time.Time
	if currentTime.Month()+2 > 12 {
		nextMonthEnd = time.Date(currentTime.Year()+1, currentTime.Month()+2-12, 1, 0, 0, 0, 0, currentTime.Location())
	} else {
		nextMonthEnd = time.Date(currentTime.Year(), currentTime.Month()+2, 1, 0, 0, 0, 0, currentTime.Location())
	}
	// Note: the top part of the range is always 1 nanosecond too high. This is fixed later.
	switch filter.RelativeTimePeriod {
	case "Yesterday":
		yesterdayTime := currentTime.Add(-24 * time.Hour)
		lowerLimit = time.Date(yesterdayTime.Year(), yesterdayTime.Month(), yesterdayTime.Day(), 0, 0, 0, 0, yesterdayTime.Location())
		upperLimit = time.Date(currentTime.Year(), currentTime.Month(), currentTime.Day(), 0, 0, 0, 0, currentTime.Location())
	case "Today":
		lowerLimit = time.Date(currentTime.Year(), currentTime.Month(), currentTime.Day(), 0, 0, 0, 0, currentTime.Location())
		tomorrowTime := currentTime.Add(24 * time.Hour)
		upperLimit = time.Date(tomorrowTime.Year(), tomorrowTime.Month(), tomorrowTime.Day(), 0, 0, 0, 0, tomorrowTime.Location())
	case "Tomorrow":
		tomorrowTime := currentTime.Add(24 * time.Hour)
		lowerLimit = time.Date(tomorrowTime.Year(), tomorrowTime.Month(), tomorrowTime.Day(), 0, 0, 0, 0, tomorrowTime.Location())
		twoDaysTime := currentTime.Add(24 * 2 * time.Hour)
		upperLimit = time.Date(twoDaysTime.Year(), twoDaysTime.Month(), twoDaysTime.Day(), 0, 0, 0, 0, twoDaysTime.Location())
	case "This Week (M - Su)":
		mondayThisWeek := time.Date(currentTime.Year(), currentTime.Month(), currentTime.Day(), 0, 0, 0, 0, currentTime.Location())
		for mondayThisWeek.Weekday() != time.Monday {
			mondayThisWeek = mondayThisWeek.Add(time.Hour * -24)
		}
		SundayThisWeek := time.Date(currentTime.Year(), currentTime.Month(), currentTime.Day(), 0, 0, 0, 0, currentTime.Location())
		for SundayThisWeek.Weekday() != time.Sunday {
			SundayThisWeek = SundayThisWeek.Add(time.Hour * 24)
		}
		lowerLimit = mondayThisWeek
		upperLimit = SundayThisWeek
	case "This Week (Su - Sa)":
		sundayThisWeek := time.Date(currentTime.Year(), currentTime.Month(), currentTime.Day(), 0, 0, 0, 0, currentTime.Location())
		for sundayThisWeek.Weekday() != time.Sunday {
			sundayThisWeek = sundayThisWeek.Add(time.Hour * -24)
		}
		saturdayThisWeek := time.Date(currentTime.Year(), currentTime.Month(), currentTime.Day(), 0, 0, 0, 0, currentTime.Location())
		for saturdayThisWeek.Weekday() != time.Saturday {
			saturdayThisWeek = saturdayThisWeek.Add(time.Hour * 24)
		}
		lowerLimit = sundayThisWeek
		upperLimit = saturdayThisWeek
	case "Last Week (M - Su)":
		lastWeek := currentTime.Add(-7 * 24 * time.Hour)
		mondayLastWeek := time.Date(lastWeek.Year(), lastWeek.Month(), lastWeek.Day(), 0, 0, 0, 0, lastWeek.Location())
		for mondayLastWeek.Weekday() != time.Monday {
			mondayLastWeek = mondayLastWeek.Add(time.Hour * -24)
		}
		sundayLastWeek := time.Date(lastWeek.Year(), lastWeek.Month(), lastWeek.Day(), 0, 0, 0, 0, lastWeek.Location())
		for sundayLastWeek.Weekday() != time.Sunday {
			sundayLastWeek = sundayLastWeek.Add(time.Hour * 24)
		}
		lowerLimit = mondayLastWeek
		upperLimit = sundayLastWeek
	case "Last Week (Su - Sa)":
		lastWeek := currentTime.Add(-7 * 24 * time.Hour)
		sundaylastWeek := time.Date(lastWeek.Year(), lastWeek.Month(), lastWeek.Day(), 0, 0, 0, 0, lastWeek.Location())
		for sundaylastWeek.Weekday() != time.Sunday {
			sundaylastWeek = sundaylastWeek.Add(time.Hour * -24)
		}
		saturdayLastWeek := time.Date(lastWeek.Year(), lastWeek.Month(), lastWeek.Day(), 0, 0, 0, 0, lastWeek.Location())
		for saturdayLastWeek.Weekday() != time.Saturday {
			saturdayLastWeek = saturdayLastWeek.Add(time.Hour * 24)
		}
		lowerLimit = sundaylastWeek
		upperLimit = saturdayLastWeek
	case "Next Week (M - Su)":
		nextWeek := currentTime.Add(7 * 24 * time.Hour)
		mondayNextWeek := time.Date(nextWeek.Year(), nextWeek.Month(), nextWeek.Day(), 0, 0, 0, 0, nextWeek.Location())
		for mondayNextWeek.Weekday() != time.Monday {
			mondayNextWeek = mondayNextWeek.Add(time.Hour * -24)
		}
		sundayNextWeek := time.Date(nextWeek.Year(), nextWeek.Month(), nextWeek.Day(), 0, 0, 0, 0, nextWeek.Location())
		for sundayNextWeek.Weekday() != time.Sunday {
			sundayNextWeek = sundayNextWeek.Add(time.Hour * 24)
		}
		lowerLimit = mondayNextWeek
		upperLimit = sundayNextWeek
	case "Next Week (Su - Sa)":
		nextWeek := currentTime.Add(7 * 24 * time.Hour)
		sundayNextWeek := time.Date(nextWeek.Year(), nextWeek.Month(), nextWeek.Day(), 0, 0, 0, 0, nextWeek.Location())
		for sundayNextWeek.Weekday() != time.Sunday {
			sundayNextWeek = sundayNextWeek.Add(time.Hour * -24)
		}
		saturdayNextWeek := time.Date(nextWeek.Year(), nextWeek.Month(), nextWeek.Day(), 0, 0, 0, 0, nextWeek.Location())
		for saturdayNextWeek.Weekday() != time.Saturday {
			saturdayNextWeek = saturdayNextWeek.Add(time.Hour * 24)
		}
		lowerLimit = sundayNextWeek
		upperLimit = saturdayNextWeek
	case "Last Month":
		lowerLimit = lastMonthStart
		upperLimit = thisMonthStart
	case "This Month":
		lowerLimit = thisMonthStart
		upperLimit = nextMonthStart
	case "Next Month":
		lowerLimit = nextMonthStart
		upperLimit = nextMonthEnd
	case "Last Quarter (Jan - Mar, Apr - Jun, etc)":
		switch thisMonthStart.Month() {
		case time.January, time.February, time.March:
			lowerLimit = time.Date(thisMonthStart.Year()-1, time.October, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year(), time.January, 1, 0, 0, 0, 0, thisMonthStart.Location())
		case time.April, time.May, time.June:
			lowerLimit = time.Date(thisMonthStart.Year(), time.January, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year(), time.April, 1, 0, 0, 0, 0, thisMonthStart.Location())
		case time.July, time.August, time.September:
			lowerLimit = time.Date(thisMonthStart.Year(), time.April, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year(), time.July, 1, 0, 0, 0, 0, thisMonthStart.Location())
		case time.October, time.November, time.December:
			lowerLimit = time.Date(thisMonthStart.Year(), time.July, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year(), time.October, 1, 0, 0, 0, 0, thisMonthStart.Location())
		}
	case "This Quarter (Jan - Mar, Apr - Jun, etc)":
		switch thisMonthStart.Month() {
		case time.January, time.February, time.March:
			lowerLimit = time.Date(thisMonthStart.Year(), time.January, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year(), time.April, 1, 0, 0, 0, 0, thisMonthStart.Location())
		case time.April, time.May, time.June:
			lowerLimit = time.Date(thisMonthStart.Year(), time.April, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year(), time.July, 1, 0, 0, 0, 0, thisMonthStart.Location())
		case time.July, time.August, time.September:
			lowerLimit = time.Date(thisMonthStart.Year(), time.July, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year(), time.October, 1, 0, 0, 0, 0, thisMonthStart.Location())
		case time.October, time.November, time.December:
			lowerLimit = time.Date(thisMonthStart.Year(), time.July, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year()+1, time.January, 1, 0, 0, 0, 0, thisMonthStart.Location())
		}
	case "Next Quarter (Jan - Mar, Apr - Jun, etc)":
		switch thisMonthStart.Month() {
		case time.January, time.February, time.March:
			lowerLimit = time.Date(thisMonthStart.Year(), time.April, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year(), time.July, 1, 0, 0, 0, 0, thisMonthStart.Location())
		case time.April, time.May, time.June:
			lowerLimit = time.Date(thisMonthStart.Year(), time.July, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year(), time.October, 1, 0, 0, 0, 0, thisMonthStart.Location())
		case time.July, time.August, time.September:
			lowerLimit = time.Date(thisMonthStart.Year(), time.October, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year()+1, time.January, 1, 0, 0, 0, 0, thisMonthStart.Location())
		case time.October, time.November, time.December:
			lowerLimit = time.Date(thisMonthStart.Year()+1, time.January, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year()+1, time.April, 1, 0, 0, 0, 0, thisMonthStart.Location())
		}
	case "Last year (Jan - Dec)":
		lowerLimit = lastYearStart
		upperLimit = thisYearStart
	case "Last year (Jul - Jun)":
		// Ex: it is july 2021. last fiscal year would run from june 1 2020 until july 1 2021 at midnight.
		// Ex: it is june 2021. Last fiscal year would run from june 1 2019 until july 1 2020 at midnight. Current fiscal year ends July 1 2021 at midnight.
		switch thisMonthStart.Month() {
		case time.January, time.February, time.March, time.April, time.May, time.June:
			lowerLimit = time.Date(thisMonthStart.Year()-2, time.July, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year()-1, time.June, 1, 0, 0, 0, 0, thisMonthStart.Location())
		case time.July, time.August, time.September, time.October, time.November, time.December:
			lowerLimit = time.Date(thisMonthStart.Year()-1, time.July, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year(), time.June, 1, 0, 0, 0, 0, thisMonthStart.Location())
		}
	case "Last year (Oct - Sep)":
		switch thisMonthStart.Month() {
		case time.January, time.February, time.March, time.April, time.May, time.June, time.July, time.August, time.September:
			lowerLimit = time.Date(thisMonthStart.Year()-2, time.October, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year()-1, time.September, 1, 0, 0, 0, 0, thisMonthStart.Location())
		case time.October, time.November, time.December:
			lowerLimit = time.Date(thisMonthStart.Year()-1, time.October, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year(), time.September, 1, 0, 0, 0, 0, thisMonthStart.Location())
		}
	case "This year (Jan - Dec)":
		lowerLimit = thisYearStart
		upperLimit = nextYearStart
	case "This year (Jul - Jun)":
		switch thisMonthStart.Month() {
		case time.January, time.February, time.March, time.April, time.May, time.June:
			lowerLimit = time.Date(thisMonthStart.Year()-1, time.July, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year(), time.June, 1, 0, 0, 0, 0, thisMonthStart.Location())
		case time.July, time.August, time.September, time.October, time.November, time.December:
			lowerLimit = time.Date(thisMonthStart.Year(), time.July, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year()+1, time.June, 1, 0, 0, 0, 0, thisMonthStart.Location())
		}
	case "This year (Oct - Sep)":
		switch thisMonthStart.Month() {
		case time.January, time.February, time.March, time.April, time.May, time.June, time.July, time.August, time.September:
			lowerLimit = time.Date(thisMonthStart.Year()-1, time.October, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year(), time.September, 1, 0, 0, 0, 0, thisMonthStart.Location())
		case time.October, time.November, time.December:
			lowerLimit = time.Date(thisMonthStart.Year(), time.October, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year()+1, time.September, 1, 0, 0, 0, 0, thisMonthStart.Location())
		}
	case "Next year (Jan - Dec)":
		lowerLimit = nextYearStart
		upperLimit = nextYearEnd
	case "Next year (Jul - Jun)":
		switch thisMonthStart.Month() {
		// Ex: it is currently June 2021. Next fiscal year runs July 2021 - June 2022
		// Ex: it is currently July 2021. Next fiscal year runs July 2022 - June 2023
		case time.January, time.February, time.March, time.April, time.May, time.June:
			lowerLimit = time.Date(thisMonthStart.Year(), time.July, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year()+1, time.June, 1, 0, 0, 0, 0, thisMonthStart.Location())
		case time.July, time.August, time.September, time.October, time.November, time.December:
			lowerLimit = time.Date(thisMonthStart.Year()+1, time.July, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year()+2, time.June, 1, 0, 0, 0, 0, thisMonthStart.Location())
		}
	case "Next year (Oct - Sep)":
		switch thisMonthStart.Month() {
		case time.January, time.February, time.March, time.April, time.May, time.June, time.July, time.August, time.September:
			lowerLimit = time.Date(thisMonthStart.Year(), time.October, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year()+1, time.September, 1, 0, 0, 0, 0, thisMonthStart.Location())
		case time.October, time.November, time.December:
			lowerLimit = time.Date(thisMonthStart.Year()+1, time.October, 1, 0, 0, 0, 0, thisMonthStart.Location())
			upperLimit = time.Date(thisMonthStart.Year()+2, time.September, 1, 0, 0, 0, 0, thisMonthStart.Location())
		}
	case "January":
		lowerLimit = thisYearStart
		upperLimit = time.Date(currentTime.Year(), time.February, 1, 0, 0, 0, 0, currentTime.Location())
	case "February":
		lowerLimit = time.Date(currentTime.Year(), time.February, 1, 0, 0, 0, 0, currentTime.Location())
		upperLimit = time.Date(currentTime.Year(), time.March, 1, 0, 0, 0, 0, currentTime.Location())
	case "March":
		lowerLimit = time.Date(currentTime.Year(), time.March, 1, 0, 0, 0, 0, currentTime.Location())
		upperLimit = time.Date(currentTime.Year(), time.April, 1, 0, 0, 0, 0, currentTime.Location())
	case "April":
		lowerLimit = time.Date(currentTime.Year(), time.April, 1, 0, 0, 0, 0, currentTime.Location())
		upperLimit = time.Date(currentTime.Year(), time.May, 1, 0, 0, 0, 0, currentTime.Location())
	case "May":
		lowerLimit = time.Date(currentTime.Year(), time.May, 1, 0, 0, 0, 0, currentTime.Location())
		upperLimit = time.Date(currentTime.Year(), time.June, 1, 0, 0, 0, 0, currentTime.Location())
	case "June":
		lowerLimit = time.Date(currentTime.Year(), time.June, 1, 0, 0, 0, 0, currentTime.Location())
		upperLimit = time.Date(currentTime.Year(), time.July, 1, 0, 0, 0, 0, currentTime.Location())
	case "July":
		lowerLimit = time.Date(currentTime.Year(), time.July, 1, 0, 0, 0, 0, currentTime.Location())
		upperLimit = time.Date(currentTime.Year(), time.August, 1, 0, 0, 0, 0, currentTime.Location())
	case "August":
		lowerLimit = time.Date(currentTime.Year(), time.August, 1, 0, 0, 0, 0, currentTime.Location())
		upperLimit = time.Date(currentTime.Year(), time.September, 1, 0, 0, 0, 0, currentTime.Location())
	case "September":
		lowerLimit = time.Date(currentTime.Year(), time.September, 1, 0, 0, 0, 0, currentTime.Location())
		upperLimit = time.Date(currentTime.Year(), time.October, 1, 0, 0, 0, 0, currentTime.Location())
	case "October":
		lowerLimit = time.Date(currentTime.Year(), time.October, 1, 0, 0, 0, 0, currentTime.Location())
		upperLimit = time.Date(currentTime.Year(), time.November, 1, 0, 0, 0, 0, currentTime.Location())
	case "November":
		lowerLimit = time.Date(currentTime.Year(), time.November, 1, 0, 0, 0, 0, currentTime.Location())
		upperLimit = time.Date(currentTime.Year(), time.December, 1, 0, 0, 0, 0, currentTime.Location())
	case "December":
		lowerLimit = time.Date(currentTime.Year(), time.December, 1, 0, 0, 0, 0, currentTime.Location())
		upperLimit = time.Date(currentTime.Year()+1, time.January, 1, 0, 0, 0, 0, currentTime.Location())
	case "First Quarter (Jan - Mar)":
		lowerLimit = time.Date(currentTime.Year(), time.January, 1, 0, 0, 0, 0, currentTime.Location())
		upperLimit = time.Date(currentTime.Year(), time.April, 1, 0, 0, 0, 0, currentTime.Location())
	case "Second Quarter (Apr - Jun)":
		lowerLimit = time.Date(currentTime.Year(), time.April, 1, 0, 0, 0, 0, currentTime.Location())
		upperLimit = time.Date(currentTime.Year(), time.July, 1, 0, 0, 0, 0, currentTime.Location())
	case "Third Quarter (Jul - Sep)":
		lowerLimit = time.Date(currentTime.Year(), time.July, 1, 0, 0, 0, 0, currentTime.Location())
		upperLimit = time.Date(currentTime.Year(), time.October, 1, 0, 0, 0, 0, currentTime.Location())
	case "Fourth Quarter (Oct - Dec)":
		lowerLimit = time.Date(currentTime.Year(), time.October, 1, 0, 0, 0, 0, currentTime.Location())
		upperLimit = time.Date(currentTime.Year()+1, time.January, 1, 0, 0, 0, 0, currentTime.Location())
	}
	return chartFilter{
		FieldName:  filter.TimeField,
		UpperLimit: fmt.Sprintf("%v", upperLimit.Unix()-1), // Subtract 1 because the top part of the range is exclusive & the filters are inclusive.
		LowerLimit: fmt.Sprintf("%v", lowerLimit.Unix()),   // Don't modify because the bottom part of the range is inclusive
	}
}

// checkFilters returns true if Data fits all of the passed in filters and false otherwise.
func checkFilters(d *common.Data, filters []chartFilter) bool {
	for i := range filters {
		switch filters[i].FieldName {
		case "First Load Timestamp":
			compareValue := 0.0
			targetValue := d.AF[common.AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, "Load Timestamps")]
			if len(targetValue) > 0 {
				compareValue = targetValue[0]
			} else {
				compareValue = 0.0
			}
			d.F[common.AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, "First Load Timestamp")] = common.Float64(compareValue)
		case "First Done Timestamp":
			compareValue := 0.0
			targetValue := d.AF[common.AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, "Done Timestamps")]
			if len(targetValue) > 0 {
				compareValue = targetValue[0]
			} else {
				compareValue = 0.0
			}
			d.F[common.AddFieldPrefix(d.IndustryID, d.DomainID, d.SchemaID, "First Done Timestamp")] = common.Float64(compareValue)
		default:
		}
		if !checkFilter(d, filters[i]) {
			return false
		}
	}
	return true
}

// checkFilter returns true if Data fit the passed in filter and false otherwise.
//
// If an array is passed, it verifies that every element in the array meets the conditions listed.
func checkFilter(d *common.Data, filter chartFilter) bool {
	for k, v := range d.S {
		if strings.HasSuffix(k, "_"+filter.FieldName) {
			return checkStringFilter(v, filter)
		}
	}
	for k, v := range d.B {
		if strings.HasSuffix(k, "_"+filter.FieldName) {
			return checkBoolFilter(v, filter)
		}
	}
	for k, v := range d.I {
		if strings.HasSuffix(k, "_"+filter.FieldName) {
			return checkIntFilter(v, filter)
		}
	}
	for k, v := range d.F {
		if strings.HasSuffix(k, "_"+filter.FieldName) {
			return checkFloatFilter(v, filter)
		}
	}
	for k, v := range d.AF {
		if strings.HasSuffix(k, "_"+filter.FieldName) {
			if len(v) > 0 {
				for _, arrayValue := range v {
					isOk := checkFloatFilter(common.Float64(arrayValue), filter)
					if !isOk {
						return false
					}
				}
				return true
			} else {
				return false
			}
		}
	}
	for k, v := range d.AI {
		if strings.HasSuffix(k, "_"+filter.FieldName) {
			if len(v) > 0 {
				for _, arrayValue := range v {
					isOk := checkIntFilter(common.Int64(arrayValue), filter)
					if !isOk {
						return false
					}
				}
				return true
			} else {
				return false
			}
		}
	}
	for k, v := range d.AS {
		if strings.HasSuffix(k, "_"+filter.FieldName) {
			if len(v) > 0 {
				for _, arrayValue := range v {
					isOk := checkStringFilter(common.String(arrayValue), filter)
					if !isOk {
						return false
					}
				}
				return true
			} else {
				return false
			}
		}
	}
	for k, v := range d.AB {
		if strings.HasSuffix(k, "_"+filter.FieldName) {
			if len(v) > 0 {
				for _, arrayValue := range v {
					isOk := checkBoolFilter(common.Bool(arrayValue), filter)
					if !isOk {
						return false
					}
				}
				return true
			} else {
				return false
			}
		}
	}
	return false
}

func checkStringFilter(v *string, filter chartFilter) bool {
	if v == nil {
		return true
	}
	checkLower := len(filter.LowerLimit) > 0
	checkUpper := len(filter.UpperLimit) > 0
	if checkLower && filter.LowerLimit >= *v {
		return false
	}
	if checkUpper && *v >= filter.UpperLimit {
		return false
	}
	return true
}

func checkBoolFilter(v *bool, filter chartFilter) bool {
	if v == nil {
		return true
	}
	checkLower := len(filter.LowerLimit) > 0
	checkUpper := len(filter.UpperLimit) > 0
	lowerLimitBool, err := strconv.ParseBool(filter.LowerLimit)
	if err != nil {
		checkLower = false
	}
	upperLimitBool, err := strconv.ParseBool(filter.LowerLimit)
	if err != nil {
		checkUpper = false
	}
	if checkLower && lowerLimitBool != *v {
		return false
	}
	if checkUpper && upperLimitBool != *v {
		return false
	}
	return true
}

func checkIntFilter(v *int64, filter chartFilter) bool {
	if v == nil {
		return true
	}
	lowerLimit, err := strconv.ParseFloat(filter.LowerLimit, 64)
	if err != nil {
		lowerLimit = -1 * math.MaxFloat64
	}
	upperLimit, err := strconv.ParseFloat(filter.UpperLimit, 64)
	if err != nil {
		upperLimit = math.MaxFloat64
	}
	if lowerLimit <= float64(*v) && float64(*v) <= upperLimit {
		return true
	}
	return false
}

func checkFloatFilter(v *float64, filter chartFilter) bool {
	if v == nil {
		return true
	}
	lowerLimit, err := strconv.ParseFloat(filter.LowerLimit, 64)
	if err != nil {
		lowerLimit = -1 * math.MaxFloat64
	}
	upperLimit, err := strconv.ParseFloat(filter.UpperLimit, 64)
	if err != nil {
		upperLimit = math.MaxFloat64
	}
	if lowerLimit <= float64(*v) && float64(*v) <= upperLimit {
		return true
	}
	return false
}

func overTime(data []*common.Data, field string) (labels, chartData []string, Mean, StandardDeviation, Median, Minimum, Maximum, Sum float64) {
	if len(data) < 1 {
		return
	}
	s := sortData{
		Data:          data,
		FieldToSortOn: common.AddFieldPrefix(data[0].IndustryID, data[0].DomainID, data[0].SchemaID, "Done Timestamps"),
	}
	sort.Sort(s)
	var numDatas []float64
	for i := range data {
		v := castValue(data[i], field)
		numValue, err := strconv.ParseFloat(v, 64)
		if err == nil {
			numDatas = append(numDatas, numValue)
		}
		chartData = append(chartData, v)
		lastDoneTimestamp := strconv.FormatInt(math.MaxInt64, 10)
		if len(data[i].AF[common.AddFieldPrefix(data[0].IndustryID, data[0].DomainID, data[0].SchemaID, "Done Timestamps")]) > 0 {
			intTimestamp := int64(data[i].AF[common.AddFieldPrefix(data[0].IndustryID, data[0].DomainID, data[0].SchemaID, "Done Timestamps")][0])
			lastDoneTimestamp = time.Unix(intTimestamp, 0).Format(time.Stamp)
		}
		labels = append(labels, lastDoneTimestamp)
	}
	if len(numDatas) > 0 && len(numDatas) == len(data) {
		Mean, StandardDeviation, Median, Minimum, Maximum, Sum = getStats(numDatas)
	}
	return
}

func getStats(numDatas []float64) (Mean, StandardDeviation, Median, Minimum, Maximum, Sum float64) {
	if len(numDatas) < 1 {
		return
	}
	sort.Float64s(numDatas)
	Minimum = math.MaxFloat64
	Maximum = math.MaxFloat64 * -1
	for i := range numDatas {
		Sum += numDatas[i]
		if numDatas[i] < Minimum {
			Minimum = numDatas[i]
		}
		if numDatas[i] > Maximum {
			Maximum = numDatas[i]
		}
	}
	Mean = Sum / float64(len(numDatas))
	middle := math.Floor(float64(len(numDatas)) / float64(2))
	if middle > 0 {
		Median = numDatas[int(middle)-1]
	} else {
		Median = numDatas[0]
	}
	var partial float64 = 0
	for i := range numDatas {
		partial += math.Pow(numDatas[i]-Mean, 2)
	}
	StandardDeviation = math.Sqrt((1.0 / float64(len(numDatas))) * partial)
	return
}

// TODO
func averagedOverTime(data []*common.Data, field string, timespan string) (labels, chartData []string, Mean, StandardDeviation, Median, Minimum, Maximum, Sum float64) {
	s := sortData{
		Data:          data,
		FieldToSortOn: common.AddFieldPrefix(data[0].IndustryID, data[0].DomainID, data[0].SchemaID, "Done Timestamps"),
	}
	sort.Sort(s)
	var numDatas []float64
	for i := range data {
		v := castValue(data[i], field)
		numValue, err := strconv.ParseFloat(v, 64)
		if err == nil {
			numDatas = append(numDatas, numValue)
		}
		chartData = append(chartData, v)
		lastDoneTimestamp := strconv.FormatInt(math.MaxInt64, 10)
		if len(data[i].AF[common.AddFieldPrefix(data[0].IndustryID, data[0].DomainID, data[0].SchemaID, "Done Timestamps")]) > 0 {
			intTimestamp := int64(data[i].AF[common.AddFieldPrefix(data[0].IndustryID, data[0].DomainID, data[0].SchemaID, "Done Timestamps")][0])
			lastDoneTimestamp = time.Unix(intTimestamp, 0).Format(time.Stamp)
		}
		labels = append(labels, lastDoneTimestamp)
	}
	if len(numDatas) > 0 && len(numDatas) == len(data) {
		Mean, StandardDeviation, Median, Minimum, Maximum, Sum = getStats(numDatas)
	}
	return
}

type sortData struct {
	Data          []*common.Data
	FieldToSortOn string
	IsArray       bool
}

func (s sortData) Len() int {
	return len(s.Data)
}

func (s sortData) Less(i, j int) bool {
	iLastDone := s.Data[i].I[s.FieldToSortOn]
	jLastDone := s.Data[j].I[s.FieldToSortOn]
	if s.IsArray {
		if len(s.Data[i].AI[s.FieldToSortOn]) > 0 {
			iLastDone = common.Int64(s.Data[i].AI[s.FieldToSortOn][0])
		}
		if len(s.Data[i].AI[s.FieldToSortOn]) > 0 {
			jLastDone = common.Int64(s.Data[i].AI[s.FieldToSortOn][0])
		}
	}
	// sort real values first, empty values last
	if iLastDone == nil {
		return false
	}
	if jLastDone == nil {
		return true
	}
	if *iLastDone < *jLastDone {
		return true
	}
	return false
}

func (s sortData) Swap(i, j int) {
	temp := s.Data[i]
	s.Data[i] = s.Data[j]
	s.Data[j] = temp
}

func fromFrequencyCounts(frequencyCounts map[string]int) (labels, data []string) {
	labels = make([]string, 0)
	data = make([]string, 0)
	for k, v := range frequencyCounts {
		labels = append(labels, k)
		data = append(data, strconv.Itoa(v))
	}
	return labels, data
}

type chart struct {
	// Type is type of the chart
	Type string
	// AxisType is one of "cartesian", "category", "linear", "logarithmic", "timecartesian", "timeseries", but only cartesian and category have been implemented.
	AxisType string
	// DatasetLabel is the name of the overall data set
	DatasetLabel string
	// XAxisLabel is the label associated with the X Axis.
	XAxisLabel string
	// YAxisLabel is the label associated with the Y Axis.
	YAxisLabel string
	// Labels are displayed under their part of the chart
	Labels []string
	// Data is the data we are displaying
	Data []string
	// BeginAtZero if true sets this option
	BeginAtZero bool
	// Sum (optional) of the dataset.
	Sum float64
	// Mean (optional) of the dataset.
	Mean float64
	// Mean (optional) of the dataset.
	StandardDeviation float64
	// Mean (optional) of the dataset.
	Median float64
	// Minimum (optional) of the dataset.
	Minimum float64
	// Maximum (optional) of the dataset.
	Maximum float64
	// NumFiltered documents
	NumFiltered int
	// NumShown documents
	NumShown int
	// StatsAreTime if true converts the statistics into hours / minutes / seconds.
	StatsAreTime          bool
	SumTime               string
	MeanTime              string
	StandardDeviationTime string
	MedianTime            string
	MinimumTime           string
	MaximumTime           string
	EditReportLink        string
	DataID                string
}

func getHoursMinutesSecondsFormatted(timeString string) string {
	out := ""
	stringNoHour := timeString
	if strings.Contains(timeString, "h") {
		out += strings.Split(timeString, "h")[0] + " Hours, "
		stringNoHour = strings.Split(timeString, "h")[1]
	}
	stringNoMinute := timeString
	if strings.Contains(timeString, "m") {
		out += strings.Split(stringNoHour, "m")[0] + " Minutes, "
		stringNoMinute = strings.Split(stringNoHour, "m")[1]
	}
	if strings.Contains(timeString, "s") {
		out += strings.Split(stringNoMinute, "s")[0] + " Seconds"
	}
	return out
}

func makeChart(c chart, w io.Writer) error {
	c.NumShown = len(c.Data)
	if c.StatsAreTime {
		c.SumTime = getHoursMinutesSecondsFormatted((time.Second * time.Duration(c.Sum)).String())
		c.MeanTime = getHoursMinutesSecondsFormatted((time.Second * time.Duration(c.Mean)).String())
		c.StandardDeviationTime = getHoursMinutesSecondsFormatted((time.Second * time.Duration(c.StandardDeviation)).String())
		c.MedianTime = getHoursMinutesSecondsFormatted((time.Second * time.Duration(c.Median)).String())
		c.MinimumTime = getHoursMinutesSecondsFormatted((time.Second * time.Duration(c.Minimum)).String())
		c.MaximumTime = getHoursMinutesSecondsFormatted((time.Second * time.Duration(c.Maximum)).String())
	}
	chartTemplate := `{{/* . is type chart  */}}
	{{define "chartTemplate"}}
<head>
    <title>{{.DatasetLabel}}</title>
    <meta name="Description" content="Chart showing {{.DatasetLabel}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="/assets/giraffe192.jpg">
    <link rel="shortcut icon" href="/assets/giraffe192.jpg">
    <!-- matches theme_color in manifest.json -->
    <meta name="theme-color" content="#fdff85">
	<link rel="stylesheet" href="/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="/bootstrap/bootstrap-grid.min.css">
	<link rel="stylesheet" href="/bootstrap/bootstrap-reboot.min.css">
	<link rel="stylesheet" type="text/css" href="/DataTables/datatables.min.css"/>

	<script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.1/dist/chart.min.js"></script>

	<script defer src="/jquery/jquery.min.js"></script>
    <script defer src="/bootstrap/bootstrap.bundle.min.js"></script>
    <script defer type="text/javascript" src="/DataTables/datatables.min.js"></script>
    <script async src="https://use.fontawesome.com/e9ce45a312.js"></script>

	<!-- every CSS file we own should exist here -->
	<link rel="stylesheet" href="/index.css">
	<link href="/summernote/summernote-bs4.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.39.0/css/tempusdominus-bootstrap-4.min.css" integrity="sha512-3JRrEUwaCkFUBLK1N8HehwQgu8e23jTH4np5NHOmQOobuC4ROQxFwFgBLTnhcnQRMs84muMh0PnnwXlPq5MGjg==" crossorigin="anonymous" />
	<script src="https://www.gstatic.com/firebasejs/7.15.5/firebase-app.js"></script>
	<script src="https://www.gstatic.com/firebasejs/7.15.5/firebase-auth.js"></script>
	<script src="/main.js"></script>
	
	<script async src="/html5sortable/html5sortable.min.js"></script>
	
	<script defer src="/bootstrap-select/js/bootstrap-select.min.js"></script>
	<link rel="stylesheet" href="/bootstrap-select/css/bootstrap-select.min.css">
	
	<script defer type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.0/moment.min.js"></script>
</head>
<body>
	<nav class="navbar-light bg-light" style="top:0;width:100%;height:38px;" id="polyappTopBar">
		<!-- based on https://getbootstrap.com/docs/3.4/components/#navbar -->
		<span class="floatLeft">
			<a href="/" class="btn" aria-label="navigate home">
				<img src="/assets/icons/home-24px.svg" width="24px" height="24px" alt="navigate home" class="d-inline-block align-top" />
				<span>Polyapp</span>
			</a>
			<a href="/polyappChangeContext?industry=polyapp&domain=Reporting" class="btn" aria-label="change domain">
				<img src="/assets/icons/folder-24px.svg" width="24px" height="24px" alt="folder" class="d-inline-block align-top" />
				<span>Change Domain</span>
			</a>
			<a href="/polyappChangeTask?industry=polyapp&domain=Reporting&schema=polyappTask" class="btn" aria-label="change task">
				<img src="/assets/icons/work-24px.svg" width="24px" height="24px" alt="work" class="d-inline-block align-top" />
				<span>Change Task</span>
			</a>
			<a href="/polyappChangeData?industry=polyapp&domain=Reporting&task=ltygzBCYpjlqeXRhSVPZtzJKb&ux=cYdflemxtmQCdlsgiGVcsgFpm&schema=nMyfcJQJSykcdLpvjqFRZXkeO" class="btn" aria-label="change data">
				<img src="/assets/icons/insert_drive_file-24px.svg" width="24px" height="24px" alt="work" class="d-inline-block align-top" />
				<span>Change Data</span>
			</a>
			<a href="{{.EditReportLink}}" class="btn" aria-label="Edit Report">
				<img src="/assets/icons/edit-24px.svg" width="24px" height="24px" alt="edit" class="d-inline-block align-top" />
				<span>Edit Report</span>
			</a>
			<a href="/t/polyapp/Reporting/oirJixuiCaUiigcRQuenayNHo?ux=lFUmSlLZpIMUUpLpLbHTODHgK&schema=oyFnvZuDqpgkhTJCzrHbYrSZx&data=pEtpSZvNQFsaMtcTxCInMuiKO&modify={{.DataID}}" class="btn" aria-label="Refresh Report">
				<img src="/assets/icons/refresh_black_24dp.svg" width="24px" height="24px" alt="edit" class="d-inline-block align-top" />
				<span>Refresh Report</span>
			</a>
		</span>
	</nav>
<div class="container">
	<canvas id="polyappJSChartID"></canvas>
<div class="container">
{{- if .StatsAreTime -}}
	<p>Sum: {{.SumTime}}</p>
	<p>Mean: {{.MeanTime}}</p>
	<p>Standard Deviation: {{.StandardDeviationTime}}</p>
	<p>Median: {{.MedianTime}}</p>
	<p>Minimum: {{.MinimumTime}}</p>
	<p>Maximum: {{.MaximumTime}}</p>
{{- else if .Mean -}}
	<p>Sum: {{.Sum}}</p>
	<p>Mean: {{.Mean}}</p>
	<p>Standard Deviation: {{.StandardDeviation}}</p>
	<p>Median: {{.Median}}</p>
	<p>Minimum: {{.Minimum}}</p>
	<p>Maximum: {{.Maximum}}</p>
{{- end -}}
	<p>Number of Filtered Documents: {{.NumFiltered}}</p>
	<p>Number of Shown Documents: {{.NumShown}}</p>
</div>
<script id="polyappJSCreateChartFromDataScript">
function randomColorGenerator(length) {
	let lastHue = 360;
	let out = [];
	for (let i=0;i<length;i++) {
		lastHue -= 30;
		if (lastHue < 0) {
			lastHue += 360;
		}
		out.push('hsl('+lastHue+', 100%, 65%, 0.5)');
	}
	return out;
};

var ctx = document.getElementById('polyappJSChartID').getContext('2d');
var myChart = new Chart(ctx, {
    type: '{{.Type}}',
    data: {
        labels: [{{range $index, $element := .Labels}}{{if ne $index 0}},{{end}}'{{$element}}'{{end}}],
        datasets: [{
            label: '{{.DatasetLabel}}',
			{{if eq .AxisType "cartesian" -}}
			borderColor: 'rgb(75, 192, 192)',
			backgroundColor: 'rgba(75, 192, 192, 0.5)',
			{{- else if eq .AxisType "category" -}}
			backgroundColor: randomColorGenerator({{.DatasetLabel}}.length),
			{{- end -}}
            data: [{{range $index, $element := .Data}}{{if ne $index 0}},{{end}}'{{$element}}'{{end}}]
        }]
    },
{{if eq .AxisType "cartesian"}}
    options: {
		responsive: true,
		maintainAspectRatio: true,
		scales: {
			x: {
				title: {
					{{if .XAxisLabel}}text: '{{.XAxisLabel}}',{{end}}
					display: true
				}
			},
			y: {
				{{if .BeginAtZero}}beginAtZero: true,{{end}}
				title: {
					{{if .YAxisLabel}}text: '{{.YAxisLabel}}',{{end}}
					display: true
				}
			}
		}
    }
{{else if eq .AxisType "category"}}
    options: {
		responsive: true,
		maintainAspectRatio: true
    }
{{end}}
});
</script></div></body>
{{end}}`
	t, err := template.New("chartTemplate").Parse(chartTemplate)
	if err != nil {
		return err
	}
	err = t.Execute(w, c)
	if err != nil {
		return err
	}
	return nil
}

// ActionSetInitialHiddenColumns determines if a Task appears to be brand new. If so, it sets the Hidden Columns to the
// metadata columns.
func ActionSetInitialHiddenColumns(data *common.Data, response *common.POSTResponse) error {
	for k := range data.ARef {
		if len(data.ARef[k]) != 0 {
			// Not a new Data
			return nil
		}
	}
	for k := range data.S {
		if data.S[k] != nil && len(*data.S[k]) != 0 {
			// Not a new Data
			return nil
		}
	}
	data.AS[common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, "Hidden Columns")] = []string{
		"Load Timestamps",
		"Done Timestamps",
		"Load User IDs",
		"Done User IDs",
		"Done",
	}
	err := allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}
	return nil
}

type CreateTableFromData struct {
	Name          string
	Schema        string
	TableHTML     []byte `suffix:"Table HTML"`
	TimeFilter    []chartFilter
	HiddenColumns []string
	ChartFilters  []chartFilter
}

// isColumnHidden returns true if a column is in the hidden list or false otherwise.
func isColumnHidden(hiddenColumns []string, prefixedColumnName string) bool {
	for _, hiddenCol := range hiddenColumns {
		if hiddenCol == prefixedColumnName || strings.HasSuffix(prefixedColumnName, "_"+hiddenCol) {
			return true
		}
	}
	return false
}

// getHiddenColumns returns a list of hiddenColumns attached to this report.
func getHiddenColumns(data *common.Data) (hiddenColumns []string, err error) {
	for k := range data.AS {
		if strings.HasSuffix(k, "Hidden Columns") {
			hiddenColumns = data.AS[k]
		}
	}
	return
}

// ActionCreateTableFromData creates a table containing all of the Data in a Schema and stores it into the Table HTML field.
func ActionCreateTableFromData(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	CreateTableFromData := CreateTableFromData{}
	err = common.DataIntoStructure(data, &CreateTableFromData)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}
	if CreateTableFromData.Schema == "" {
		return nil
	}
	CreateTableFromData.ChartFilters, err = getChartFilters(data)
	if err != nil {
		return fmt.Errorf("getChartFilters: %w", err)
	}
	CreateTableFromData.HiddenColumns, err = getHiddenColumns(data)
	if err != nil {
		return fmt.Errorf("getHiddenColumns: %w", err)
	}

	err = createTableFromData(&CreateTableFromData, request)
	if err != nil {
		return fmt.Errorf("createTableFromData: %w", err)
	}

	err = common.StructureIntoData(request.IndustryID, request.DomainID, request.SchemaID, &CreateTableFromData, data)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}

	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}

	// /blob/assets/downloads/ allows you to get a single blob file from the server via main.go's BlobDownloadHandler
	response.NewURL = "/blob/assets/" + data.FirestoreID + "/" + common.AddFieldPrefix(request.IndustryID, request.DomainID, request.SchemaID, "Table HTML") + "?cacheBuster=" + common.GetRandString(10)

	return nil
}

func createTableFromData(createTableFromData *CreateTableFromData, request *common.POSTRequest) error {
	s, err := allDB.ReadSchema(createTableFromData.Schema)
	if err != nil {
		return fmt.Errorf("allDB.ReadSchema: %w", err)
	}
	t := dataTable{
		Name:    createTableFromData.Name,
		Columns: make([]string, 0),
		Data:    make([][]string, 0),
	}
	columnKeys := []string{
		"Open Data",
	}
	t.Columns = []string{
		"Open Data",
	}
	hiddenColumns := createTableFromData.HiddenColumns
	for _, key := range s.DataKeys {
		if key[0] != '_' && !isColumnHidden(hiddenColumns, key) {
			columnKeys = append(columnKeys, key)
			t.Columns = append(t.Columns, common.RemoveFieldPrefix(key))
		}
	}

	q := allDB.Query{}
	q.Init(s.IndustryID, s.DomainID, createTableFromData.Schema, common.CollectionData)
	q.AddEquals(common.PolyappSchemaID, createTableFromData.Schema)
	q.SetLength(10_000) // limit from https://gitlab.com/polyapp-open-source/polyapp/-/issues/11
	iter, err := q.QueryRead()
	if err != nil {
		return fmt.Errorf("q.QueryRead: %w", err)
	}
	var uxID, taskID string
	for {
		d, err := iter.Next()
		if err != nil && err == common.IterDone {
			break
		}
		if err != nil {
			return fmt.Errorf("iter.Next: %w", err)
		}
		data := d.(*common.Data)
		if !checkFilters(data, createTableFromData.ChartFilters) {
			t.NumFiltered++
			continue
		}
		row := make([]string, len(columnKeys))
		if uxID == "" {
			queryable, err := allDB.QueryEquals(common.CollectionData, common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Schema ID"), createTableFromData.Schema)
			if err != nil {
				return fmt.Errorf("allDB.QueryEquals: %w", err)
			}
			if queryable == nil {
				return fmt.Errorf("QueryEquals returned nil when searching at key %v value %v", common.AddFieldPrefix("polyapp", "TaskSchemaUX", "gjGLMlcNApJyvRjmgQbopsXPI", "Schema ID"), createTableFromData.Schema)
			}
			editTaskData := queryable.(*common.Data)
			taskID = *editTaskData.S[common.AddFieldPrefix(editTaskData.IndustryID, editTaskData.DomainID, editTaskData.SchemaID, "Task ID")]
			uxID = *editTaskData.S[common.AddFieldPrefix(editTaskData.IndustryID, editTaskData.DomainID, editTaskData.SchemaID, "UX ID")]
		}
		for i := range columnKeys {
			if i == 0 {
				linkToData := common.CreateURL(s.IndustryID, s.DomainID, taskID, uxID, createTableFromData.Schema, data.FirestoreID, "", "", "", "")
				row[0] = `<a href="` + linkToData + `" class="btn btn-link d-flex justify-content-center"><i class="fa fa-link"></i></a>`
			} else {
				row[i] = castValue(data, columnKeys[i])
			}
		}
		t.Data = append(t.Data, row)
	}
	t.EditReportLink = common.CreateURLFromRequest(*request)
	t.DataID = request.DataID

	var h bytes.Buffer
	err = makeTable(t, &h)
	if err != nil {
		return fmt.Errorf("makeTable: %w", err)
	}
	createTableFromData.TableHTML = h.Bytes()

	return nil
}

type dataTable struct {
	// Name is shown at the top of the table
	Name           string
	Columns        []string
	Data           [][]string
	DataEscaped    [][]template.HTML
	NumFiltered    int64
	EditReportLink string
	DataID         string
}

// makeTable returns all the code needed for a new data table which has all of its information stored on the client.
func makeTable(dataTable dataTable, w io.Writer) error {
	dataTable.DataEscaped = make([][]template.HTML, len(dataTable.Data))
	// func enhanceTableWithHTML
	emailRegex := regexp.MustCompile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
	urlRegex := regexp.MustCompile("https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)")
	unixTimestampRegex := regexp.MustCompile("\\b[0-9]{8,11}\\b")
	allRegex := []*regexp.Regexp{emailRegex, urlRegex, unixTimestampRegex}
	allRegexToReplace := []func(string, string) string{
		func(toReplace string, colName string) string {
			return "<a href='mailto:" + toReplace + "'>" + toReplace + "</a>"
		},
		func(toReplace string, colName string) string {
			return "<a href='" + toReplace + "'>" + toReplace + "</a>"
		},
		func(toReplace string, colName string) string {
			keyLowered := strings.ToLower(colName)
			intTime, err := strconv.ParseInt(toReplace, 10, 64)
			if err == nil && (strings.Contains(keyLowered, " time ") || strings.Contains(keyLowered, " date ") ||
				strings.Contains(keyLowered, " datetime ") || strings.HasSuffix(keyLowered, "time") ||
				strings.HasSuffix(keyLowered, "date") || strings.HasSuffix(keyLowered, "datetime") ||
				strings.HasPrefix(keyLowered, "time") || strings.HasPrefix(keyLowered, "date")) {
				timeToShow := time.Unix(intTime, 0)
				return timeToShow.Format("1/2/2006 15:04:05 MST")
			}
			return toReplace
		},
	}
	// Data filters are the equivalent of "dataFilter" on the client
	for rowIndex := 0; rowIndex < len(dataTable.Data); rowIndex += 1 {
		rowHTML := make([]template.HTML, len(dataTable.Data[rowIndex]))
		for colIndex := 0; colIndex < len(dataTable.Data[rowIndex]); colIndex += 1 {
			rowHTML[colIndex] = template.HTML(html.EscapeString(dataTable.Data[rowIndex][colIndex]))
			for regexIndex, regex := range allRegex {
				matches := regex.FindAllString(string(rowHTML[colIndex]), -1)
				if matches == nil {
					continue
				}
				for _, toReplace := range matches {
					rowHTML[colIndex] = template.HTML(strings.ReplaceAll(string(rowHTML[colIndex]), toReplace, allRegexToReplace[regexIndex](toReplace, dataTable.Columns[colIndex])))
				}
			}
		}
		dataTable.DataEscaped[rowIndex] = rowHTML
	}
	tableTemplate := `{{define "tableTemplate"}}
<head>
	<title>{{.Name}}</title>
    <meta name="Description" content="Polyapp can be used to create forms, enter information, and automate filling out forms.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="/assets/giraffe192.jpg">
    <link rel="shortcut icon" href="/assets/giraffe192.jpg">
    <!-- matches theme_color in manifest.json -->
    <meta name="theme-color" content="#fdff85">

	<link rel="stylesheet" href="/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="/bootstrap/bootstrap-grid.min.css">
	<link rel="stylesheet" href="/bootstrap/bootstrap-reboot.min.css">
	<link rel="stylesheet" type="text/css" href="/DataTables/datatables.min.css"/>

	<script defer src="/jquery/jquery.min.js"></script>
    <script defer src="/bootstrap/bootstrap.bundle.min.js"></script>
    <script defer type="text/javascript" src="/DataTables/datatables.min.js"></script>
    <script async src="https://use.fontawesome.com/e9ce45a312.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css" />
	<script defer src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
	<script defer src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>

	<!-- required for XLSX file saving -->
	<script defer src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<!-- following are all for PDF support. Must be included in this order. FYI these are large files -->
	<script defer src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script defer src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

	<!-- every CSS file we own should exist here -->
	<link rel="stylesheet" href="/index.css">
	<link href="/summernote/summernote-bs4.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.39.0/css/tempusdominus-bootstrap-4.min.css" integrity="sha512-3JRrEUwaCkFUBLK1N8HehwQgu8e23jTH4np5NHOmQOobuC4ROQxFwFgBLTnhcnQRMs84muMh0PnnwXlPq5MGjg==" crossorigin="anonymous" />
	<script src="https://www.gstatic.com/firebasejs/7.15.5/firebase-app.js"></script>
	<script src="https://www.gstatic.com/firebasejs/7.15.5/firebase-auth.js"></script>
	<script src="/main.js"></script>
	
	<script async src="/html5sortable/html5sortable.min.js"></script>
	
	<script defer src="/bootstrap-select/js/bootstrap-select.min.js"></script>
	<link rel="stylesheet" href="/bootstrap-select/css/bootstrap-select.min.css">
	
	<script defer type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.0/moment.min.js"></script>
</head>
<body>
	<nav class="navbar-light bg-light" style="top:0;width:100%;height:38px;" id="polyappTopBar">
		<!-- based on https://getbootstrap.com/docs/3.4/components/#navbar -->
		<span class="floatLeft">
			<a href="/" class="btn" aria-label="navigate home">
				<img src="/assets/icons/home-24px.svg" width="24px" height="24px" alt="navigate home" class="d-inline-block align-top" />
				<span>Polyapp</span>
			</a>
			<a href="/polyappChangeContext?industry=polyapp&domain=Reporting" class="btn" aria-label="change domain">
				<img src="/assets/icons/folder-24px.svg" width="24px" height="24px" alt="folder" class="d-inline-block align-top" />
				<span>Change Domain</span>
			</a>
			<a href="/polyappChangeTask?industry=polyapp&domain=Reporting&schema=polyappTask" class="btn" aria-label="change task">
				<img src="/assets/icons/work-24px.svg" width="24px" height="24px" alt="work" class="d-inline-block align-top" />
				<span>Change Task</span>
			</a>
			<a href="/polyappChangeData?industry=polyapp&domain=Reporting&task=zIoQjcmACDOdQsrKckUmmzLTZ&ux=GjxnRTamLcVfyALAMjpWaywYm&schema=IYrpkFurrcTnXqbLuMawFknMd" class="btn" aria-label="change data">
				<img src="/assets/icons/insert_drive_file-24px.svg" width="24px" height="24px" alt="work" class="d-inline-block align-top" />
				<span>Change Data</span>
			</a>
			<a href="{{.EditReportLink}}" class="btn" aria-label="Edit Report">
				<img src="/assets/icons/edit-24px.svg" width="24px" height="24px" alt="edit" class="d-inline-block align-top" />
				<span>Edit Report</span>
			</a>
			<a href="/t/polyapp/Reporting/oirJixuiCaUiigcRQuenayNHo?ux=lFUmSlLZpIMUUpLpLbHTODHgK&schema=oyFnvZuDqpgkhTJCzrHbYrSZx&data=pEtpSZvNQFsaMtcTxCInMuiKO&modify={{.DataID}}" class="btn" aria-label="Refresh Report">
				<img src="/assets/icons/refresh_black_24dp.svg" width="24px" height="24px" alt="edit" class="d-inline-block align-top" />
				<span>Refresh Report</span>
			</a>
		</span>
	</nav>
<div class="container-fluid">
<h1>{{.Name}}</h1>
<p>Number of filtered rows: {{.NumFiltered}}</p>
{{- if eq (len .Data) 0 -}}
<p>No Data found in the Schema</p>
{{- else if eq (len .Data) 10000 -}}
<p>More than 10,000 entries found. Entries after index 10,000 were not included in this table.</p>
{{- end -}}
<table id="polyappJSTableWithData" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
{{- range .Columns -}}
			<th>{{.}}</th>
{{- end -}}
		</tr>
	</thead>
	<tbody>
{{- range .DataEscaped -}}
		<tr>
{{- range . -}}
			<td>{{.}}</td>
{{- end -}}
		</tr>
{{- end -}}
	</tbody>
</table>
<div class="container">
	<h3>Table Help</h3>
	<p>Sorting: You can sort a column, change its sort direction, or remove sorting by clicking on a column header. You can sort on more than one column by holding shift while clicking on column headers.</p>
	<p>Searching: You can search throughout the entire table by using the search bar. Searching includes more than just one column.</p>
</div>
</div>
<script>
	window.addEventListener('DOMContentLoaded', function() {
        run();
    });
	function run() {
		$(document).ready( function () {
			var table = $('#polyappJSTableWithData').DataTable({
				"order": [[ 1, "asc" ]],
				/*"dom": 'Bfrtip',// this adds the buttons to the DOM. The other option to add them is to get the buttons from table.buttons().container() and insert them yourself */
				"buttons": [
					'print', 'copy', 'excel', 'csv', 'pdf'
				],
				"lengthChange": true,
			});
			// Create a container for the buttons which will be adjacent to the "Show X entries" dropdown
			let a = document.getElementById("polyappJSTableWithData_length").parentNode;
			a.classList.add('col-lg-4');
			a.classList.remove('col-md-6');
			a.nextElementSibling.classList.add('col-lg-4');
			a.nextElementSibling.classList.remove('col-md-6');
			a.insertAdjacentHTML('afterEnd', '<div class="col-sm-12 col-lg-4"><div id="polyappJSTableWithData_polyapp_buttonContainer"></div></div>');
			table.buttons().container().appendTo('#polyappJSTableWithData_polyapp_buttonContainer');
		} );
	}
</script>
</body>
{{end}}`
	t, err := template.New("tableTemplate").Parse(tableTemplate)
	if err != nil {
		return fmt.Errorf("template.New.Parse: %w", err)
	}
	err = t.Execute(w, dataTable)
	if err != nil {
		return fmt.Errorf("t.Execute: %w", err)
	}
	return nil
}

// ActionRefreshRecentReport refreshes the most recent report you viewed and then redirects the browser to view that report.
//
// Call this: /t/polyapp/Reporting/oirJixuiCaUiigcRQuenayNHo?ux=lFUmSlLZpIMUUpLpLbHTODHgK&schema=oyFnvZuDqpgkhTJCzrHbYrSZx&data=pEtpSZvNQFsaMtcTxCInMuiKO
func ActionRefreshRecentReport(request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	if request.UserCache == nil || request.UserCache.FirestoreID == "" {
		u, err := allDB.ReadUser(request.UserID)
		if err != nil {
			return fmt.Errorf("allDB.ReadUser: %w", err)
		}
		request.UserCache = &u
	}
	var mostRecentReportURL common.GETRequest
	if request.ModifyID != "" {
		reportData := common.Data{
			FirestoreID: request.ModifyID,
		}
		err = allDB.Read(&reportData)
		if err != nil {
			return fmt.Errorf("allDB.Read %v: %w", request.ModifyID, err)
		}
		mostRecentReportURL.IndustryID = reportData.IndustryID
		mostRecentReportURL.DomainID = reportData.DomainID
		mostRecentReportURL.SchemaID = reportData.SchemaID
		mostRecentReportURL.DataID = reportData.FirestoreID
	} else {
		q := allDB.Query{}
		q.Init("polyapp", "User", "iRQJHSitiUYNJJFfNSNCiFxwo", common.CollectionData)
		q.AddEquals("polyapp_User_iRQJHSitiUYNJJFfNSNCiFxwo_User ID", request.UserID)
		err := q.AddSortBy("polyapp_User_iRQJHSitiUYNJJFfNSNCiFxwo_Event Time", "desc")
		if err != nil {
			return fmt.Errorf("q.AddSortBy: %w", err)
		}
		q.SetLength(100)
		iter, err := q.QueryRead()
		if err != nil {
			return fmt.Errorf("q.QueryRead: %w", err)
		}
		for {
			queryable, err := iter.Next()
			if err != nil && err == common.IterDone {
				break
			}
			if err != nil {
				return fmt.Errorf("iter.Next: %w", err)
			}
			d := queryable.(*common.Data)
			ua := UserActivity{}
			err = common.DataIntoStructure(d, &ua)
			if err != nil {
				return fmt.Errorf("DataIntoStructure: %w", err)
			}
			parsedURL, err := url.Parse(ua.EventURL)
			if err != nil {
				return fmt.Errorf("url.Parse: %w", err)
			}
			getReq, err := common.CreateGETRequest(*parsedURL)
			if err != nil {
				return fmt.Errorf("common.CreateGETRequest: %w", err)
			}
			if getReq.SchemaID == "IYrpkFurrcTnXqbLuMawFknMd" || getReq.SchemaID == "nMyfcJQJSykcdLpvjqFRZXkeO" {
				mostRecentReportURL = getReq
				break
			}
		}
	}
	if mostRecentReportURL.SchemaID == "" {
		return errors.New("no reports were recently edited so we can't refresh the most recent report because we don't know the most recent report")
	}

	fieldName := ""
	if mostRecentReportURL.SchemaID == "IYrpkFurrcTnXqbLuMawFknMd" {
		fieldName = "Table%20HTML"
	} else if mostRecentReportURL.SchemaID == "nMyfcJQJSykcdLpvjqFRZXkeO" {
		fieldName = "Chart%20HTML"
	}
	response.NewURL = "/blob/assets/" + mostRecentReportURL.DataID + "/polyapp_Reporting_" + mostRecentReportURL.SchemaID + "_" + fieldName + "?cacheBuster=" + common.GetRandString(10)

	// Refresh the report
	currentData := common.Data{
		FirestoreID: mostRecentReportURL.DataID,
	}
	err = allDB.Read(&currentData)
	if err != nil {
		return fmt.Errorf("currentData allDB.Read: %w", err)
	}
	if mostRecentReportURL.SchemaID == "IYrpkFurrcTnXqbLuMawFknMd" {
		err = ActionCreateTableFromData(&currentData, &common.POSTRequest{
			IndustryID: currentData.IndustryID,
			DomainID:   currentData.DomainID,
			TaskID:     "zIoQjcmACDOdQsrKckUmmzLTZ",
			UXID:       "GjxnRTamLcVfyALAMjpWaywYm",
			SchemaID:   "IYrpkFurrcTnXqbLuMawFknMd",
			DataID:     currentData.FirestoreID,
			UserID:     request.UserID,
			UserCache:  request.UserCache,
			RoleID:     request.RoleID,
		}, &common.POSTResponse{
			BrowserActions: []common.BrowserAction{},
			ModDOMs:        []common.ModDOM{},
		})
		if err != nil {
			return fmt.Errorf("ActionCreateTableFromData: %w", err)
		}
	} else if mostRecentReportURL.SchemaID == "nMyfcJQJSykcdLpvjqFRZXkeO" {
		err = ActionCreateChartFromData(&currentData, &common.POSTRequest{
			IndustryID: currentData.IndustryID,
			DomainID:   currentData.DomainID,
			TaskID:     "zIoQjcmACDOdQsrKckUmmzLTZ",
			UXID:       "GjxnRTamLcVfyALAMjpWaywYm",
			SchemaID:   "IYrpkFurrcTnXqbLuMawFknMd",
			DataID:     currentData.FirestoreID,
			UserID:     request.UserID,
			UserCache:  request.UserCache,
			RoleID:     request.RoleID,
		}, &common.POSTResponse{
			BrowserActions: []common.BrowserAction{},
			ModDOMs:        []common.ModDOM{},
		})
		if err != nil {
			return fmt.Errorf("ActionCreateChartFromData: %w", err)
		}
	}

	return nil
}

type Link struct {
	Name string
	URL  string
}

type CreateDashboard struct {
	Header          string
	ReportIDs       []string `suffix:"Report IDs"`
	SetAsHomePage   bool     `suffix:"Set As Home Page"`
	Fields          []Field
	Links           []Link
	DashboardHTML   []byte `suffix:"Dashboard HTML"`
	DashboardDataID string
	ExportToJSON    bool `suffix:"Export to JSON"`
	industry        string
	domain          string
	schema          string
}

// ActionCreateDashboard creates a Dashboard for use as a home page or other way to reach other pages in Polyapp.
func ActionCreateDashboard(data *common.Data, request *common.POSTRequest, response *common.POSTResponse) error {
	var err error
	CreateDashboard := CreateDashboard{}
	err = allDB.DataIntoStructureComplex(data, &CreateDashboard)
	if err != nil {
		return fmt.Errorf("common.DataIntoStructureComplex: %w", err)
	}
	for i := range CreateDashboard.Fields {
		err = CreateDashboard.Fields[i].Validate()
		if err != nil {
			return fmt.Errorf("CreateDashboard.Fields[%v].Validate: %w", i, err)
		}
	}

	CreateDashboard.industry = data.IndustryID
	CreateDashboard.domain = data.DomainID
	CreateDashboard.schema = data.SchemaID
	CreateDashboard.DashboardDataID = data.FirestoreID

	if request.UserCache == nil {
		u, err := allDB.ReadUser(request.UserID)
		if err != nil {
			return fmt.Errorf("allDB.ReadUser: %w", err)
		}
		request.UserCache = &u
	}

	err = createDashboard(&CreateDashboard, request.UserCache)
	if err != nil {
		return fmt.Errorf("CreateDashboard: %w", err)
	}

	if CreateDashboard.ExportToJSON {
		err := fileCRUD.Init("")
		if err != nil {
			return fmt.Errorf("fileCRUD.Init: %w", err)
		}
		err = CreateFileDataRecursively(data)
		if err != nil {
			return fmt.Errorf("CreateFileCRUDRecursively: %w", err)
		}
	}
	response.NewURL = "/blob/assets/" + data.FirestoreID + "/polyapp_Reporting_ZhcLBvwSBdjcpFVApHUyAygqv_Dashboard%20HTML?cacheBuster=" + common.GetRandString(15)

	return nil
}

func createDashboard(CreateDashboard *CreateDashboard, user *common.User) error {
	var w bytes.Buffer
	temp, err := template.New("").Parse(`
<head>
    <title>{{if .Header}}{{.Header}}{{else}}Dashboard{{end}}</title>
    <meta name="Description" content="Dashboard">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="/assets/giraffe192.jpg">
    <link rel="shortcut icon" href="/assets/giraffe192.jpg">
    <!-- matches theme_color in manifest.json -->
    <meta name="theme-color" content="#fdff85">
	<link rel="stylesheet" href="/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="/bootstrap/bootstrap-grid.min.css">
	<link rel="stylesheet" href="/bootstrap/bootstrap-reboot.min.css">
	<link rel="stylesheet" type="text/css" href="/DataTables/datatables.min.css"/>

	<script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.1/dist/chart.min.js"></script>

	<script defer src="/jquery/jquery.min.js"></script>
    <script defer src="/bootstrap/bootstrap.bundle.min.js"></script>
    <script defer type="text/javascript" src="/DataTables/datatables.min.js"></script>
    <script async src="https://use.fontawesome.com/e9ce45a312.js"></script>

	<!-- every CSS file we own should exist here -->
	<link rel="stylesheet" href="/index.css">
	<link href="/summernote/summernote-bs4.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.39.0/css/tempusdominus-bootstrap-4.min.css" integrity="sha512-3JRrEUwaCkFUBLK1N8HehwQgu8e23jTH4np5NHOmQOobuC4ROQxFwFgBLTnhcnQRMs84muMh0PnnwXlPq5MGjg==" crossorigin="anonymous" />
	<script src="https://www.gstatic.com/firebasejs/7.15.5/firebase-app.js"></script>
	<script src="https://www.gstatic.com/firebasejs/7.15.5/firebase-auth.js"></script>
	<script src="/main.js"></script>
	
	<script async src="/html5sortable/html5sortable.min.js"></script>
	
	<script defer src="/bootstrap-select/js/bootstrap-select.min.js"></script>
	<link rel="stylesheet" href="/bootstrap-select/css/bootstrap-select.min.css">
	
	<script defer type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.0/moment.min.js"></script>
</head>
<body>
	<nav class="navbar-light bg-light" style="top:0;width:100%;height:38px;" id="polyappTopBar">
		<!-- based on https://getbootstrap.com/docs/3.4/components/#navbar -->
		<span class="floatLeft">
			<a href="/" class="btn" aria-label="navigate home">
				<img src="/assets/icons/home-24px.svg" width="24px" height="24px" alt="navigate home" class="d-inline-block align-top" />
				<span>Polyapp</span>
			</a>
			<a href="/polyappChangeContext?industry=polyapp&domain=Reporting" class="btn" aria-label="change domain">
				<img src="/assets/icons/folder-24px.svg" width="24px" height="24px" alt="folder" class="d-inline-block align-top" />
				<span>Change Domain</span>
			</a>
			<a href="/polyappChangeTask?industry=polyapp&domain=Reporting&task=EPCoNRhUCTAIHlxtdHKblUoCv&schema=polyappTask" class="btn" aria-label="change task">
				<img src="/assets/icons/work-24px.svg" width="24px" height="24px" alt="work" class="d-inline-block align-top" />
				<span>Change Task</span>
			</a>
			<a href="/polyappChangeData?industry=polyapp&domain=Reporting&task=EPCoNRhUCTAIHlxtdHKblUoCv&schema=ZhcLBvwSBdjcpFVApHUyAygqv&ux=ReUEYowRGHuPPMGGgNeHsJvvo" class="btn" aria-label="change data">
				<img src="/assets/icons/insert_drive_file-24px.svg" width="24px" height="24px" alt="work" class="d-inline-block align-top" />
				<span>Change Data</span>
			</a>
			<a href="/t/polyapp/Reporting/EPCoNRhUCTAIHlxtdHKblUoCv?ux=ReUEYowRGHuPPMGGgNeHsJvvo&schema=ZhcLBvwSBdjcpFVApHUyAygqv&data={{.DashboardDataID}}" class="btn" aria-label="edit dashboard">
				<img src="/assets/icons/edit-24px.svg" width="24px" height="24px" alt="edit" class="d-inline-block align-top" />
				<span>Edit Dashboard</span>
			</a>
		</span>
	</nav>
	<div class="container-fluid">
		<div class="row main-height">
			{{if .Links}}<div class="col col-0 col-md-2 navbar-light bg-light">
				<a class="navbar-brand" href="">Links</a>
				<ul class="nav flex-column navbar-nav">
					{{range .Links}}
					<li class="nav-item">
						<a class="nav-link" href="{{.URL}}">{{.Name}}</a>
					</li>
					{{end}}
				</ul>
			</div>{{end}}
			<div class="col col-12 col-md-10">
				<div class="container-fluid">
					<h1 class="display-4">{{.Header}}</h1>`)
	if err != nil {
		return fmt.Errorf("temp.New.Parse: %w", err)
	}
	err = temp.Execute(&w, CreateDashboard)
	if err != nil {
		return fmt.Errorf("temp.Execute: %w", err)
	}

	w.WriteString(`<div class="row">`)
	for i := range CreateDashboard.ReportIDs {
		reportData := common.Data{
			FirestoreID: CreateDashboard.ReportIDs[i],
		}
		err = allDB.Read(&reportData)
		if err != nil {
			return fmt.Errorf("allDB.Read: %w", err)
		}
		htmlField := "Chart HTML"
		if reportData.SchemaID == "IYrpkFurrcTnXqbLuMawFknMd" {
			htmlField = "Table HTML"
		}
		u := "/blob/assets/" + reportData.FirestoreID + "/" + common.AddFieldPrefix(reportData.IndustryID, reportData.DomainID, reportData.SchemaID, htmlField) + "?cacheBuster=" + common.GetRandString(15)
		w.WriteString(`<div class="col col-lg-6 col-12">
	<div class="border rounded my-2" style="border-color: #dee2e6!important;">
	<iframe src="` + u + `" width="100%" height="90%" style="height:50vh;"></iframe>
	<a href="/blob/assets/PqpUQvYqCfUQkhZwbKMaFaGBd/polyapp_Reporting_IYrpkFurrcTnXqbLuMawFknMd_Table HTML?cacheBuster=OGsRReyWLryNCGq" width="100%" class="btn btn-link"><i class="fa fa-link"></i>&nbsp;&nbsp;Open Report</a>
	</div>
</div>`)
	}
	w.WriteString(`</div>`)

	w.WriteString(`<div>`)
	for _, f := range CreateDashboard.Fields {
		err = writeFieldToBuffer(&w, f, &common.Schema{
			IndustryID:  "polyapp",
			DomainID:    "Reporting",
			SchemaID:    CreateDashboard.schema,
			FirestoreID: CreateDashboard.schema,
		}, map[string]string{})
		if err != nil {
			return fmt.Errorf("writeFieldToBuffer: %w", err)
		}
	}
	w.WriteString(`</div>`)

	// container-fluid; col col-11 col-md-10; row; container-fluid; body
	w.WriteString(`</div></div></div></div></body>`)

	CreateDashboard.DashboardHTML = w.Bytes()
	data := &common.Data{}
	data.Init(nil)
	data.IndustryID = CreateDashboard.industry
	data.DomainID = CreateDashboard.domain
	data.SchemaID = CreateDashboard.schema
	data.FirestoreID = CreateDashboard.DashboardDataID
	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, CreateDashboard, data)
	if err != nil {
		return fmt.Errorf("common.StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(data)
	if err != nil {
		return fmt.Errorf("allDB.UpdateData: %w", err)
	}

	if CreateDashboard.SetAsHomePage {
		// I am not going to check access because everyone should be able to set their own home page
		// regardless of home much access they have to the system.
		// I imagine I will do something similar with themes.
		//haveAccess := false
		//for _, r := range user.Roles {
		//	role, err := allDB.ReadRole(r)
		//	if err != nil {
		//		return fmt.Errorf("cached role (%v) not readable for RoleAuthorize user (%v): %w", r, request.UserID, err)
		//	}
		//	haveAccess, err = common.RoleHasAccess(&role, "polyapp", "User", "TkfMDeIjfSaXCMSByYRsEbubp", 'u')
		//	if err != nil {
		//		return fmt.Errorf("common.RoleHasAccess: %w", err)
		//	}
		//	if haveAccess {
		//		break
		//	}
		//}
		//if !haveAccess {
		//	return errors.New("You do not have access to edit your User profile. Therefore you can't set this Dashboard as your home page.")
		//}
		userDoc, err := allDB.QueryEquals(common.CollectionData, common.AddFieldPrefix("polyapp", "User", "TkfMDeIjfSaXCMSByYRsEbubp", "User ID"), user.FirestoreID)
		if err != nil {
			return fmt.Errorf("allDB.QueryEquals: %w", err)
		}
		homeData := CreateDashboard.DashboardDataID
		homeSchema := "DisplayDashboard"
		homeTask := "DisplayDashboard"
		homeUX := "DisplayDashboard"
		userData := userDoc.(*common.Data)
		userData.S[common.AddFieldPrefix("polyapp", "User", "TkfMDeIjfSaXCMSByYRsEbubp", "Home Data")] = common.String(homeData)
		userData.S[common.AddFieldPrefix("polyapp", "User", "TkfMDeIjfSaXCMSByYRsEbubp", "Home Schema")] = common.String(homeSchema)
		userData.S[common.AddFieldPrefix("polyapp", "User", "TkfMDeIjfSaXCMSByYRsEbubp", "Home Task")] = common.String(homeTask)
		userData.S[common.AddFieldPrefix("polyapp", "User", "TkfMDeIjfSaXCMSByYRsEbubp", "Home UX")] = common.String(homeUX)

		user.HomeData = homeData
		user.HomeSchema = homeSchema
		user.HomeTask = homeTask
		user.HomeUX = homeUX

		err = allDB.UpdateUser(user)
		if err != nil {
			return fmt.Errorf("allDB.UpdateUser: %w", err)
		}
		err = allDB.UpdateData(userData)
		if err != nil {
			return fmt.Errorf("allDB.UpdateData: %w", err)
		}
	}

	return nil
}

// CreateFileDataRecursively goes through a Data and its children and their children, etc., and creates them via Create
// in package fileCRUD.
//
// This function is a great way to implement ExportToJSON or "Export to JSON" field's functionality.
// The only thing you would be missing is if you need to save any generated Data, like Task or Schema or User, to the file
// system too. This function will not do that for you.
func CreateFileDataRecursively(data *common.Data) error {
	var err error
	for _, v := range data.Ref {
		greq, err := common.ParseRef(*v)
		if err != nil {
			return fmt.Errorf("common.ParseRef %v: %w", *v, err)
		}
		refData := &common.Data{
			FirestoreID: greq.DataID,
		}
		err = allDB.Read(refData)
		if err != nil {
			return fmt.Errorf("allDB.ReadData for greq %v: %w", refData.FirestoreID, err)
		}
		err = CreateFileDataRecursively(refData)
		if err != nil {
			return fmt.Errorf("CreateFileDataRecursively %v: %w", refData.FirestoreID, err)
		}
	}
	for _, allDatas := range data.ARef {
		for _, v := range allDatas {
			greq, err := common.ParseRef(v)
			if err != nil {
				return fmt.Errorf("common.ParseRef %v: %w", v, err)
			}
			refData := &common.Data{
				FirestoreID: greq.DataID,
			}
			err = allDB.Read(refData)
			if err != nil {
				return fmt.Errorf("allDB.ReadData for greq %v: %w", refData.FirestoreID, err)
			}
			err = CreateFileDataRecursively(refData)
			if err != nil {
				return fmt.Errorf("CreateFileDataRecursively %v: %w", refData.FirestoreID, err)
			}
		}
	}
	err = fileCRUD.Create(data)
	if err != nil {
		return fmt.Errorf("Create: %w", err)
	}
	return nil
}
