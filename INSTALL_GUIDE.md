# Installation Guide
Polyapp currently only works in Google Cloud. It usually operates in the free tier of Google Cloud, although if you exceed the free tier limits you will be charged. You can learn more about the free tier of the resources for: [Firestore](https://firebase.google.com/docs/firestore/quotas#free-quota), [App Engine](https://cloud.google.com/free/docs/gcp-free-tier#free-tier-usage-limits), and [Cloud Storage](https://cloud.google.com/free/docs/gcp-free-tier#free-tier-usage-limits) (used for images and blobs).

A video walkthrough of the installation process is available here: https://youtu.be/0lj4Danp2Bg

The best way to get started is to use the installation script. The steps to get started with the script are below.
1. If you do not already have a Google Account, you will need one.
2. If you do not already have access to Google Cloud, you need to create an account. https://cloud.google.com/
3. If you do not already have a Cloud Billing account, you need to create one. https://console.cloud.google.com/billing
4. Go here: https://shell.cloud.google.com/
5. Run these commands after Cloud Shell starts up:
6. `git clone --depth 1 https://gitlab.com/polyapp-open-source/polyapp`
7. `cd polyapp/`
8. `./google_cloud_install.sh`

The installation program will guide you through the rest of the installation process.

# Installation Failures
What do you do if installation fails for any reason? Re-run the installer in the Cloud Shell! The installer can be re-run as often as you want. Don't forget to enter the project ID of the project which was created the last time you ran the installer!
```
cd ..
rm -rf polyapp
git clone --depth 1 https://gitlab.com/polyapp-open-source/polyapp
cd polyapp/
./google_cloud_install.sh
```

Then don't forget to enter the project ID of the project which was created the last time you ran the installer!

If the error persists, please ask for help either by creating a Gitlab Issue or sending us an email at support@polyapp.tech

# Updates and Updating
There are 2 supported ways to update Polyapp

## Option 1 - For Everyone
You can re-run the installation procedure to update Polyapp.
```
rm -rf polyapp
git clone --depth 1 https://gitlab.com/polyapp-open-source/polyapp
cd polyapp/
./google_cloud_install.sh
```
At this point the installer will begin. Copy-paste your existing Project ID at the first prompt or else Polyapp will create a new project.

Please keep in mind that the re-installation process will delete all documents which were created by Polyapp and stored in this repository.
This effectively means deleting the admin user. After the process is over, visit https://console.firebase.google.com/ and navigate to "Authentication", find the admin user, and reset their password.

## Option 2 - For Developers
You can use a CI/CD pipeline to update Polyapp.
1. If you haven't installed Polyapp yet using the installer you need to do so.
2. Fork Polyapp in Gitlab.
3. Set the CI/CD variables discussed at the top of [.gitlab-ci.yml](.gitlab-ci.yml) (GOOGLE_CLOUD_PROJECT_ID and DEPLOY_KEY).
4. Push into your repository. Gitlab will start the pipeline automatically. When the "deploy_gae" stage is done Polyapp will be updated.

Depending on how you set up the pipeline, you can avoid having to reset the admin password.
