#! /bin/bash
# This script deploys Polyapp on Google Cloud.
# Example: ./google_cloud_install.sh
# Quiet Example: ./google_cloud_install.sh -q 1 -p polyapp-public
# Quiet does not prompt you for a google cloud project and assumes you have already set up Firebase.

quiet=0
GOOGLE_CLOUD_PROJECT=""
while getopts ":q:p:" opt
do
    case "${opt}" in
        q) quiet=1;;
        p) GOOGLE_CLOUD_PROJECT="$OPTARG";;
        *) echo "some flags were invalid";;
    esac
done

echo "Welcome to the Polyapp Installation Program"
echo ""
echo "You can re-run this program without breaking anything in Google Cloud."
echo "If the program encounters an error, it is often best to kill it (Ctrl + C) and restart it."
if [ "$GOOGLE_CLOUD_PROJECT" = "" ] || [ "$quiet" = 0 ]; then
  echo "Press Enter to proceed in a new Google Cloud Project. Enter a Google Cloud Project ID and press enter to use that Project instead."
  read GOOGLE_CLOUD_PROJECT
fi
# Remove leading whitespace and reduce trailing whitespace
$GOOGLE_CLOUD_PROJECT | xargs

if [ "$GOOGLE_CLOUD_PROJECT" = "" ] || [ "$GOOGLE_CLOUD_PROJECT" = " " ]; then
  GOOGLE_CLOUD_PROJECT=polyapp-$RANDOM$RANDOM$RANDOM
  gcloud projects create $GOOGLE_CLOUD_PROJECT --name=Polyapp
else
  echo "Polyapp will be installed into $GOOGLE_CLOUD_PROJECT which may disrupt its existing App Engine, Firestore, Datastore, Firebase, and other cloud services."
  echo "Press Ctrl + C to cancel"
fi
gcloud config set project $GOOGLE_CLOUD_PROJECT
export GOOGLE_CLOUD_PROJECT=$GOOGLE_CLOUD_PROJECT

# Need a billing account to be set up and linked
FIRST_BILLING_ACCOUNT=$(gcloud beta billing accounts list --filter=open=true --format="value(name)" --limit=1)
if [ "$FIRST_BILLING_ACCOUNT" = "" ]
then
  echo "No active billing accounts were found. Please create a billing account and then re-run this program."
  exit 1
else
  IS_ENABLED=$(gcloud beta billing projects list --billing-account $FIRST_BILLING_ACCOUNT --filter=PROJECT_ID=$GOOGLE_CLOUD_PROJECT --format="value(billingEnabled)")
  if [ "$IS_ENABLED" = "True" ]; then
    echo "Billing account exists and is already set up for this project."
  else
    echo "Using billing account number $FIRST_BILLING_ACCOUNT for $GOOGLE_CLOUD_PROJECT"
    gcloud beta billing projects link $GOOGLE_CLOUD_PROJECT --billing-account $FIRST_BILLING_ACCOUNT
  fi
fi

echo "Beginning Firebase setup"
gcloud services enable firebase.googleapis.com --project=$GOOGLE_CLOUD_PROJECT

go build -o initGCP ./cmd/initGCP/main.go
./initGCP $GOOGLE_CLOUD_PROJECT

if [ $? -ne 0 ]
then
  echo "initGCP failed. Try re-running the program and/or restarting your cloud shell."
  exit $?
fi

if [ "$quiet" -ne 1 ]
then
  echo ""
  echo "Although we have automatically set up Firebase, we haven't automatically configured Firebase Authentication."
  echo "You need to do this manually. Go here: https://console.firebase.google.com/project/"$GOOGLE_CLOUD_PROJECT"/authentication/providers"
  read -p "Press enter to proceed"
  echo ""
  echo ""
  echo "Click 'Get Started'"
  read -p "Press enter to proceed"
  echo ""
  echo "You need to enable 3 options. The Email/Password option, the Google option, and the anonymous option."
  read -p "Press enter to proceed"
  echo ""
  echo "Under Authorized Domains add: https://"$GOOGLE_CLOUD_PROJECT".appspot.com"
  read -p "Press enter to proceed"
  echo ""
  echo "We are now done setting up Firebase. The rest of the setup process is automated."
  read -p "Press enter to proceed"
  echo ""
fi

CREATE_SUCCESS=$(gcloud app create --region=us-central 2>&1)
if [ "$CREATE_SUCCESS" = "" ]; then
  echo ""
elif [[ $CREATE_SUCCESS == *"already contains an App Engine application"* ]]; then
  echo "gcloud app is already deployed"
elif [[ $CREATE_SUCCESS == *"You are creating an app for project"* ]]; then
  echo ""
else
  echo "gcloud app create error: $CREATE_SUCCESS"
  exit 1
fi

# There will be an interactive session where you choose the location of your App Engine application.

# Make sure we create the database in Firestore mode
gcloud services enable firestore.googleapis.com --project=$GOOGLE_CLOUD_PROJECT
REGION=$(gcloud app describe --format="value(locationId.scope())")
gcloud services enable appengine.googleapis.com --project=$GOOGLE_CLOUD_PROJECT
gcloud firestore databases create --region=$REGION --project=$GOOGLE_CLOUD_PROJECT

# Need a Cloud Storage Bucket to help store big blobs.
# It is important to make sure our BUCKET starts with a # and there is only 1 such bucket.
# See firestoreCRUD/data.go where it references google_cloud_install.sh for more information.
OUT=$(gsutil ls | grep -G "gs://[0-9]")
if [ "$OUT" = "" ]; then
  echo "Creating a new storage bucket"
  BUCKET=$RANDOM$RANDOM$RANDOM
  gsutil mb gs://$BUCKET
fi

# Enable Secrets Manager and give App Engine access so we can store API Keys there
gcloud services enable secretmanager.googleapis.com --project=$GOOGLE_CLOUD_PROJECT
gcloud projects add-iam-policy-binding $GOOGLE_CLOUD_PROJECT --member=serviceAccount:$GOOGLE_CLOUD_PROJECT@appspot.gserviceaccount.com --role=roles/secretmanager.admin

gcloud services enable sqladmin.googleapis.com --project=$GOOGLE_CLOUD_PROJECT
gcloud services enable compute.googleapis.com --project=$GOOGLE_CLOUD_PROJECT

# Postgres instances are used because they support full-text search. Some day they may supplant the Firestore database
# Because you can use Postgres in any cloud without too much trouble.
if [ "$(gcloud sql instances list --limit 1 --format="value(state)")" == RUNNABLE ]; then
  echo "SQL instance already exists. Creating new Username and Password."
  POSTGRES_PASSWORD=$RANDOM$RANDOM$RANDOM$RANDOM
  POSTGRES_USERNAME=polyapp$RANDOM$RANDOM
  SQL_INSTANCE_NAME=$(gcloud sql instances list --limit 1 --format="value(name)")
  SQL_IP_ADDRESS=$(gcloud sql instances list --limit 1 --format="value(ipAddresses[0].ipAddress)")
  CLIENT_CERTIFICATE_NAME=$(gcloud sql ssl client-certs list --format="value(commonName)" --instance=$SQL_INSTANCE_NAME --sort-by=commonName --limit=1)
  gcloud sql users set-password $POSTGRES_USERNAME --instance=$SQL_INSTANCE_NAME --password=$POSTGRES_PASSWORD
else
  # Create a Cloud SQL (PostgreSQL) instance.
  # This is used for full text searching because Firestore does not support that type of search. Approximate cost is 8 dollars per month.
  SQL_INSTANCE_NAME=polyapp-postgres-$RANDOM
  POSTGRES_PASSWORD=$RANDOM$RANDOM$RANDOM$RANDOM
  # Random username helps with redeployment. By specifying a new username, which means when we set the password
  # When redeploying it doesn't disrupt the existing deployment.
  POSTGRES_USERNAME=polyapp$RANDOM$RANDOM
  gcloud sql instances create $SQL_INSTANCE_NAME --database-version=POSTGRES_13 \
   --tier=db-f1-micro --region=$REGION --availability-type=zonal --no-backup \
   --storage-type=SSD --assign-ip --storage-size=10 --async \
   --maintenance-window-day=SAT --maintenance-window-hour=4 --quiet
  CREATE_SQL_OP=$(gcloud sql operations list --instance=$SQL_INSTANCE_NAME --limit=1 --format="value(NAME)")
  gcloud beta sql operations wait --timeout=1200 --project $GOOGLE_CLOUD_PROJECT $CREATE_SQL_OP
  gcloud sql users set-password $POSTGRES_USERNAME --instance=$SQL_INSTANCE_NAME --password=$POSTGRES_PASSWORD
  SQL_IP_ADDRESS=$(gcloud sql instances describe $SQL_INSTANCE_NAME --format=text --format="value(ipAddresses[0].ipAddress)")
  CLIENT_CERTIFICATE_NAME=polyapp-postgres-$RANDOM$RANDOM$RANDOM
  gcloud sql ssl client-certs create $CLIENT_CERTIFICATE_NAME client-key.pem --instance=$SQL_INSTANCE_NAME
  SQL_OP=$(gcloud beta sql operations list --instance=$SQL_INSTANCE_NAME --filter="status != DONE" --limit=1 --format="value(NAME)")
  if [ "$SQL_OP" != "" ]; then
    gcloud beta sql operations wait --timeout=600 --project $GOOGLE_CLOUD_PROJECT $SQL_OP
  fi
  gcloud sql databases create polyapp --instance=$SQL_INSTANCE_NAME --async
  SQL_OP=$(gcloud beta sql operations list --instance=$SQL_INSTANCE_NAME --filter="status != DONE" --limit=1 --format="value(NAME)")
  if [ "$SQL_OP" != "" ]; then
    gcloud beta sql operations wait --timeout=1200 --project $GOOGLE_CLOUD_PROJECT $SQL_OP
  fi
fi

# SSL Certificate is required to access our SQL instance because we used the --require-ssl flag
SQL_SERVER_CERT=$(gcloud beta sql ssl server-ca-certs list --instance=$SQL_INSTANCE_NAME --format="value(cert)")
POSTGRES_SSL_KEY=$(gcloud secrets versions access latest --secret=postgres)
if [ 0 == ${#POSTGRES_SSL_KEY} ]; then
  # Key length is 0; Does not exist
  rm client-key.pem
  gcloud sql ssl client-certs delete $CLIENT_CERTIFICATE_NAME --quiet --instance=$SQL_INSTANCE_NAME
  gcloud sql ssl client-certs create $CLIENT_CERTIFICATE_NAME client-key.pem --instance=$SQL_INSTANCE_NAME
  POSTGRES_SSL_KEY=$(<client-key.pem)
  gcloud secrets create postgres
  echo "$POSTGRES_SSL_KEY" | gcloud secrets versions add postgres --data-file=client-key.pem
  rm client-key.pem
else
  # Exists
  POSTGRES_SSL_KEY=$(gcloud secrets versions access latest --secret=postgres)
fi

POSTGRES_SSL_PUBLIC=$(gcloud sql ssl client-certs describe $CLIENT_CERTIFICATE_NAME --instance=$SQL_INSTANCE_NAME --format="value(cert)")

# SQL_REGION example: us-central1 (vs. REGION: us-central)
SQL_REGION=$(gcloud compute zones list --filter="region ~ $REGION" --limit=1 --format="value(region)")

CONNECTOR_NAME="test"

# Write the environment variables into app.yaml
# An alternative is to create another program like cmd/defaultservice/initGCP
# Another alternative is to add something to install()
# But this seemed like the simplest option.
# Syntax example: https://github.com/koalaman/shellcheck/wiki/SC2129
{
  echo $"  POSTGRES_USERNAME: \"$POSTGRES_USERNAME\"
  POSTGRES_PASSWORD: \"$POSTGRES_PASSWORD\"
  POSTGRES_IP_ADDRESS: \"$SQL_IP_ADDRESS\"
  POSTGRES_DB_NAME: \"polyapp\"
  INSTANCE_CONNECTION_NAME: \"$GOOGLE_CLOUD_PROJECT:$SQL_REGION:$SQL_INSTANCE_NAME\""
} >> ./cmd/defaultservice/app.yaml

# Write variables to files. They will be deployed when the system is deployed to app engine, but these file names
# are excluded from git so they will not be committed.
rm sslrootcert
rm sslcert
rm sslkey
echo "$SQL_SERVER_CERT" >> sslrootcert
echo "$POSTGRES_SSL_PUBLIC" >> sslcert
echo "$POSTGRES_SSL_KEY" >> sslkey

# The application relies upon a few dual-key indexes
# Moved this down in the execution order because sometimes I got the error "This operation is not available for Cloud Firestore projects in Datastore Mode".
# FYI the time it takes to create these indexes in a new DB just skyrocketed from ~10 seconds to ~2 minutes. Consequently I've switched these to --async calls.
gcloud firestore indexes composite create --async --collection-group=data --field-config=field-path="\`polyapp_User_iRQJHSitiUYNJJFfNSNCiFxwo_User ID\`",order=ascending --field-config=field-path="\`polyapp_User_iRQJHSitiUYNJJFfNSNCiFxwo_Event Time\`",order=descending
gcloud firestore indexes composite create --async --collection-group=data --field-config=field-path=polyappIndustryID,order=ascending --field-config=field-path=polyappDomainID,order=ascending --field-config=field-path=polyappSchemaID,order=ascending
gcloud firestore indexes composite create --async --collection-group=data --field-config=field-path=polyappIndustryID,order=ascending --field-config=field-path=polyappDomainID,order=ascending --field-config=field-path=Name,order=ascending
gcloud firestore indexes composite create --async --collection-group=task --field-config=field-path=polyappIndustryID,order=ascending --field-config=field-path=polyappDomainID,order=ascending --field-config=field-path=Name,order=ascending

DEPLOY_SUCCESS=$(gcloud --quiet app deploy ./cmd/defaultservice/app.yaml)
if [ "$DEPLOY_SUCCESS" = "" ]; then
  echo ""
else
  # If at first you don't succeed, try, try again
  gcloud --quiet app deploy ./cmd/defaultservice/app.yaml
fi

echo "Beginning install"
STATUSCODE=$(curl --silent --output /dev/stderr --write-out "%{http_code}" 'https://'$GOOGLE_CLOUD_PROJECT'.appspot.com/polyapp/internal/JT2ZhFXXaKCyO184Qs3F/')

if test $STATUSCODE -ne 200; then
  STATUSCODE=$(curl --silent --output /dev/stderr --write-out "%{http_code}" 'https://'$GOOGLE_CLOUD_PROJECT'.appspot.com/polyapp/internal/JT2ZhFXXaKCyO184Qs3F/')

  if test $STATUSCODE -ne 200; then
      exit 1
  fi
fi

echo ""
echo "Polyapp should now be installed. Thank you for trying Polyapp!"
echo "Open Polyapp: https://$GOOGLE_CLOUD_PROJECT.appspot.com/signIn"
echo ""
echo "After opening Polyapp, click Settings in upper right > Sign Out"
echo "You can sign in again with email 'support@polyapp.tech' and password 'polyapp'"
echo "You can change the password from Home > Edit Users > polyappAdminUser > enter the new password under 'New Password' and click 'Done'"
